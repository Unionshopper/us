﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using US.Comomn;

namespace US
{
    public partial class LoadOrdersList : System.Web.UI.Page
    {

        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        string sLoginString = "";
        string CurrentKey = "";

        XDocument xAB = null; XDocument xCsCase = null; XDocument xOpportunity = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");
            xCsCase = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            xOpportunity = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");
            xAB = XDocument.Load(path);

            this.sLoginString = this.loginToMax();
            if (this.IsPostBack)
                return;
            if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]))
            {

                Bind(ClientID);
            }
            this.oAbEntryList = this.oAbEntryAccess.ReadList("EQ(ClientID," + Request.QueryString["ClientID"] + ")");
            foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
            {
                CurrentKey = oAbEntry.Key;
                this.lbluser.Text = oAbEntry.FirstName.Value + " " + oAbEntry.LastName.Value;
                this.lblAddress.Text = oAbEntry.CurrentAddress.AddressLine1.Value + " " + oAbEntry.CurrentAddress.City.Value + " " + oAbEntry.CurrentAddress.StateProvince.Value + " " + oAbEntry.CurrentAddress.ZipCode.Value;
                this.lblHomeNumber.Text = oAbEntry.Phone1.Value;
                this.lblEmailAddress.Text = oAbEntry.EmailAddress1.Value;
                //this.lblBusNumber.Text = oAbEntry.Phone3.Value;
                string fieldValue1 = this.oUDFAccess.GetFieldValue(GetUdfKey("Member ID"), oAbEntry.Key);
                string fieldValue2 = this.oUDFAccess.GetFieldValue(GetUdfKey("Union ID"), oAbEntry.Key);
                this.lblMember.Text = fieldValue2;
                //this.lblUnion.Text = fieldValue2;
            }

        }

        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                {
                    string appSetting = ConfigurationManager.AppSettings["Database"];
                    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                    {
                        sAddressBookKey = addressBook.Key;
                        break;
                    }
                }
                string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                string appSetting2 = ConfigurationManager.AppSettings["Password"];
                this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }

        public string GetUdfKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xAB.Element("ab").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfOppKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            //var path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            //xOpportunity = XDocument.Load(path);
            var sFilterItems = xOpportunity.Element("Opp").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        public void Bind(string ClientID, int FilterCountRecords = 50)
        {
            ClientID = Request.QueryString["ClientID"];
            try
            {
                sLoginString = loginToMax();
                DataTable dt = new DataTable();
                dt.Columns.Add("Brand");
                dt.Columns.Add("Product");
                dt.Columns.Add("Model");
                dt.Columns.Add("Members Price");
                dt.Columns.Add("Ordered Price");
                dt.Columns.Add("Vendor Ordered");
                dt.Columns.Add("Item Status");
                dt.Columns.Add("Vendor Status");

                string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");

                double RevenuValue = 0;

                oAbEntryList = this.oAbEntryAccess.ReadList("EQ(ClientID," + Request.QueryString["ClientID"] + ")");
                foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                {
                    CurrentKey = oAbEntry.Key;
                }
                Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("KEY(" + CurrentKey + ")", FilterCountRecords);
                string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                var items = "";
                var OppKeystring = "";
                string UDFBrandKey = GetUdfOppKey("Brand Name");
                string UDFModelKey = GetUdfOppKey("Model");
                string UDFProductNameKey = GetUdfOppKey("Product Name");
                string UDFOrderFromKey = GetUdfOppKey("Ordered From");
                string UDFMemberPriceDescKey = GetUdfOppKey("Member Price Desc");
                

                foreach (Maximizer.Data.Opportunity opp in OppList)
                {
                    var CaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);
                    string CSCaseKey = "";
                    string filter = "EQ(CaseNumber," + CaseNumber + ")";
                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                    foreach (CSCase oCSCase in oCSCaseList)
                    {
                        CSCaseKey = oCSCase.Key;
                    }
                    CurrentKey = opp.ParentKey;
                    var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                    items = itemNumber + ",";
                    OppKeystring = opp.Key + "-" + itemNumber + ",";
                    RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                    string UDFOrderStatusKey = GetUdfCSCaseKey("Ordered Item Status "+itemNumber);
                    var OrderStatus = oUDFAccess.GetFieldValue(UDFOrderStatusKey, CSCaseKey);
                    var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                    var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                    var Product = oUDFAccess.GetFieldValue(UDFProductNameKey, opp.Key);
                    var OrderFrom = oUDFAccess.GetFieldValue(UDFOrderFromKey, opp.Key);
                    var UDFMemberPriceDesc = oUDFAccess.GetFieldValue(UDFMemberPriceDescKey, opp.Key);
                    if (string.IsNullOrEmpty(UDFMemberPriceDesc))
                    {
                        UDFMemberPriceDesc = "0";
                    }
                    var status = opp.StatusName.Value;
                    if (opp.StatusName.Value == "Won")
                    {
                        status = "Confirmed";
                    }
                    dt.Rows.Add(Brand, Product, Model, UDFMemberPriceDesc, RevenuValue, OrderFrom, OrderStatus, status);
                }
                gvLoadOrder.DataSource = dt;
                gvLoadOrder.DataBind();
                if (dt.Rows.Count > 0)
                {

                    //here add code for column total sum and show in footer  

                    gvLoadOrder.FooterRow.Cells[2].Text = "Total";
                    gvLoadOrder.FooterRow.Cells[2].Font.Bold = true;
                    gvLoadOrder.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Left;

                    int sum = 0;
                    int sumMembersPrice = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        sum += Convert.ToInt32(dr["Ordered Price"]);
                        sumMembersPrice += Convert.ToInt32(dr["Members Price"]);
                    }

                    gvLoadOrder.FooterRow.Cells[3].Text = sumMembersPrice.ToString();
                    gvLoadOrder.FooterRow.Cells[3].Font.Bold = true;

                    gvLoadOrder.FooterRow.Cells[4].Text = sum.ToString();
                    gvLoadOrder.FooterRow.Cells[4].Font.Bold = true;
                    gvLoadOrder.FooterRow.BackColor = System.Drawing.Color.Beige;
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        protected void ddlFilterCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]))
                {
                    Bind(Request.QueryString["ClientID"], Convert.ToInt32(ddlFilterCount.SelectedValue));
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void btnSearchRecords_Click(object sender, EventArgs e)
        {
            var ClientID = Request.QueryString["ClientID"];
            try
            {
                if (!string.IsNullOrEmpty(txtFromDate.Text) && !string.IsNullOrEmpty(txtToDate.Text))
                {
                    string fromDate = MaximizerDate(txtFromDate.Text);
                    string toDate = MaximizerDate(txtToDate.Text);

                    oAbEntryList = this.oAbEntryAccess.ReadList("EQ(ClientID," + Request.QueryString["ClientID"] + ")");
                    foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                    {
                        CurrentKey = oAbEntry.Key;
                    }

                    string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                    var strFilterKey = "AND(KEY(" + CurrentKey + "), " + DateFilter + ")";
                    sLoginString = loginToMax();
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Brand");
                    dt.Columns.Add("Product");
                    dt.Columns.Add("Model");
                    dt.Columns.Add("Members Price");
                    dt.Columns.Add("Ordered Price");
                    dt.Columns.Add("Vendor");
                    dt.Columns.Add("Item Status");
                    dt.Columns.Add("Vendor Status");

                    string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");

                    double RevenuValue = 0;

                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList(strFilterKey, Convert.ToInt32(ddlFilterCount.SelectedValue));
                    string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                    var items = "";
                    var OppKeystring = "";
                    string UDFBrandKey = GetUdfOppKey("Brand Name");
                    string UDFModelKey = GetUdfOppKey("Model");
                    string UDFProductNameKey = GetUdfOppKey("Product Name");
                    string UDFOrderFromKey = GetUdfOppKey("Ordered From");
                    string UDFMemberPriceDescKey = GetUdfOppKey("Member Price Desc");

                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        var CaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);
                        string CSCaseKey = "";
                        string filter = "EQ(CaseNumber," + CaseNumber + ")";
                        CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                        foreach (CSCase oCSCase in oCSCaseList)
                        {
                            CSCaseKey = oCSCase.Key;
                        }

                        CurrentKey = opp.ParentKey;
                        var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                        items = itemNumber + ",";
                        OppKeystring = opp.Key + "-" + itemNumber + ",";
                        RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                        string UDFOrderStatusKey = GetUdfCSCaseKey("Ordered Item Status "+itemNumber);
                        var OrderStatus = oUDFAccess.GetFieldValue(UDFOrderStatusKey, CSCaseKey);
                        var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                        var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                        var Product = oUDFAccess.GetFieldValue(UDFProductNameKey, opp.Key);
                        var OrderFrom = oUDFAccess.GetFieldValue(UDFOrderFromKey, opp.Key);
                        var UDFMemberPriceDesc = oUDFAccess.GetFieldValue(UDFMemberPriceDescKey, opp.Key);
                        if (string.IsNullOrEmpty(UDFMemberPriceDesc))
                        {
                            UDFMemberPriceDesc = "0";
                        }
                        var status = opp.StatusName.Value;
                        if (opp.StatusName.Value == "Won")
                        {
                            status = "Confirmed";
                        }
                        dt.Rows.Add(Brand, Product, Model, UDFMemberPriceDesc, RevenuValue, OrderFrom, OrderStatus, status);
                    }
                    gvLoadOrder.DataSource = dt;
                    gvLoadOrder.DataBind();
                    if (dt.Rows.Count > 0)
                    {
                        //here add code for column total sum and show in footer  
                        gvLoadOrder.FooterRow.Cells[2].Text = "Total";
                        gvLoadOrder.FooterRow.Cells[2].Font.Bold = true;
                        gvLoadOrder.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Left;

                        int sum = 0;
                        int sumMembersPrice = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            sum += Convert.ToInt32(dr["Ordered Price"]);
                            sumMembersPrice += Convert.ToInt32(dr["Members Price"]);
                        }

                        gvLoadOrder.FooterRow.Cells[3].Text = sumMembersPrice.ToString();
                        gvLoadOrder.FooterRow.Cells[3].Font.Bold = true;

                        gvLoadOrder.FooterRow.Cells[4].Text = sum.ToString();
                        gvLoadOrder.FooterRow.Cells[4].Font.Bold = true;
                        gvLoadOrder.FooterRow.BackColor = System.Drawing.Color.Beige;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public string MaximizerDate(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                string[] datesplit = date.Split('-');
                string[] yearsplit = datesplit[2].Split(' ');
                string newdate = yearsplit[0] + "-" + datesplit[1] + "-" + datesplit[0];
                return Convert.ToDateTime(newdate).ToString("yyyy-MM-dd");
            }
            return "";
        }
    }
}