﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorEnquiries.aspx.cs" Inherits="US.VendorEnquiries" ValidateRequest="false" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.css" />

    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="Scripts/fSelect.js"></script>

    <style>
        .ui-datepicker-year,
        .ui-datepicker-month {
            color: #333;
        }

        .aMore:hover {
            text-decoration: none !important;
        }

        .aMorePrice:hover {
            text-decoration: none !important;
        }
    </style>
    <script>
        function BindGrid(response) {
            
            $("div#divLoading").removeClass('show');
            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var emptyRow = $('#hfRowEmptyOrNot').val();
            if (emptyRow == "") {
                var row1 = $("[id$=GridView1] tr").eq(1).clone(true);
                var tableTr = xml.find("Table1");
                $("[id$=GridView1] .loader").remove();
                $('[id$=GridView1] tbody > tr').remove();
                tableTr.each(function () {
                    var items = $(this);
                    var url = '<a href="../' + items.find("URL").text() + '"><i class="zmdi zmdi-border-color"></i></a>';
                    var row = $("[id$=GridView1] tr").eq(1).clone(true);
                    if (row.length <= 0) {
                        row = row1;
                        $('#hfRowEmptyOrNot').val("AddedEmpty");
                    }
                    $(row).find('td').eq(0).html(items.find("CaseNo").text());
                    $(row).find('td').eq(1).html(items.find("DateFormat").text());
                    $(row).find('td').eq(2).html(items.find("Time").text());
                    $(row).find('td').eq(3).html(items.find("Assigned").text());
                    $(row).find('td').eq(4).html(items.find("Desc").text());
                    $(row).find('td').eq(5).html(items.find("Revenue").text());
                    $(row).find('td').eq(6).html(items.find("Status").text());
                    $(row).find('td').eq(7).html(url);
                    $("[id$=GridView1]").append(row);
                });
            }
        }
        $(document).ready(function () {
            
            $('#txtSearchData').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    $('#btnSearchFilterType').click();
                }
            });
             $('#txtFromDate #txtToDate').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    $('#btnSearchRecords').click();
                }
            });
            

            $('.StatusType').fSelect({
                placeholder: '----- Status Type -----',
                numDisplayed: 4,
                overflowText: '{n} selected',
                searchText: 'Status Type',
                showSearch: false
            });

            if ($('#ctl06_hfResult').val() == "No Records Found") {
                $('.zmdi-border-color').css({ "display": "none;" });
            }
            $('#ddlVendorData').val("");
            $('#txtSearchData').val("");


            $("#ctl06_GridView1").prepend($("<thead></thead>").append($('#ctl06_GridView1').find("tr:first"))).DataTable({
                //"order": [[0, "desc"]],
                "ordering": false,
                "searching": false,
                "paging": false,
                "info": false
            });
            $('.menulink').each(function () {

                if ($(this).hasClass('active')) {
                    $('.input-headings').find("h4").text($(this).text());
                    $('#hfActiveTab').val($(this).text());

                    if ($(this).text().toUpperCase() == "COMPLETED ORDERS") {
                        $('#ddlVendorData').append('<option value="Vendor Order Number">Vendor Order Number</option>');
                    } else {
                        $("#ddlVendorData option[value='Vendor Order Number']").remove();
                    }
                }
            });
            var i = 0;

            $('.more').each(function () {
                i++;
            });
            if (i == 1) {
                $('.aMore').hide();
                $('.aMorePrice').hide();
            }

            $(document).on("click", "#btnSearchFilterType", function () {
                
                $('#hfRowEmptyOrNot').val("");
                $("div#divLoading").addClass('show');
                var isValid = true;
                if ($('#txtSearchData').val() == "") {
                    isValid = false;
                    alert("Required text search");
                }
                if ($('#ddlVendorData').val() == null || $('#ddlVendorData').val() == "Select") {
                    isValid = false;
                    alert("Required Filter Type");
                } 
                if (isValid) {
                    $.ajax({
                        type: "POST",
                        url: "VendorEnquiries.aspx/GetGridDataForFilterType",
                        data: '{ActiveTab:"' + $('#hfActiveTab').val() + '",sLoginString: "' + $('#hfLoginString').val() + '",VendorType: "' + $('#hfVendor').val() + '",RequestType: "' + $('#hfRequestType').val() + '",FilterType: "' + $('#ddlVendorData').val() + '",SearchString: "' + $('#txtSearchData').val() + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: BindGrid,
                        failure: function (response) {
                            ////alert(response.d);
                            $("div#divLoading").removeClass('show');
                        },
                        error: function (response) {
                            ////alert(response.d);
                            $("div#divLoading").removeClass('show');
                        }
                    });
                }
            });

            $(document).on("click", "#btnSearchRecords", function () {
                
                $('#hfRowEmptyOrNot').val("");
                $("div#divLoading").addClass('show');
                var dateAr = $('#txtFromDate').val().split('-');
                var fromDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);

                dateAr = $('#txtToDate').val().split('-');
                var toDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);
                  var isValid = true;
                if ($('#txtFromDate').val() == "" || $('#txtToDate').val()=="") {
                    isValid = false;
                    alert("Required Date");
                }
               
                if (isValid) {
                    $.ajax({
                        type: "POST",
                        url: "VendorEnquiries.aspx/GetGridDataForFilterData",
                        data: '{ActiveTab:"' + $('#hfActiveTab').val() + '",sLoginString: "' + $('#hfLoginString').val() + '",VendorType: "' + $('#hfVendor').val() + '",RequestType: "' + $('#hfRequestType').val() + '",fromDate: "' + fromDate + '",toDate: "' + toDate + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: BindGrid,
                        failure: function (response) {
                            ////alert(response.d);
                            $("div#divLoading").removeClass('show');
                        },
                        error: function (response) {
                            ////alert(response.d);
                            $("div#divLoading").removeClass('show');
                        }
                    });
                }
            });

            $(document).on("click", ".aMore", function () {

                var td = $(this).closest("td");
                var more = td.find('.more');
                if ($(this).text() == "...more") {
                    $(this).text("less");
                    $(more).each(function () {
                        $(this).show();
                    });
                } else {
                    var i = 0;
                    $(this).text("...more");
                    $(more).each(function () {
                        if (i != 0)
                            $(this).hide();

                        i++;
                    });
                }


                var tdPrice = td.next("td").find(".aMorePrice");

                if (tdPrice.text() == "...more") {
                    tdPrice.text("less");
                    $('.morePrice').each(function () {
                        $(this).show();
                    });
                } else {
                    var i = 0;
                    tdPrice.text("...more");
                    $('.morePrice').each(function () {
                        if (i != 0)
                            $(this).hide();

                        i++;
                    });
                }

            });

            $(document).on("click", ".aMorePrice", function () {

                var td = $(this).closest("td");
                if ($(this).text() == "...more") {
                    $(this).text("less");
                    $('.morePrice').each(function () {
                        $(this).show();
                    });
                } else {
                    var i = 0;
                    $(this).text("...more");
                    $('.morePrice').each(function () {
                        if (i != 0)
                            $(this).hide();

                        i++;
                    });
                }


                var tdDesc = td.prev("td").find(".aMore");
                var more = td.prev("td").find('.more');
                if (tdDesc.text() == "...more") {
                    tdDesc.text("less");
                    $(more).each(function () {
                        $(this).show();
                    });
                } else {
                    var i = 0;
                    tdDesc.text("...more");
                    $(more).each(function () {
                        if (i != 0)
                            $(this).hide();

                        i++;
                    });
                }
            });

        });
    </script>

    <style>
         #divLoading {
            display: none;
        }

            #divLoading.show {
                display: block;
                position: fixed;
                z-index: 9999;
                /*background-image: url('../../../../Images/loader-codify.gif');*/
                background-color: rgba(255, 255, 255, 0.5);
                /*opacity: 0.4;*/
                background-repeat: no-repeat;
                background-position: center;
                left: 0;
                bottom: 0;
                right: 0;
                top: 0;
            }

            #divLoading img {
                max-width: 100px;
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
            }

        #loadinggif.show {
            left: 50%;
            top: 50%;
            position: absolute;
            z-index: 101;
            width: 32px;
            height: 32px;
            margin-left: -16px;
            margin-top: -16px;
        }
        .menulink {
            color: #000;
        }

        .enquiry-sheet-part {
            background: #B9D530 none repeat scroll 0 0;
            color: #fff;
            font-family: "OpenSans-Bold";
            font-size: 14px;
            margin: 2px 0 0;
            text-transform: uppercase;
            letter-spacing: 2px;
            height: 51px;
            padding: 9px 0 0 0;
        }

        .dataTables_wrapper .dataTables_filter {
            float: right;
            text-align: right;
            width: 50%;
        }

        input[type="search"] {
            background: #fff url("images/eye-search.png") no-repeat scroll 8px center;
            border: 2px solid #c4c5c6;
            border-radius: 5px;
            padding: 0 10px 0 40px;
            position: relative;
            z-index: 1;
            float: right !important;
            width: 94%;
            margin-top: -16px;
            margin-right: 1px;
            height: 34px;
            opacity: 1;
        }

        div.dataTables_length {
            margin-top: -4px;
        }

        table.dataTable thead th, table.dataTable thead td {
            padding: 10px 18px !important;
            color: #fff !important;
        }

        table.dataTable thead th, table.dataTable thead td {
            background: #78797a;
            font-size: 12px !important;
        }

        table.dataTable tbody tr {
            color: black !important;
        }

        #ctl06_GridView1_length {
            margin-top: 10px;
        }

        #ctl06_GridView1 tr {
            border: 1px !important;
            color: black !important;
        }

        #ctl06_GridView1 i.zmdi.zmdi-border-color {
            background: #B9D530;
            font-size: 19px;
            padding: 6px;
            color: black;
        }

        .table-secation tbody td {
            font-size: 13px !important;
        }

        .table-secation thead th {
            font-size: 14px !important;
        }

        #ctl06_GridView1_wrapper {
            font-size: 12px;
        }

        body {
            font-family: Arial;
        }

        .left-pnl {
            list-style: none;
        }

            .left-pnl ul li {
            }

        .lrft-pnl ul li a {
            border: 1px solid #000;
            padding: 5px;
            background: none;
            display: block;
            text-decoration: none;
            margin-bottom: 5px;
            font-family: Arial;
            cursor: pointer;
        }

            .lrft-pnl ul li a.active {
                background: #B9D530;
                color: #000;
            }

            .lrft-pnl ul li a:hover {
                background: #B9D530;
                color: #fff;
            }

        .table-right td {
            padding: 5px;
        }
    </style>

</head>
<body style="background: #e8ebec !important">
    <div id="divLoading">
        <img src="https://4.bp.blogspot.com/-hO_3iF0kUXc/Ui_Qa3obNpI/AAAAAAAAFNs/ZP-S8qyUTiY/s200/pageloader.gif" />
    </div>
    <form id="form1" runat="server">

        <asp:HiddenField ID="hfLoginString" runat="server" />
        <asp:HiddenField ID="hfVendor" runat="server" />
        <asp:HiddenField ID="hfEnquiryType" runat="server" />
        <asp:HiddenField ID="hfCurrentKey" runat="server" />
        <asp:HiddenField ID="hfRequestType" runat="server" />
        <asp:HiddenField ID="hfActiveTab" runat="server" />
        <asp:HiddenField ID="hfResponse" runat="server" />
        <asp:HiddenField ID="hfRowEmptyOrNot" runat="server" Value=""/>

        <div>
            <div class="enquiry-sheet-part">

                <div class="enquiry-member-btn">
                    <div class="input-headings find-enquiry" style="text-align: center; width: 100%; float: left;">
                        <h4 style="float: left; margin-left: 45%;">CURRENT ENQUIRIES</h4>
                        <asp:Label ID="lblVendor" runat="server" Style="float: right; margin-right: 2%;"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <div style="margin: 1.5% 2%;">

            <table>
                <tr>
                    <td style="width: 25%; vertical-align: top;" class="lrft-pnl">
                        <ul style="margin-top: 46px;">
                            <li style="list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkCurrantEnquiries" OnClick="lnkCurrantEnquiries_Click" data-id="CurrentEnquies" EnableViewState="false" CssClass="active menulink"> CURRENT ENQUIRIES</asp:LinkButton>
                            </li>
                            <li style="list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkCompletedEnquiries" OnClick="lnkCompletedEnquiries_Click" data-id="CompletedEnquies" EnableViewState="false" CssClass="menulink"> COMPLETED ENQUIRIES</asp:LinkButton>
                            </li>
                            <li style="list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkCurrantOrders" OnClick="lnkCurrantOrders_Click" data-id="CurrentOrder" EnableViewState="false" CssClass="menulink"> CURRENT ORDERS</asp:LinkButton>
                            </li>
                            <li style="list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkCompletedOrders" OnClick="lnkCompletedOrders_Click" data-id="CompletedOrders" EnableViewState="false" CssClass="menulink">COMPLETED ORDERS</asp:LinkButton>
                            </li>
                            <li style="list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkReports" OnClick="lnkReports_Click" data-id="CompletedOrders" EnableViewState="false" CssClass="menulink">REPORTS</asp:LinkButton>
                            </li>

                        </ul>
                    </td>
                    <td style="width: 5%"></td>
                    <td style="width: 70%">
                        <div class="row" style="margin-bottom: 15px;" id="divFilter">
                            <div class="col-md-2" style="width: 10%;">
                                <p style="margin-top: 4px;">Filter Type</p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList runat="server" ID="ddlVendorData" CssClass="form-control" Style="margin-left: -30px;">
                                    <asp:ListItem Text="-- Select --" Value="Select"></asp:ListItem>
                                    <asp:ListItem Text="Case Number" Value="Case Number"></asp:ListItem>
                                    <asp:ListItem Text="Name" Value="Name"></asp:ListItem>

                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtSearchData" runat="server" CssClass="form-control form-control-sm mr-3 w-75" placeholder="Search" aria-label="Search" Style="width: 90%; float: left; padding-right: 18px; margin-left: -40px;">
                                </asp:TextBox>
                                <i class="fa fa-search active btn btn-primary" aria-hidden="true" style="margin-top: 3px; float: left; margin-left: -38px;" id="btnSearchFilterType"></i>

                            </div>
                            <div class="col-md-5" style="margin-left: -50px;">
                                <div class="input-group input-daterange">
                                    <div class="input-group-addon">From</div>
                                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control date-range-filter"></asp:TextBox>

                                    <div class="input-group-addon">To</div>
                                    <asp:TextBox runat="server" ID="txtToDate" CssClass="form-control date-range-filter"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btnSearchRecords" class="btn btn-primary">Search</button>
                                <%-- <asp:Button ID="btnSearchRecords" runat="server" CssClass="btn btn-primary"  Text="Search" OnClick="btnSearchRecords_Click"/>--%>
                            </div>
                        </div>
                        <asp:Panel ID="pnlControls" EnableViewState="false" runat="server"></asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Button ID="btnGenerateCSV" runat="server" CssClass="btn btn-primary" Text="Generate Report" OnClick="btnGenerateCSV_Click" Style="display: none;" />
        <input type="hidden" runat="server" id="hfFrom" />
        <input type="hidden" runat="server" id="hfTo" />
    </form>
</body>
</html>
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>
    $.noConflict();
    jQuery(document).ready(function () {
        
        var date = new Date();
        // If no date object supplied, use current date
        // Copy date so don't modify supplied date
        var now = date ? new Date(date) : new Date();

        // set time to some convenient value
        now.setHours(0, 0, 0, 0);

        // Get the previous Monday
        var monday = new Date(now);
        monday.setDate(monday.getDate() - monday.getDay() + 1);

        var dd = monday.getDate();
        var mm = monday.getMonth() + 1;

        var yyyy = monday.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var monday = dd + '-' + mm + '-' + yyyy;

        // Get next Sunday
        // sunday = new Date(now);
        //sunday.setDate(sunday.getDate() - sunday.getDay() + 7);

        var today = new Date();
        dd = today.getDate();
        mm = today.getMonth() + 1;

        yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = dd + '-' + mm + '-' + yyyy;


        var dateToday = new Date();
        var sevedayAgoDate = new Date(dateToday.setDate(dateToday.getDate() - 7));

        mm = sevedayAgoDate.getMonth() + 1;

        yyyy = sevedayAgoDate.getFullYear();
        dd = sevedayAgoDate.getDate();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var fromDate = dd + '-' + mm + '-' + yyyy;


        jQuery('#txtFromDate').val(fromDate);
        jQuery('#txtToDate').val(today);
        jQuery('#txtFromDate').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'
        });
        jQuery('#txtToDate').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

        if (jQuery('.active').html() == "REPORTS") {
            jQuery('#divFilter').html("");
        } else {
            jQuery('#divReport').html("");
            jQuery('#btnGenerateCSV').remove();
        }
    });


</script>
