﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace US
{
    public partial class loadSelectedCSCase : System.Web.UI.Page
    {
        Maximizer.Data.AddressBookMaster oAb = new Maximizer.Data.AddressBookMaster();
		Maximizer.Data.AbEntryAccess oAbEntryAccess = null;
		Maximizer.Data.GlobalAccess ga = new Maximizer.Data.GlobalAccess();
		Maximizer.Data.AbEntryList oAbEntryList = null;
		Maximizer.Data.UserAccess oUserAccess = null;
		//Maximizer.Data.AbEntry oAbEntry = null;
		Maximizer.Data.UdfAccess oUDFAccess = null;
		Maximizer.Data.NoteAccess oNoteAccess = null;
		Maximizer.Data.TaskAccess oTaskAccess = null;
		Maximizer.Data.OpportunityAccess oOppAccess = null;
		Maximizer.Data.DetailFieldAccess oDetailFieldAccess = null;
		Maximizer.Data.CSCaseAccess oCSCaseAccess = null;
		string sLoginString = "";



		protected void Page_Load(object sender, EventArgs e)
		{
			Maximizer.Data.AddressBookList abList = ga.ReadAddressBookList();
			string sRt = "";
            string caseNumber = "";

			string CurrentModule = Convert.ToString(Session["CurrentModule"]);
			string abKey = "";
			if (CurrentModule == "cs")
			{
				if (Session["loginString"] != null)
				{
					sLoginString = Session["loginString"].ToString();
				}
				string CSCaseKey = Convert.ToString(Session["cases!_CurrentKey"]);
				oCSCaseAccess = oAb.CreateCSCaseAccess(sLoginString);
				Maximizer.Data.CSCaseList oCSCaseList = oCSCaseAccess.ReadList("Key(" + CSCaseKey + ")");
				foreach (Maximizer.Data.CSCase oCSCase in oCSCaseList)
				{
					abKey = oCSCase.ParentKey;
                    caseNumber = oCSCase.CaseNumber.Value;
                    break;
                }
			}
			else
			{
				abKey = Convert.ToString(Session["abGrid!_CurrentKey"]);
			}
			if (!string.IsNullOrEmpty(abKey)) //load our defaults
			{
				if (Session["loginString"] != null)
				{
					sLoginString = Session["loginString"].ToString();
				}

			}
			else
			{
				sLoginString = "221265267238022264342011230776z7660786473670f4d64435f186357404453464e0f76716570707263720a7e535e524b595b625f5b5d47036377606473660e5c535c1c42435f1d07066a5f5349000604050d627b77057d7361667366086465740c525d5d4445585a4a4b4b09";
				abKey = "SW5kaXZpZHVhbAkxNzExMjAyNTExMjUwNTY0MjAwMDdDCTA=";

			}

			if (sLoginString == "" || sLoginString == null)
			{
				foreach (Maximizer.Data.AddressBook ab in abList)
				{
					string database = System.Configuration.ConfigurationManager.AppSettings["Database"];
					if (0 == ab.Name.ToString().ToUpper().CompareTo(database.ToUpper()))
					{
						sRt = ab.Key;
						break;
					}
				}

				string Username = System.Configuration.ConfigurationManager.AppSettings["UserName"];
				string password = System.Configuration.ConfigurationManager.AppSettings["Password"];
				sLoginString = oAb.LoginMaximizer(sRt, Username.ToUpper(), password);


			}

			// Always need this to create access to Maximizer and access to special fields.
			oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
			oUDFAccess = oAb.CreateUdfAccess(sLoginString); // always use to setup to write or read from custom fields.

			//  oNoteAccess = oAb.(Access(sLoginString);
			oUserAccess = oAb.CreateUserAccess(sLoginString);
			oOppAccess = oAb.CreateOpportunityAccess(sLoginString);
			oDetailFieldAccess = oAb.CreateDetailFieldAccess(sLoginString);

            var User = oAbEntryAccess.GetLoggedInUser(sLoginString);
            if (User != null)
            {
                string url = "CreateCSCase.aspx?CurrentKey=" + abKey + "&Loginstring=" + sLoginString + "&CaseNo=" + caseNumber + "&RequestType=CallCenter";
                if (User.UserId.Value.ToUpper() == "VENJBHIFI")
                    url = "CreateCSCase.aspx?CurrentKey=" + abKey + "&Loginstring=" + sLoginString + "&CaseNo=" + caseNumber + "&RequestType=CallCenter&Vendor=JBHIFI";
                else if (User.UserId.Value.ToUpper() == "VENTGG")
                    url = "CreateCSCase.aspx?CurrentKey=" + abKey + "&Loginstring=" + sLoginString + "&CaseNo=" + caseNumber + "&RequestType=CallCenter&Vendor=GG";

                Response.Redirect(url);
            }
            //var suffix = "&Vendor=" + Request.QueryString["Vendor"] + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=CallCenter";
            //var URL = "CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + suffix;

   //         string url = "CreateCSCase.aspx?CurrentKey=" + abKey + "&Loginstring=" + sLoginString+ "&CaseNo="+caseNumber+"&RequestType=CallCenter";

			//Response.Redirect(url);


		}

		public string GetUdfKeyOpp(string strUdfName, string strLoginString, Maximizer.Data.AddressBookMaster abMaster)
		{
			Maximizer.Data.UdfAccess udfAccess =
			abMaster.CreateUdfAccess(strLoginString);
			Maximizer.Data.UdfDefinitionList udfDefList =
			udfAccess.ReadUdfDefinitionList(new Maximizer.Data.DefaultOpportunityEntry().Key);
			foreach (Maximizer.Data.UdfDefinition udfDef in udfDefList)
			{
				if (udfDef.FieldName.Equals(strUdfName))
				{
					return udfDef.Key;
				}
			}
			return String.Empty;
		}
		Maximizer.Data.SalesProcessAccess oSalesProcess = null;
		public string GetSalesProcesAndStage(Maximizer.Data.Opportunity opp)
		{
			oSalesProcess = oAb.CreateSalesProcessAccess(sLoginString);
			string salesKey = GetSaleProcessKey(opp.Key, opp.SalesProcessName.Value);//Get Sales Process Key
			Maximizer.Data.SalesProcessStageList spsl = oSalesProcess.ReadSalesProcessStages(salesKey);

			foreach (Maximizer.Data.SalesProcessStage sps in spsl)
			{
				if (opp.ProbabilityClosing.Value == sps.ProbabilityClose.Value)
				{
					return sps.Description.Value;
				}
			}
			return "";
		}
		public string GetSaleProcessKey(string key, string salestage)
		{
			string SalesProcessKey = "";
			try
			{
				Maximizer.Data.SalesProcessList salesProcessList = oSalesProcess.ReadSalesProcessDefinitions();

				foreach (Maximizer.Data.SalesProcess sale in salesProcessList)
				{
					if (sale.Description.Value.ToLower() == salestage.ToLower())
					{
						SalesProcessKey = sale.Key;
						return SalesProcessKey;
					}
				}
			}
			catch (Exception ex)
			{

			}
			return SalesProcessKey;
		}
    }
}