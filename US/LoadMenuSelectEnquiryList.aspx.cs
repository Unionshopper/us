﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace US
{
    public partial class LoadMenuSelectEnquiryList : System.Web.UI.Page
    {
        Maximizer.Data.AddressBookMaster oAb = new Maximizer.Data.AddressBookMaster();
        Maximizer.Data.AbEntryAccess oAbEntryAccess = null;
        Maximizer.Data.GlobalAccess ga = new Maximizer.Data.GlobalAccess();
        Maximizer.Data.AbEntryList oAbEntryList = null;
        Maximizer.Data.UserAccess oUserAccess = null;
        //Maximizer.Data.AbEntry oAbEntry = null;
        Maximizer.Data.UdfAccess oUDFAccess = null;
        Maximizer.Data.NoteAccess oNoteAccess = null;
        Maximizer.Data.TaskAccess oTaskAccess = null;
        Maximizer.Data.OpportunityAccess oOppAccess = null;
        Maximizer.Data.DetailFieldAccess oDetailFieldAccess = null;
        Maximizer.Data.CSCaseAccess oCSCaseAccess = null;
        string sLoginString = "";



        protected void Page_Load(object sender, EventArgs e)
        {
            Maximizer.Data.AddressBookList abList = ga.ReadAddressBookList();
            string sRt = "";

            string CurrentModule = Convert.ToString(Session["CurrentModule"]);
            string abKey = "";
            string ClientID = "";
            if (CurrentModule == "cs")
            {
                if (Session["loginString"] != null)
                {
                    sLoginString = Session["loginString"].ToString();
                }
                string CSCaseKey = Convert.ToString(Session["cases!_CurrentKey"]);
                Maximizer.Data.CSCaseAccess oCSCaseAccessCS = oAb.CreateCSCaseAccess(sLoginString);
                Maximizer.Data.CSCaseList oCSCaseList = oCSCaseAccessCS.ReadList("Key(" + CSCaseKey + ")");
                foreach (Maximizer.Data.CSCase oCSCase in oCSCaseList)
                {
                    abKey = oCSCase.ParentKey;
                }
            }
            else
            {
                if (Session["loginString"] != null)
                {
                    sLoginString = Session["loginString"].ToString();
                }

                abKey = Convert.ToString(Session["abGrid!_CurrentKey"]);
               
                Maximizer.Data.AbEntryAccess oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
               oAbEntryList = oAbEntryAccess.ReadList("KEY(" + abKey + ")");
                foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                {
                    ClientID = oAbEntry.ClientId.Value;
                }
            }
            if (!string.IsNullOrEmpty(abKey)) //load our defaults
            {
                if (Session["loginString"] != null)
                {
                    sLoginString = Session["loginString"].ToString();
                }

            }
            else
            {
                sLoginString = "221265267238022264342011230776z7660786473670f4d64435f186357404453464e0f76716570707263720a7e535e524b595b625f5b5d47036377606473660e5c535c1c42435f1d07066a5f5349000604050d627b77057d7361667366086465740c525d5d4445585a4a4b4b09";
                abKey = "SW5kaXZpZHVhbAkxNzExMjAyNTExMjUwNTY0MjAwMDdDCTA=";

            }

            if (sLoginString == "" || sLoginString == null)
            {
                foreach (Maximizer.Data.AddressBook ab in abList)
                {
                    string database = System.Configuration.ConfigurationManager.AppSettings["Database"];
                    if (0 == ab.Name.ToString().ToUpper().CompareTo(database.ToUpper()))
                    {
                        sRt = ab.Key;
                        break;
                    }
                }

                string Username = System.Configuration.ConfigurationManager.AppSettings["UserName"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Password"];
                sLoginString = oAb.LoginMaximizer(sRt, Username.ToUpper(), password);


            }

            // Always need this to create access to Maximizer and access to special fields.
            oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
            oUDFAccess = oAb.CreateUdfAccess(sLoginString); // always use to setup to write or read from custom fields.

            //  oNoteAccess = oAb.(Access(sLoginString);
            oUserAccess = oAb.CreateUserAccess(sLoginString);
            oOppAccess = oAb.CreateOpportunityAccess(sLoginString);
            oDetailFieldAccess = oAb.CreateDetailFieldAccess(sLoginString);
            oCSCaseAccess = oAb.CreateCSCaseAccess(sLoginString);
            
            string url = "LoadEnquiries.aspx?ClientID=" + ClientID;

            Response.Redirect(url);


        }
        public string GetUdfCSCaseKey(string strUdfName, string strLoginString, AddressBookMaster abMaster)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)abMaster.CreateUdfAccess(strLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
        public string GetUdfKeyOpp(string strUdfName, string strLoginString, Maximizer.Data.AddressBookMaster abMaster)
        {
            Maximizer.Data.UdfAccess udfAccess =
            abMaster.CreateUdfAccess(strLoginString);
            Maximizer.Data.UdfDefinitionList udfDefList =
            udfAccess.ReadUdfDefinitionList(new Maximizer.Data.DefaultOpportunityEntry().Key);
            foreach (Maximizer.Data.UdfDefinition udfDef in udfDefList)
            {
                if (udfDef.FieldName.Equals(strUdfName))
                {
                    return udfDef.Key;
                }
            }
            return String.Empty;
        }
        Maximizer.Data.SalesProcessAccess oSalesProcess = null;
        public string GetSalesProcesAndStage(Maximizer.Data.Opportunity opp)
        {
            oSalesProcess = oAb.CreateSalesProcessAccess(sLoginString);
            string salesKey = GetSaleProcessKey(opp.Key, opp.SalesProcessName.Value);//Get Sales Process Key
            Maximizer.Data.SalesProcessStageList spsl = oSalesProcess.ReadSalesProcessStages(salesKey);

            foreach (Maximizer.Data.SalesProcessStage sps in spsl)
            {
                if (opp.ProbabilityClosing.Value == sps.ProbabilityClose.Value)
                {
                    return sps.Description.Value;
                }
            }
            return "";
        }
        public string GetSaleProcessKey(string key, string salestage)
        {
            string SalesProcessKey = "";
            try
            {
                Maximizer.Data.SalesProcessList salesProcessList = oSalesProcess.ReadSalesProcessDefinitions();

                foreach (Maximizer.Data.SalesProcess sale in salesProcessList)
                {
                    if (sale.Description.Value.ToLower() == salestage.ToLower())
                    {
                        SalesProcessKey = sale.Key;
                        return SalesProcessKey;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return SalesProcessKey;
        }
    }
}