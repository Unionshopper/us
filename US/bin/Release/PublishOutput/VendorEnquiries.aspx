﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorEnquiries.aspx.cs" Inherits="US.VendorEnquiries" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        body {
            font-family: Arial;
        }

        .left-pnl {
            list-style: none;
        }

            .left-pnl ul li {
                  
            }

        .lrft-pnl ul li a {
            border: 1px solid #000;
            padding: 5px;
            background: none;
            display: block;
            text-decoration: none;
            margin-bottom: 5px;
            font-family: Arial;
            cursor:pointer;
        }
        .lrft-pnl ul li a.active {
           background:#00ff21;
           color:#000;
        }
        .lrft-pnl ul li a:hover {
           background:#00ff21;
           color:#000;
        }

        .table-right td {
            padding: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
         <div style="width: 1200px; margin: auto;">

            <table>
                <tr>
                    <td valign="top" style="width: 30%" class="lrft-pnl">
                        <ul style="margin-top: 46px;">
                            <li style=" list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkCurrantEnquiries" OnClick="lnkCurrantEnquiries_Click" CssClass="active"> CURRANT ENQUIRIES</asp:LinkButton>
                            </li>
                            <li style=" list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkCompletedEnquiries" OnClick="lnkCompletedEnquiries_Click"> COMPLETED ENQUIRIES</asp:LinkButton>
                            </li>
                            <li style=" list-style: none !important;" >
                                <asp:LinkButton runat="server" ID="lnkCurrantOrders" OnClick="lnkCurrantOrders_Click"> CURRANT ORDERS</asp:LinkButton>
                            </li>
                            <li style=" list-style: none !important;">
                                <asp:LinkButton runat="server" ID="lnkCompletedOrders" OnClick="lnkCompletedOrders_Click">COMPLETED ORDERS</asp:LinkButton>
                            </li>

                        </ul>
                    </td>
                    <td style="width: 5%"></td>
                    <td valign="top" style="width: 65%">
                        <asp:Panel ID="pnlControls" runat="server"></asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
         
    </form>
</body>
</html>
