﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberMaintenance.aspx.cs" Inherits="US.MemberMaintenance" %>

<%@ Register Src="MenuSection.ascx" TagName="MenuSection" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Member Maintenance</title>
</head>
<body>
    <form runat="server">
        <uc1:MenuSection ID="MenuSection" runat="server" />
        <section class="mide">
            <script>
                (function ($) {
                    $(window).load(function () {

                        $("#content-1").mCustomScrollbar({
                            theme: "minimal"
                        });
                    });
                })(jQuery);
            </script>

            <div class="container">
                <section class="member-maintenance-section">

                    <div class="member-maintenance">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>First Name</label>
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Last Name</label>
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Gender</label>
                                <div class="label-select">
                                    <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control select-box">
                                        <asp:ListItem Text="Gender" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                        <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Member type</label>
                                <div class="label-select">
                                    <asp:DropDownList ID="ddlMemberType" runat="server" CssClass="form-control select-box">
                                        <asp:ListItem Text="Member type" Value="" Selected="True"></asp:ListItem>                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Union/org</label>
                                <div class="label-select">
                                    <asp:DropDownList ID="ddlUnion" runat="server" CssClass="form-control select-box">
                                        <asp:ListItem Text="Union/org" Value="" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Union/org Number</label>
                                <asp:TextBox ID="txtUnionNumner" runat="server" CssClass="form-control" placeholder="Union/Org Number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Position</label>
                               <asp:DropDownList ID="ddlPosition" runat="server" CssClass="form-control select-box">
                                        <asp:ListItem Text="Position" Value="" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Salutation</label>
                                <asp:TextBox ID="txtSalution" runat="server" CssClass="form-control" placeholder="Salution"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Residential Address</label>
                                <asp:TextBox ID="txtResidentialAddress" runat="server" CssClass="form-control add" placeholder="Residential Address"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Mailing Address</label>
                                <asp:TextBox ID="txtMalingAddress" runat="server" CssClass="form-control add" placeholder="Mailing Address"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form checkbox-secation">
                                <asp:CheckBox ID="chkNoValidate" runat="server" Text="No Validate" />

                                <asp:TextBox ID="txtNoValidateAddress" runat="server" CssClass="form-control add" type="text" placeholder="No Validate Address"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form checkbox-secation">
                                <asp:CheckBox ID="chkWasReturned" runat="server" Text="Was Returned" />
                                <asp:TextBox ID="txtWasReturnedAddress" runat="server" CssClass="form-control add" placeholder="Was Returned Address"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>ddlSuburb1</label>
                                <div class="label-select sixty">
                                    <asp:DropDownList ID="ddlSurb1" runat="server" CssClass="form-control select-box">
                                        <asp:ListItem Text="Suburb" Value="" Selected="True"></asp:ListItem>
                                       
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group member-maintenance-form thirty-seven">
                                    <asp:TextBox ID="txtPostCode" runat="server" CssClass="form-control add swenty" placeholder="Post Code"></asp:TextBox>
                                    <button type="button"><i class="fa fa-server" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Suburb</label>
                                <div class="label-select sixty">
                                    <asp:DropDownList ID="ddlSuburb2" runat="server" CssClass="form-control select-box">
                                        <asp:ListItem Text="Suburb" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group member-maintenance-form thirty-seven">
                                    <asp:TextBox ID="txtPostCode1" runat="server" CssClass="form-control add swenty" placeholder="Post Code"></asp:TextBox>
                                    <button type="button"><i class="fa fa-server" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Home</label>
                                <asp:TextBox ID="txtHomeNumber" runat="server" CssClass="form-control add" placeholder="Home Number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Work</label>
                                <asp:TextBox ID="txtWorkNumber" runat="server" CssClass="form-control add" placeholder="Work Number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Mobile</label>
                                <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control add" type="text" placeholder="Mobile Number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                            <div class="form-group member-maintenance-form">
                                <label>Fax</label>
                                <asp:TextBox ID="txtFaxNumber" runat="server" CssClass="form-control add" placeholder="Fax Number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-sep">
                            <h4>Authorisation</h4>
                            <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form chek checkbox-secation">
                                    <asp:CheckBox ID="chkNA" runat="server" Text="NA" />
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form ">
                                    <label>First Name</label>
                                    <asp:TextBox ID="txtAuthFirstName" runat="server" CssClass="form-control add" placeholder="Auth First Name"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Last Name</label>
                                    <asp:TextBox ID="txtAuthLastName" runat="server" CssClass="form-control add" placeholder="Auth Last Name"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Relationship</label>
                                    <div class="label-select">
                                        <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="form-control select-box">
                                            <asp:ListItem Text="Relationship" Value="" Selected="True"></asp:ListItem>                                            
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form chek-two">
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Password (Optional)</label>
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control add" type="password" placeholder="Password"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Home</label>
                                    <asp:TextBox ID="txtAuthHomeNumber" runat="server" CssClass="form-control add" placeholder="Auth Home Number"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Mobile</label>
                                    <asp:TextBox ID="txtAuthMobileNumber" runat="server" CssClass="form-control add" placeholder="Auth Mobile Number"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form chek-two">
                                </div>
                            </div>

                            <div class="col-md-11 col-lg-11 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Work</label>
                                    <asp:TextBox ID="txtAuthWorkNumber" runat="server" CssClass="form-control add" placeholder="Auth Work Number"></asp:TextBox>
                                </div>
                            </div>


                        </div>

                        <div class="form-sep padd">
                            <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form chek checkbox-secation">
                                    <asp:CheckBox ID="chkAllowMail" runat="server" Text="Allow Mail?" />
                                </div>
                            </div>

                            <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form chek checkbox-secation">
                                    <asp:CheckBox ID="chkAllowEmail" runat="server" Text="Allow Email?" />
                                </div>
                            </div>

                            <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form chek-formate checkbox-secation">
                                    <label>Format</label>
                                    <asp:CheckBox ID="chkHTML" runat="server" Text="HTML" />
                                    <asp:CheckBox ID="chkText" runat="server" Text="Text" />
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form chek-formate checkbox-secation">
                                    <label>May we contact you to obtain feedback on our service?</label>
                                    <asp:CheckBox ID="chkFeedBack" runat="server" Text="FeedBack?" />
                                    <asp:TextBox ID="txtFeedBackDate" runat="server" CssClass="form-control add feed-date" placeholder="Feedback Date"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Email Address</label>
                                    <asp:TextBox ID="txtEmailaddress" runat="server" CssClass="form-control add" placeholder="Email Address"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Preferred Contact</label>
                                    <div class="label-select">
                                        <asp:DropDownList ID="DropDownList6" runat="server" CssClass="form-control select-box">
                                            <asp:ListItem Text="Preferred Contact" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-sep">
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Comment</label>
                                    <textarea class="maintence-textarea">Lorem ipsum is dummy text</textarea>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Source</label>
                                    <div class="label-select">
                                        <asp:DropDownList ID="DropDownList7" runat="server" CssClass="form-control select-box">
                                            <asp:ListItem Text="Source" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-sep">
                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Opretor</label>
                                    <asp:TextBox ID="txtOpretor" runat="server" CssClass="form-control add" placeholder="Opretor"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Date Joined</label>
                                    <asp:TextBox ID="txtDateJoined" runat="server" CssClass="form-control add" placeholder="Date Joined"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Last Active Date</label>
                                    <asp:TextBox ID="txtLastActiveDate" runat="server" CssClass="form-control add" placeholder="Last Active Date"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>MShip Start</label>
                                    <asp:TextBox ID="txtMStartDate" runat="server" CssClass="form-control add" placeholder="Mebership Start Date"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group member-maintenance-form">
                                    <label>Mship End</label>
                                    <asp:TextBox ID="txtMEndDate" runat="server" CssClass="form-control add" placeholder="MemberShip End Date"></asp:TextBox>
                                </div>
                            </div>

                            <div class="maintence-btn-section">
                                <button type="button" class="maintence-btn">Internet User Details</button>
                            </div>

                        </div>

                        <div class="form-sep">
                            <div class="maintence-footer">
                                <ul>
                                    <li>
                                        <button type="button" class="maintence-footer-btn"><i class="fa fa-trash" aria-hidden="true"></i>Delete</button></li>
                                    <li class="maintence-footer-input-text">
                                        <asp:TextBox ID="txtMemberID" runat="server" CssClass="maintence-footer-input" placeholder="Member ID"></asp:TextBox></li>
                                    <li>
                                        <button type="button" class="maintence-footer-btn"><i class="zmdi zmdi-close zmdi-hc-fw"></i>Cancel</button></li>
                                    <li>
                                        <button type="button" class="maintence-footer-btn"><i class="zmdi zmdi-check zmdi-hc-fw"></i>Save</button></li>
                                    <li>
                                        <button type="button" class="maintence-footer-btn"><i class="fa fa-history" aria-hidden="true"></i>transfer history</button></li>
                                    <li>
                                        <button type="button" class="maintence-footer-btn">Dept type</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </form>
</body>
</html>
