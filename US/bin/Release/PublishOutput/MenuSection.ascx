﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuSection.ascx.cs" Inherits="US.MenuSection" %>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.css" />

<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="css/resat.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css">

<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.1/css/alertify.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.1/css/themes/default.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.1/css/themes/semantic.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.1/alertify.min.js"></script>
<style>
	html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, center, dl, dt, dd, ol, ul, li, form, label, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, vide {
	font-family:'Roboto', sans-serif !important;
	}
	.alertify-notifier.ajs-bottom.ajs-right {
		top: 0px !important;
	}
</style>
<header>
<%--	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">
						<img src="images/logo.png" alt="Logo"></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling(N) -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="EnquirySystem.aspx"><i class="zmdi zmdi-file zmdi-hc-fw"></i>ENQUIRY SYSTEM </a></li>
						<li><a href="MemberEnquiry.aspx"><i class="zmdi zmdi-account"></i>Member ENQUIRY </a></li>
						<li><a href="EnquirySheet.aspx"><i class="zmdi zmdi-comment-edit"></i>ENQUIRY Entry </a></li>
						<li><a href="MemberMaintenance.aspx"><i class="zmdi zmdi-accounts-list"></i>Member maintenance </a></li>

					</ul>
				</div>
				<!-- /.navbar-collapse(N) -->
			</div>
			<!-- /.container-fluid -->
		</nav>
	</div>--%>

</header>
