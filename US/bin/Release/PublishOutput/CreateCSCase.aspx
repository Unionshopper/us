﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateCSCase.aspx.cs" Inherits="US.CreateCSCase" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="MenuSection.ascx" TagName="MenuSection" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Create Enquiry</title>
    <link href="css/fSelect.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <style>
        .divMemberPrice {
            display: none;
        }

        .DivQuotePrice {
            display: none;
        }

        [for="chkPickupWarranty"]:before, [for="chkPickupWarranty"]:after {
            display: none;
        }

        [for="chkPickupWarranty"] {
            padding-left: 5px !important;
        }

        [for="chkDeliveryWarranty"]:before, [for="chkDeliveryWarranty"]:after {
            display: none;
        }

        [for="chkDeliveryWarranty"] {
            padding-left: 5px !important;
        }

        .lbl-PlaceOrderText {
            font-size: 11px;
        }

        .col-lg-3 {
            height: 68px !important;
        }

        li.nil {
            display: inherit !important;
        }

        #loading {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(images/gears.gif) 50% 50% no-repeat rgba(130, 134, 130, 0.62);
        }

        .heading-notes {
            background: #F5F5F5;
            margin-top: 11px;
            width: 100%;
            margin-left: 0px !important;
            text-align: justify;
        }

        #divnoteshistory, #divenquirynoteshistory {
            background-color: #fff;
            padding: 10px 0 10px 0;
        }

        .btnAddNotes {
            color: #000;
        }

        .detailsection {
            border-radius: 5px;
            border: 2px solid rgba(185,213,49,.87) !important;
            padding: 9px;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .subheading {
            color: rgba(255,255,255,0.87) !important;
            height: 27px;
            text-decoration: none !important;
            display: block;
            background-color: #37474F;
            padding: 6px 0 5px 10px;
            box-sizing: border-box;
            border-radius: 3px 3px 0 0;
        }

        .sub-Heading-possition {
            margin-left: -23px;
        }

        .show_hide {
            text-decoration: none !important;
            display: block;
            box-sizing: border-box;
            /* border-radius: 5px; */
            background: #37474F;
            width: 25px;
            text-align: center;
            position: absolute;
            margin-left: -36px;
            margin-top: 21px;
            color: rgba(255,255,255,0.87) !important;
            padding: 5px;
        }

        #RadNotesCenter {
            height: 21em !important;
        }


        .modal-body {
            padding: 6px;
        }

        .modal-footer {
            padding: 7px 20px 20px;
        }

        .reRow {
            display: none;
        }

        .POPrice {
            display: none;
        }

        select#ddlFilternote, select#ddlEnquiryFilternote {
            color: #000;
        }

        .divshowhide {
            background-color: #37474F;
        }

        .textNoteHeading {
            padding-top: 7px;
        }

        .divshowhide.btn.btn-primary.show_hide {
            margin-left: 0px !important;
            margin-top: 0px !important;
            position: relative !important;
            border-radius: 0px !important;
            padding: 2px !important;
            background-color: #37474F !important;
        }

        #txtsearch {
            background: #fff url("images/eye-search.png") no-repeat scroll 8px center;
            border: 2px solid #c4c5c6;
            border-radius: 5px;
            padding: 0 10px 0 40px;
            position: relative;
            z-index: 1;
            float: right !important;
            height: 34px;
            opacity: 1;
        }

        .btnSearchItems {
            background-color: #616d65;
            color: #fff;
            border-radius: 0px;
            margin-top: 14px;
            text-align: center;
            width: 100%;
            font-size: 14px;
        }

        .btn:hover, .btn:focus, .btn.focus {
            color: #e5dfdf;
            text-decoration: none;
        }

        .btnAddNotes i {
            padding-right: 5px;
        }

        .btnPlaceOrder {
            background-color: #93AD16;
            border-radius: 0px;
            width: 91%;
            margin-top: 21px;
            color: #fff;
            font-weight: bold;
            font-size: 12px !important;
        }

        .RadVendor label {
            padding: 5px;
        }

        #radFilterType label {
            padding-left: 6px;
            font-weight: bold;
        }

        span.ui-button-icon.ui-icon.ui-icon-triangle-1-s {
            background-image: none !important;
        }

        multi-select-container {
            display: inline-block;
            position: relative;
        }

        .multi-select-menu {
            position: absolute;
            left: 0;
            top: 0.8em;
            float: left;
            min-width: 100%;
            background: #fff;
            margin: 1em 0;
            padding: 0.4em 0;
            border: 1px solid #aaa;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
            display: none;
        }

            .multi-select-menu input {
                margin-right: 0.3em;
                vertical-align: 0.1em;
            }

        .multi-select-button {
            display: inline-block;
            font-size: 0.875em;
            padding: 0.2em 0.6em;
            max-width: 20em;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            vertical-align: -0.5em;
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
            cursor: default;
        }

            .multi-select-button:after {
                content: "";
                display: inline-block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0.4em 0.4em 0 0.4em;
                border-color: #999 transparent transparent transparent;
                margin-left: 0.4em;
                vertical-align: 0.1em;
            }

        .multi-select-container--open .multi-select-menu {
            display: block;
        }

        .multi-select-container--open .multi-select-button:after {
            border-width: 0 0.4em 0.4em 0.4em;
            border-color: transparent transparent #999 transparent;
        }

        .btn-group, .btn-group-vertical {
            position: absolute;
            display: inline-block;
            vertical-align: middle;
            padding-top: 7px !important;
        }



            .btn-group > .btn:first-child {
                margin-left: 0;
                width: 235px;
            }
    </style>

</head>
<body style="background-color: #e8ebec !important;">
    <div id="loading" style="display: none;">
    </div>
    <form runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>

        <uc1:MenuSection ID="MENUSECTION" runat="SERVER" />
        <asp:HiddenField ID="hfCountItems" runat="server" Value="1" />
        <asp:HiddenField ID="hfCountForPlaceOrder" runat="server" Value="1" />
        <asp:HiddenField ID="hfVendorFlag" runat="server" Value="1" />
        <asp:HiddenField ID="VendorPlaceOrder" runat="server" />
        <asp:HiddenField ID="RevenuePrice" runat="server" />
        <asp:HiddenField ID="hfItemsNumber" runat="server" />
        <asp:HiddenField ID="hfConfirmOrderOppKey" runat="server" />
        <asp:HiddenField ID="hfVendorName" runat="server" />
        <asp:HiddenField ID="hdfProduct" runat="server" />

        <%--<asp:UpdatePanel ID="updatepanel" runat="server">
			<ContentTemplate>--%>
        <asp:Button ID="btnHFPlaceOrder" runat="server" Text="Place Order" Style="display: none;" OnClick="btnHFPlaceOrder_Click" />
        <%--</ContentTemplate>
		</asp:UpdatePanel>--%>

        <section class="mide">
            <section class="enquiry-sheet-part">
                <div class="enquiry-sheet-heading-part">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                <div class="enquiry-number-part" id="EQNumber" runat="server">
                                    <p>Enquiry Number</p>
                                    <div class="enquiry-number">
                                        <asp:Label ID="lblEnquiryNumber" runat="server"></asp:Label>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                <div class="enquiry-name">
                                    &nbsp;
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 ">
                                <div class="enquiry-number-part">
                                    <h3>
                                        <asp:Label ID="lblMidEQ" runat="server"></asp:Label></h3>
                                </div>
                            </div>

                            <%--<div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 pull-right">
								<div class="enquiry-complet">
									<i class="fa fa-check-circle" aria-hidden="true"></i>
									<span>Completed</span>
								</div>
							</div>--%>
                        </div>
                    </div>
                </div>
            </section>

            <section class="member-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading" style="display: none;">
                                    Member  <span>
                                        <asp:Label ID="lblMember" runat="server"></asp:Label></span>
                                </div>
                                <div class="member-content">
                                    <div class="member-content-icon">
                                        <i class="zmdi zmdi-account zmdi-hc-fw"></i>
                                    </div>
                                    <div class="member-content-details half">
                                        <h4>
                                            <asp:Label ID="lbluser" runat="server"></asp:Label></h4>
                                        <span>
                                            <asp:Label ID="lblAddress" runat="server"></asp:Label></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    Member
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul id="menu">
                                            <%-- <li class="nil">Home: <span>
                                            <asp:Label ID="lblHomeNumber" runat="server" Visible="false"></asp:Label></span>
                                            </li>
                                              <li class="nil">Bus: <span>
                                          
                                                <asp:Label ID="lblBusNumber" runat="server"></asp:Label></span>
                                            </li>
                                            <li class="nil">Mob :<span>
                                                <asp:Label ID="lblMobileNumber" runat="server"></asp:Label></span>
                                            </li>--%>
                                            <li class="nil">Main: 
                                                  <span>
                                                      <asp:Label ID="lblBusNumber" runat="server"></asp:Label></span>
                                            </li>

                                            <li class="nil">Cell:
                                                  <span>
                                                      <asp:Label ID="lblMobileNumber" runat="server"></asp:Label></span>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 sep mem">
                            <div class="member-part">
                                <div class="member-heading">
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 sep mem">
                            <div class="member-part">
                                <div class="member-heading">
                                    Authorised Contact
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>VC Name: <span>
                                                <asp:Label ID="lblVCName" runat="server"></asp:Label></span></li>
                                            <li>VC Contact: <span>
                                                <asp:Label ID="lblVCContact" runat="server"></asp:Label></span></li>
                                            <li>VC Relationship: <span>
                                                <asp:Label ID="lblVCRelationship" runat="server"></asp:Label></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--<div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
							<div class="member-part">
								<div class="member-heading">
									&nbsp;
								</div>
								<div class="member-content">
									<div class="member-content-details">
										<ul>
											<li>Union/Org <span>
												<asp:Label ID="lblUnion" runat="server"></asp:Label></span></li>
											<li>Num</li>
											<li>Auth <span></span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>--%>

                        <%--						<div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
							<div class="member-part">
								<div class="member-heading">
									&nbsp;
								</div>
								<div class="member-content">
									<div class="member-content-details">
										<ul>
											<li>Last Enq  <span>
												<asp:Label ID="lblLastEnqDate" runat="server"></asp:Label></span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>--%>
                    </div>
                </div>
            </section>



            <section class="key-section" style="padding: 17px 0;">
                <div class="container" style="background-color: #fdfdfd; padding: 10px 10px;">
                    <div class="row" style="background: #c9c4c4; padding: 5px 10px; margin: 17px 0 10px 0;" id="rwHeaderOption" runat="server">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <asp:RadioButtonList ID="radFilterType" runat="server" RepeatDirection="Horizontal" Style="width: 79%; font-size: 16px;">
                                <asp:ListItem Text="Direct Enquiry" Value="Direct Enquiry"></asp:ListItem>
                                <asp:ListItem Text="Direct Order" Value="Direct Order"></asp:ListItem>
                            </asp:RadioButtonList>

                        </div>
                        <div class="col-lg-4">&nbsp;</div>
                        <div class="col-lg-4 hide" id="divSendEmail">
                            <asp:Label ID="lblemail" runat="server" CssClass="QuoteEmaillbl" Text="Send Quote Email:-"></asp:Label>
                            <asp:CheckBoxList ID="chkEmail" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="GG" Value="GG" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="JBHIFI" Value="JBHIFI" Selected="True"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>

                    </div>
                    <div class="row hfEmail" style="margin-top: 17px; margin-bottom: 10px;" id="DivSearch">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <asp:Label ID="lblSearchItems" runat="server" Style="font-size: 15px;">Search Item</asp:Label>
                            <asp:TextBox ID="txtsearch" runat="server" CssClass="form-control" Style="border-radius: 0px; height: 33px; background-color: #fff;"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a id="btnGG" runat="server" class="btn btnSearchItems">Search GG</a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a id="btnjbhifi" runat="server" class="btn btnSearchItems">Search JBHIFI</a>
                        </div>
                    </div>
                    <div class="row hfEmail" id="DivNotes">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="divshowhide">
                                <a href="javascript:void(0)" data-text="" data-id="divNotesHistory" class="show_hide">+</a>
                            </div>
                            <div class="detailsection">
                                <div class="subheading" style="height: 41px;">
                                    <div class="col-lg-3">
                                        <span id="Label11" class="subheading sub-Heading-possition"><b>CLIENT NOTES HISTORY:</b></span>
                                    </div>
                                    <div class="col-lg-7 text-right">
                                        <div id="divClientNotes" runat="server" style="display: none;">
                                            <button type="button" onclick="AddNote();" class="btnAddNotes">
                                                <i class="zmdi zmdi-border-color"></i>Add Note
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="filter-label">Filter:</label>
                                        <asp:DropDownList ID="ddlFilternote" runat="server" OnSelectedIndexChanged="ddlFilternote_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                            <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>


                                </div>
                                <div class="form-area" id="divNotesHistory" style="display: none;">
                                    <ul class="home-form">
                                        <li>
                                            <asp:UpdatePanel runat="server" ID="updatePanel1">
                                                <ContentTemplate>
                                                    <div id="divnoteshistory" runat="server">
                                                        <div class="row">
                                                            <div class="col-lg-12 text-center"><span style="font-size: 17px; color: red"><b>Record not found</b></span></div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>

                    </div>

                    <div class="row" style="margin-top: 21px;" runat="server">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <asp:Label ID="Label2" runat="server">Enquiry Comments</asp:Label>
                            <asp:TextBox ID="txtEnquiryComments" runat="server" CssClass="txt-item" />
                        </div>
                    </div>
                    <div class="row" style="margin-top: 21px;" id="item1" runat="server">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <fieldset>
                                <legend id="legendItem1" runat="server">Item #1</legend>
                                <div class="row">
                                    <div class="col-lg-3 selectdiv">
                                        <asp:Label ID="lblBrand" runat="server">Brand</asp:Label>
                                        <asp:DropDownList ID="ddlBrand" runat="server" CssClass="ddlBrand" Width="100%">
                                            <asp:ListItem Text="" Value=""></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <div class="col-lg-3 selectdiv">
                                        <asp:Label ID="lblProduct" runat="server">Product</asp:Label>
                                        <asp:DropDownList ID="ddlProduct" runat="server" CssClass="ddlProduct" Width="100%">
                                            <asp:ListItem Text="" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:Label ID="lblModel" runat="server">Model</asp:Label>
                                        <asp:TextBox ID="txtModel" runat="server" CssClass="txt-item" />
                                    </div>
                                    <div class="col-lg-3 DivQuotePrice">
                                        <div class="QuotedPrice">

                                            <asp:Label ID="lblQuotedPrice" runat="server">Members Price:$</asp:Label>
                                            <%--  <asp:Label ID="lblQuotedPrice" runat="server">Member Price Description</asp:Label>--%>
                                            <asp:TextBox ID="txtQuotedPrice" TextMode="Number" runat="server" CssClass="DirectQuotePrice txt-item" step="0.01" />
                                            <%--  <asp:TextBox ID="txtQuotedPrice" runat="server" CssClass="DirectQuotePrice txt-item" step="0.01" />--%>
                                            <asp:HiddenField ID="txtQuotedPriceHF" runat="server" />


                                        </div>
                                        <%--    <div class="QuotedPrice">
                                            <asp:Label ID="lblMemPriceDesc" runat="server"></asp:Label>
                                            <asp:TextBox ID="txtMemPriceDesc" TextMode="Number" runat="server" CssClass="txt-item" step="0.01" />
                                            <asp:HiddenField ID="txtMemPriceDescHF" runat="server" />
                                        </div>--%>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:Label ID="lblPayBY" runat="server">Pay BY</asp:Label>
                                        <%-- <asp:DropDownList ID="ddlPayBY" runat="server" CssClass="ddlPayBY">
                                            <asp:ListItem Text="" Value=""></asp:ListItem>
                                        </asp:DropDownList>--%>
                                        <asp:Literal ID="ltrPayBY" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hfPayBY" runat="server" />
                                    </div>
                                    <div class="col-lg-3 DeliveryAddress" id="divDeliveryAddress" runat="server">
                                        <span id="lblDeliveryAddress">Street Address</span>
                                        <asp:TextBox ID="txtDeliveryAddress" runat="server" CssClass="txt-item" Style="width: 100%;" />
                                    </div>
                                    <div class="col-lg-3">
                                        <span id="lblDeliveryPostCode">Suburb and Postcode</span>
                                        <asp:TextBox ID="txtDeliveryPostCode" runat="server" CssClass="txt-item" Style="width: 100%;" />
                                    </div>
                                    <div class="col-lg-3 selectdiv divMemberPrice" id="divPickupOrDelivery" runat="server">
                                        <asp:Label ID="lblPickupOrDelivery" runat="server">Member Price Option</asp:Label>
                                        <asp:DropDownList ID="ddlPickupOrDelivery" runat="server" CssClass="ddlPickupOrDelivery" data-id="">
                                            <asp:ListItem Text="" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Pickup" Value="Pickup"></asp:ListItem>
                                            <asp:ListItem Text="Delivery" Value="Delivery"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <%--  MultiSelect
                                   <div class="col-lg-3 divPickupOrDelivery" id="divPickupOrDelivery">
                                        <asp:Label ID="lblPickupOrDeliveryy" runat="server">Pickup or Delivery </asp:Label>
                                        <br />
                                        <span id="span" runat="server">
                                            <asp:Literal ID="ltrPickupOrDeliveryy" runat="server"></asp:Literal>
                                        </span>
                                        <asp:HiddenField ID="hfPickupOrDeliveryy1" runat="server" />
                                    </div>--%>
                                    <%--<asp:ListBox ID="ddlPickupOrDelivery" runat="server"></asp:ListBox>--%>
                                    <div class="col-lg-3">
                                        <asp:Label ID="lblDescription" runat="server">Description</asp:Label>
                                        <asp:TextBox ID="txtDesc" runat="server" CssClass="txt-item" TextMode="MultiLine" />
                                    </div>
                                    <div class="col-lg-3 ">
                                        <div class="SubmitTo">
                                            <asp:Label ID="lblSubmitTo" runat="server">Submit To </asp:Label>
                                            <br />
                                            <asp:Literal ID="ltrSubmitedTo" runat="server"></asp:Literal>
                                            <asp:HiddenField ID="hfSubmitTo1" runat="server" />

                                        </div>

                                    </div>



                                  <%--  <div class="col-lg-3">
                                        &nbsp;
                                    </div>

                                    <div id="divPriceValue" runat="server" visible="false">
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblComments1" runat="server" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtComments1" runat="server" CssClass="txt-item" Style="width: 100%;" Visible="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblProductPrice1" runat="server" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtProductPrice1" onkeypress="return isNumber(event)" runat="server" CssClass="txt-item" Style="width: 100%;" Visible="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblDeliveryPrice1" runat="server" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtDeliveryPrice1" onkeypress="return isNumber(event)" runat="server" CssClass="txt-item" Style="width: 100%;" Visible="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblInstallPrice1" runat="server" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtInstallPrice1" onkeypress="return isNumber(event)" runat="server" CssClass="txt-item" Style="width: 100%;" Visible="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblRemovalPrice1" runat="server" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtRemovalPrice1" onkeypress="return isNumber(event)" runat="server" CssClass="txt-item" Style="width: 100%;" Visible="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblWarrentyPrice1" runat="server" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtWarrentyPrice1" onkeypress="return isNumber(event)" runat="server" CssClass="txt-item" Style="width: 100%;" Visible="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblWarrentyDesc1" runat="server" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtWarrentyDesc1" runat="server" CssClass="txt-item" Style="width: 100%;" Visible="false" />
                                        </div>
                                    </div>--%>


                                </div>
                                <%--  <div class="row" style="display: none;" id="divDeliveryAddress" runat="server">
                                    <div class="col-lg-3 SubmitTo">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        <span id="lblDeliveryAddress">Delivery Address</span>
                                        <asp:TextBox ID="txtDeliveryAddress" runat="server" CssClass="txt-item" Style="width: 100%;" />
                                    </div>
                                    <div class="col-lg-3">
                                        <span id="lblOptions">Options</span>
                                        <br />
                                        <asp:Literal ID="ltrOptions" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hfOptions" runat="server" />
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:CheckBox ID="chkDeliveryWarranty" runat="server" Text="Warranty" Style="margin: 29px 4px;"></asp:CheckBox>

                                        <input type="text" id="txtwarrantyForDirectOrder" name="txtwarrantyForDirectOrder" class="txt-item" style="display: none; margin-top: 3px;" />
                                    </div>
                                </div>
                                <div class="row" style="display: none;" id="divPickup" runat="server">
                                    <div class="col-lg-3 SubmitTo" style="height: auto !important;">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3" style="height: auto !important;">
                                        <asp:CheckBox ID="chkPickupWarranty" runat="server" Text="Warranty" Style="margin: 11px 4px;"></asp:CheckBox>
                                        <input type="text" id="txtwarrantyForPickUpOrder" name="txtwarrantyForPickUpOrder" class="txt-item" style="display: none; margin-top: 3px;" />
                                    </div>
                                </div>--%>

                                <div class="row" id="DivDO" style="display: none; margin-top: 15px;" runat="server">
                                </div>
                                <div class="row" id="divchkDirectOrder" style="display: none;">
                                    <div class="col-lg-3">
                                        <asp:Label ID="lblDirectOrder" runat="server" Text="Direct Order"></asp:Label>

                                        <asp:CheckBox ID="chkGG1" CssClass="DO" runat="server" Text="GG" Value="GG" onclick="diplayOrderList(this);"></asp:CheckBox>
                                        <asp:CheckBox ID="chkJBHIFI1" CssClass="DO" runat="server" Text="JBHIFI" Value="JBHIFI" onclick="diplayOrderList(this);"></asp:CheckBox>

                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right SubmitTo" id="addItemsDiv" runat="server">
                            <input id="btnAdd" type="button" value="ADD" class="btn btn-success btnAdd" onclick="addItem(0);" />
                            <asp:HiddenField ID="hfRequestType" runat="server" />
                            <asp:HiddenField ID="hfddlBrand" runat="server" />
                            <asp:HiddenField ID="hfddlProduct" runat="server" />
                        </div>
                    </div>
                </div>
                <div id="dvAddItemsHTML" runat="server">
                </div>


            </section>


            <section class="enquiry-sheet-bottom-part">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 enquiry-sheet-bottom text-center">
                            <asp:Button ID="btnCreateEnquiry" runat="server" CssClass="btn btn-success btn-createEnquery" Text="Create Enquiry" OnClick="btnCreateEnquiry_Click" />
                            <asp:Button ID="btnConfirmOrder" runat="server" CssClass="btn btn-success" Text="Confirm Order" Style="display: none;" OnClick="btnConfirmOrder_Click" />

                        </div>
                    </div>
                </div>
            </section>
        </section>

        <!-- Add Notes for Contact -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">
                            <asp:Label ID="lblHeader" runat="server"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <telerik:RadEditor RenderMode="Lightweight" EditModes="Design,Preview" ToolsFile="DefaultToolsFile.xml" runat="server" ID="RadNotes" Width="100%" Height="350">
                                </telerik:RadEditor>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="margin-top: 10px; padding-left: 17px;">
                                <label>Category</label>
                                <asp:DropDownList ID="ddlcategory" runat="server" Style="border-radius: 4px; width: 230px; font-size: 14px; padding: 2px; margin: 5px;"
                                    CssClass="form-control">
                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                    <asp:ListItem Value="Vendor" Text="Vendor"></asp:ListItem>
                                    <asp:ListItem Value="Union Shopper" Text="Union Shopper"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: right;">
                        <asp:UpdatePanel runat="server" ID="updatePanelNote">
                            <ContentTemplate>
                                <asp:Button ID="btnSaveClient" runat="server" CssClass="btn btn-success hide" Text="Save" OnClick="btnSaveClient_Click" />
                                <asp:Button ID="btnSaveEnquiry" runat="server" CssClass="btn btn-success hide" Text="Save" OnClick="btnSaveEnquiry_Click" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <asp:HiddenField ID="hfContactId" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <style>
        .nil {
            display: inline;
            white-space: nowrap;
        }
    </style>
    <script src="Scripts/fSelect.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" />

    <script type="text/jscript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

    <script type="text/jscript">
        jQuery.noConflict();
        (function ($) {
            $(function () {
                filterType();
                function resetItems() {

                    $('#dvAddItemsHTML').html("");
                    $('.txt-item').each(function () {
                        $(this).val("");
                    });
                    $('#ddlPickupOrDelivery').val("");
                    $('.fs-label-wrap').each(function () {
                        $(this).find('.fs-label').html("-----Select-----");
                    });
                    $("input:checkbox").prop('checked', false);
                    $('.g0').removeClass("selected");

                    $("#hfCountItems").val("1");

                    //$('#divDeliveryAddress').css("display", "none");
                    $('#divPickup').css("display", "none");

                    $(".ddlBrand").val("");
                    $(".ddlProduct").val("");
                    $('#txtwarrantyForPickUpOrder').css("display", "none");
                    $('#txtwarrantyForDirectOrder').css("display", "none");
                }

                function filterType() {
                    var checked_radio = $("[id*=radFilterType] input:checked");
                    var value = checked_radio.val();
                    if (value == "Direct Enquiry") {
                        // $('#lblQuotedPrice').text("Member Price Description");
                        //$('.btnAdd').show();
                        //$('legend').show();
                        $('#divSendEmail').show();
                        $('.DivQuotePrice').show();
                        $('#divchkDirectOrder').hide();
                        $('.SubmitTo').find('.fs-wrap').addClass("multiple");
                        $('#btnCreateEnquiry').text("Send Direct Enquiry");
                        $('#btnCreateEnquiry').val("Send Direct Enquiry");

                        $('.divMemberPrice').show();
                        $('#divDeliveryAddress').show();

                        //$('#dvAddItemsHTML').show();
                    } else if (value == "Direct Order") {
                        //$('#lblQuotedPrice').text("Direct Order Price $");
                        //$('.btnAdd').hide();
                        //$('legend').hide();
                        $('.divMemberPrice').hide();
                        $('#divSendEmail').hide();
                        $('.DivQuotePrice').hide();

                        $('#divchkDirectOrder').hide();
                        $('#btnCreateEnquiry').text("Send Direct Order");
                        $('#btnCreateEnquiry').val("Send Direct Order");

                        $('.SubmitTo').find('.fs-wrap').removeClass("multiple");
                        $('#divDeliveryAddress').hide();
                        //$('#dvAddItemsHTML').hide();

                    }
                }
                $("[id*=radFilterType]").click(function () {

                    filterType();
                    resetItems();
                    $('#DivDO').html("");
                });

                $.widget("custom.combobox", {
                    _create: function () {
                        this.wrapper = $("<span>")
                            .addClass("custom-combobox")
                            .insertAfter(this.element);

                        this.element.hide();
                        this._createAutocomplete();
                        this._createShowAllButton();
                    },

                    _createAutocomplete: function () {

                        var selected = this.element.children(":selected"), value = selected.val() ? selected.text() : "";
                        var id = "hf" + this.element[0].id;
                        $('#' + id).val(selected.val());
                        this.input = $("<input>")
                            .appendTo(this.wrapper)
                            .val(value)
                            .attr("title", "")
                            .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                            .autocomplete({
                                delay: 0,
                                minLength: 0,
                                source: $.proxy(this, "_source")
                            })
                            .tooltip({
                                classes: {
                                    "ui-tooltip": "ui-state-highlight"
                                }
                            });
                        this._on(this.input, {
                            autocompleteselect: function (event, ui) {

                                var id = "hf" + this.element[0].id;
                                $('#' + id).val(ui.item.value);
                                ui.item.option.selected = true;
                                this._trigger("select", event, {
                                    item: ui.item.option
                                });
                            },

                            autocompletechange: "_removeIfInvalid"
                        });
                    },
                    _createShowAllButton: function () {
                        var input = this.input,
                            wasOpen = false;

                        $("<a>")
                            .attr("tabIndex", -1)
                            .attr("title", "Show All Items")
                            .tooltip()
                            .appendTo(this.wrapper)
                            .button({
                                icons: {
                                    primary: "ui-icon-triangle-1-s"
                                },
                                text: false
                            })
                            .removeClass("ui-corner-all")
                            .addClass("custom-combobox-toggle ui-corner-right")
                            .on("mousedown", function () {
                                wasOpen = input.autocomplete("widget").is(":visible");
                            })
                            .on("click", function () {
                                input.trigger("focus");
                                // Close if already visible
                                if (wasOpen) {
                                    return;
                                }
                                // Pass empty string as value to search for, displaying all results
                                input.autocomplete("search", "");
                            });
                    },
                    _source: function (request, response) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        response(this.element.children("option").map(function () {
                            var text = $(this).text();
                            if (this.value && (!request.term || matcher.test(text)))
                                return {
                                    label: text,
                                    value: text,
                                    option: this
                                };
                        }));
                    },
                    _removeIfInvalid: function (event, ui) {

                        // Selected an item, nothing to do
                        if (ui.item) {
                            return;
                        }
                        // Search for a match (case-insensitive)
                        var value = this.input.val(),
                            valueLowerCase = value.toLowerCase(),
                            valid = false;
                        this.element.children("option").each(function () {
                            if ($(this).text().toLowerCase() === valueLowerCase) {
                                this.selected = valid = true;
                                return false;
                            }
                        });

                        // Found a match, nothing to do
                        if (valid) {
                            return;
                        }


                        var id = "hf" + this.element[0].id;
                        $('#' + id).val(this.input.val());

                        // Remove invalid value
                        this.input
                            //.val("")


                            .attr("title", value + " didn't match any item")
                            .tooltip("open");
                        //this.element.val("");
                        this._delay(function () {
                            this.input.tooltip("close").attr("title", "");
                        }, 2500);
                        this.input.autocomplete("instance").term = "";
                    },
                    _destroy: function () {
                        this.wrapper.remove();
                        this.element.show();
                    }
                });
                debugger;
                var RequestType = $('#hfRequestType').val();
                if (RequestType == "Email" || RequestType == "Vendor") {
                    debugger;
                    if (RequestType == "Vendor") {
                        $('.SubmitTo').parent(".col-lg-3").hide();
                        $('.btnAdd').hide();
                    }
                    else {
                        $('.SubmitTo').hide();
                    }

                    $('.QuotedPrice').parent(".col-lg-3").hide();
                    // $('.QuotedPrice').hide();

                    $('.hfEmail').hide();

                    //$(".ddlBrand").combobox("option", "disabled", true);
                    //$(".ddlProduct").combobox("option", "disabled", true);

                    // $(".ddlBrand").closest(".ui-widget").find("input, button" ).prop("disabled", true)
                    if (RequestType == "Vendor") {
                        //$('.QuotedPrice').parent().show();
                        $('.btnPlaceOrder').css("display", "none !important");
                        $('#btnCreateEnquiry').text("Update Order").val("Update Order");
                        //$('#btnConfirmOrder').css("display", "block");
                        $('#lblMidEQ').text("");
                        $('#txtEnquiryComments').attr("disabled");
                        $('.POPrice').show();
                    } else {
                        $('.member-content-details').hide();
                    }
                    $('#DivNotes').show();

                } else {
                    $(".ddlBrand").combobox();
                    $(".ddlProduct").combobox();
                    $('.POPrice').show();
                }
            });
        })(jQuery);
    </script>
    <style>
        .btnAdd {
            padding: 4px 9px !important;
            font-size: 12px !important;
            border-radius: 0px !important;
        }

        .txt-item {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            margin: 8px 0;
        }

        legend {
            font-weight: bold;
            font-size: 16px !important;
        }

        .fs-dropdown {
            margin-top: 0px !important;
        }

        .fs-wrap {
            width: 100% !important;
            margin-top: 6px;
        }

        .fs-dropdown {
            width: 81% !important;
        }

        .fs-label-wrap {
            height: 34px;
        }

        .fs-arrow {
            border-top: 5px solid #B9D530 !important;
        }

        #chkEmail_0 {
            display: none;
        }

        #chkEmail_1 {
            display: none;
        }

        div {
            font-size: 14px !important;
        }

        .fs-label-wrap .fs-label {
            padding: 9px 22px 6px 8px;
        }


        input.acdd-input {
            margin-top: 8px;
            height: 34px;
        }

        .selectdiv:after {
            content: '\f078';
            font: normal normal normal 11px/1 FontAwesome;
            color: #93AD16;
            right: 22px;
            top: 20px;
            height: 34px;
            padding: 15px 0px 0px 8px;
            /* border-left: 1px solid #0ebeff; */
            position: absolute;
            pointer-events: none;
        }

        /* IE11 hide native button (thanks Matt!) */
        select::-ms-expand {
            display: none;
        }

        .selectdiv select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            /* Add some styling */
            display: block;
            width: 100%;
            max-width: 320px;
            height: 34px;
            float: right;
            margin: 8px 0px;
            padding: 0px 5px;
            font-size: 14px;
            color: #333;
            background-color: #ffffff;
            background-image: none;
            -ms-word-break: normal;
            word-break: normal;
        }

        .ui-widget.ui-widget-content {
            max-height: 250px;
            overflow-y: auto;
        }

        .btnDelete {
            padding: 4px 6px !important;
            border-radius: 0px !important;
        }

        .DO [type=checkbox] {
            display: none;
        }

        .custom-combobox {
            position: relative;
            display: inline-block;
            width: 100%;
            height: 34px;
            margin-top: 8px;
        }

        .custom-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;
            margin-left: -1px;
            padding: 0;
        }

        .custom-combobox-input {
            margin: 0;
            padding: 5px 10px;
        }

        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
            border-bottom-left-radius: 0px !important;
            background-color: #fff;
            height: 34px;
            width: 90%;
        }

        a.ui-button.ui-widget.ui-button-icon-only.custom-combobox-toggle.ui-corner-right {
            background-color: #fff;
        }

        a.ui-button.ui-widget.ui-button-icon-only.custom-combobox-toggle.ui-corner-right {
            border-radius: 0px !important;
        }

        .QuoteEmaillbl {
            float: left;
            margin-right: 10px;
            margin-top: 2px;
            font-weight: bold;
        }
    </style>




    <script>

        function AddNote() {
            debugger;
            jQuery.noConflict();
            var name = $('#lbluser').text().split('-')[0];
            $('#myModal').modal('toggle');
            $('#lblHeader').text("Add Note For " + name);
            $('#btnSaveEnquiry').addClass("hide");
            $('#btnSaveClient').removeClass("hide");

        }

        $('.show_hide').click(function () {
            jQuery.noConflict();

            var text = $(this).attr("data-text");
            var divID = $(this).attr("data-id");
            if ($("#" + divID).hasClass("show")) {
                $("#" + divID).slideUp();
                $("#" + divID).removeClass("show");
                if (text != "undefined") {
                    $(this).text("+" + text);
                } else {
                    $(this).text("+");
                }
            }
            else {
                $("#" + divID).slideDown();
                $("#" + divID).addClass("show");
                if (text != "undefined") {
                    $(this).text("-" + text);
                } else {
                    $(this).text("-");
                }
            }
        });

        $("#btnGG").click(function () {
            jQuery.noConflict();

            var values = $('#txtsearch').val();
            if (values != "" && values.length > 0) {
                //var URL = "https://www.thegoodguys.com.au/" + values.toLowerCase();
                var URL = "https://www.thegoodguys.com.au/SearchDisplay?categoryId=&storeId=900&catalogId=30000&langId=-1&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q&pageView=&beginIndex=0&orderBy=0&pageSize=60&searchTerm=" + values.toLowerCase();
                window.open(URL, '_blank');
            } else {
                alertify.error("Required GG Serach Item");
            }
        });

        $("#btnjbhifi").click(function () {
            jQuery.noConflict();

            var values = $('#txtsearch').val();
            if (values != "" && values.length > 0) {
                var URL = "https://www.jbhifi.com.au/?q=" + values;
                window.open(URL, '_blank');
            }
            else {
                alertify.error("Required JBHIFI Serach Item");
            }
        });


        function PlaceOrder(Order) {
            debugger;
            var RequestType = $('#hfRequestType').val();
            if (RequestType == "CallCenter" || RequestType == "Vendor") {
                var isValid = true;
                var arr = Order.split('-');
                var ItemNo = arr[0];
                var VendorType = arr[1];
                //if ($('#hfSubmitTo' + ItemNo).val() != "") {
                //	VendorType = $('#hfSubmitTo' + ItemNo).val();
                //}

                var count = "";
                //if (ItemNo != 1)
                var deliveryType = "";
                count = ItemNo;


                var amount = 0;
                var valueType = "";
                //var flag = false;
                var count = 0;



                //if (count != 1 && flag) {
                //    alertify.error("Please enter only one $ value before submitting, Not Multiple");
                //    isValid = false;
                //}
                var warrantyFlag = false;
                //if (!flag) {
                //    alertify.error("Please enter a $ value before submitting");
                //    isValid = false;
                //}
                //else {
                debugger;
                var countChecked = 0;
                $("input[name='POPrice" + VendorType + ItemNo + "']").each(function () {


                    if ($(this).is(":checked") == true) {
                        var text = $(this).attr('data-id');
                        if (text.indexOf("Warranty Price") == -1) {
                            countChecked++;
                        } else {
                            warrantyFlag = true;
                        }
                    }
                });
                debugger;
                if (countChecked != 0) {
                    var warrantyCost = 0;
                    debugger;
                    $("input[name='POPrice" + VendorType + ItemNo + "']:checked").each(function () {

                        var amt = $(this).next().val();
                        valueType = $(this).attr("data-id");
                        if (valueType.indexOf("Warranty Price") == -1) {
                            debugger;
                            amount = parseFloat(amount) + parseFloat(amt);
                        }
                    });
                    if (amount == "" && amount <= 0) {
                        alertify.error("Please enter a $ value before submitting");
                        isValid = false;
                    } else {
                        $('#RevenuePrice').val(parseFloat(amount).toFixed(2));
                    }


                }
                else if (countChecked == 0) {


                    alertify.error("Please select $ value type for Opportunity");
                    isValid = false;
                }
                //else {
                //    debugger;
                //    if (valueType.indexOf("Warranty Price") == -1) {
                //        //alertify.error("Please select only one $ value type for Opportunity");
                //        isValid = false;
                //    }
                //}
                //}

                if (isValid) {
                    debugger;
                    var IsProductRate = true;
                    $("input[name='POPrice" + VendorType + ItemNo + "']").each(function () {
                        if ($(this).is(":checked") == false) {
                            if ($(this).attr("data-id").toLowerCase() == (VendorType + " Product Price").toLowerCase()) {
                                IsProductRate = false;
                            } else {
                                $(this).next().val(0);
                            }
                        }
                    });

                    if (!IsProductRate) {
                        alertify.error("Required Product Price");
                        isValid = false;
                    }
                }
                if (isValid) {
                    var msg = '';
                    if (RequestType == "Vendor") {
                        msg = 'Are you sure you want to update Order?';
                    } else {
                        if ($("#hdfProduct").val() == '0') {
                            alertify.error("Please select product price");
                            return false;
                        }
                        msg = 'Are you sure you wish to place an order with ' + VendorType + '?';
                    }
                    if (confirm(msg)) {

                        $('#hfCountForPlaceOrder').val(ItemNo);
                        $('#loading ').css('display', 'block');
                        $('#VendorPlaceOrder').val(VendorType);
                        var i = "";
                        if (count != 1)
                            i = count;
                        $("#txtQuotedPriceHF" + i).val($("#txtQuotedPrice" + i).val());

                        if (RequestType != "Vendor") {
                            $('#btnHFPlaceOrder').click();

                            $("#btnPlaceOrder" + VendorType + ItemNo).text("Order Placed " + VendorType);
                            $("#btnPlaceOrder" + VendorType + ItemNo).attr("disabled", true);
                            alertify.success("Place Order Successfully");
                            //setTimeout($('#loading ').css('display', 'none'), 4000);
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                alertify.error("Opps! there is something wrong!!");
                return false;
            }
        }

        //$(".divPickupOrDelivery").change(function () {
        //    //alert(this.id);
        //    
        //    var idd = (this.id).substr(-1);
        //    // alert(idd);
        //    if (idd == "y") {
        //        idd = "";
        //    }
        //    var value = document.getElementById('divPickupOrDelivery' + idd).innerText;
        //    var strArray = value.split('\n');
        //    if (strArray[1] == "Delivery") {
        //        $('#divDeliveryAddress' + idd).show();
        //    }
        //    else if (strArray[1] == "Pickup, Delivery") {
        //        $('#divDeliveryAddress' + idd).show();
        //    }
        //    else {
        //        $('#divDeliveryAddress' + idd).hide();
        //    }
        //});


        function addItem(idNumber) {

            jQuery.noConflict();
            var valid = true;
            if (idNumber == 0) {
                valid = Validation("");
            } else {
                valid = Validation(idNumber);
            }
            if (valid) {

                var count = 0;
                var countItems = $('#hfCountItems').val();
                if (parseInt(countItems) < parseInt("10")) {

                    var ddlBrand = document.getElementById('ddlBrand');
                    var DllBrandOptions = "";
                    for (var i = 0; i < ddlBrand.options.length; i++) {

                        DllBrandOptions += "<option value='" + ddlBrand.options[i].value + "'>" + ddlBrand.options[i].text + "</option>";
                    }

                    var ddlProduct = document.getElementById('ddlProduct');
                    var DdlProductOptions = "";
                    for (var i = 0; i < ddlProduct.options.length; i++) {

                        DdlProductOptions += "<option value='" + ddlProduct.options[i].value + "'>" + ddlProduct.options[i].text + "</option>";
                    }

                    var ddlBrand = document.getElementById('ddlBrand');
                    var DllBrandOptions = "";
                    for (var i = 0; i < ddlBrand.options.length; i++) {

                        DllBrandOptions += "<option value='" + ddlBrand.options[i].value + "'>" + ddlBrand.options[i].text + "</option>";
                    }

                    var ddlPayBY = document.getElementById('ddlPayBY');
                    var ddlPayByOption = "";
                    for (var i = 0; i < ddlPayBY.options.length; i++) {

                        ddlPayByOption += "<option value='" + ddlPayBY.options[i].value + "'>" + ddlPayBY.options[i].text + "</option>";
                    }

                    //var ddlPickupOrDelivery = document.getElementById('ddlPickupOrDelivery');
                    //var DdlPickupOrDeliveryyOption = "";
                    //for (var i = 0; i < ddlPickupOrDelivery.options.length; i++) {

                    //    DdlPickupOrDeliveryyOption += "<option value='" + ddlPickupOrDelivery.options[i].value + "'>" + ddlPickupOrDelivery.options[i].text + "</option>";
                    //}
                    //var ddlOption = document.getElementById('ddlOption');
                    //var DdlOption = "";
                    //for (var i = 0; i < ddlOption.options.length; i++) {

                    //    DdlOption += "<option value='" + ddlOption.options[i].value + "'>" + ddlOption.options[i].text + "</option>";
                    //}

                    var ddlPickupOrDeliveryy = document.getElementById('ddlPickupOrDelivery');
                    var DdlPickupOrDeliveryyOption = "";
                    for (var i = 0; i < ddlPickupOrDeliveryy.options.length; i++) {

                        DdlPickupOrDeliveryyOption += "<option value='" + ddlPickupOrDeliveryy.options[i].value + "'>" + ddlPickupOrDeliveryy.options[i].text + "</option>";
                    }

                    var ddlSubmitTo = document.getElementById('ddlSubmitTo');
                    var DdlSubmitToOption = "";
                    for (var i = 0; i < ddlSubmitTo.options.length; i++) {

                        DdlSubmitToOption += "<option value='" + ddlSubmitTo.options[i].value + "'>" + ddlSubmitTo.options[i].text + "</option>";
                    }
                    debugger;
                    var str = "";
                    countItems = parseInt(countItems) + 1;
                    str += "<div class='container' style='background-color: #fdfdfd; padding: 10px 10px;'>";
                    str += "<div class='row'><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='margin-bottom=15px;'><fieldset><legend>Item #" + countItems + "</legend>";
                    str += "<div class='row'><div class='col-lg-3 selectdiv'><span id='lblBrand'>Brand</span><select name='ddlBrand" + countItems + "' id='ddlBrand" + countItems + "' class='ddlBrand'>" + DllBrandOptions + "";
                    str += "</select></div><div class='col-lg-3 selectdiv'><span id='lblProduct'>Product</span><select name='ddlProduct" + countItems + "' id='ddlProduct" + countItems + "'  class='ddlProduct'>" + DdlProductOptions + "</select></div><div class='col-lg-3'>";
                    str += "<span id='lblModel'>Model</span><input name='txtModel" + countItems + "' type='text' id='txtModel" + countItems + "' class='txt-item'></div><div class='col-lg-3 DivQuotePrice'><div class='QuotedPrice'><span class='QuotePrice' id='lblQuotedPrice'>Member Price $:</span>";
                    //str += "<input name='txtQuotedPrice" + countItems + "' type='number'  id='txtQuotedPrice" + countItems + "' class='txt-item' step='0.01'><input type='hidden' name='txtQuotedPriceHF" + countItems + "' id='txtQuotedPriceHF" + countItems + "'/> </div></div><div class='col-lg-3'><span id='lblPayBY'>Pay BY</span> <select name='ddlPayBY" + countItems + "' multiple='multiple' id='ddlPayBY" + countItems + "' data-id='" + countItems + "'   class='ddlPayBY'>" + ddlPayByOption + "</select></div>";
                    str += "<input name='txtQuotedPrice" + countItems + "'  id='txtQuotedPrice" + countItems + "' class='DirectQuotePrice txt-item' step='0.01'><input type='hidden' name='txtQuotedPriceHF" + countItems + "' id='txtQuotedPriceHF" + countItems + "'/> </div></div>";
                    str += "<div class='col-lg-3'><span id='lblPayBY'>Pay BY</span> <select name='ddlPayBY" + countItems + "' multiple='multiple' id='ddlPayBY" + countItems + "' data-id='" + countItems + "'   class='ddlPayBY'>" + ddlPayByOption + "</select></div>";


                    //str += ddlPayByOption + "</select></div><div class='col-lg-3 divPickupOrDelivery' id='divPickupOrDelivery" + countItems + "' > <span id='lblPickupOrDeliveryy'>Pickup or Delivery</span><span id='span'><select name='ddlPickupOrDeliveryy" + countItems + "' id='ddlPickupOrDeliveryy" + countItems + "' multiple='multiple'  class='ddlPickupOrDeliveryy' >";
                    str += "<div class='col-lg-3 DivStreetAddress'><span id='lblDeliveryAddress" + countItems + "'>Street Address</span><input name='txtDeliveryAddress" + countItems + "' type='text' id='txtDeliveryAddress" + countItems + "' class='txt-item' style='width: 100%;'></div>";
                    str += "<div class='col-lg-3 '><span id='lblDeliveryPostCode" + countItems + "'>Suburb and Postcode</span><input name='txtDeliveryPostCode" + countItems + "' type='text' id='txtDeliveryPostCode" + countItems + "' class='txt-item' style='width: 100%;'></div>"

                    str += "<div class='row'><div class='col-lg-3 selectdiv'><span id='lblPickupOrDelivery'>Member Price Option</span><select name='ddlPickupOrDelivery" + countItems + "' id='ddlPickupOrDelivery" + countItems + "' class='ddlPickupOrDelivery'><option value=''></option><option value='Pickup'>Pickup</option><option value='Delivery'>Delivery</option></select></div></div>";


                    //str += "<div class='col-lg-3 selectdiv divMemberPrice'> <span id='lblPickupOrDelivery'>Member Price Option</span><select name='ddlPickupOrDelivery" + countItems + "' id='ddlPickupOrDelivery" + countItems + "' data-id='" + countItems + "'  class='ddlPickupOrDelivery'>" + DdlPickupOrDeliveryyOption + "</select></span></div>";
                    str += "<div class='col-lg-3'><span id='lblDescription'>Description</span><textarea name='txtDesc" + countItems + "' rows='2' cols='20' id='txtDesc" + countItems + "' class='txt-item'></textarea></div>";
                    str += "<div class='col-lg-3'><div class='SubmitTo'> <span id='lblSubmitTo'>Submit To </span> <select name='ddlSubmitTo" + countItems + "' id='ddlSubmitTo" + countItems + "' multiple='multiple'  class='ddlSubmitTo'>" + DdlSubmitToOption + "</select></div></div>";








                    //str += "<div class='row' style='display: none;' id='divDeliveryAddress" + countItems + "'><div class='col-lg-3 SubmitTo'>&nbsp;</div><div class='col-lg-3'><span id='lblDeliveryAddress" + countItems + "'>Delivery Address</span>";
                    //str += "<input name='txtDeliveryAddress" + countItems + "' type='text' id='txtDeliveryAddress" + countItems + "' class='txt-item' style='width: 100%;'></div><div class='col-lg-3'><span id='lblOptions" + countItems + "'>Options</span><select name='ddlOption" + countItems + "' multiple='multiple' id='ddlOption" + countItems + "' data-id='" + countItems + "'  class='ddlOption'>" + DdlOption + "</select></div>";
                    //str += " <div class='col-lg-3'><input type='checkbox' name='chkDeliveryWarranty" + countItems + "' id='chkDeliveryWarranty" + countItems + "' style='margin: 0 4px;'/>Warranty<input type='text' id='txtwarrantyForDirectOrder" + countItems + "' name='txtwarrantyForDirectOrder" + countItems + "' class='txt-item' style='display:none;margin-top: 3px;'></div></div>";

                    //str += "<div class='row' style='display: none;' id='divPickup" + countItems + "'><div class='col-lg-3 SubmitTo'>&nbsp;</div><div class='col-lg-3'>";
                    //str += "<input type='checkbox' name='chkPickupWarranty" + countItems + "' id='chkPickupWarranty" + countItems + "' style='margin: 0 4px;'/>Warranty<input type='text' id='txtwarrantyForPickUpOrder" + countItems + "' name='txtwarrantyForPickupOrder" + countItems + "' class='txt-item' style='display:none;margin-top: 3px;'></div></div>";


                    //str += "<div class='row' id='divDirectOrder" + countItems + "' style='display: none;'><div class='col-lg-3'><span id='lblPComment" + countItems + "'>P" + countItems + " JBHIFI Comments</span>";
                    //str += "<input name='txtP1GGComments" + countItems + "' type='text' id='txtP1GGComments" + countItems + "' class='txt-item' style='width: 100%;'></div><div class='col-lg-3'>";
                    //str += "<span id='lblPricePickUp" + countItems + "'>P" + countItems + " JBHIFI Price Pick Up:$</span><input name='txtP1GGPickUp" + countItems + "' type='text' id='txtP1GGPickUp" + countItems + "' class='txt-item' style='width: 100%;'>";
                    //str += "</div><div class='col-lg-3'><span id='lblPriceDelivery" + countItems + "'>P" + countItems + " JBHIFI Price Delivery:$</span><input name='txtP1GGPriceDelivery" + countItems + "' type='text' id='txtP1GGPriceDelivery" + countItems + "' class='txt-item' style='width: 100%;'>";
                    //str += "</div><div class='col-lg-3'><input type='button' id='btnPlaceOrder" + countItems + "' class='btn btnPlaceOrder' onclick='PlaceOrder(" + countItems + ");' value='Place Order'/></div>";
                    //str += "</div></div>";
                    str += "<div class='row'><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right SubmitTo'><input id='btnAdd' type='button' value='ADD' class='btn btn-success btnAdd' onclick='addItem(" + countItems + "); '> <a id='btnDelete' href='javascript:void(0)' class='btn btn-danger btnDelete' onclick='DeleteDiv(this);'><i class='zmdi zmdi-delete zmdi-hc-fw'></i> Delete</a>";
                    str += "<input type='hidden' name='hfddlBrand" + countItems + "' id='hfddlBrand" + countItems + "' value=''><input type='hidden' name='hfProduct" + countItems + "' id='hfProduct" + countItems + "'></div></div>";

                    //str += "<div class='row'style='display: inline;'><div class='col-lg-3'><span id='lblDirectOrder'>Direct Order</span><span class='DO'><input id='chkGG" + countItems + "' type='checkbox' name='chkGG" + countItems + "' onclick='diplayOrderList(this);'><label for='chkGG" + countItems + "'>GG</label></span>";
                    //str += "<span class='DO'><input id='chkJBHIFI" + countItems + "' type='checkbox' name='chkJBHIFI" + countItems + "' onclick='diplayOrderList(this);'><label for='chkJBHIFI" + countItems + "'>JBHIFI</label></span></div>";

                    if ($('#hfRequestType').val() == "Email") {

                       str += "<div class='col-lg-3'>&nbsp;</div>";
                                    str += "<div class='col-lg-3'><span id='lblComments" + i + "'>" + VendorType + " Comments</span><input name='txtP"+ VendorType + "Comment" + i + "' type='text' id='txtP" + VendorType + "Comment" + i + "' class='txt-item PO1' style='width: 100%;background-color:yellow;' value='" + Comment + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblProductPrice" + i + "'>" + VendorType + " Product Price : $</span><input name='txtP" + VendorType + "PickUp" + i + "' type='text'  id='txtP" + VendorType + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:yellow;' value='" + PricePickUp + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblDeliveryPrice" + i + "'>" + VendorType + " +Delivery Price : $</span><input name='txtP" + VendorType + "PriceDelivery" + i + "' type='text' id='txtP" + VendorType + "PriceDelivery" + i + "'  class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:yellow;' value='" + PriceDelivery + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblInstallPrice" + i + "'>" + VendorType + " +Install Price : $</span><input name='txtPInst" + VendorType +  i + "' type='text' id='txtPInst" + VendorType + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:yellow;' value='" + PriceInstall + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblRemovalPrice" + i + "'>" + VendorType + " +Removal Price : $</span><input name='txtPDelInstRem" + VendorType + i + "' type='text' id='txtPDelInstRem" + VendorType + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:yellow;' value='" + PriceRemoval + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblWarrentyPrice" + i + "'>" + VendorType + " Warrenty Price : $</span><input name='txtPWarranty" + VendorType  + i + "' type='text' id='txtPWarranty" + VendorType + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:yellow;' value='" + PriceWarranty + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblWarrentyDesc" + i + "'>" + VendorType + " Warrenty Desc</span><input name='txtPWarrantyDesc" + VendorType + i + "' type='text' id='txtPWarrantyDesc" + VendorType + i + "' class='txt-item PO1 ' style='width: 100%;background-color:yellow;'  value='" + WarrantyDesc + "'></div>";
                                
                    }








                    var checked_radio = $("[id*=radFilterType] input:checked");
                    var value = checked_radio.val();
                    if (value == "Direct Order") {
                        str += '<div id="DivDO' + countItems + '" class="row" style="display:block;margin-top:15px;"></div>';
                    }
                    $('#dvAddItemsHTML').append(str);
                    $('#hfCountItems').val(countItems);
                    ddlItesm();


                    debugger;
                    var checked_radio = $("[id*=radFilterType] input:checked");
                    var value = checked_radio.val();
                    if (value == "Direct Order") {
                        $('.SubmitTo').find('.fs-wrap').removeClass("multiple");
                        $('.QuotePrice').text("Direct Order Price $");
                        $('.DivQuotePrice').hide();
                        $('.DivStreetAddress').hide();
                    } else {
                        $('.SubmitTo').find('.fs-wrap').addClass("multiple");
                        $('.DivQuotePrice').show();
                        $('.DivStreetAddress').show();
                    }
                }
            }
        }
        $(document).on("click", "[id*=chkDeliveryWarranty]", function () {
            debugger;
            if ($('#hfRequestType').val() != "CallCenter") {
                var checked_radio = $("[id*=radFilterType] input:checked");
                var value = checked_radio.val();
                if (value == "Direct Order") {
                    $(this).css('margin', '0px !important');
                    if ($(this).prop('checked')) {
                        if ($(this).parent("span").length > 0) {
                            $(this).parent("span").next("input").show();
                        } else {
                            $(this).next("input").show();
                        }
                    } else {
                        if ($(this).parent("span").length > 0) {
                            $(this).parent("span").next("input").hide();
                            $(this).parent("span").next("input").val("");
                        } else {
                            $(this).next("input").hide();
                            $(this).next("input").val("");
                        }
                    }
                } else {
                    $('[id*=txtwarrantyForDirectOrder]').hide();
                    $('[id*=txtwarrantyForDirectOrder]').val("");
                }
            }
            //$(this).parent("span").length
            //$(this).parent("span") .append("")
        });
        $(document).on("click", ".ConfirmOrder", function () {
            debugger;
            var OppKey = $(this).attr("data-OppKey");
            $('#hfConfirmOrderOppKey').val(OppKey);
            $('#btnConfirmOrder').click();
        })
        $(document).on("click", "[id*=chkPickupWarranty]", function () {
            debugger;
            if ($('#hfRequestType').val() != "CallCenter") {
                $this = $(this);
                var checked_radio = $("[id*=radFilterType] input:checked");
                var value = checked_radio.val();
                if (value == "Direct Order") {
                    $(this).css({ "margin": "0px" })
                    if ($(this).prop('checked')) {
                        if ($(this).parent("span").length > 0) {
                            $(this).parent("span").next("input").show();
                        } else {
                            $(this).next("input").show();
                        }
                    } else {
                        if ($(this).parent("span").length > 0) {
                            $(this).parent("span").next("input").hide();
                            $(this).parent("span").next("input").val("");
                        } else {
                            $(this).next("input").hide();
                            $(this).next("input").val("");
                        }
                    }
                } else {
                    $('[id*=txtwarrantyForPickUpOrder]').hide();
                    $('[id*=txtwarrantyForPickUpOrder]').val("");
                }
            }
            //$(this).parent("span").length
            //$(this).parent("span") .append("")
        });
        $(document).on("click", ".fs-option-label", function () {
            debugger;
            var checked_radio = $("[id*=radFilterType] input:checked");
            var value = checked_radio.val();
            if (value == "Direct Order") {
                if ($(this).html().trim() == "The Good Guys") {
                    debugger;
                    var IdText = $(this).closest('.fs-dropdown').next().attr("id");
                    var idNumber = IdText.replace(/[^\d.]/g, '');
                    if (idNumber == '') {
                        idNumber = 1;
                    }
                    var html = CustomPricesFiledsHTML(idNumber, 'The Good Guys');
                    if (idNumber == 1) {
                        $('#DivDO').show();
                        $('#DivDO').html("");
                        $('#DivDO').html(html);
                    } else {
                        $('#DivDO' + idNumber).show();
                        $('#DivDO' + idNumber).html("");
                        $('#DivDO' + idNumber).html(html);
                    }
                } else if ($(this).html().trim() == "JBHiFI") {
                    var IdText = $(this).closest('.fs-dropdown').next().attr("id");
                    var idNumber = IdText.replace(/[^\d.]/g, '');
                    if (idNumber == '') {
                        idNumber = 1;
                    }
                    var html = CustomPricesFiledsHTML(idNumber, 'jbhifi');
                    if (idNumber == 1) {
                        $('#DivDO').show();
                        $('#DivDO').html("");
                        $('#DivDO').html(html);
                    } else {
                        $('#DivDO' + idNumber).show();
                        $('#DivDO' + idNumber).html("");
                        $('#DivDO' + idNumber).html(html);
                    }
                }
            }
        });

        function Validation(idNumber) {

            var IsVaild = true;
            if ($('#hfRequestType').val() != "Email") {
                var Brand = $('#hfddlBrand' + idNumber).val();
                if (Brand == "" && Brand.length == 0) {
                    alertify.error("Please Select Brand");
                    IsVaild = false;
                }
                var Product = $('#hfddlProduct' + idNumber).val();
                if (Product == "" && Product.length == 0) {
                    alertify.error("Please Select Product");
                    IsVaild = false;
                }

                var Model = $('#txtModel' + idNumber).val();
                if (Model == "" && Model.length == 0) {
                    alertify.error("Required Model");
                    IsVaild = false;
                }
                var checked_radio = $("[id*=radFilterType] input:checked");
                var value = checked_radio.val();
                if (value == "Direct Enquiry") {
                    var QuotedPrice = $('#txtQuotedPrice' + idNumber).val();
                    if (QuotedPrice == "" && QuotedPrice.length == 0) {
                        alertify.error("Required Members Price");
                        IsVaild = false;
                    }
                }

                var PayBY = $('#ddlPayBY' + idNumber).val();
                if (PayBY == "" && PayBY.length == 0) {
                    alertify.error("Please Select PayBY");
                    IsVaild = false;
                }

                var checked_radio = $("[id*=radFilterType] input:checked");
                var value = checked_radio.val();
                if (value == "Direct Enquiry") {

                    var PickupOrDelivery = $('#ddlPickupOrDelivery' + idNumber).val();
                    if (PickupOrDelivery == "" && PickupOrDelivery.length == 0) {
                        //alertify.error("Please Select Pickup Or Delivery");
                        //IsVaild = false;
                    }
                    if (PickupOrDelivery == "Delivery") {
                        var deliveryAddress = $('#txtDeliveryAddress' + idNumber).val();
                        if (deliveryAddress == "" && deliveryAddress.length == 0) {
                            alertify.error("Required Delivery Address");
                            IsVaild = false;
                        }
                    }
                }

                var SubmitTo = $('#ddlSubmitTo' + idNumber).val();
                if (SubmitTo == null) {
                    alertify.error("Please Select Submit To");
                    IsVaild = false;
                }


            } else {
                debugger;



                if (idNumber == "") {
                    idNumber = "1";
                }
                var flag = false;
                var count = 0;

                $('.PlaceOrderAmount').each(function (index, value) {
                    debugger;




                    var itemNumber = $(this).attr("data-item");
                    if (idNumber == itemNumber) {
                        debugger;
                        if ($(this).val() != "" && $(this).val().length > 0) {
                            flag = true;
                            count++;
                        }
                    }
                });








                //if (count != 1 && flag) {
                //    alertify.error("Please enter only one $ value before submitting, Not Multiple");
                //    IsVaild = false;
                //}
                debugger;

                //Closed by kaushik 30.03.2019

                //if (!flag) {
                //    alertify.error("Please enter a $ value before submitting");
                //    IsVaild = false;
                //}

                //End
            }
            return IsVaild;

        }
        $(document).ready(function () {
            var ItemsNumber = '';
            $("legend").each(function (index) {
                if (!$(this).hasClass("hide")) {

                    var idNumber = $(this).text().replace(/[^\d.]/g, '');

                    ItemsNumber += idNumber + ",";
                }
            });
            $('#hfItemsNumber').val(ItemsNumber);
        });
        function ddlItesm() {
            (function ($) {
                $(function () {
                    $(".ddlBrand").combobox();
                    $(".ddlProduct").combobox();
                    $('.ddlPayBY').fSelect({
                        placeholder: '----- Select Options -----',
                        numDisplayed: 4,
                        overflowText: '{n} selected',
                        searchText: 'Options',
                        showSearch: true
                    });
                    $('.ddlOption').fSelect({
                        placeholder: '----- Select Options -----',
                        numDisplayed: 4,
                        overflowText: '{n} selected',
                        searchText: 'Options',
                        showSearch: true
                    });
                    $('.ddlSubmitTo').fSelect({
                        placeholder: '----- Select Submit To -----',
                        numDisplayed: 4,
                        overflowText: '{n} selected',
                        searchText: 'Submit To',
                        showSearch: true
                    });
                });
            })(jQuery);
        }
        function DeleteDiv(thisel) {

            thisel.closest(".container").remove();
        }

        (function ($) {

            $(function () {

                $('.ddlPayBY').fSelect({
                    placeholder: '----- Select Options -----',
                    numDisplayed: 4,
                    overflowText: '{n} selected',
                    searchText: 'Options',
                    showSearch: true
                });

                $('.ddlOption').fSelect({
                    placeholder: '----- Select Options -----',
                    numDisplayed: 4,
                    overflowText: '{n} selected',
                    searchText: 'Options',
                    showSearch: true
                });
                $('.ddlSubmitTo').fSelect({
                    placeholder: '----- Select Submit To -----',
                    numDisplayed: 4,
                    overflowText: '{n} selected',
                    searchText: 'Submit To',
                    showSearch: true
                }).trigger('change');
                if ($('#hfRequestType').val() == "Email") {

                    $(".fs-option").addClass("disabled");
                }

                $("[id*=RadVendor]").click(function () {

                    var checked_radio = $("[id*=RadVendor] input:checked");
                    var value = checked_radio.val();
                    if (value == "GG") {
                        $('#lblP1GGPickUp').text("GG Price Pick Up:$");
                        $('#lblP1GGPriceDelivery').text("GG Price Delivery:$");
                        $('#lblP1Comment').text("GG Comments");
                    }
                    return false;
                });

                $('#btnCreateEnquiry').click(function () {
                    debugger;
                    var valid = true;
                    $("legend").each(function (index) {
                        debugger;
                        if (!$(this).hasClass("hide")) {
                            if (!valid) {
                                return false;
                            }
                            var x = $(this).text();
                            var idNumber = $(this).text().replace(/[^\d.]/g, '');

                            if (idNumber == 1) {
                                valid = Validation("");
                            } else {
                                valid = Validation(idNumber);
                            }
                            if (valid && $('#hfRequestType').val() == "Vendor") {
                                debugger;
                                var Items = idNumber + "-" + $('#hfVendorName').val();
                                valid = PlaceOrder(Items)
                            }
                        }
                    });
                    if (!valid) {
                        $('#loading ').hide();
                        $('#loading ').css('display', 'none');
                        return false;
                    } else {
                        $('#loading ').show();
                        $('#loading ').css('display', 'block');
                        return true;
                    }
                });
                //$(document).on('change', '.ddlPickupOrDelivery', function () {

                //    var idNumber = $(this).attr("data-id");
                //    var value = $(this).val();
                //    if (value == "Delivery") {
                //        $('#divDeliveryAddress' + idNumber).show();
                //        $('#divPickup' + idNumber).hide();
                //    } else {
                //        $('#txtDeliveryAddress' + idNumber).val("");
                //        $('#divDeliveryAddress' + idNumber).hide();
                //        $('#divPickup' + idNumber).show();
                //    }
                //});

                $(document).on('keyup', '.PlaceOrderAmount', function (e) {
                    if (/[^0-9\.]/g.test(this.value)) {
                        // Filter non-digits from input value.
                        this.value = this.value.replace(/[^0-9\.]/g, '');
                        alertify.error("Please enter only numeric ");
                    }
                });
                $(document).on('keyup', '.DirectQuotePrice', function () {
                    debugger;
                    var $this = $(this);
                    var checked_radio = $("[id*=radFilterType] input:checked");
                    var value = checked_radio.val();
                    //if (value == "Direct Order") {
                    if (/[^0-9\.]/g.test($this.val())) {
                        // Filter non-digits from input value.
                        $this.val($this.val().replace(/[^0-9\.]/g, ''));
                        alertify.error("Please enter only numeric ");
                    }
                    //}
                });

            });
        })(jQuery);

        function AddNoteEnquiry() {
            var name = $('#lbluser').text().split('-')[0];
            $('#myModal').modal('toggle');
            $('#lblHeader').text("Add Note For " + name);
            $('#btnSaveClient').addClass("hide");
            $('#btnSaveEnquiry').removeClass("hide");

        }
        function notesection(id) {
            var divID = $(id).attr("data-id");
            if ($("#" + divID).hasClass("show")) {
                $("#" + divID).slideUp();
                $("#" + divID).removeClass("show");
                $(id).text("+ ");
            }
            else {
                $("#" + divID).slideDown();
                $("#" + divID).addClass("show");
                $(id).text("- ");
            }
        }
        function diplayOrderList(thisel) {

            if ($(thisel).prop('checked') == true) {
                var type = "";
                var id = thisel.id;
                var getId = id.replace(/[^0-9]/g, '');
                if (id.indexOf("JBHIFI") > 0) {
                    var unchkId = id.replace("JBHIFI", "GG");
                    $('#' + unchkId).prop('checked', false);
                    type = "JBHIFI";
                } else {
                    var unchkId = id.replace("GG", "JBHIFI");
                    $('#' + unchkId).prop('checked', false);
                    type = "GG";
                }

                $('#lblPComment' + getId).text(type + " Comments");
                $('#lblPricePickUp' + getId).text(type + " Price Pick Up:$");
                $('#lblPriceDelivery' + getId).text(type + " Price Delivery:$");
                $('#btnPlaceOrder' + getId).val("Direct Order " + type + "");

                $('#divDirectOrder' + getId).show();

            } else {
                var id = thisel.id;
                var getId = id.replace(/[^0-9]/g, '');
                $('#divDirectOrder' + getId).hide();

                $('#txtP' + getId + 'GGComments').val("");
                $('#txtP' + getId + 'GGPickUp').val("");
                $('#txtP' + getId + 'GGPriceDelivery').val("");
            }

        }
        function bindDropdownChkbox() {

            $('select[multiple]').multiselect({

                columns: 1,     // how many columns should be use to show options
                search: false, // include option search box
                // search filter options
                searchOptions: {
                    delay: 250,                  // time (in ms) between keystrokes until search happens
                    showOptGroups: false,                // show option group titles if no options remaining
                    searchText: true,                 // search within the text
                    searchValue: false,                // search within the value
                    onSearch: function (element) { } // fires on keyup before search on options happens
                },
                // plugin texts
                texts: {
                    placeholder: 'Select options', // text to use in dummy input
                    search: 'Search',         // search input placeholder text
                    selectedOptions: ' selected',      // selected suffix text
                    selectAll: 'Select all',     // select all text
                    unselectAll: 'Unselect all',   // unselect all text
                    noneSelected: 'None Selected'   // None selected text
                },
                selectAll: false, // add select all option
                selectGroup: false, // select entire optgroup
                showCheckbox: true,  // display the checkbox to the user
                optionAttributes: [],  // attributes to copy to the checkbox from the option element
                // Callbacks
                onLoad: function (element) { },  // fires at end of list initialization
                onOptionClick: function (element, option) { }, // fires when an option is clicked
                onControlClose: function (element) { }, // fires when the options list is closed
                onSelectAll: function (element) { },         // fires when (un)select all is clicked
                // @NOTE: these are for future development
                minSelect: false, // minimum number of items that can be selected
                maxSelect: false, // maximum number of items that can be selected
            });
            $("#ddlSubmitTo").hide();
            $("#ddlPickupOrDeliveryy").hide();


        }
        function CustomPricesFiledsHTML(item, SubmitType) {
            debugger;
            var str = "";
            var checked_radio = $("[id*=radFilterType] input:checked");
            var value = checked_radio.val();
            if (value == "Direct Enquiry") {
                if (SubmitType.toLowerCase() == "jbhifi") {
                    str = '<div class="row" id="DirectOrderJBHIFI' + item + '" style="display:inline;"><div class="col-lg-3"><span id="lblPComment' + item + '"> JBHIFI Comments</span><textarea name="txtPJBHIFIComment' + item + '" data-id="JBHIFI Comment" id="txtPJBHIFIComment' + item + '" class="txt-item PO1" style="width: 100%; background-color:Yellow"></textarea></div><div class="col-lg-3"><span id="lblPricePickUp' + item + '"> JBHIFI Price Pick Up:$</span><input type="checkbox" data-id="JBHIFI PickUp Price" id="POPrice' + item + '" name="POPriceJBHIFI' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPJBHIFIPickUp' + item + '" data-id="JBHIFI Price PickUp" type="text" id="txtPJBHIFIPickUp' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:Yellow"></div><div class="col-lg-3"><span id="lblPriceDelivery' + item + '"> JBHIFI Price Delivery:$</span><input type="checkbox" data-id="JBHIFI Delivery Price" id="POPrice' + item + '" name="POPriceJBHIFI' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPJBHIFIPriceDelivery' + item + '" type="text" data-id="JBHIFI Price Delivery" id="txtPJBHIFIPriceDelivery' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:Yellow" ></div><div class="col-lg-3"><span id="lblPrice' + item + '"> JBHIFI Del+Inst Cost:$</span><input type="checkbox" data-id="JBHIFI Del/Installation Price" id="POPrice' + item + '" name="POPriceJBHIFI' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPInstJBHIFI' + item + '" type="text" data-id="JBHIFI Inst Cost" id="txtPInstJBHIFI' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:Yellow" ></div><div class="col-lg-3"><span id="lblPrice' + item + '">JBHIFI Del+Inst+Rem:$</span><input type="checkbox" data-id="JBHIFI Del/Inst/Rem Price" id="POPrice' + item + '" name="POPriceJBHIFI' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPDelInstRemJBHIFI' + item + '" type="text" data-id="JBHIFI Removal Cost" id="txtPDelInstRemJBHIFI' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:Yellow" ></div><div class="col-lg-3"><span id="lblPrice' + item + '"> JBHIFI Del+Rem Cost:$</span><input type="checkbox" data-id="JBHIFI Del/Rem Price" id="POPrice' + item + '" name="POPriceJBHIFI' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPDelRemJBHIFI' + item + '" type="text" data-id="JBHIFI Removal Cost" id="txtPDelRemJBHIFI' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:Yellow" ></div><div class="col-lg-3"><span id="lblPrice' + item + '">JBHIFI Warranty Cost:$</span><input type="checkbox" data-id="JBHIFI Warranty Price" id="POPrice' + item + '" name="POPriceJBHIFI' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPWarrantyJBHIFI' + item + '" type="text" data-id="JBHIFI Warranty Cost" id="txtPWarrantyJBHIFI' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:Yellow" ></div><div class="col-lg-3"><span id="lblJWDesc' + item + '">JBHIFI Warranty Description:</span><input type="checkbox" data-id="JBHIFI Warranty Description" id="ChkJWDesc' + item + '" name="POPriceJBHIFI' + item + '" class="JWDesc" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPWarrantyDesc' + item + '" type="text" data-id="JBHIFI Warranty Description" id="txtPWarrantyDesc' + item + '" class="txt-item " data-item="' + item + '" style="width: 100%; background-color:Yellow"><input name="txtProductPriceJBHIFI' + item + '" type="text" data-id="JBHIFI Product Price" id="txtProductPriceJBHIFI' + item + '" class="txt-item "data-item="' + item + '" style="width: 100%;background-color:Yellow"></div></div>';
                }
                else if (SubmitType.toLowerCase() == "the good guys") {
                    str = '<div class="row" id="DirectOrderGG' + item + '" style="display:inline;"><div class="col-lg-3"><span id="lblPComment' + item + '"> GG Comments</span><textarea name="txtPGGComment' + item + '" data-id="GG Comment" id="txtPGGComment' + item + '" class="txt-item PO1" style="width: 100%; background-color:#90EE90;"></textarea></div><div class="col-lg-3"><span id="lblPricePickUp' + item + '"> GG Price Pick Up:$</span><input type="checkbox" data-id="GG PickUp Price" id="POPrice' + item + '" name="POPriceGG' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPGGPickUp' + item + '" data-id="GG Price PickUp" type="text" id="txtPGGPickUp' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;" ></div><div class="col-lg-3"><span id="lblPriceDelivery' + item + '"> GG Price Delivery:$</span><input type="checkbox" data-id="GG Delivery Price" id="POPrice' + item + '" name="POPriceGG' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPGGPriceDelivery' + item + '" type="text" data-id="GG Price Delivery" id="txtPGGPriceDelivery' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;"></div><div class="col-lg-3"><span id="lblPrice' + item + '"> GG Del+Inst Cost:$</span><input type="checkbox" data-id="GG Del/Installation Price" id="POPrice' + item + '" name="POPriceGG' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPInstGG' + item + '" type="text" data-id="GG Inst Cost" id="txtPInstGG' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;"></div><div class="col-lg-3"><span id="lblPrice' + item + '">GG Del+Inst+Rem:$</span><input type="checkbox" data-id="GG Del/Inst/Rem Price" id="POPrice' + item + '" name="POPriceGG1" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPDelInstRemGG' + item + '" type="text" data-id="GG Removal Cost" id="txtPDelInstRemGG' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;"></div><div class="col-lg-3"><span id="lblPrice' + item + '"> GG Del+Rem Cost:$</span><input type="checkbox" data-id="GG Del/Rem Price" id="POPrice' + item + '" name="POPriceGG' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPDelRemGG' + item + '" type="text" data-id="GG Removal Cost" id="txtPDelRemGG' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;" ></div><div class="col-lg-3"><span id="lblPrice' + item + '">GG Warranty Cost:$</span><input type="checkbox" data-id="GG Warranty Price" id="POPrice' + item + '" name="POPriceGG' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPWarrantyGG' + item + '" type="text" data-id="GG Warranty Cost" id="txtPWarrantyGG' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;" ></div><div class="col-lg-3"><span id="lblPrice' + item + '">GG Warranty Desc:</span><input type="checkbox" data-id="GG Warranty Desc" id="POPrices' + item + '" name="POPriceGG' + item + '" class="POPrices" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPWarrantyDescGG' + item + '" type="text" data-id="GG Warranty Desc" id="txtPWarrantyDescGG' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;" ></div></div>';
                }
            } else {
                if (SubmitType.toLowerCase() == "jbhifi") {
                    str = '<div class="row" id="DirectOrderJBHIFI' + item + '" style="display:inline;"><div class="col-lg-3"><span id="lblPricePickUp' + item + '"> JBHIFI Price Pick Up:$</span><input type="checkbox" data-id="JBHIFI PickUp Price" id="POPrice' + item + '" name="POPriceJBHIFI' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPJBHIFIPickUp' + item + '" data-id="JBHIFI Price PickUp" type="text" id="txtPJBHIFIPickUp' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:Yellow" ></div></div>';
                }
                else if (SubmitType.toLowerCase() == "the good guys") {
                    str = '<div class="row" id="DirectOrderGG' + item + '" style="display:inline;"><div class="col-lg-3"><span id="lblPricePickUp' + item + '"> GG Price Pick Up:$</span><input type="checkbox" data-id="GG PickUp Price" id="POPrice' + item + '" name="POPriceGG' + item + '" class="POPrice" style="margin: 0px 0px 0px 5px; display: none;"><input name="txtPGGPickUp' + item + '" data-id="GG Price PickUp" type="text" id="txtPGGPickUp' + item + '" class="txt-item PlaceOrderAmount PO1" data-item="' + item + '" style="width: 100%; background-color:#90EE90;" ></div></div>';
                }
            }
            return str;
        }

    </script>
    <script type="text/javascript">
        function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>



</body>
</html>
