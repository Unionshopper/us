﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnquirySheet.aspx.cs" Inherits="US.EnquirySheet" Trace="true" %>

<%@ Register Src="MenuSection.ascx" TagName="MenuSection" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Enquiry System</title>
</head>
<body>
    <form runat="server">
        <uc1:MenuSection ID="MenuSection" runat="server" />

        <section class="mide">
            <script>
                (function ($) {
                    $(window).load(function () {

                        $("#content-1").mCustomScrollbar({
                            theme: "minimal"
                        });

                    });
                })(jQuery);
            </script>
            <section class="enquiry-sheet-part">
                <div class="enquiry-sheet-heading-part">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                <div class="enquiry-number-part">
                                    <p>Enquiry Number</p>
                                    <div class="enquiry-number">
                                        01234
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                <div class="enquiry-name">
                                    AMANDAP
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 ">
                                <div class="enquiry-sheet-heading">
                                    <h3>ENQUIRY SHEET</h3>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 pull-right">
                                <div class="enquiry-complet">
                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                    <span>Completed</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="member-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    Member  <span>530617413</span>
                                </div>
                                <div class="member-content">
                                    <div class="member-content-icon">
                                        <i class="zmdi zmdi-account zmdi-hc-fw"></i>
                                    </div>
                                    <div class="member-content-details half">
                                        <h4><asp:Label id="lbluser" runat="server">LEE PALMER</asp:Label></h4>
                                        <span>10 Keperra court 
                                arana hills qld 4054</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    Member
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Home <span>(07)31720632</span></li>
                                            <li>Bus <span>()</span></li>
                                            <li>Mob <span>(0402) 435805</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 sep mem">
                            <div class="member-part">
                                <div class="member-heading">
                                    Authorised Person
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Home <span>(07)35014318</span></li>
                                            <li>Bus <span>()</span></li>
                                            <li>Mob <span>(0402) 635805</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    &nbsp;
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Union/Org <span>ASU</span></li>
                                            <li>Num</li>
                                            <li>Auth <span>Kath Reilly</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    &nbsp;
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Last Enq  <span>25/11/15</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="in-out-part">
                <div class="container">
                    <div class="row">
                        <div class="in-out-section">
                            <div class="in-out">
                                <h4>IN</h4>
                                <div class="in-out-date">
                                    05-09-2016
                                </div>

                                <div class="in-out-time">
                                    03:38:36 PM
                                </div>
                            </div>

                            <div class="in-out">
                                <h4>OUT</h4>
                                <div class="in-out-date">
                                    05-09-2016
                                </div>

                                <div class="in-out-time">
                                    03:38:36 PM
                                </div>
                            </div>

                            <div class="in-out">
                                <h4>Source</h4>
                                <div class="form-group member">
                                    <div class="label-select-member">
                                        <select class="form-control select-box-member">
                                            <option>Prev</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="key-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="key-part">
                                <div class="key-part-shortcut">
                                    <ul>
                                        <li>
                                            <button type="button"><i class="zmdi zmdi-plus zmdi-hc-fw"></i></button>
                                        </li>
                                        <li>
                                            <button type="button"><i class="zmdi zmdi-minus zmdi-hc-fw"></i></button>
                                        </li>
                                        <li>
                                            <button type="button"><i class="zmdi zmdi-border-color zmdi-hc-fw"></i></button>
                                        </li>
                                        <li>
                                            <button type="button"><i class="zmdi zmdi-close zmdi-hc-fw"></i></button>
                                        </li>
                                        <li>
                                            <button type="button"><i class="mem-text">W</i></button></li>
                                        <li>
                                            <button type="button"><i class="mem-text">s</i></button></li>
                                        <li class="member-ket-text">Astg18kmCa 5KW Reserve CYC</li>
                                    </ul>
                                </div>

                                <div class="table-secation table-responsive ">
                                    <div class="nano">
                                        <table class="table table-fixed marnone">
                                            <thead class="thead-inverse table-headings boldi">
                                                <tr>
                                                    <th class="col-xs-2">CAt</th>
                                                    <th class="col-xs-2">Brand</th>
                                                    <th class="col-xs-3">Product </th>
                                                    <th class="col-xs-5">Description  <span class="member-input">
                                                        <input type="checkbox">Don't Fax</span></th>
                                                </tr>
                                            </thead>

                                            <tbody class="hei-two mCustomScrollbar regular">
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">aaa</td>
                                                    <td class="col-xs-2">Fujitsu</td>
                                                    <td class="col-xs-3">A/CON</td>
                                                    <td class="col-xs-5">Lorem Ipsum is simply dummy text </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>

                            <div class="member-delivery">
                                <div class="col-lg-5">
                                    <div class="form-group delivery-dropi-part">
                                        <p>Delivery To {
                                            <input type="checkbox">
                                            Override? }</p>
                                        <div class="label-select-member-delivery-dropi">
                                            <select class="form-control delivery-dropi">
                                                <option>Keperra QLD 4054</option>
                                                <option>Keperra QLD 4055</option>
                                                <option>Keperra QLD 4056</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7">
                                    <div class="form-group delivery-dropi-part">
                                        <p>Payment Method</p>
                                        <div class="label-select-member-delivery-dropi flo">
                                            <select class="form-control delivery-dropi">
                                                <option>CA</option>
                                                <option>CA</option>
                                                <option>CA</option>
                                            </select>
                                        </div>
                                        <span>AND</span>
                                        <div class="label-select-member-delivery-dropi flo">
                                            <select class="form-control delivery-dropi">
                                                <option>CA</option>
                                                <option>CA</option>
                                                <option>CA</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="member-date-section">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="member-date-date">
                                            <input type="text" placeholder="date" class="member-date-input">
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group delivery-dropi-part">
                                            <div class="label-select-member-delivery-dropi">
                                                <select class="form-control delivery-dropi">
                                                    <option>Supplier</option>
                                                    <option>Supplier</option>
                                                    <option>Supplier</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group delivery-dropi-part">
                                            <div class="label-select-member-delivery-dropi">
                                                <select class="form-control delivery-dropi">
                                                    <option>Pay By</option>
                                                    <option>Pay By</option>
                                                    <option>Pay By</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="member-date-delete">
                                            <input type="checkbox">
                                            DElete
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="delivery-comments">
                                            <textarea class="delivery-comments-text" placeholder="delivery comments"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="delivery-comments">
                                            <input type="text" class="member-date-amount" placeholder="Amount">
                                            <input type="checkbox">
                                            Urgent
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <!-- key right part -->
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="key-part">
                                <div class="key-part-shortcut">
                                    <div class="price-key-part">
                                        <div class="price-key">
                                            <label>Price</label>
                                            <input type="text" class="price-key-text" placeholder="$1,469.00">
                                        </div>

                                        <div class="price-key">
                                            <label>From</label>
                                            <input type="text" class="price-key-text" placeholder="Same">
                                        </div>

                                        <div class="price-key pull-right del">
                                            <label class="radio-inline">
                                                <input type="radio" class="pu" checked>
                                                PU
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" class="pu" value="option2">
                                                DEl
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-secation table-responsive ">
                                    <div class="nano">
                                        <table class="table table-fixed marnone">
                                            <thead class="thead-inverse table-headings boldi">
                                                <tr>
                                                    <th class="col-xs-3">Supp </th>
                                                    <th class="col-xs-2">Type</th>
                                                    <th class="col-xs-2">Pu </th>
                                                    <th class="col-xs-2">DEl</th>
                                                    <th class="col-xs-1">Op</th>
                                                    <th class="col-xs-2">CO</th>
                                                </tr>
                                            </thead>

                                            <tbody class="hei-two mCustomScrollbar regular">
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1">lo</td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1"></td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1">lo</td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1"></td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1">lo</td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1"></td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1">lo</td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1"></td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1">lo</td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1"></td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1">lo</td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-3">ggscomm</td>
                                                    <td class="col-xs-2">Ca</td>
                                                    <td class="col-xs-2">$1,397.00</td>
                                                    <td class="col-xs-2">$1,452.0</td>
                                                    <td class="col-xs-1"></td>
                                                    <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                            <div class="enquiry_logo_section">
                                <div class="enquiry-key-part">
                                    <input type="text" class="enquiry-key" placeholder="Knudsem’s Betta Home Living (07) 3865 2071 Virginia Qld">
                                </div>

                                <div class="delivery-comments">
                                    <textarea class="delivery-comments-text" placeholder="Supplier Comments"></textarea>
                                </div>

                                <div class="enquiry-log-btn-section">
                                    <button type="button"><i class="fa fa-calendar" aria-hidden="true"></i>Enquiry Log</button>
                                    <button type="button"><i class="zmdi zmdi-eye zmdi-hc-fw"></i>Viewing history</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="file-info-secton">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="file-info-part">
                                <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                                    <div class="file-comments-section">
                                        <div class="file-comment-heading">
                                            File Comments
                                        </div>
                                        <div class="delivery-comments">
                                            <textarea class="delivery-comments-text two">For Lee's Parents del to: 27 Mungarie St Keperra 25/8/16am / Emailed JB ETA STK
                                    </textarea>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                                    <div class="file-comments-section">
                                        <div class="file-comment-heading">
                                            Commitled
                                    
                                    <div class="price-key pull-right dile-radio">
                                        <label class="radio-inline">
                                            <input class="pu" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked="" type="radio">
                                            Y
                                        </label>
                                        <label class="radio-inline">
                                            <input class="pu" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
                                            N
                                        </label>
                                        <label class="radio-inline">
                                            <input class="pu" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
                                            M
                                        </label>
                                    </div>
                                        </div>

                                        <div class="form-group file-dropi-part">
                                            <label>Contact</label>
                                            <div class="label-select-member-file-dropi">
                                                <select class="form-control file-dropi">
                                                    <option>Lee</option>
                                                    <option>Lee</option>
                                                    <option>Lee</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group file-dropi-part">
                                            <label>Phone</label>
                                            <div class="price-key dile-radio two">
                                                <label class="radio-inline">
                                                    <input class="pu" checked="" type="checkbox">
                                                    H
                                                </label>
                                                <label class="radio-inline">
                                                    <input class="pu" type="checkbox" checked="">
                                                    M
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                    <div class="file-comments-section">
                                        <div class="file-comment-heading">
                                            Callback Comment
                                        </div>

                                        <div class="delivery-comments two">
                                            <input class="member-date-amount" placeholder="Amount" type="text">
                                            <input type="checkbox">
                                            Urgent
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
                                    <div class="key-part">

                                        <div class="table-secation table-responsive ">
                                            <div class="nano">
                                                <table class="table table-fixed marnone">
                                                    <thead class="thead-inverse table-headings boldi-section">
                                                        <tr>
                                                            <th class="col-xs-3">Contact Status</th>
                                                            <th class="col-xs-3">Consultant</th>
                                                            <th class="col-xs-2">Status </th>
                                                            <th class="col-xs-2">Ph.</th>
                                                            <th class="col-xs-2">Note</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody class="hei-file mCustomScrollbar regular-mini">
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3">08-09-2016 1:20:55</td>
                                                            <td class="col-xs-3">Lesley</td>
                                                            <td class="col-xs-2">INQ</td>
                                                            <td class="col-xs-2">b</td>
                                                            <td class="col-xs-2"><i class="zmdi zmdi-border-color zmdi-hc-fw key-edit"></i></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="enquiry-sheet-bottom-part">
                <div class="container">
                    <div class="row">
                        <div class="enquiry-sheet-bottom">
                            <div class="col-lg-1">
                                <button class="remove-btn" type="button">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>

                            <div class="col-lg-2">
                                <div class="enquiry-sheet-bottom-name">
                                    Leepal
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="enquiry-sheet-bottom-time">
                                    02 September, 2016 <span>2:51 PM</span>
                                </div>
                            </div>

                            <div class="col-lg-5">
                                <div class="enquiry-sheet-bottom-btn-area">
                                    <button type="button" class="enquiry-sheet-bottom-btn"><i class="zmdi zmdi-mail-send zmdi-hc-fw"></i>Send Order</button>
                                    <button type="button" class="enquiry-sheet-bottom-btn"><i class="zmdi zmdi-refresh-sync-alert zmdi-hc-fw"></i>Process</button>
                                    <button type="button" class="enquiry-sheet-bottom-btn"><i class="zmdi zmdi-check zmdi-hc-fw"></i>Complete</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </section>
    </form>
</body>
</html>
