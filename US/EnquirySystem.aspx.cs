﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace US
{
    public partial class EnquirySystem : System.Web.UI.Page
    {
        Maximizer.Data.AddressBookMaster oAb = new Maximizer.Data.AddressBookMaster();
        Maximizer.Data.AbEntryAccess oAbEntryAccess = null;
        Maximizer.Data.GlobalAccess ga = new Maximizer.Data.GlobalAccess();
        Maximizer.Data.AbEntryList oAbEntryList = null;
        Maximizer.Data.UdfAccess oUDFAccess = null;
        Maximizer.Data.NoteAccess oNoteAccess = null;
        Maximizer.Data.TaskAccess oTaskAccess = null;
        Maximizer.Data.UserAccess oUserAccess = null;
        Maximizer.Data.OpportunityAccess oOppAccess = null;
        Maximizer.Data.DetailFieldAccess oDetailFieldAccess = null;
        Maximizer.Data.AddressAccess oAddressAccess = null;
        Maximizer.Data.CSCaseAccess oCSCaseAccess = null;
        string sLoginString = "";
        string CurrentKey = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            sLoginString = loginToMax();

            #region Set Case Create Date
            string UDFCreatedDateKey = GetUdfKeyOpp("Created Date", sLoginString, oAb);
            Opportunity opp = new Opportunity();
            OpportunityList oOpportunityList = oOppAccess.ReadList();
            foreach (Opportunity oOpportunityItems in oOpportunityList)
            {
                opp = oOpportunityItems;
                try
                {
                    string createDate = opp.CreationDate.Value;

                    createDate = Convert.ToDateTime(createDate).ToString("yyyy-MM-dd");
                    oUDFAccess.SetFieldValue(UDFCreatedDateKey, opp.Key, createDate);

                }
                catch (Exception ex)
                {

                }
            }
            #endregion

            //for (int i = 1; i <= 10; i++)
            //{
            //    //createUDF("Item Price Tick Marked JBHIFI"+i);
            //    //createUDF("Item Price Tick Marked GG" + i);
            //    //<Vendor> Ordered Item Status <item number>
            //    //createUDF("JBHIFI Ordered Item Status "+i);
            //    //createUDF("Ordered Item Status " + i);
            //    //DeleteUDF("GG Ordered Item Status " + i);
            //    //DeleteUDF("JBHIFI Ordered Item Status " + i);
            //    //DeleteUDF("Submit To");
            //    //DeleteUDF("Pay By");
            //    //DeleteUDF("Pickup or Delivery");

            //    //createUDF("Brand");
            //    //createUDF("Product");
            //    //createUDF("Submit To");
            //    //createUDF("Pay By");
            //    //createUDF("Pickup or Delivery");

            //    //FillDropDownWithCSCaseUDF("Brand");
            //    //FillDropDownWithCSCaseUDF("Product");
            //    //FillDropDownWithCSCaseUDF("Submit To");
            //    //FillDropDownWithCSCaseUDF("Pay By");
            //    //FillDropDownWithCSCaseUDF("Pickup or Delivery");

            //    //createUDF("Quoted Price");
            //    //createUDF("Product Price Pick Up");
            //    //createUDF("Product Price Delivery");

            //    //createUDF("Model");
            //    //createUDF("Description");
            //    //createUDF("Delivery Address");
            //    //createUDF("Product Comment");			
            //    //DeleteUDF("P" + i + " Price Delivery");
            //    //DeleteUDF("P" + i + " Price Pick Up");

            //    //createUDF("Options" + i);



            //    //DeleteUDF("P" + i + " GG Removal Cost");
            //    //DeleteUDF("P" + i + " GG Inst Cost");
            //    //P5 GG Price Install
            //    //DeleteUDF("P" + i + " Price Removal");
            //    //DeleteUDF("P" + i + " Price Install");
            //    //DeleteUDF("P" + i + " Warranty Desc");
            //    //DeleteUDF("P" + i + " Price Pick Up");
            //    //DeleteUDF("P" + i + " Price Delivery");
            //    //DeleteUDF("P" + i + " Price Warranty");

            //    //createUDF("P" + i + " GG Price Removal");
            //    //createUDF("P" + i + " GG Price Install");
            //    //createUDF("P" + i + " GG Warranty Desc");
            //    //createUDF("P" + i + " GG Price Pick Up");
            //    //createUDF("P" + i + " GG Price Delivery");
            //    //createUDF("P" + i + " GG Price Warranty");

            //    //createUDF("P" + i + " JBHIFI Price Removal");
            //    //createUDF("P" + i + " JBHIFI Price Install");
            //    //createUDF("P" + i + " JBHIFI Warranty Desc");
            //    //createUDF("P" + i + " JBHIFI Price Pick Up");
            //    //createUDF("P" + i + " JBHIFI Price Delivery");
            //    //createUDF("P" + i + " JBHIFI Price Warranty");




            //    //if (i != 1)
            //    //{
            //    //DeleteUDF("GG Del+Inst+Removal Cost");
            //    //createUDF("GG Del+Inst Cost");
            //    //}

            //    //createUDF("JBHIFI Del+Inst+Removal Cost");
            //    //createUDF("JBHIFI Del+Inst Cost");

            //    //createUDF("P" + i + " GG Comment");
            //    //createUDF("P" + i + " JBHIFI Comment");

            //    //createUDF("P" + i + " Ordered From");  

            //    //DeleteUDF("P" + i + " GG Warrenty Cost");
            //    //DeleteUDF("P" + i + " JBHIFI Warrenty Cost");

            //    //createUDF("P" + i + " GG Warranty Cost");
            //    //createUDF("P" + i + " JBHIFI Warranty Cost");
            //    //createUDF("P" + i + " GG Del+Inst+Rem+Warranty Cost");
            //    //createUDF("P" + i + " GG Del+Inst+Warranty Cost");
            //    //createUDF("P" + i + " GG Del+Rem+Warranty Cost");


            //    //createUDF("P" + i + " JBHIFI Del+Inst+Rem+Warranty Cost");
            //    //createUDF("P" + i + " JBHIFI Del+Inst+Warranty Cost");
            //    //createUDF("P" + i + " JBHIFI Del+Rem+Warranty Cost");

            //    //createUDF("P" + i + " GG Price Pick Up+Warranty");
            //    //createUDF("P" + i + " JBHIFI Price Pick Up+Warranty");

            //    //createUDF("P" + i + " Warranty");

            //    //DeleteUDF("Delivery Postcode" + i);
            //    //createUDF("Suburb and Postcode" + i);
            //}
        }
        public void createUDF(string DestinationUDFName)
        {
            sLoginString = loginToMax();
            try
            {
                string DestinationFolderName = "Enquiry";

                //UdfSetupNumeric udfStpT = new UdfSetupNumeric(Maximizer.Data.UdfSetupFor.CSCase);
                //udfStpT.Name.Value = DestinationUDFName;
                //udfStpT.Individual.Value = true;
                //udfStpT.Company.Value = true;
                //udfStpT.Contact.Value = true;
                //udfStpT.DecimalPlaces.Value = 2;
                //udfStpT.DisplayCurrencySymbol.Value = true;

                //UdfSetupAlphanumeric udfStpT = new UdfSetupAlphanumeric(Maximizer.Data.UdfSetupFor.CSCase);
                //udfStpT.Name.Value = DestinationUDFName;
                //udfStpT.MaximumCharacters.Value = 255;

                UdfSetupTable udfStpT = new UdfSetupTable(Maximizer.Data.UdfSetupFor.CSCase);
                udfStpT.Name.Value = DestinationUDFName;

                //UdfSetupYesNoTable udfStpT = new UdfSetupYesNoTable(Maximizer.Data.UdfSetupFor.CSCase);
                //udfStpT.Name.Value = DestinationUDFName;

                UdfSetupFolder UF = GetUDFFolderSetup(DestinationFolderName);
                if (UF != null)
                {
                    udfStpT.ParentFolder.Value = UF.Key;
                }

                udfStpT.Owner.Value = oAbEntryAccess.GetLoggedInUserKey();

                oUDFAccess.UdfSetupInsert(udfStpT);

                sLoginString = loginToMax();
                string udfKey = GetUdfCSCaseKey(DestinationUDFName, sLoginString, oAb);
                AddTableUDFItems(udfKey, "Product Price");
                AddTableUDFItems(udfKey, "Delivery Price");
                AddTableUDFItems(udfKey, "Install Price");
                AddTableUDFItems(udfKey, "Removal Price");
                AddTableUDFItems(udfKey, "Warranty Price");
            }
            catch (Exception ex)
            {

            }


        }
        public UdfSetupFolder GetUDFFolderSetup(string FolderName)
        {
            string folderKey = "";
            try
            {
                UdfSetupList setupList = oUDFAccess.ReadCSCaseUdfSetupList();
                foreach (UdfSetup setup in setupList)
                {
                    if (setup is UdfSetupFolder)
                    {
                        //you may want to check the folder path here also
                        if (setup.Name.Value == FolderName)
                        {
                            return (UdfSetupFolder)setup;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public void DeleteUDF(string UDFName)
        {
            sLoginString = loginToMax();
            try
            {
                UdfSetupList setupList = oUDFAccess.ReadCSCaseUdfSetupList();
                // UdfSetupList setupList = oUDFAccess.ReadOpportunityUdfSetupList();
                var oUDFList = setupList.Cast<UdfSetup>().Where(x => x.Name.Value == UDFName);
                foreach (UdfSetup setup in oUDFList)
                {
                    oUDFAccess.UdfSetupDelete(setup.Key);
                }


                //AddTableUDFItems(oAb1, sLoginstring1, "Fee Configure Type", DestinationUDFName);
            }
            catch (Exception ex)
            {

            }


        }
        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                {
                    string appSetting = ConfigurationManager.AppSettings["Database"];
                    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                    {
                        sAddressBookKey = addressBook.Key;
                        break;
                    }
                }
                string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                string appSetting2 = ConfigurationManager.AppSettings["Password"];
                this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }

        public string GetUdfCSCaseKey(string strUdfName, string strLoginString, AddressBookMaster abMaster)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)abMaster.CreateUdfAccess(strLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }

        private int FillDropDownWithCSCaseUDF(string UDFName)
        {
            try
            {
                sLoginString = loginToMax();
                string UDFKey = GetUdfKeyOpp(UDFName, sLoginString, oAb);
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadCSCaseUdfSetupList("KEY(" + GetUdfCSCaseKey(UDFName + "1", sLoginString, oAb) + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                        AddTableUDFItems(UDFKey, udfSetupTableItem.Name.Value);
                }
                return -1;
            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);
                return -1;
            }
        }
        public string GetUdfKey(string strUdfName, string strLoginString, Maximizer.Data.AddressBookMaster abMaster)
        {
            Maximizer.Data.UdfAccess udfAccess =
            abMaster.CreateUdfAccess(strLoginString);
            Maximizer.Data.UdfDefinitionList udfDefList =
            udfAccess.ReadUdfDefinitionList(new Maximizer.Data.AbEntry().Key);
            var oList = udfDefList.Cast<Maximizer.Data.UdfDefinition>().Where(x => x.FieldName.Equals(strUdfName));
            foreach (Maximizer.Data.UdfDefinition udfDef in oList)
            {
                return udfDef.Key;
            }
            return String.Empty;

        }
        public string GetUdfKeyOpp(string strUdfName, string strLoginString, Maximizer.Data.AddressBookMaster abMaster)
        {
            Maximizer.Data.UdfAccess udfAccess =
            abMaster.CreateUdfAccess(strLoginString);
            Maximizer.Data.UdfDefinitionList udfDefList =
            udfAccess.ReadUdfDefinitionList(new Maximizer.Data.Opportunity().Key);
            var oList = udfDefList.Cast<Maximizer.Data.UdfDefinition>().Where(x => x.FieldName.Equals(strUdfName));
            foreach (Maximizer.Data.UdfDefinition udfDef in oList)
            {
                return udfDef.Key;
            }
            return String.Empty;

        }
        private int AddTableUDFItems(string UDFKey, string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    Maximizer.Data.UdfSetupList setupList;

                    setupList = oUDFAccess.ReadCSCaseUdfSetupList("KEY(" + UDFKey + ")", true);

                    foreach (Maximizer.Data.UdfSetupTable tbl in setupList)
                    {
                        if (!tbl.Items.Cast<Maximizer.Data.UdfSetupTableItem>().Where(x => x.Name.Value.ToLower() == value.ToLower()).Any())
                        {
                            //foreach (Maximizer.Data.UdfSetupTableItem item in tbl.Items)
                            //{
                            Maximizer.Data.UdfSetupTableItem newItemIns = new Maximizer.Data.UdfSetupTableItem();
                            // newItemIns = item;
                            newItemIns.Name.Value = value;
                            oUDFAccess.UdfSetupTableItemInsert(UDFKey, ref newItemIns);// inset Item

                            //oUDFAccess.UdfSetupTableItemUpdate(udfCSCaseKey, ref newItemIns);//UPdate Items
                            //}
                        }
                    }
                }
                return -1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}