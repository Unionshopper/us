using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Telerik.Web.UI;
using US.Comomn;

namespace US
{

	public partial class LoadEnquiries : System.Web.UI.Page
	{
		AddressBookMaster oAb = new AddressBookMaster();
		AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
		GlobalAccess ga = new GlobalAccess();
		AbEntryList oAbEntryList = (AbEntryList)null;
		UdfAccess oUDFAccess = (UdfAccess)null;
		NoteAccess oNoteAccess = (NoteAccess)null;
		UserAccess oUserAccess = (UserAccess)null;
		TaskAccess oTaskAccess = (TaskAccess)null;
		OpportunityAccess oOppAccess = (OpportunityAccess)null;
		DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
		SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
		CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
		connection connection = new connection();
		string sLoginString = "";
        string CurrentKey = "";

        XDocument xAB = null; XDocument xCsCase = null; XDocument xOpportunity = null;
        protected void Page_Load(object sender, EventArgs e)
		{
            string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");
            xCsCase = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            xOpportunity = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");
            xAB = XDocument.Load(path);

            this.sLoginString = this.loginToMax();
			if (this.IsPostBack)
				return;
            if(!string.IsNullOrEmpty(Request.QueryString["ClientID"]))
            {
                Bind(Request.QueryString["ClientID"]);
            }
			this.oAbEntryList = this.oAbEntryAccess.ReadList("EQ(ClientID," + Request.QueryString["ClientID"] + ")");
			foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
			{
				this.lbluser.Text = oAbEntry.FirstName.Value + " " + oAbEntry.LastName.Value;
				this.lblAddress.Text = oAbEntry.CurrentAddress.AddressLine1.Value + " " + oAbEntry.CurrentAddress.City.Value + " " + oAbEntry.CurrentAddress.StateProvince.Value + " " + oAbEntry.CurrentAddress.ZipCode.Value;
                this.lblHomeNumber.Text = oAbEntry.Phone1.Value;
                this.lblEmailAddress.Text = oAbEntry.EmailAddress1.Value;
                //this.lblBusNumber.Text = oAbEntry.Phone3.Value;
                string fieldValue1 = this.oUDFAccess.GetFieldValue(GetUdfKey("Member ID"), oAbEntry.Key);
				string fieldValue2 = this.oUDFAccess.GetFieldValue(GetUdfKey("Union ID"), oAbEntry.Key);
                this.lblMember.Text = fieldValue2;
                //this.lblUnion.Text = fieldValue2;
            }
			
		}
        
		private string loginToMax()
		{
			AddressBookList addressBookList = this.ga.ReadAddressBookList();
			string sAddressBookKey = "";
			this.sLoginString = this.Request.QueryString["Loginstring"];
			if (this.sLoginString == "" || this.sLoginString == null)
			{
				foreach (AddressBook addressBook in (CollectionBase)addressBookList)
				{
					string appSetting = ConfigurationManager.AppSettings["Database"];
					if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
					{
						sAddressBookKey = addressBook.Key;
						break;
					}
				}
				string appSetting1 = ConfigurationManager.AppSettings["UserName"];
				string appSetting2 = ConfigurationManager.AppSettings["Password"];
				this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
			}
			this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
			this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
			this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
			this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
			this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
			this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
			this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
			this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
			this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
			return this.sLoginString;
		}

        public string GetUdfKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xAB.Element("ab").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfOppKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            //var path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            //xOpportunity = XDocument.Load(path);
            var sFilterItems = xOpportunity.Element("Opp").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        public void Bind(string ClientID,int FilterCountRecords=50)
        {
            try
            {
                sLoginString = loginToMax();
                DataTable dt = new DataTable();
                dt.Columns.Add("Case Number");
                dt.Columns.Add("Date");
                dt.Columns.Add("Status");
                dt.Columns.Add("Action");

                string sFilterKey = "EQ(ClientID," + ClientID + ")";
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(sFilterKey, FilterCountRecords);
                foreach (CSCase oCSCase in oCSCaseList)
                {
                    var URL = "CustomDialogs/LoadOrders.aspx?CaseNumber=" + oCSCase.CaseNumber.Value;
                    dt.Rows.Add(oCSCase.CaseNumber.Value, oCSCase.CreationDate.DateValue, oCSCase.StatusName.Value, URL);
                }
                gvLoadEnquiry.DataSource = dt;
                gvLoadEnquiry.DataBind();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void ddlFilterCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]))
                {
                    Bind(Request.QueryString["ClientID"],Convert.ToInt32(ddlFilterCount.SelectedValue));
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void btnSearchRecords_Click(object sender, EventArgs e)
        {
            try
            {
                if(!string.IsNullOrEmpty(txtFromDate.Text) && !string.IsNullOrEmpty(txtToDate.Text))
                {
                    string fromDate = MaximizerDate(txtFromDate.Text);
                    string toDate = MaximizerDate(txtToDate.Text);

                    string UDFCreatedDateKey = GetUdfCSCaseKey("Created Date");
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                 var   strFilterKey = "AND(EQ(ClientID, " + Request.QueryString["ClientID"] + "), " + DateFilter + ")";
                    sLoginString = loginToMax();
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Case Number");
                    dt.Columns.Add("Date");
                    dt.Columns.Add("Status");
                    dt.Columns.Add("Action");

                    
                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList(strFilterKey, Convert.ToInt32(ddlFilterCount.SelectedValue));
                    foreach (CSCase oCSCase in oCSCaseList)
                    {
                        var URL = "/CustomDialogs/LoadOrders.aspx?CaseNumber=" + oCSCase.CaseNumber.Value;
                        dt.Rows.Add(oCSCase.CaseNumber.Value, oCSCase.CreationDate.DateValue, oCSCase.StatusName.Value, URL);
                    }
                    gvLoadEnquiry.DataSource = dt;
                    gvLoadEnquiry.DataBind();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public string MaximizerDate(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                string[] datesplit = date.Split('-');
                string[] yearsplit = datesplit[2].Split(' ');
                string newdate = yearsplit[0] + "-" + datesplit[1] + "-" + datesplit[0];
                return Convert.ToDateTime(newdate).ToString("yyyy-MM-dd");
            }
            return "";
        }
    }
}
