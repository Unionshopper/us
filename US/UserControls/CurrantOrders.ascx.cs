﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using US.Comomn;

namespace US.UserControls
{
    public partial class CurrantOrders : System.Web.UI.UserControl
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        string sLoginString = "";
        XDocument xAB = null; XDocument xCsCase = null; XDocument xOpportunity = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");
            xCsCase = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            xOpportunity = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");
            xAB = XDocument.Load(path);
            BindGrid();
        }
        public void BindGrid()
        {
            sLoginString = this.loginToMax();
            DataTable dt = new DataTable();
            dt = BindCurrentOrderGrid(sLoginString, Request.QueryString["Vendor"], Request.QueryString["RequestType"]);
            if (dt.Rows.Count <= 0)
            {
                hfResult.Value = "No Records Found";
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            else
            {
                DataView dv = dt.DefaultView;
                dv.Sort = "CaseNo desc";
                dt = dv.ToTable();
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();

        }
        public void BindGridOLD()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CaseNo");
            dt.Columns.Add("DateFormat");
            dt.Columns.Add("Time");
            dt.Columns.Add("Assigned");
            dt.Columns.Add("Status");
            dt.Columns.Add("Desc");
            dt.Columns.Add("Description1");
            dt.Columns.Add("EnquiryStatus");
            dt.Columns.Add("Revenue");
            dt.Columns.Add("URL");

            string VendorShort = "";
            var vendorItemNumber = "";
            string Vendor = Request.QueryString["Vendor"];
            if (Vendor == "GG")
            {
                VendorShort = "GG";
                vendorItemNumber = "2";
                Vendor = "The Good Guys";
            }
            else
            {
                VendorShort = "JBHIFI";
                vendorItemNumber = "1";
                Vendor = "JBHiFI";
            }

            string RequestType = Request.QueryString["RequestType"];
            if (!string.IsNullOrEmpty(Vendor))
            {
                this.sLoginString = this.loginToMax();


                string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                var Pending = GetCSCaseStausKey("Pending");
                var Complete = GetCSCaseStausKey("Complete");
                var CallBack = GetCSCaseStausKey("Call Back");
                string searchkey = "OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + CallBack + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' ))";

                var fromDate = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                var toDate = DateTime.Now.ToString("yyyy-MM-dd");
                string UDFCreatedDateKey = GetUdfCSCaseKey("Created Date");

                string strFilterKey = "";
                string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";

                List<Maximizer.Data.CSCase> oList = new List<Maximizer.Data.CSCase>();
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(strFilterKey);
                if (oCSCaseList != null)
                {
                    oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().OrderByDescending(x => x.CaseNumber.Value).ToList();

                    if (oCSCaseList.Count >= 500)
                    {
                       // oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().OrderByDescending(x => x.CaseNumber.Value).Take(500).ToList();
                    }
                }
                string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                foreach (CSCase oCsCase in oList)
                {
                    var cancel = GetCSCaseStausKey("Cancelled");
                    if (oCsCase.Status.Value == cancel)//4
                        continue;


                    if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(CallBack))
                    {
                        var hide = "";
                        var more = "<a href='#' class='aMore'>...more</a>";
                        var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                        int j = 0;
                        int i = 1;


                        if (i != 1)
                        {
                            hide = "style='display:none;'";
                        }


                        i++;

                        string userName = "";
                        if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                        {
                            UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                            foreach (User oUser in oUserList)
                            {
                                userName = oUser.DisplayName;
                            }
                        }

                        string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                        string CaseNumber = oCsCase.CaseNumber.Value;
                        double RevenuValue = 0;
                        string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");
                        var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                        //0Status for New and 1 for Updated
                        Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(OR(EQ(Status,0),EQ(Status,1),EQ(Status,2)),EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "'))");
                        var items = "";
                        var OppKeystring = "";
                        string status = "PENDING";


                        foreach (Maximizer.Data.Opportunity opp in OppList)
                        {
                            //
                            //if(oCsCase.CaseNumber.Value== "US-01581")
                            // {

                            // }
                            if (opp.Status.Value == 1 || opp.Status.Value == 2)
                                status = "UPDATED";
                            else
                                status = "PENDING";

                            string Description = "";
                            string Price = "";
                            var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                            if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                            {
                                var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                items = itemNumber;
                                OppKeystring = opp.Key + "-" + itemNumber;
                                RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                string UDFBrandKey = GetUdfOppKey("Brand Name");
                                string UDFProductKey = GetUdfOppKey("Product Name");
                                string UDFModelKey = GetUdfOppKey("Model");
                                string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                string DeliveryAddressKey = GetUdfOppKey("Delivery Address");

                                var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                if (VendorShort == "JBHIFI")
                                {
                                    EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                }
                                else if (VendorShort == "GG")
                                {
                                    EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                }
                                if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                {
                                    j++;
                                    if (string.IsNullOrEmpty(Description))
                                    {
                                        hide = "";
                                    }
                                    Description += "<span class='more' " + hide + ">";
                                    Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress;
                                    Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                    Description += "</span></br>";

                                    string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                    string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                    string UDFInstKey = GetUdfOppKey("+Inst");
                                    string UDFRemKey = GetUdfOppKey("+Rem");

                                    var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                    var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                    var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                    var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);
                                    Price += "<span class='morePrice' " + hide + ">";
                                    Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                                    Price += "</span></br>";
                                }

                                var suffix = "&Vendor=" + Request.QueryString["Vendor"] + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CURRORD";
                                var URL = "CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + Request.QueryString["Loginstring"] + suffix;


                                string EnquiryStatus = oCsCase.Status.Value.ToString();
                                //string Description = opp.Comment.Value;
                                //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                //    Description = Description.Substring(0, 50) + ".....";

                                //if (OppList.Count>1)
                                //{
                                //    Description += more;
                                //    Price += morePrice;
                                //}
                                dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(opp.CreationDate.DateValue).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);
                            }
                        }





                    }

                }

            }
            if (dt.Rows.Count <= 0)
            {
                hfResult.Value = "No Records Found";
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            else
            {
                DataView dv = dt.DefaultView;
                dv.Sort = "CaseNo desc";
                dt = dv.ToTable();
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        public string GetUdfOppKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xOpportunity.Element("Opp").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                //foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                //{
                //    string appSetting = ConfigurationManager.AppSettings["Database"];
                //    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                //    {
                //        sAddressBookKey = addressBook.Key;
                //        break;
                //    }
                //}
                //string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                //string appSetting2 = ConfigurationManager.AppSettings["Password"];
                //this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
                this.sLoginString = ConfigurationManager.AppSettings["VENJBHIFILoginKey"];
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }

        public string GetUdfKey(string strUdfName, string strLoginString, AddressBookMaster abMaster)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)abMaster.CreateUdfAccess(strLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xAB.Element("ab").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string caseNo = e.CommandArgument.ToString();
                    //CSCaseList ocSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + caseNo + ")");

                    Response.Redirect("CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&CaseNo=" + caseNo + "&RequestType=CallCenter");
                }
            }
            catch (Exception ex)
            {

            }
        }
        public int GetCSCaseStausKey(string Status)
        {
            int Key = 0;
            try
            {
                //sLoginString = loginToMax();
                var oCSCaseStatusList = oCSCaseAccess.ReadStatusOptionListForAssign();
                foreach (Maximizer.Data.Option item in oCSCaseStatusList)
                {
                    if (item.DisplayValue.ToLower() == Status.ToLower())
                    {
                        return Convert.ToInt32(item.FieldValue);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return Key;
        }
        public DataTable BindCurrentOrderGrid(string sLoginString, string VendorType, string RequestType)
        {
                DataTable dt = new DataTable();
                try
                {
                    string VendorShort = "";
                    var vendorItemNumber = "";
                    string Vendor = VendorType;
                    if (Vendor == "GG")
                    {
                        VendorShort = "GG";
                        vendorItemNumber = "2";
                        Vendor = "The Good Guys";
                    }
                    else
                    {
                        VendorShort = "JBHIFI";
                        vendorItemNumber = "1";
                        Vendor = "JBHiFI";
                    }


                    if (!string.IsNullOrEmpty(Vendor))
                    {
                        sLoginString = loginToMax();

                        dt.Columns.Add("CaseNo");
                        dt.Columns.Add("DateFormat");
                        dt.Columns.Add("Time");
                        dt.Columns.Add("Assigned");
                        dt.Columns.Add("Status");
                        dt.Columns.Add("Desc");
                        dt.Columns.Add("Description1");
                        dt.Columns.Add("EnquiryStatus");
                        dt.Columns.Add("Revenue");
                        dt.Columns.Add("URL");

                        string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                        string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                        string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                        string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                        string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                        string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                        string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                        string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                        string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                        string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                        string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");

                        var Pending = GetCSCaseStausKey("Pending");
                        var Complete = GetCSCaseStausKey("Complete");
                        var CallBack = GetCSCaseStausKey("Call Back");

                        string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");

                        double RevenuValue = 0;

                        string UDFVendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");
                    var fromDate = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                    var toDate = DateTime.Now.ToString("yyyy-MM-dd");

                    string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                    //strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";

                    //0Status for New and 1 for Updated
                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(" + DateFilter + ",OR(EQ(Status,0),EQ(Status,1),EQ(Status,2)))");
                        var items = "";
                        var OppKeystring = "";
                        string status = "PENDING";


                        foreach (Maximizer.Data.Opportunity opp in OppList)
                        {
                            var CaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);

                            string CSsearchkey = "AND( EQ(CaseNumber, " + CaseNumber + "),OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + CallBack + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";
                            CSCaseList oCSCaseList = oCSCaseAccess.ReadList(CSsearchkey);
                            string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                            string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                            foreach (CSCase oCsCase in oCSCaseList)
                            {
                                var cancel = GetCSCaseStausKey("Cancelled");
                                if (oCsCase.Status.Value == cancel)//4
                                    continue;


                                if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(CallBack))
                                {

                                    var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);

                                    var hide = "";
                                    var more = "<a href='#' class='aMore'>...more</a>";
                                    var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                                    int j = 0;
                                    int i = 1;


                                    if (i != 1)
                                    {
                                        hide = "style='display:none;'";
                                    }


                                    i++;

                                    string userName = "";
                                    if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                                    {
                                        UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                                        foreach (User oUser in oUserList)
                                        {
                                            userName = oUser.DisplayName;
                                        }
                                    }


                                    //
                                    //if(oCsCase.CaseNumber.Value== "US-01581")
                                    // {

                                    // }
                                    if (opp.Status.Value == 1 || opp.Status.Value == 2)
                                        status = "UPDATED";
                                    else
                                        status = "PENDING";

                                    string Description = "";
                                    string Price = "";
                                    var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                                    if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                                    {
                                        var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                        items = itemNumber;
                                        OppKeystring = opp.Key + "-" + itemNumber;
                                        RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                        string UDFBrandKey = GetUdfOppKey("Brand Name");
                                        string UDFProductKey = GetUdfOppKey("Product Name");
                                        string UDFModelKey = GetUdfOppKey("Model");
                                        string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                        string DeliveryAddressKey = GetUdfOppKey("Delivery Address");

                                        var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                        var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                        var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                        var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                        var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                        if (VendorShort == "JBHIFI")
                                        {
                                            EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                        }
                                        else if (VendorShort == "GG")
                                        {
                                            EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                        }
                                        if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                        {
                                            j++;
                                            if (string.IsNullOrEmpty(Description))
                                            {
                                                hide = "";
                                            }
                                            Description += "<span class='more' " + hide + ">";
                                            Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress;
                                            Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                            Description += "</span></br>";

                                            string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                            string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                            string UDFInstKey = GetUdfOppKey("+Inst");
                                            string UDFRemKey = GetUdfOppKey("+Rem");

                                            var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                            var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                            var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                            var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);
                                            Price += "<span class='morePrice' " + hide + ">";
                                            Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                                            Price += "</span></br>";
                                        }

                                        var suffix = "&Vendor=" + VendorShort + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CURRORD";
                                        var URL = "/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;


                                        string EnquiryStatus = oCsCase.Status.Value.ToString();
                                        //string Description = opp.Comment.Value;
                                        //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                        //    Description = Description.Substring(0, 50) + ".....";

                                        //if (OppList.Count>1)
                                        //{
                                        //    Description += more;
                                        //    Price += morePrice;
                                        //}
                                        dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oUDFAccess.GetFieldValue(UDFCreatedDateKey, opp.Key)).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);

                                    }





                                }

                            }
                        }







                    }
                    else
                    {

                    }

                }
                catch (Exception ex)
                {

                    throw;
                }
                if (dt.Rows.Count <= 0)
                {
                    dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
                }
                return dt;
            

        }

    }
}