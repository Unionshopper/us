﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompletedOrders.ascx.cs" Inherits="US.UserControls.CompletedOrders" %>




<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<div class="table-secation CompletedOrders">
     <asp:HiddenField ID="hfResult" runat="server" />
    <div class="nano" id="dvGrid">
        <asp:GridView ID="GridView1" runat="server" EnableViewState="false" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" DataKeyNames="CaseNo" Width="100%" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4">
            <Columns>
                <asp:BoundField DataField="CaseNo" HeaderText="CASE NUMBER">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="DateFormat" HeaderText="DATE">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Time" HeaderText="TIME">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Assigned" HeaderText="NAME">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="DESCRIPTION">
                    <ItemTemplate>
                        <%# Eval("Desc")%>
                    </ItemTemplate>
                      <HeaderStyle Width="250px" />
                    <ItemStyle Width="250px" />
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="PRICE OPTION">
                    <ItemTemplate>
                        <%# Eval("Revenue")%>
                    </ItemTemplate>
                      <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </asp:TemplateField>
                <asp:BoundField DataField="status" HeaderText="STATUS">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>

                <asp:TemplateField HeaderText="Enquiry Status" Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="hdnStatusid" runat="server" Visible="false" Text='<%#Eval("EnquiryStatus") %>'></asp:Label>
                        <asp:Label ID="lblCaseNo" runat="server" Style="display: none;" Text='<%#Eval("CaseNo") %>'></asp:Label>

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

                            <ContentTemplate>
                                <asp:DropDownList Width="90" runat="server" ID="ddlEnquiryStatus" ClientIDMode="Static" AutoPostBack="True" onchange="javascript:UpdateEnquiry(this);">
                                    <asp:ListItem Text="Abandoned" Value="57994"></asp:ListItem>
                                    <asp:ListItem Text="Assigned" Value="57998"></asp:ListItem>
                                    <asp:ListItem Text="Call Back" Value="57996"></asp:ListItem>
                                    <%--  <asp:ListItem Text="Completed" Value="57993"></asp:ListItem>--%>
                                    <asp:ListItem Text="Confirmed" Value="57993"></asp:ListItem>
                                    <%-- <asp:ListItem Text="Ordered" Value="57997"></asp:ListItem>--%>
                                    <asp:ListItem Text="Pending" Value="57999" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Wait for Customer" Value="57995"></asp:ListItem>
                                    <asp:ListItem Text="Won" Value="57997"></asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                    <EditItemTemplate>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Open">
                    <ItemTemplate>
                        <a href="../CustomDialogs/<%#Eval("URL") %>" ><i class="zmdi zmdi-border-color" ></i></a>
                        <%--<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("CaseNo")%>' Text="" CommandName="Edit"><i class="zmdi zmdi-border-color"></i></asp:LinkButton>--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle  Font-Bold="True" ForeColor="#fff" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#fff" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <SortedAscendingCellStyle BackColor="#EDF6F6" />
            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
            <SortedDescendingCellStyle BackColor="#D6DFDF" />
            <SortedDescendingHeaderStyle BackColor="#fff" />
        </asp:GridView>

    </div>

</div>

<style>
    #dvGrid
    {
       max-height:85vh;
        overflow:scroll;
    }
</style>
<script type="text/javascript">
    var pageIndex = 1;
    var pageCount=100;
    //$(function () {
    //    //Remove the original GridView header
    //    $("[id$=GridView1] tr").eq(0).remove();
    //});

    //Load GridView Rows when DIV is scrolled
    $("#dvGrid").on("scroll", function (e) {
        var $o = $(e.currentTarget);
        if ($o[0].scrollHeight - $o.scrollTop() <= $o.outerHeight()) {
            GetRecords();
        }
    });

    //Function to make AJAX call to the Web Method
    function GetRecords() {
        pageIndex++;
        if (pageIndex == 2 || pageIndex <= pageCount) {

            //Show Loader
            if ($("[id$=GridView1] .loader").length == 0) {
                var row = $("[id$=GridView1] tr").eq(0).clone(true);
                row.addClass("loader");
                row.children().remove();
                row.append('<td colspan = "999" style = "background-color:white"><img id="loader" alt="" src="https://www.aspsnippets.com/demos/103.gif"  /></td>');
                $("[id$=GridView1]").append(row);
            }
            var dateAr = $('#txtFromDate').val().split('-');
            var fromDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);
            
            dateAr = $('#txtToDate').val().split('-');
            var toDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);

            $.ajax({
                type: "POST",
                url: "VendorEnquiries.aspx/GetCompletedOrder",
                data: '{pageIndex: ' + pageIndex + ',sLoginString: "'+$('#hfLoginString').val()+'",VendorType: "'+$('#hfVendor').val()+'",RequestType: "'+$('#hfRequestType').val()+'",fromDate: "' + fromDate + '",toDate: "' + toDate + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    //alert(response.d);
                },
                error: function (response) {
                    //alert(response.d);
                }
            });
        }
    }

    //Function to recieve XML response append rows to GridView
    function OnSuccess(response) {
        
        
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        //pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
        var tableTr = xml.find("Table1");
        $("[id$=GridView1] .loader").remove();
        tableTr.each(function () {
            var items = $(this);
            var url = '<a href="../'+items.find("URL").text()+'"><i class="zmdi zmdi-border-color"></i></a>';
            var row = $("[id$=GridView1] tr").eq(1).clone(true);
             if (items.find("CaseNo").text() == "") {
                 $('#hfRowEmptyOrNot').val("AddedEmpty");
            }
            if ($('#hfRowEmptyOrNot').val() == "") {
                $(row).find('td').eq(0).html(items.find("CaseNo").text());
                $(row).find('td').eq(1).html(items.find("DateFormat").text());
                $(row).find('td').eq(2).html(items.find("Time").text());
                $(row).find('td').eq(3).html(items.find("Assigned").text());
                $(row).find('td').eq(4).html(items.find("Desc").text());
                $(row).find('td').eq(5).html(items.find("Revenue").text());
                $(row).find('td').eq(6).html(items.find("Status").text());
                $(row).find('td').eq(7).html(url);
                $("[id$=GridView1]").append(row);
            }
        });

       

        //Hide Loader
        $("#loader").hide();
    }
</script>