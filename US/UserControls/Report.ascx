﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Report.ascx.cs" Inherits="US.UserControls.Report" %>

<div class="container">
    <div class="row" id="divReport">
        <div class="col-md-8">
            <div class="form-group" style="width:28%;float:left;">
                <asp:DropDownList ID="ddlStatusType" runat="server" CssClass="StatusType" Width="100px" multiple='multiple'>
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="Won" Value="Won"></asp:ListItem>
                    <asp:ListItem Text="Cancelled" Value="Cancelled"></asp:ListItem>
                </asp:DropDownList>

                <input type="hidden" id="hfStatus" name="hfStatus"/>
            </div>
            <div class="input-group input-daterange" style="width:70%">

                <div class="input-group-addon">From</div>
                <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control date-range-filter"></asp:TextBox>

                <div class="input-group-addon">To</div>
                <asp:TextBox runat="server" ID="txtToDate" CssClass="form-control date-range-filter"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-4">
            <%--<asp:Button ID="btnGenerateCSV1" runat="server" CssClass="btn btn-primary" Text="Generate Report" OnClick="btnGenerateCSV_Click"/>--%>
            <button type="button" id="DempbtnGenerateCSV1" class="btn btn-primary">Generate Report</button>
        </div>

    </div>
</div>

 <link href="css/fSelect.css" rel="stylesheet" />
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
    rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<%--<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!-- Bootstrap Date-Picker Plugin -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>--%>

<script>
    $.noConflict();

    jQuery(function () {

        jQuery(document).ready(function () {
            jQuery('#txtFromDate').datepicker('setDate', 'currentweek');
            jQuery('#txtToDate').datepicker('setDate', 'currentweek');

            jQuery(document).on("click", "#DempbtnGenerateCSV1", function () {
                
                var dateAr = jQuery('#<%=txtFromDate.ClientID%>').val().split('-');
                var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);

                jQuery('#hfFrom').val(newDate);
                var dateAr1 = jQuery('#<%=txtToDate.ClientID%>').val().split('-');
                var newDate1 = dateAr1[2] + '-' + dateAr1[1] + '-' + dateAr1[0].slice(-2);
                jQuery('#hfTo').val(newDate1);

                var isValid = true;
                if (jQuery('#<%=txtFromDate.ClientID%>').val() == "" || jQuery('#<%=txtToDate.ClientID%>').val()=="") {
                    isValid = false;
                    alert("Required Date");
                }
                if ($('.StatusType').val() == null) {
                    isValid = false;
                    alert("Required Status Type");
                } else {
                    $('#hfStatus').val($('.StatusType').val().join(","));
                }
                if (isValid) {
                    jQuery('#btnGenerateCSV').click();
                    return false;
                }
            });
            jQuery(".date-range-filter").datepicker({
                prevText: "click for previous months",
                nextText: "click for next months",
                showOtherMonths: true,
                selectOtherMonths: false, dateFormat: 'dd-mm-yy'
            });
        })
    });
</script>
