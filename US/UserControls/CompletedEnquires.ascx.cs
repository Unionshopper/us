﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Maximizer.Data;
using US.Comomn;

namespace US.UserControls
{
    public partial class CompletedEnquires : System.Web.UI.UserControl
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        string sLoginString = "";
        XDocument xAB = null; XDocument xCsCase = null; XDocument xOpportunity = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");
            xCsCase = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            xOpportunity = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");
            xAB = XDocument.Load(path);

            BindGrid();

        }
        public void BindGrid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CaseNo");
            dt.Columns.Add("DateFormat");
            dt.Columns.Add("Time");
            dt.Columns.Add("Assigned");
            dt.Columns.Add("Status");
            dt.Columns.Add("Desc");
            dt.Columns.Add("Description1");
            dt.Columns.Add("EnquiryStatus");
            dt.Columns.Add("Revenue");
            dt.Columns.Add("URL");
            this.sLoginString = this.loginToMax();
            var vendorItemNumber = "";
            string Vendor = Request.QueryString["Vendor"];
            string VendorShort = "";
            string UDFUpdatedByKey = "";
            if (Vendor == "GG")
            {
                VendorShort = "GG";
                vendorItemNumber = "2";
                Vendor = "The Good Guys";
                UDFUpdatedByKey = GetUdfCSCaseKey("Updated by TheGoodGuys");

            }
            else
            {
                VendorShort = "JBHIFI";
                vendorItemNumber = "1";
                Vendor = "JBHiFI";
                UDFUpdatedByKey = GetUdfCSCaseKey("Updated by JBHIFI");
            }

            string RequestType = Request.QueryString["RequestType"];
            if (!string.IsNullOrEmpty(Vendor))
            {


               


                //CSCaseList oCSCaseList1 = oCSCaseAccess.ReadList("EQ(CaseNumber," + "US-01326" + ")");
                //CSCaseList oCSCaseList = oCSCaseAccess.ReadList("KEY(" + this.Request.QueryString["CurrentKey"] + ")");

                //string search = String.Format("LIKE(EQ(UDF," + UDFKey + ",'" + Vendor + "'))");

                string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");



                //AND(EQ(City,Vancouver), OR(LIKE(Phone1,604%), LIKE(Phone2,604%)))

                //string search1 = "EQ(UDF,"+ UDFSubmitTo1Key + ",'1' )";
                //string search = String.Format("LIKE(UDF," + UDFSubmitTo1Key + ", \"{0}%\")", Vendor);
                //string Cate1 = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", UDFSubmitTo1Key, Vendor);
                //string CurrentKeys = "KEY(" + this.Request.QueryString["CurrentKey"] + ")";
                //string Searchkey = "LIKE(UDF, " + UDFSubmitTo1Key + ",'1')";

                //case status=2 means Update
                // string searchkey = "AND(EQ(Status,2),EQ(UDF," + UDFUpdatedByKey + ",'1' ), OR(EQ(UDF," + UDFSubmitTo1Key + ",'"+ vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";
                var CancellEQ = GetCSCaseStausKey("Cancelled");
                var QuotedEQ = GetCSCaseStausKey("Quoted");
                var WaitforcustomerEQ = GetCSCaseStausKey("Wait for customer");


                string searchkey = "EQ(UDF," + UDFUpdatedByKey + ",'1' ), OR(EQ(Status," + CancellEQ + "),EQ(Status," + QuotedEQ + "),EQ(Status," + WaitforcustomerEQ + "),EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' ))";

                var fromDate = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                var toDate = DateTime.Now.ToString("yyyy-MM-dd");
                string UDFCreatedDateKey = GetUdfCSCaseKey("Created Date");

                string strFilterKey = "";
                string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";

                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(strFilterKey, 20);
                foreach (CSCase oCsCase in oCSCaseList)
                {
                    //var cancel = GetCSCaseStausKey("Cancelled");
                    //if (oCsCase.Status.Value == cancel)//4
                    //    continue;

                    var hide = "";
                    var more = "<a href='#' class='aMore'>...more</a>";
                    var morePrice = "<a href='#' class='aMorePrice'>...more</a>";

                    string Description = "";
                    string Price = "";
                    int j = 0;
                    string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");
                    var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                    for (int i = 1; i <= 10; i++)
                    {
                        if (i != 1)
                        {
                            hide = "style='display:none;'";
                        }
                        var SubmitTo = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Submit To" + i), oCsCase.Key);
                        if (string.IsNullOrEmpty(SubmitTo))
                        {
                            continue;
                        }
                        if (!string.IsNullOrEmpty(SubmitTo) && !SubmitTo.ToLower().Contains(Vendor.ToLower()))
                        {
                            continue;
                        }
                        string UDFBrandKey = GetUdfCSCaseKey("Brand" + i);
                        string UDFProductKey = GetUdfCSCaseKey("Product" + i);
                        string UDFModelKey = GetUdfCSCaseKey("Model" + i);
                        string UDFStateCodeKey = GetUdfCSCaseKey("Suburb and Postcode" + i);

                        var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, oCsCase.Key);
                        var Product = oUDFAccess.GetFieldValue(UDFProductKey, oCsCase.Key);
                        var Model = oUDFAccess.GetFieldValue(UDFModelKey, oCsCase.Key);
                        var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, oCsCase.Key);

                        if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                        {
                            j++;
                            if (string.IsNullOrEmpty(Description))
                            {
                                hide = "";
                            }
                            Description += "<span class='more' " + hide + ">";
                            Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>:" + Model + "</br><b>Suburb and Postcode: </b>" + State;
                            Description += "</span></br>";
                            string UDFPUKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Pick Up");
                            string UDFDelKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Delivery");
                            string UDFInstKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Install");
                            string UDFRemKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Removal");


                            var PU = oUDFAccess.GetFieldValue(UDFPUKey, oCsCase.Key);
                            var Del = oUDFAccess.GetFieldValue(UDFDelKey, oCsCase.Key);
                            var Inst = oUDFAccess.GetFieldValue(UDFInstKey, oCsCase.Key);
                            var Rem = oUDFAccess.GetFieldValue(UDFRemKey, oCsCase.Key);


                            Price += "<span class='morePrice' " + hide + ">";
                            Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                            Price += "</span></br>";
                        }
                        else
                        {
                            break;
                        }



                    }
                    Description += "<b>Enquiry Comments: </b>" + EnquiryComments + "</br>";
                    if (j > 1)
                    {
                        Description += more;
                        Price += morePrice;
                    }

                    string userName = "";
                    if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                    {
                        UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                        foreach (User oUser in oUserList)
                        {
                            userName = oUser.DisplayName;
                        }
                    }

                    //string UDFKey = GetUdfOppKey("Submit To");
                    string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                    string CaseNumber = oCsCase.CaseNumber.Value;
                    double RevenuValue = 0;

                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "')");
                    string UDFItemNumberKey = GetUdfOppKey("ItemNumber");

                    var items = "";
                    var OppKeystring = "";
                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                        items = itemNumber + ",";
                        OppKeystring = opp.Key + "-" + itemNumber + ",";
                        RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);
                    }
                    OppKeystring = OppKeystring.TrimEnd(',');
                    items = items.TrimEnd(',');
                    string status = "UPDATED";
                    if (oCsCase.Status.Value == CancellEQ)
                        status = "Cancelled".ToUpper();

                    string EnquiryStatus = oCsCase.Status.Value.ToString();
                    //string Description = oCsCase.Description.Value;
                    //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                    //    Description = Description.Substring(0, 50) + ".....";

                    //var suffix = "&Vendor=" + Request.QueryString["Vendor"] + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=CallCenter";
                    //var URL = "CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + suffix;

                    var suffix = "&Vendor=" + Request.QueryString["Vendor"] + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Email&ET=CE";
                    var URL = "CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + Request.QueryString["Loginstring"] + suffix;

                    dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oCsCase.CreationDate.DateValue).ToString("dd-MM-yyyy"), Convert.ToDateTime(oCsCase.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, oCsCase.Description.Value, EnquiryStatus, Price, URL);

                    //}

                }
                
            }
            if (dt.Rows.Count <= 0)
            {
                hfResult.Value = "No Records Found";
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            else
            {
                DataView dv = dt.DefaultView;
                dv.Sort = "CaseNo desc";
                dt = dv.ToTable();
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        public string GetUdfOppKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xOpportunity.Element("Opp").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                //foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                //{
                //    string appSetting = ConfigurationManager.AppSettings["Database"];
                //    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                //    {
                //        sAddressBookKey = addressBook.Key;
                //        break;
                //    }
                //}
                //string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                //string appSetting2 = ConfigurationManager.AppSettings["Password"];
                //this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
                this.sLoginString = ConfigurationManager.AppSettings["VENJBHIFILoginKey"];
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }

        public string GetUdfKey(string strUdfName, string strLoginString, AddressBookMaster abMaster)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)abMaster.CreateUdfAccess(strLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xAB.Element("ab").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        protected void GridView1_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            //GridView1.PageIndex = e.NewPageIndex;
            //this.BindGrid();
        }


        protected void GridView1_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string caseNo = e.CommandArgument.ToString();
                    //CSCaseList ocSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + caseNo + ")");

                    Response.Redirect("CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&CaseNo=" + caseNo + "&RequestType=CallCenter");
                }
            }
            catch (Exception ex)
            {

            }
        }
        public int GetCSCaseStausKey(string Status)
        {
            int Key = 0;
            try
            {
                //sLoginString = loginToMax();
                var oCSCaseStatusList = oCSCaseAccess.ReadStatusOptionListForAssign();
                foreach (Maximizer.Data.Option item in oCSCaseStatusList)
                {
                    if (item.DisplayValue.ToLower() == Status.ToLower())
                    {
                        return Convert.ToInt32(item.FieldValue);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return Key;
        }
    }
}