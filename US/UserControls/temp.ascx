﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="temp.ascx.cs" Inherits="US.UserControls.temp" %>


<table style="width: 100%;">
    <tr>
        <td style="vertical-align: bottom">COMPLETED ENQUIRIES
        </td>
        <td style="text-align: right">
            <button type="button" style="background: #ffd800; border: none;">Resfresh</button><br />
            <input type="search" />
        </td>
    </tr>
</table>

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>


            <div class="table-secation">
                <div class="nano">

                  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CaseNo" Width="100%" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" >



                        <Columns>
                            <asp:BoundField DataField="CaseNo" HeaderText="CASE NUMBER">
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DateFormat" HeaderText="DATE">
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Time" HeaderText="TIME">
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Assigned" HeaderText="NAME">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="DESCRIPTION">
                                <ItemTemplate>
                                    <%# Eval("Desc")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="" HeaderText="PRICE OPTION">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="status" HeaderText="STATUS">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Open">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("CaseNo")%>' Text="" CommandName="Edit"><i class="zmdi zmdi-border-color"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <HeaderStyle Font-Bold="True" ForeColor="#fff" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#fff" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                        <SortedDescendingHeaderStyle BackColor="#fff" />
                    </asp:GridView>

                </div>

            </div>