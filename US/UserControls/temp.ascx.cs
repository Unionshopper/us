﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Maximizer.Data;
using US.Comomn;

namespace US.UserControls
{
    public partial class temp : System.Web.UI.UserControl
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        string sLoginString = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            BindGrid();


        }
        public void BindGrid()
        {
            string Vendor = Request.QueryString["Vendor"];
            if (Vendor == "GG")
            {
                Vendor = "JBHiFI";
            }
            else
            { Vendor = "The Good Guys"; }

            string RequestType = Request.QueryString["RequestType"];
            if (!string.IsNullOrEmpty(Vendor))
            {
                this.sLoginString = this.loginToMax();

                DataTable dt = new DataTable();
                dt.Columns.Add("CaseNo");
                dt.Columns.Add("DateFormat");
                dt.Columns.Add("Time");
                dt.Columns.Add("Assigned");
                dt.Columns.Add("Status");
                dt.Columns.Add("Desc");
                dt.Columns.Add("Description1");
                dt.Columns.Add("EnquiryStatus");




                CSCaseList oCSCaseList = oCSCaseAccess.ReadList("KEY(" + this.Request.QueryString["CurrentKey"] + ")");


                foreach (CSCase oCsCase in oCSCaseList)
                {
                    if (oCsCase.Status.ToString() == "57993")
                    {
                        string userName = "";
                        if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                        {
                            UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                            foreach (User oUser in oUserList)
                            {
                                userName = oUser.DisplayName;
                            }
                        }

                        string UDFKey = GetUdfOppKey("Submit To");
                        string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                        var UDFCaseNumber = "";
                        var UDFValue = "";

                        Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("KEY(" + this.Request.QueryString["CurrentKey"] + ")");
                        Maximizer.Data.Opportunity oOPp = null;
                        foreach (Maximizer.Data.Opportunity opp in OppList)
                        {
                            UDFValue = oUDFAccess.GetFieldValue(UDFKey, opp.Key);
                            UDFCaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);
                            if (UDFCaseNumber == oCsCase.CaseNumber.Value)
                            { break; }

                        }

                        string status = "COMPLETE";


                        string EnquiryStatus = oCsCase.Status.Value.ToString();
                        string Description = oCsCase.Description.Value;
                        if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                            Description = Description.Substring(0, 50) + ".....";

                        dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oCsCase.CreationDate.DateValue).ToString("dd-MM-yyyy"), Convert.ToDateTime(oCsCase.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, oCsCase.Description.Value, EnquiryStatus);

                    }
                }
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {

            }
        }
        public string GetUdfOppKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }

        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                //foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                //{
                //    string appSetting = ConfigurationManager.AppSettings["Database"];
                //    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                //    {
                //        sAddressBookKey = addressBook.Key;
                //        break;
                //    }
                //}
                //string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                //string appSetting2 = ConfigurationManager.AppSettings["Password"];
                //this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
                this.sLoginString = ConfigurationManager.AppSettings["VENJBHIFILoginKey"];
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }

        public string GetUdfKey(string strUdfName, string strLoginString, AddressBookMaster abMaster)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)abMaster.CreateUdfAccess(strLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }

        protected void GridView1_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            GridView1.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }
    }
}