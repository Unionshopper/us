﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderReports.aspx.cs" Inherits="US.OrderReports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
  <div class="col-md-4 pull-right">
    <div class="input-group input-daterange">
        <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="From:"></asp:TextBox>
      
      <div class="input-group-addon">to</div>
        <asp:TextBox runat="server" ID="txtToDate" CssClass="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="To:"></asp:TextBox>
      

    </div>
  </div>
</div>
    </form>
</body>
</html>
