﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadOrdersList.aspx.cs" Inherits="US.LoadOrdersList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Load Order</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />


    <style>
     
      
        .ui-datepicker-year,
        .ui-datepicker-month {
            color: #333;
        }

        th, td {
            text-align: left !important;
            padding: 4px;
        }

        .enquiry-sheet-part {
            background: #e8ebec !important;
        }

        div {
            font-size: 14px !important;
        }

        body {
            background-color: #FFF !important;
        }

        .heading-notes {
            background: #F5F5F5;
            margin-top: 11px;
            width: 100%;
            margin-left: 0px !important;
            text-align: justify;
        }

        #divnoteshistory, #divenquirynoteshistory {
            background-color: #fff;
            padding: 10px 0 10px 0;
        }

        .btnAddNotes {
            color: #000;
        }

        .detailsection {
            border-radius: 5px;
            border: 2px solid rgba(185,213,49,.87) !important;
            padding: 9px;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .subheading {
            color: rgba(255,255,255,0.87) !important;
            height: 39px;
            text-decoration: none !important;
            display: block;
            background-color: #37474F;
            padding: 6px 0 5px 10px;
            box-sizing: border-box;
            border-radius: 3px 3px 0 0;
        }

        .sub-Heading-possition {
            margin-top: -4px;
            margin-left: -23px;
        }

        .show_hide {
            text-decoration: none !important;
            display: block;
            box-sizing: border-box;
            /* border-radius: 5px; */
            background: #37474F;
            width: 25px;
            text-align: center;
            position: absolute;
            margin-left: -36px;
            margin-top: 21px;
            color: rgba(255,255,255,0.87) !important;
            padding: 5px;
        }

        #RadNotesCenter {
            height: 25em !important;
        }

        .modal-body {
            padding: 6px;
        }

        .modal-footer {
            padding: 7px 20px 20px;
        }

        .reRow {
            display: none;
        }

        select#ddlFilternote, select#ddlEnquiryFilternote {
            color: #000;
        }

        .btnSearchItems {
            background-color: #616d65;
            color: #fff;
            border-radius: 0px;
            margin-top: 24px;
            text-align: center;
            width: 100%;
            font-size: 17px;
        }

        .btn:hover, .btn:focus, .btn.focus {
            color: #e5dfdf;
            text-decoration: none;
        }

        .divshowhide {
            background-color: #37474F;
        }

        .textNoteHeading {
            padding-top: 7px;
        }

        .divshowhide.btn.btn-primary.show_hide {
            margin-left: 0px !important;
            margin-top: 0px !important;
            position: relative !important;
            border-radius: 0px !important;
            padding: 2px !important;
            background-color: #37474F !important;
        }

        .form-control {
            height: 35px !important;
        }

        .member-content-details > span {
            display: block;
        }


        .contactList {
            margin: 37px 0px;
        }

        span > b, li > b {
            color: #858282;
        }

        #btnSearchRecords {
            color: #fff;
        }

        @media screen and (max-width: 768px) {
             td:first-child
            {
                width:100px !important;
            }

            table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > th:first-child:before {
                top: 0px !important;
            }


            tr:last-child td:first-child:before {
                display: none !important;
            }
            #divEnquiryNotesHistory > div {
                overflow: scroll;
            }

            .member-content-details h4 {
                text-align: center;
                font-weight: 800;
                font-size: 20px;
                padding: 10px 0px;
            }

            .member-part .member-heading {
                display: none;
            }

            .member-part ul {
                padding-left: 0px;
            }

            .enquiry-sheet-heading-part img {
                width: 50%;
                margin-left: auto;
                margin-right: auto;
                display: block;
            }

            .contactList {
                margin: 0px;
            }

            .input-daterange, #btnSearchRecords {
                margin: 5px -6px;
            }
        }
    </style>

<%--    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />--%>
   
    <%--<script src="js/bootstrap.min.js"></script>--%>
    <script src='https://code.jquery.com/jquery-3.3.1.js'></script>
    <script src='https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js'></script>
    <script src='https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js'></script>
    <script src='https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js'></script>
    <script src='https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js'></script>
    <script src='https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js'></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">        
        $(document).ready(function () {
            var table = $('#gvLoadOrder').prepend($("<thead></thead>").append($('#gvLoadOrder').find("tr:first"))).DataTable({
                responsive: true,
                ordering: false,
                searching: false,
                paging: false,
                info: false
            });
            new $.fn.dataTable.FixedHeader(table);
        });
    </script>

     <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function () {
            jQuery('#txtFromDate').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
            jQuery('#txtToDate').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
        });
    </script>
</head>
<body>
    <form runat="server">
        <%--<uc1:MenuSection ID="MENUSECTION" runat="SERVER" />--%>
        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <section class="mide">


            <section class="enquiry-sheet-part">
                <div class="enquiry-sheet-heading-part">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12" style="padding: 10px;">
                                <img src="https://www.unionshopper.com.au/wp-content/themes/unionshopper-2019/library/img/us-logo.png" />


                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="member-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                </div>
                                <div class="member-content">
                                    <%-- <div class="member-content-icon">
                                        <i class="zmdi zmdi-account zmdi-hc-fw"></i>
                                    </div>--%>
                                    <div class="member-content-details half">
                                        <h4>
                                            <asp:Label ID="lbluser" runat="server"></asp:Label></h4>
                                         <span style="padding:5px 0px;clear:both; float:left;"><b>Address:</b>
                                            <asp:Label ID="lblAddress" runat="server"></asp:Label></span>
                                        <span style="clear: both; float: left;"><b>Union ID:</b>
                                            <asp:Label ID="lblMember" runat="server"></asp:Label></span>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-content">
                                    <div class="member-content-details contactList">
                                        <ul style="list-style: none;">
                                            <li><b>Phone :</b><span><asp:Label ID="lblHomeNumber" runat="server"></asp:Label></span></li>
                                            <li><b>Email :</b><span><asp:Label ID="lblEmailAddress" runat="server"></asp:Label></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="key-section">
                <div class="container">

                    <div class="row" style="margin-top: 17px; margin-bottom: 10px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="detailsection">
                                <div class="form-area show" id="divEnquiryNotesHistory">

                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label>Filter</label>
                                            <asp:DropDownList runat="server" ID="ddlFilterCount" OnSelectedIndexChanged="ddlFilterCount_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                                <asp:ListItem Text="200" Value="200"></asp:ListItem>
                                                <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                                <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group input-daterange">
                                                <div class="input-group-addon">From</div>
                                                <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control date-range-filter"></asp:TextBox>

                                                <div class="input-group-addon">To</div>
                                                <asp:TextBox runat="server" ID="txtToDate" CssClass="form-control date-range-filter"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <asp:Button ID="btnSearchRecords" runat="server" CssClass="btn btn-warning" Text="Search" OnClick="btnSearchRecords_Click" />
                                        </div>
                                    </div>
                                    <asp:GridView ID="gvLoadOrder" runat="server" GridLines="Horizontal" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" Width="100%" ShowFooter="true">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#37474F" />
                                        <HeaderStyle BackColor="#37474F" Font-Bold="True" ForeColor="#F7F7F7" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#37474F" HorizontalAlign="Right" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#37474F" />
                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </div>
                            </div>


                        </div>

                    </div>

                </div>
            </section>
        </section>
    </form>
</body>
</html>


