﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace US
{
    public partial class vendorlogin : System.Web.UI.Page
    {
        Maximizer.Data.AddressBookMaster oAb = new Maximizer.Data.AddressBookMaster();
        Maximizer.Data.AbEntryAccess oAbEntryAccess = null;
        Maximizer.Data.GlobalAccess ga = new Maximizer.Data.GlobalAccess();
        Maximizer.Data.AbEntryList oAbEntryList = null;
        Maximizer.Data.UdfAccess oUDFAccess = null;
        Maximizer.Data.NoteAccess oNoteAccess = null;
        Maximizer.Data.TaskAccess oTaskAccess = null;
        Maximizer.Data.UserAccess oUserAccess = null;
        Maximizer.Data.OpportunityAccess oOppAccess = null;
        Maximizer.Data.DetailFieldAccess oDetailFieldAccess = null;
        Maximizer.Data.AddressAccess oAddressAccess = null;
        string sLoginString = "";
        string CurrentKey = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                Maximizer.Data.AddressBookMaster oAbMax = new Maximizer.Data.AddressBookMaster();
                Maximizer.Data.GlobalAccess ga = new Maximizer.Data.GlobalAccess();
                string sRt = "";
                Maximizer.Data.AddressBookList abList = ga.ReadAddressBookList();

                string database = ConfigurationManager.AppSettings["Database"];
                //foreach (Maximizer.Data.AddressBook ab in abList)
                //{
                //    if (0 == ab.Name.ToString().ToUpper().CompareTo(database.ToUpper()))
                //    {

                //        sRt = ab.Key;
                //        //  sRt = "226691894027948274753882216022z76607f607c63054267415e176a514a4452464a0e77796c73707065750f67610965736b677d6b09677b7914017a066307797d656a6c6109647f740f64777c7c7471787e700f6065730459594a5e595e4f564a03";
                //        break;
                //    }
                //}

                //    sLoginString = oAbMax.LoginMaximizer(sRt, txtUserName.Text.Trim(), txtPWD.Text.Trim());

                if (txtUserName.Text.ToUpper() == "VENJBHIFI")
                    sLoginString = ConfigurationManager.AppSettings["VENJBHIFILoginKey"];
                else if (txtUserName.Text.ToUpper() == "VENTGG")
                    sLoginString = ConfigurationManager.AppSettings["VENGGLoginKey"];

                // Always need this to create access to Maximizer and access to special fields.
                oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
                oUDFAccess = oAb.CreateUdfAccess(sLoginString); // always use to setup to write or read from custom fields.

                //  oNoteAccess = oAb.(Access(sLoginString);
                oUserAccess = oAb.CreateUserAccess(sLoginString);
                oOppAccess = oAb.CreateOpportunityAccess(sLoginString);
                oDetailFieldAccess = oAb.CreateDetailFieldAccess(sLoginString);
                var User = oAbEntryAccess.GetLoggedInUser(sLoginString);
                if (User != null)
                {

                    string LiveUrl = WebConfigurationManager.AppSettings["LiveURl"];
                    LiveUrl = LiveUrl + "/MaximizerWebAccess/Dialogs/CustomDialogs/";

                    string url = "MemberEnquiry.aspx?Loginstring=" + sLoginString + "&RequestType=CallCenter";
                    if (User.UserId.Value.ToUpper() == "VENJBHIFI")
                        url = "VendorEnquiries.aspx?Loginstring=" + sLoginString + "&RequestType=CallCenter&Vendor=JBHIFI";
                    else if (User.UserId.Value.ToUpper() == "VENTGG")
                        url = "VendorEnquiries.aspx?Loginstring=" + sLoginString + "&RequestType=CallCenter&Vendor=GG";

                    Response.Redirect(url);
                }
            }
            catch (Exception ex)
            {

                //throw;
            }
        }

        public string GetUdfKey(string strUdfName, string strLoginString, Maximizer.Data.AddressBookMaster abMaster)
        {
            Maximizer.Data.UdfAccess udfAccess =
            abMaster.CreateUdfAccess(strLoginString);
            Maximizer.Data.UdfDefinitionList udfDefList =
            udfAccess.ReadUdfDefinitionList(new Maximizer.Data.AbEntry().Key);
            var oList = udfDefList.Cast<Maximizer.Data.UdfDefinition>().Where(x => x.FieldName.Equals(strUdfName));
            foreach (Maximizer.Data.UdfDefinition udfDef in oList)
            {
                return udfDef.Key;
            }
            return String.Empty;

        }

    }
}