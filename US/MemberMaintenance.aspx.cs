﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace US
{
    public partial class MemberMaintenance : System.Web.UI.Page
    {
        Maximizer.Data.AddressBookMaster oAb = new Maximizer.Data.AddressBookMaster();
        Maximizer.Data.AbEntryAccess oAbEntryAccess = null;
        Maximizer.Data.GlobalAccess ga = new Maximizer.Data.GlobalAccess();
        Maximizer.Data.AbEntryList oAbEntryList = null;
        Maximizer.Data.UdfAccess oUDFAccess = null;
        Maximizer.Data.NoteAccess oNoteAccess = null;
        Maximizer.Data.TaskAccess oTaskAccess = null;
        Maximizer.Data.UserAccess oUserAccess = null;
        Maximizer.Data.OpportunityAccess oOppAccess = null;
        Maximizer.Data.DetailFieldAccess oDetailFieldAccess = null;
        Maximizer.Data.AddressAccess oAddressAccess = null;
        string sLoginString = "";
        string CurrentKey = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //sLoginString = loginToMax();

            //if (!IsPostBack)
            //{
            //    string sFilterKey = "KEY(" + CurrentKey + ")";
            //    oAbEntryList = oAbEntryAccess.ReadList(sFilterKey);
            //    foreach (Maximizer.Data.AbEntry oAbE in oAbEntryList)
            //    {
            //        Maximizer.Data.Individual oIndividual = (Maximizer.Data.Individual)(oAbE);
            //        txtFirstName.Text = oIndividual.FirstName.Value;
            //        txtLastName.Text = oIndividual.LastName.Value;

            //        txtHomeNumber.Text = oIndividual.Phone1.Value;
            //        txtWorkNumber.Text = oIndividual.Phone2.Value;
            //        txtMobileNumber.Text = oIndividual.Phone3.Value;
            //        txtFaxNumber.Text = oIndividual.Phone4.Value;

            //        string Gender = oUDFAccess.GetFieldValue(GetUdfKey("Gender", sLoginString, oAb), oIndividual.Key);
            //        if (!string.IsNullOrEmpty(Gender))
            //            //ddlGender.Items.FindByText(Gender).Selected = true;

            //        FillWithNormalUDF(oAb, sLoginString, "", ddlMemberType, GetUdfKey("Member Type", sLoginString, oAb));
            //        string MemberType = oUDFAccess.GetFieldValue(GetUdfKey("Member Type", sLoginString, oAb), oIndividual.Key);
            //        if (!string.IsNullOrEmpty(MemberType))
            //            //ddlMemberType.Items.FindByText(MemberType).Selected = true;

            //        FillWithNormalUDF(oAb, sLoginString, "", ddlUnion, GetUdfKey("Union ID", sLoginString, oAb));
            //        string UnionID = oUDFAccess.GetFieldValue(GetUdfKey("Union ID", sLoginString, oAb), oIndividual.Key);
            //        if (!string.IsNullOrEmpty(UnionID))
            //            ddlUnion.Items.FindByText(UnionID).Selected = true;

            //        txtUnionNumner.Text = oUDFAccess.GetFieldValue(GetUdfKey("UNION_NUM", sLoginString, oAb), oIndividual.Key);

            //        FillWithNormalUDF(oAb, sLoginString, "", ddlPosition, GetUdfKey("POSITION", sLoginString, oAb));
            //        string Position = oUDFAccess.GetFieldValue(GetUdfKey("POSITION", sLoginString, oAb), oIndividual.Key);
            //        if (!string.IsNullOrEmpty(Position))
            //            //ddlPosition.Items.FindByText(Position).Selected = true;

            //        //SALUTATION

            //        txtResidentialAddress.Text = oIndividual.CurrentAddress.AddressLine1.Value;
            //        txtPostCode.Text = oIndividual.CurrentAddress.ZipCode.Value;
            //        string sAddressFilterKey = "KEY(" + oIndividual.Key + ")";
            //        Maximizer.Data.AddressList oAddressList = oAddressAccess.ReadList(sAddressFilterKey);
            //        foreach (Maximizer.Data.Address address in oAddressList)
            //        {
            //            txtMalingAddress.Text = address.AddressLine1.Value;
            //            txtPostCode1.Text = address.ZipCode.Value;
            //        }


            //        txtAuthFirstName.Text = oUDFAccess.GetFieldValue(GetUdfKey("AUTH_FIRSTNAME", sLoginString, oAb), oIndividual.Key);
            //        txtAuthLastName.Text = oUDFAccess.GetFieldValue(GetUdfKey("AUTH_LASTNAME", sLoginString, oAb), oIndividual.Key);

            //        FillWithNormalUDF(oAb, sLoginString, "", ddlRelationship, GetUdfKey("AUTHORISATION_RELATION", sLoginString, oAb));
            //        string authRelation = oUDFAccess.GetFieldValue(GetUdfKey("AUTHORISATION_RELATION", sLoginString, oAb), oIndividual.Key);
            //        if (!string.IsNullOrEmpty(authRelation))
            //            //ddlRelationship.Items.FindByText(authRelation).Selected = true;

            //        txtPassword.Text = oUDFAccess.GetFieldValue(GetUdfKey("PASSWORD", sLoginString, oAb), oIndividual.Key);

            //        txtAuthHomeNumber.Text = oUDFAccess.GetFieldValue(GetUdfKey("AUTH_HPH", sLoginString, oAb), oIndividual.Key);
            //        txtAuthWorkNumber.Text = oUDFAccess.GetFieldValue(GetUdfKey("AUTH_WPH", sLoginString, oAb), oIndividual.Key);
            //        txtAuthMobileNumber.Text = oUDFAccess.GetFieldValue(GetUdfKey("AUTH_MPH", sLoginString, oAb), oIndividual.Key);

            //        txtFeedBackDate.Text = oUDFAccess.GetFieldValue(GetUdfKey("FEEDBACK_ASKED", sLoginString, oAb), oIndividual.Key);
            //        txtFeedBackDate.Text = oUDFAccess.GetFieldValue(GetUdfKey("FEEDBACK_ASKED", sLoginString, oAb), oIndividual.Key);


            //        txtEmailaddress.Text = oUDFAccess.GetFieldValue(GetUdfKey("AUTH_EMAIL", sLoginString, oAb), oIndividual.Key);
            //        txtOpretor.Text = oUDFAccess.GetFieldValue(GetUdfKey("Joining Operator", sLoginString, oAb), oIndividual.Key);
            //        txtDateJoined.Text = oUDFAccess.GetFieldValue(GetUdfKey("Date Joined", sLoginString, oAb), oIndividual.Key);
            //        txtLastActiveDate.Text = oUDFAccess.GetFieldValue(GetUdfKey("Last Data Active", sLoginString, oAb), oIndividual.Key);
            //        txtMStartDate.Text = oUDFAccess.GetFieldValue(GetUdfKey("MSHIP_BEG", sLoginString, oAb), oIndividual.Key);
            //        txtMEndDate.Text = oUDFAccess.GetFieldValue(GetUdfKey("MSHIP_END", sLoginString, oAb), oIndividual.Key);
            //        txtMemberID.Text = oUDFAccess.GetFieldValue(GetUdfKey("Member ID", sLoginString, oAb), oIndividual.Key);


            //    }
            //}
        }
        private string loginToMax()
        {
            Maximizer.Data.AddressBookList abList = ga.ReadAddressBookList();
            string sRt = "";
            sLoginString = Convert.ToString(Session["loginString"]);
            CurrentKey = Convert.ToString(Session["abGrid!_CurrentKey"]);

            if (sLoginString == "" || sLoginString == null)
            {
                string database = ConfigurationManager.AppSettings["Database"];
                var oList = abList.Cast<Maximizer.Data.AddressBook>().Where(x => x.Name.Value.ToUpper() == database.ToUpper());
                foreach (Maximizer.Data.AddressBook ab in oList)
                {
                    sRt = ab.Key;
                    break;
                }

                string Username = ConfigurationManager.AppSettings["UserName"];
                string Password = ConfigurationManager.AppSettings["Password"];
                sLoginString = oAb.LoginMaximizer(sRt, Username, Password);

            }
            if (string.IsNullOrEmpty(CurrentKey))
            {
                sLoginString = "97146828199322275262052735z7d657862736a0f43624855136157404150404b097474667671746a720c616503617d636f7c610f66707407020705620709627a71047a7067627d6003616e7d0e404b5359475d555943040307000e";
                CurrentKey = "	SW5kaXZpZHVhbAkwNDA0MDgwMDAwNjA3NzIyMzgxNjlDCTA=";
            }

            // Always need this to create access to Maximizer and access to special fields.
            oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
            oUDFAccess = oAb.CreateUdfAccess(sLoginString);
            oOppAccess = oAb.CreateOpportunityAccess(sLoginString);
            oNoteAccess = oAb.CreateNoteAccess(sLoginString);
            oTaskAccess = oAb.CreateTaskAccess(sLoginString);
            oUserAccess = oAb.CreateUserAccess(sLoginString);
            oAddressAccess = oAb.CreateAddressAccess(sLoginString);
            oDetailFieldAccess = oAb.CreateDetailFieldAccess(sLoginString);

            return sLoginString;
        }

        public string GetUdfKey(string strUdfName, string strLoginString, Maximizer.Data.AddressBookMaster abMaster)
        {
            Maximizer.Data.UdfAccess udfAccess =
            abMaster.CreateUdfAccess(strLoginString);
            Maximizer.Data.UdfDefinitionList udfDefList =
            udfAccess.ReadUdfDefinitionList(new Maximizer.Data.AbEntry().Key);
            var oList = udfDefList.Cast<Maximizer.Data.UdfDefinition>().Where(x => x.FieldName.Equals(strUdfName));
            foreach (Maximizer.Data.UdfDefinition udfDef in oList)
            {
                return udfDef.Key;
            }
            return String.Empty;

        }
        private int FillWithNormalUDF(Maximizer.Data.AddressBookMaster maxAb, string maxLoginstr, string productname, DropDownList ddlTarget, string SystemUDFID)
        {
            try
            {
                Maximizer.Data.UdfSetupList setupList;
                Maximizer.Data.UdfAccess oUDFAccess;
                oUDFAccess = maxAb.CreateUdfAccess(maxLoginstr);
                //setupList = oUDFAccess.ReadAbEntryUdfSetupList("EQ(TypeId," + SystemUDFID + ")", true);
                setupList = oUDFAccess.ReadAbEntryUdfSetupList("KEY(" + SystemUDFID + ")", true);
                int count = 0;

                foreach (Maximizer.Data.UdfSetupTable tbl in setupList)
                {
                    foreach (Maximizer.Data.UdfSetupTableItem item in tbl.Items)
                    {
                        System.Web.UI.WebControls.ListItem itemdd = new System.Web.UI.WebControls.ListItem();
                        itemdd.Text = item.Name.Value;
                        itemdd.Value = item.Key;

                        ddlTarget.Items.Add(itemdd);
                    }
                }
                return -1;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return -1;
            }
        }
    }
}