﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Maximizer.Data;
using US.Comomn;

namespace US
{
    public partial class CreateCSCase : System.Web.UI.Page
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        common common = new common();
        string sLoginString = "";

        XDocument xAB = null; XDocument xCsCase = null; XDocument xOpportunity = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");
            xCsCase = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            xOpportunity = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");
            xAB = XDocument.Load(path);

            string RequestType = Request.QueryString["RequestType"];
            this.sLoginString = this.loginToMax();
            //var UDFKeyOpp = GetUdfOppKey("JBHIFI Comment");
            //var UDFKeyOpp1 = GetUdfOppKey("TheGoodGuys Comment");
            hfsLoginString.Value = sLoginString;
            //var aa = GetUdfOppKey("Vendor Order Number");
            if (!IsPostBack)
            {
                string Vendor = Request.QueryString["Vendor"];
                if (Vendor == "GG")
                {
                    Vendor = "The Good Guys";
                }
                else
                {
                    Vendor = "JBHiFI";
                }

                lblVendor.Text = Vendor;
                string currentKey = this.Request.QueryString["CurrentKey"];
                this.oAbEntryList = this.oAbEntryAccess.ReadList("KEY(" + currentKey + ")");
                foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                {
                    lblEmailAddressId.Text = oAbEntry.EmailAddress1.Value;
                    this.lbluser.Text = oAbEntry.FirstName.Value + " " + oAbEntry.LastName.Value;
                    this.lblAddress.Text = oAbEntry.CurrentAddress.AddressLine1.Value + " " + oAbEntry.CurrentAddress.City.Value + " " + oAbEntry.CurrentAddress.StateProvince.Value + " " + oAbEntry.CurrentAddress.ZipCode.Value;

                    hfAddress.Value = oAbEntry.CurrentAddress.AddressLine1.Value;
                    hfPostCode.Value = oAbEntry.CurrentAddress.City.Value + " " + oAbEntry.CurrentAddress.StateProvince.Value + " " + oAbEntry.CurrentAddress.ZipCode.Value;

                    this.lblMobileNumber.Text = oAbEntry.Phone2.Value;//Home
                    this.lblWorkNumber.Text = oAbEntry.Phone3.Value;//Work
                    this.lblBusNumber.Text = oAbEntry.Phone1.Value;//Mobile
                    string fieldValue1 = this.oUDFAccess.GetFieldValue(GetUdfKey("Member ID"), oAbEntry.Key);
                    string fieldValue2 = this.oUDFAccess.GetFieldValue(GetUdfKey("Union ID"), oAbEntry.Key);
                    this.lblMember.Text = fieldValue1;
                    lblVCContact.Text = this.oUDFAccess.GetFieldValue(this.GetUdfKey("VC Contact"), oAbEntry.Key);
                    lblVCName.Text = this.oUDFAccess.GetFieldValue(this.GetUdfKey("VC Name"), oAbEntry.Key);
                    lblVCRelationship.Text = this.oUDFAccess.GetFieldValue(this.GetUdfKey("VC Relationship"), oAbEntry.Key);
                    if (RequestType == "CallCenter" || RequestType == "" || RequestType == null)
                    {
                        if (!string.IsNullOrEmpty(fieldValue2))
                        {
                            lblUnionId.Text = fieldValue2;
                        }
                        else
                        {
                            lblUnionId.Text = "No Union ID";
                            liUnionId.Style.Add("Color", "red");
                            lblUnionId.Style.Add("Color", "red");
                        }
                    }
                    else
                    {
                        liUnionId.Style.Add("display", "none");
                    }
                }
                FillDropDownWithCSCaseUDF(ddlBrand, "Brand1");
                FillDropDownWithCSCaseUDF(ddlProduct, "Product1");

                //FillDropDownWithCSCaseUDF(ddlPayBY, "Pay By1"); block by kaushik
                //FillDropDownWithCSCaseUDF(ddlPickupOrDelivery, "Pickup or Delivery1");
                //ltrPickupOrDeliveryy.Text = FillddlPickOrDeliveryyValue("");
                ltrSubmitedTo.Text = FillSubmitToValue("", "", "");
                //ltrOptions.Text = FillOptionsValue("", "", "");
                ltrPayBY.Text = FillPayByOptionsValue("", "", "");

                string CaseNumber = Request.QueryString["CaseNo"];
                if (!string.IsNullOrEmpty(CaseNumber))
                {
                    //lblMidEQ.Text = "UPDATE ENQUIRIES";

                    lblMidEQ.Text = "UPDATE ENQUIRY";
                    btnCreateEnquiry.Text = "Update Enquiry";
                    string CaseNo = Regex.Match(CaseNumber, @"\d+").Value;
                    lblEnquiryNumber.Text = CaseNo;
                    loadCSCase(CaseNumber);
                    EQNumber.Attributes.Add("Style", "display:inline-block;");
                    divClientNotes.Attributes.Add("style", "display:block;");
                    //string RequestType = Request.QueryString["RequestType"];
                    if (RequestType == "Email")
                    {
                        //btnCreateEnquiry.Attributes.Add("style", "display:none;");
                        lblAddress.Attributes.Add("style", "display:none;");
                        //lblHomeNumber.Attributes.Add("style", "display:none;");
                        lblBusNumber.Attributes.Add("style", "display:none;");
                        lblMobileNumber.Attributes.Add("style", "display:none;");
                        lblWorkNumber.Attributes.Add("style", "display:none;");
                    }
                    rwHeaderOption.Attributes.Add("style", "display:none;");
                }
                else
                {
                    lblLooginUserHD.Text = "";
                    CaseNumber = Request.QueryString["CaseNoNew"];

                    //   CSCase oCSCase = new CSCase(currentKey);
                    //   oCSCase.AssignedTo.Value = oAbEntryAccess.GetLoggedInUserKey();
                    //   oCSCaseAccess.Insert(oCSCase);
                    txtDeliveryAddress.Text = hfAddress.Value;
                    txtDeliveryPostCode.Text = hfPostCode.Value;

                    string CaseNo = Regex.Match(CaseNumber, @"\d+").Value;
                    lblEnquiryNumber.Text = CaseNo;
                    EQNumber.Attributes.Add("Style", "display:block;");
                    divClientNotes.Attributes.Add("style", "display:block;");

                    lblMidEQ.Text = "CREATE ENQUIRY";
                    //lblMidEQ.Text = "CREATE ENQUIRIES";Create Enquiry
                    radFilterType.Items.FindByValue("Direct Enquiry").Selected = true;

                }

                // this.ddlEnquiryFilternote.SelectedValue = "5";
                //this.fillEnquiryNotesHistory(Convert.ToInt32(this.ddlEnquiryFilternote.SelectedValue));
                this.ddlFilternote.SelectedValue = "5";
                this.fillClientNotesHistory(Convert.ToInt32(this.ddlFilternote.SelectedValue));

            }
        }
        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                {
                    string appSetting = ConfigurationManager.AppSettings["Database"];
                    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                    {
                        sAddressBookKey = addressBook.Key;
                        break;
                    }
                }
                string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                string appSetting2 = ConfigurationManager.AppSettings["Password"];
                this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }
        private int FillDropDownWithNormalUDF(DropDownList ddlTarget, string SystemUDFID)
        {
            try
            {
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadAbEntryUdfSetupList("KEY(" + SystemUDFID + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                        ddlTarget.Items.Add(new ListItem()
                        {
                            Text = udfSetupTableItem.Name.Value,
                            Value = udfSetupTableItem.Key
                        });
                }
                return -1;
            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);
                return -1;
            }
        }
        private int FillDropDownWithCSCaseUDF(DropDownList ddlTarget, string SystemUDFID)
        {
            try
            {
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadCSCaseUdfSetupList("KEY(" + GetUdfCSCaseKey(SystemUDFID) + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                        ddlTarget.Items.Add(new ListItem()
                        {
                            Text = udfSetupTableItem.Name.Value,
                            Value = udfSetupTableItem.Name.Value
                        });
                }
                return -1;
            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);
                return -1;
            }
        }
        public string FillddlPickOrDeliveryyValue(string Values, string Id)
        {
            string str = "";
            try
            {
                str = "<select id='ddlPickupOrDelivery" + Id + "' Name='ddlPickupOrDelivery" + Id + "' class='ddlPickupOrDelivery'>";
                str += "<option></option>";
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadCSCaseUdfSetupList("KEY(" + GetUdfCSCaseKey("MP Option1") + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                    {
                        string selected = "";
                        if (Values.Contains(udfSetupTableItem.Name.Value))
                        {
                            selected = "selected";
                        }
                        str += "<option value='" + udfSetupTableItem.Name.Value + "' " + selected + ">" + udfSetupTableItem.Name.Value + "</option>";
                    }
                }
                str += "</select>";

            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);

            }
            return str;
        }

        public string FillSubmitToValue(string Values, string Id, string RequestType)
        {
            string str = "";
            try
            {
                string Radonly = "";
                if (RequestType == "Email")
                    Radonly = "disabled";

                str = "<select " + Radonly + " id='ddlSubmitTo" + Id + "'  Name='ddlSubmitTo" + Id + "' class='ddlSubmitTo' multiple='multiple'>";
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadCSCaseUdfSetupList("KEY(" + GetUdfCSCaseKey("Submit To1") + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                    {
                        string selected = "";
                        if (Values.Contains(udfSetupTableItem.Name.Value))
                        {
                            selected = "selected";
                        }
                        str += "<option value= '" + udfSetupTableItem.Name.Value + "'" + selected + "> " + udfSetupTableItem.Name.Value + "</option>";
                    }
                }

                str += "</select>";
            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);
            }
            return str;
        }
        public string FillPayByOptionsValue(string Values, string Id, string RequestType)
        {
            string str = "";
            try
            {
                string Readonly = "";
                if (RequestType == "Email")
                    Readonly = "disabled";

                str = "<select " + Readonly + " id='ddlPayBY" + Id + "'  Name='ddlPayBY" + Id + "' class='ddlPayBY' multiple='multiple'>";
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadCSCaseUdfSetupList("KEY(" + GetUdfCSCaseKey("Pay By1") + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                    {
                        string selected = "";
                        if (Values.Contains(udfSetupTableItem.Name.Value))
                        {
                            selected = "selected";
                        }
                        str += "<option value= '" + udfSetupTableItem.Name.Value + "'" + selected + "> " + udfSetupTableItem.Name.Value + "</option>";
                    }
                }

                str += "</select>";
            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);
            }
            return str;
        }

        public string FillOptionsValue(string Values, string Id, string RequestType)
        {
            string str = "";
            try
            {
                string Readonly = "";
                if (RequestType == "Email")
                    Readonly = "disabled";

                str = "<select " + Readonly + " id='ddlOption" + Id + "'  Name='ddlOption" + Id + "' class='ddlOption' multiple='multiple'>";
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadCSCaseUdfSetupList("KEY(" + GetUdfCSCaseKey("Options1") + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                    {
                        string selected = "";
                        if (Values.Contains(udfSetupTableItem.Name.Value))
                        {
                            selected = "selected";
                        }
                        str += "<option value= '" + udfSetupTableItem.Name.Value + "'" + selected + "> " + udfSetupTableItem.Name.Value + "</option>";
                    }
                }

                str += "</select>";
            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);
            }
            return str;
        }


        public string GetUdfKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xAB.Element("ab").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfOppKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            //var path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            //xOpportunity = XDocument.Load(path);
            var sFilterItems = xOpportunity.Element("Opp").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        public Opportunity GetOpp(string itemNumber)
        {
            Opportunity opp = new Opportunity();
            try
            {
                string CaseNumber = "US-" + lblEnquiryNumber.Text;
                string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                string UDFKey = GetUdfOppKey("Submit To");
                string Vendor = Request.QueryString["Vendor"];
                if (Vendor == "GG")
                {
                    Vendor = "The Good Guys";
                }
                else if (Vendor.ToLower() == "JBHiFI".ToLower())
                {
                    Vendor = "JBHiFI";
                }
                //string search = String.Format("LIKE(EQ(UDF," + UDFKey + ",'" + Vendor + "'))");
                //string search = String.Format("LIKE(UDF," + UDFKey + ", \"%{0}\")", Vendor);
                string Cate1 = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", UDFKey, Vendor);
                string searchkey = "AND( EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "'),  EQ(UDF," + UDFItemNumberKey + "," + itemNumber + "))";

                Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList(searchkey);

                foreach (Maximizer.Data.Opportunity oOpp in OppList)
                {
                    var VendorName = oUDFAccess.GetFieldValue(UDFKey, oOpp.Key);
                    if (!string.IsNullOrEmpty(VendorName) && VendorName.ToLower().Contains(Vendor.ToLower()))
                        opp = oOpp;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return opp;
        }

        protected void btnCreateEnquiry_Click(object sender, EventArgs e)
        {
            string vendor = Request.QueryString["Vendor"];
            string isSavedTypeEnquiry = "";

            try
            {
                List<string> sendEmailForVendors = new List<string>();

                string str = "";
                string strDesc = "";
                string OrderedOppKeyJBHIFI = "";
                string OrderItemNumberJBHIFI = "";
                string OrderedOppKeyGG = "";
                string OrderItemNumberGG = "";
                string ClientID = "";
                string Phone = "";

                string Key = "";
                CSCase oCSCase = null;
                string CaseNumber = "US-" + lblEnquiryNumber.Text;
                if (!string.IsNullOrEmpty(CaseNumber))
                {
                    oCSCase = new CSCase();
                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + CaseNumber + ")");
                    foreach (CSCase oCSCaseItems in oCSCaseList)
                    {
                        oCSCase = oCSCaseItems;
                        isSavedTypeEnquiry = oCSCaseItems.StatusName.Value;
                        this.oAbEntryList = this.oAbEntryAccess.ReadList("KEY(" + oCSCase.ParentKey + ")");
                        foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                        {
                            ClientID = oAbEntry.ClientId.Value;
                            Phone = oAbEntry.Phone1.Value;
                        }

                    }
                }
                Key = oCSCase.Key;
                string EnquiryType = Request.QueryString["ET"];
                string Mode = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Mode"), Key);
                string RequestType = Request.QueryString["RequestType"];
                string vendorType = Request.QueryString["Vendor"];
                var EnquiryComments = Request.Form["txtEnquiryComments"];



                if (RequestType != "Email")
                {
                    this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Enquiry Comments"), oCSCase.Key, EnquiryComments);
                }
                bool isFlagCancel = true;
                bool CompleteEnquiryByUSRep = true;
                bool submitToSingleVendor = true;
                string CompareVendortoItems = "";

                int OrderedItemsCount = 0;
                int WaitForCustomerItemsCount = 0;
                int QuotedItemsCount = 0;
                int CancelledItemsCount = 0;
                int TotalItemsAddedInEnquiry = 0;


                for (int j = 1; j <= 10; j++)
                {
                    string OppKey = "";
                    string i = j.ToString();
                    if (j == 1)
                    {
                        i = "";
                    }
                    if (RequestType == "Vendor")
                    {
                        var items = Request.QueryString["Items"];
                        if (!string.IsNullOrEmpty(items))
                        {
                            //Page.Title = "Vendor Feedback";
                            //lblMidEQ.Text = "Vendor Feedback";
                            if (!items.Contains(j.ToString()))
                            {
                                continue;
                            }
                            else
                            {
                                string[] arrOpp = null;
                                var OppKeys = Request.QueryString["OppKeys"];
                                if (!string.IsNullOrEmpty(OppKeys))
                                {
                                    arrOpp = OppKeys.Split(',');
                                    OppKey = arrOpp.Where(x => x.Contains("-" + j.ToString())).FirstOrDefault();
                                    OppKey = OppKey.Replace("-" + j.ToString(), "");
                                }
                            }
                        }
                    }

                    if (RequestType == "Email")
                    {
                        string submitedVendorEQ = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Submit To" + j), oCSCase.Key);
                        if (!string.IsNullOrEmpty(submitedVendorEQ) && submitedVendorEQ.Contains(","))
                        {
                            submitToSingleVendor = false;
                        }
                        else if (!string.IsNullOrEmpty(submitedVendorEQ))
                        {
                            if (!string.IsNullOrEmpty(CompareVendortoItems))
                            {
                                if (CompareVendortoItems != submitedVendorEQ)
                                    submitToSingleVendor = false;
                            }
                            else
                            {
                                CompareVendortoItems = submitedVendorEQ;
                            }
                        }

                        ltrPayBY.Text = FillPayByOptionsValue("", "", "");

                        sendEmailForVendors.Add(vendorType);

                        var Comment = Request.Form["txtP" + vendorType.ToUpper() + "Comment" + j];
                        if (string.IsNullOrEmpty(Comment))
                            Comment = "";

                        var Pickup = Request.Form["txtP" + vendorType.ToUpper() + "PickUp" + j];
                        if (string.IsNullOrEmpty(Pickup))
                            Pickup = "";
                        else
                            Pickup = Regex.Match(Pickup, @"\d+").Value;

                        var Delivery = Request.Form["txtP" + vendorType.ToUpper() + "PriceDelivery" + j];
                        if (string.IsNullOrEmpty(Delivery))
                            Delivery = "";
                        else
                            Delivery = Regex.Match(Delivery, @"\d+").Value;

                        var Removal = Request.Form["txtPDelInstRem" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(Removal))
                        {
                            Removal = "";
                        }
                        else
                            Removal = Regex.Match(Removal, @"\d+").Value;

                        var DelRem = Request.Form["txtPDelRem" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(DelRem))
                        {
                            DelRem = "";
                        }
                        else
                            DelRem = Regex.Match(DelRem, @"\d+").Value;

                        var Inst = Request.Form["txtPInst" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(Inst))
                            Inst = "";
                        else
                            Inst = Regex.Match(Inst, @"\d+").Value;

                        var WarrantyCost = Request.Form["txtPWarranty" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(WarrantyCost))
                        {
                            WarrantyCost = "";
                        }
                        else
                            WarrantyCost = Regex.Match(WarrantyCost, @"\d+").Value;

                        var DelInstRemWarranty = Request.Form["DelInstRemWarranty" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(DelInstRemWarranty))
                        {
                            DelInstRemWarranty = "";
                        }
                        else
                            DelInstRemWarranty = Regex.Match(DelInstRemWarranty, @"\d+").Value;

                        var DelInstWarranty = Request.Form["txtPDelInstWarranty" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(DelInstWarranty))
                        {
                            DelInstWarranty = "";
                        }
                        else
                            DelInstWarranty = Regex.Match(DelInstWarranty, @"\d+").Value;

                        var DelRemWarranty = Request.Form["txtPDelRemWarranty" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(DelRemWarranty))
                        {
                            DelRemWarranty = "";
                        }
                        else
                            DelRemWarranty = Regex.Match(DelRemWarranty, @"\d+").Value;

                        var PickupWarranty = Request.Form["txtP" + vendorType.ToUpper() + "PickUp/Warranty" + j];
                        if (string.IsNullOrEmpty(PickupWarranty))
                        {
                            PickupWarranty = "";
                        }
                        else
                            PickupWarranty = Regex.Match(PickupWarranty, @"\d+").Value;

                        var WarrantyDesc = Request.Form["txtPWarrantyDesc" + vendorType.ToUpper() + j];
                        if (string.IsNullOrEmpty(WarrantyDesc))
                        {
                            WarrantyDesc = "";
                        }

                        string ET = Request.QueryString["ET"];

                        if (!string.IsNullOrEmpty(ET) && (ET == "CO" || ET == "CURRORD"))
                        {
                            var Model = Request.Form["txtModel" + i];
                            if (string.IsNullOrEmpty(Model))
                                Model = "";

                            str += "Model" + j + ":!" + Model + " ~";
                        }
                        str += "P" + j + " " + vendorType.ToUpper() + " Price Removal:!" + Removal + "~P" + j + " " + vendorType.ToUpper() + " Price Install:!" + Inst + "~ P" + j + " " + vendorType.ToUpper() + " Comment:!" + Comment + "~ P" + j + " " + vendorType.ToUpper() + " Price Delivery:!" + Delivery + "~ P" + j + " " + vendorType.ToUpper() + " Price Pick Up:!" + Pickup + "~";
                        str += "P" + j + " " + vendorType.ToUpper() + " Price Warranty:!" + WarrantyCost + "~P" + j + " " + vendorType.ToUpper() + " Warranty Desc:!" + WarrantyDesc + "~";
                    }
                    else
                    {
                        string DirectOrderWarrantyCost = "";

                        var Brand = Request.Form["ddlBrand" + i];
                        if (String.IsNullOrEmpty(Brand))
                        {
                            Brand = Request.Form["hfddlBrand" + i];
                            if (!string.IsNullOrEmpty(Brand))
                            {
                                for (int k = 1; k <= 10; k++)
                                {
                                    string udfKey = GetUdfCSCaseKey("Brand" + k);
                                    AddTableUDFItems(udfKey, Brand);
                                }
                            }
                        }

                        var Product = Request.Form["ddlProduct" + i];
                        if (String.IsNullOrEmpty(Product))
                        {
                            Product = Request.Form["hfddlProduct" + i];
                            if (!string.IsNullOrEmpty(Product))
                            {
                                for (int k = 1; k <= 10; k++)
                                {
                                    string udfKey = GetUdfCSCaseKey("Product" + k);
                                    AddTableUDFItems(udfKey, Product);
                                }
                            }

                        }
                        //VendorOrderNumber2
                        var VendorOrderNumber = Request.Form["txtVendorOrderNumber" + i];
                        if (string.IsNullOrEmpty(VendorOrderNumber))
                        {
                            VendorOrderNumber = "";
                        }
                        string Revenue = "";
                        var Model = Request.Form["txtModel" + i];
                        if (string.IsNullOrEmpty(Model))
                            Model = "";

                        var QuotePrice = Request.Form["txtQuotedPrice" + i];
                        if (string.IsNullOrEmpty(QuotePrice))
                            QuotePrice = "";
                        else
                            QuotePrice = Regex.Match(QuotePrice, @"\d+").Value;

                        Revenue = QuotePrice;
                        var PayBy = Request.Form["ddlPayBY" + i];
                        if (string.IsNullOrEmpty(PayBy))
                            PayBy = "";

                        var Desc = Request.Form["txtDesc" + i];
                        if (string.IsNullOrEmpty(Desc))
                            Desc = "";

                        var PUDEL = Request.Form["ddlPickupOrDelivery" + i];
                        if (string.IsNullOrEmpty(PUDEL))
                            PUDEL = "";

                        var PickOrDelivery = Request.Form["ddlPickupOrDelivery" + i];
                        if (string.IsNullOrEmpty(PickOrDelivery))
                            PickOrDelivery = "";

                        string Options = "";
                        string DeliveryAddress = "";
                        string Warranty = "";
                        DeliveryAddress = Request.Form["txtDeliveryAddress" + i];
                        if (string.IsNullOrEmpty(DeliveryAddress))
                            DeliveryAddress = "";

                        var PostCode = Request.Form["txtDeliveryPostCode" + i];
                        if (string.IsNullOrEmpty(PostCode))
                            PostCode = "";



                        var CancelOrdered = Request.Form["hfOrderStatus" + i];
                        // var CancelOrdered = Request.Form["ddlCancelOrdered" + i];
                        if (string.IsNullOrEmpty(CancelOrdered))
                            CancelOrdered = "";

                        if (string.IsNullOrEmpty(CancelOrdered) && radFilterType.SelectedValue == "Direct Order" && hfSaveEnquiry.Value != "SaveOnlyData")
                            CancelOrdered = "Ordered";

                        var SubmitTo = Request.Form["ddlSubmitTo" + i];
                        if (string.IsNullOrEmpty(SubmitTo))
                        {
                            SubmitTo = Request.QueryString["Vendor"];
                        }

                        if (!string.IsNullOrEmpty(SubmitTo) && SubmitTo.Contains(","))
                        {
                            string[] arrVendor = SubmitTo.Split(',');
                            foreach (var item in arrVendor)
                            {
                                if (string.IsNullOrEmpty(item))
                                    continue;
                                sendEmailForVendors.Add(item);
                            }
                            submitToSingleVendor = false;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(SubmitTo))
                            {
                                sendEmailForVendors.Add(SubmitTo);

                                if (!string.IsNullOrEmpty(CompareVendortoItems))
                                {
                                    if (CompareVendortoItems != SubmitTo)
                                        submitToSingleVendor = false;
                                }
                                else
                                {
                                    CompareVendortoItems = SubmitTo;
                                }
                            }
                        }

                        //if (string.IsNullOrEmpty(PickOrDelivery))
                        //{
                        //    PickOrDelivery = Request.Form["hfPickupOrDeliveryy" + i];
                        //}
                        if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model) && !string.IsNullOrEmpty(PayBy) && !string.IsNullOrEmpty(SubmitTo))
                        {
                            TotalItemsAddedInEnquiry++;
                            if (CancelOrdered == "Cancelled - Too Competitive" || CancelOrdered == "Cancelled - Stock Issues" || CancelOrdered == "Cancelled by Vendor" || CancelOrdered == "Cancelled - Other")
                            {
                                CancelledItemsCount++;
                            }
                            else if (CancelOrdered == "Quoted")
                            {
                                QuotedItemsCount++;
                            }
                            else if (CancelOrdered == "Ordered")
                            {
                                OrderedItemsCount++;
                            }
                            else if (CancelOrdered == "Wait for Customer")
                            {
                                WaitForCustomerItemsCount++;
                            }

                            if (string.IsNullOrEmpty(CancelOrdered) || CancelOrdered == "Ordered" || CancelOrdered == "Quoted")
                            {
                                isFlagCancel = false;
                            }
                            if (RequestType == "CallCenter" && string.IsNullOrEmpty(CancelOrdered))
                            {
                                CompleteEnquiryByUSRep = false;
                            }
                            string vendorTypeValue = "";
                            if (RequestType == "Vendor")
                            {
                                vendorTypeValue = Request.QueryString["Vendor"].ToUpper() == "JBHIFI" ? "JBHIFI" : "GG";
                            }
                            else if (!string.IsNullOrEmpty(SubmitTo) && !SubmitTo.Contains(","))
                            {

                                if (SubmitTo.ToUpper() == "The Good Guys".ToUpper() || SubmitTo.ToUpper() == "GG".ToUpper())
                                {
                                    vendorTypeValue = "GG";
                                }
                                else if (SubmitTo.ToUpper() == "JBHIFI".ToUpper())
                                {
                                    vendorTypeValue = "JBHIFI";
                                }
                            }




                            //  str += "Brand" + j + ":" + Brand + "~  Product" + j + ":" + Product + "~ Quoted Price" + j + ":" + QuotePrice + "~ Model" + j + ":" + Model + "~ Pay By" + j + ":" + PayBy + "~ Pickup or Delivery" + j + ":" + PickOrDelivery + "~ Description" + j + ":" + Desc + "~ Submit To" + j + ":" + SubmitTo + "~ Delivery Address" + j + ":" + DeliveryAddress + "~ Vendor" + j + ":" + SubmitTo + "~ Options" + j + ":" + Options + "~";
                            str += "Brand" + j + ":!" + Brand + "~  Product" + j + ":!" + Product + "~ MP Price" + j + ":!" + QuotePrice + "~ Model" + j + ":!" + Model + "~ Pay By" + j + ":!" + PayBy + "~ MP Option" + j + ":!" + PickOrDelivery + "~ Suburb and Postcode" + j + ":!" + PostCode + "~ Description" + j + ":!" + Desc + "~ Submit To" + j + ":!" + SubmitTo + "~ Delivery Address" + j + ":!" + DeliveryAddress + "~ Vendor" + j + ":!" + SubmitTo + "~ Options" + j + ":!" + Options + "~";  // + "~ Enquiry Comments" + j + ":" + EnquiryComments + "~";
                            str += " P" + j + " Warranty:!" + Warranty + "~";
                            if (string.IsNullOrEmpty(RequestType) || RequestType == "CallCenter")
                            {
                                if (Request.Form["chkSubmitBackToVendor"] != null && Request.Form["chkSubmitBackToVendor"] == "on")
                                {
                                    str += "Ordered Item Status " + j + ":!" + "" + "~ P" + j + " Ordered From:! ~";
                                }
                                else
                                {
                                    str += "Ordered Item Status " + j + ":!" + CancelOrdered + "~";
                                }

                                var SeletectedItemTypeJBHIFI = Request.Form["hfSeletectedItemTypeJBHIFI" + j];
                                if (string.IsNullOrEmpty(SeletectedItemTypeJBHIFI))
                                    SeletectedItemTypeJBHIFI = "";
                                else
                                    SeletectedItemTypeJBHIFI = SeletectedItemTypeJBHIFI.TrimEnd(',');

                                str += "Item Price Tick Marked JBHIFI" + j + ":!" + SeletectedItemTypeJBHIFI + "~";

                                var SeletectedItemTypeGG = Request.Form["hfSeletectedItemTypeGG" + j];
                                if (string.IsNullOrEmpty(SeletectedItemTypeGG))
                                    SeletectedItemTypeGG = "";
                                else
                                    SeletectedItemTypeGG = SeletectedItemTypeGG.TrimEnd(',');

                                str += "Item Price Tick Marked GG" + j + ":!" + SeletectedItemTypeGG + "~";

                            }

                            #region Price Section

                            if (RequestType == "CallCenter")
                            {
                                var SubmitToVendor = "";
                                if (!string.IsNullOrEmpty(Request.QueryString["Vendor"]))
                                    SubmitToVendor = Request.QueryString["Vendor"];
                                else
                                    SubmitToVendor = Request.Form["ddlSubmitTo" + i];

                                var arrSubmitToVendor = SubmitToVendor.Split(',');
                                //Request.QueryString["Vendor"]
                                foreach (var vendorSubmit in arrSubmitToVendor)
                                {
                                    if (vendorSubmit.ToUpper() == "The Good Guys".ToUpper())
                                    {
                                        SubmitToVendor = "GG";
                                    }
                                    else if (vendorSubmit.ToUpper() == "JBHIFI".ToUpper())
                                    {
                                        SubmitToVendor = "JBHIFI";
                                    }
                                    var Comment = Request.Form["txtP" + SubmitToVendor.ToUpper() + "Comment" + j];
                                    var Pickup = Request.Form["txtP" + SubmitToVendor.ToUpper() + "PickUp" + j];
                                    if (string.IsNullOrEmpty(Pickup))
                                        Pickup = "";
                                    else
                                        Pickup = Regex.Match(Pickup, @"\d+").Value;

                                    var Delivery = Request.Form["txtP" + SubmitToVendor.ToUpper() + "PriceDelivery" + j];
                                    if (string.IsNullOrEmpty(Delivery))
                                        Delivery = "";
                                    else
                                        Delivery = Regex.Match(Delivery, @"\d+").Value;

                                    var Removal = Request.Form["txtPDelInstRem" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(Removal))
                                    {
                                        Removal = "";
                                    }
                                    else
                                        Removal = Regex.Match(Removal, @"\d+").Value;

                                    var DelRem = Request.Form["txtPDelRem" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(DelRem))
                                    {
                                        DelRem = "";
                                    }
                                    else
                                        DelRem = Regex.Match(DelRem, @"\d+").Value;

                                    var Inst = Request.Form["txtPInst" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(Inst))
                                        Inst = "";
                                    else
                                        Inst = Regex.Match(Inst, @"\d+").Value;

                                    var WarrantyCost = Request.Form["txtPWarranty" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(WarrantyCost))
                                    {
                                        WarrantyCost = "";
                                    }
                                    else
                                        WarrantyCost = Regex.Match(WarrantyCost, @"\d+").Value;

                                    var DelInstRemWarranty = Request.Form["DelInstRemWarranty" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(DelInstRemWarranty))
                                    {
                                        DelInstRemWarranty = "";
                                    }
                                    else
                                        DelInstRemWarranty = Regex.Match(DelInstRemWarranty, @"\d+").Value;

                                    var DelInstWarranty = Request.Form["txtPDelInstWarranty" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(DelInstWarranty))
                                    {
                                        DelInstWarranty = "";
                                    }
                                    else
                                        DelInstWarranty = Regex.Match(DelInstWarranty, @"\d+").Value;

                                    var DelRemWarranty = Request.Form["txtPDelRemWarranty" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(DelRemWarranty))
                                    {
                                        DelRemWarranty = "";
                                    }
                                    else
                                        DelRemWarranty = Regex.Match(DelRemWarranty, @"\d+").Value;

                                    var PickupWarranty = Request.Form["txtP" + SubmitToVendor.ToUpper() + "PickUp/Warranty" + j];
                                    if (string.IsNullOrEmpty(PickupWarranty))
                                    {
                                        PickupWarranty = "";
                                    }
                                    else
                                        PickupWarranty = Regex.Match(PickupWarranty, @"\d+").Value;

                                    var WarrantyDesc = Request.Form["txtPWarrantyDesc" + SubmitToVendor.ToUpper() + j];
                                    if (string.IsNullOrEmpty(WarrantyDesc))
                                    {
                                        WarrantyDesc = "";
                                    }


                                    str += "P" + j + " " + SubmitToVendor.ToUpper() + " Price Removal:!" + Removal + "~P" + j + " " + SubmitToVendor.ToUpper() + " Price Install:!" + Inst + "~ P" + j + " " + SubmitToVendor.ToUpper() + " Comment:!" + Comment + "~ P" + j + " " + SubmitToVendor.ToUpper() + " Price Delivery:!" + Delivery + "~ P" + j + " " + SubmitToVendor.ToUpper() + " Price Pick Up:!" + Pickup + "~";
                                    str += "P" + j + " " + SubmitToVendor.ToUpper() + " Price Warranty:!" + WarrantyCost + "~P" + j + " " + SubmitToVendor.ToUpper() + " Warranty Desc:!" + WarrantyDesc + "~";

                                }
                            }

                            #endregion
                            strDesc += "[Brand Name:!" + Brand + "Product Name:!" + Product + "Model: " + Model + "Suburb and Postcode: " + PostCode + "],";


                            if (radFilterType.SelectedValue == "Direct Order" || RequestType == "Vendor")
                            {
                                if (string.IsNullOrEmpty(vendorType) && !string.IsNullOrEmpty(RequestType))
                                {
                                    if (SubmitTo.ToUpper() == "The Good Guys".ToUpper())
                                    {
                                        vendorType = "GG";
                                    }
                                    else if (SubmitTo.ToUpper() == "JBHIFI".ToUpper())
                                    {
                                        vendorType = "JBHIFI";
                                    }
                                }
                                else
                                {
                                    vendorType = vendorTypeValue;
                                }


                                var Comment = Request.Form["txtP" + vendorType.ToUpper() + "Comment" + j];
                                if (string.IsNullOrEmpty(Comment))
                                    Comment = "";

                                var Pickup = Request.Form["txtP" + vendorType.ToUpper() + "PickUp" + j];
                                if (string.IsNullOrEmpty(Pickup))
                                    Pickup = "";
                                else
                                    Pickup = Regex.Match(Pickup, @"\d+").Value;

                                var Delivery = Request.Form["txtP" + vendorType.ToUpper() + "PriceDelivery" + j];
                                if (string.IsNullOrEmpty(Delivery))
                                    Delivery = "";
                                else
                                    Delivery = Regex.Match(Delivery, @"\d+").Value;

                                var Removal = Request.Form["txtPDelInstRem" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(Removal))
                                {
                                    Removal = "";
                                }
                                else
                                    Removal = Regex.Match(Removal, @"\d+").Value;

                                var DelRem = Request.Form["txtPDelRem" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(DelRem))
                                {
                                    DelRem = "";
                                }
                                else
                                    DelRem = Regex.Match(DelRem, @"\d+").Value;

                                var Inst = Request.Form["txtPInst" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(Inst))
                                    Inst = "";
                                else
                                    Inst = Regex.Match(Inst, @"\d+").Value;

                                var WarrantyCost = Request.Form["txtPWarranty" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(WarrantyCost))
                                {
                                    WarrantyCost = "";
                                }
                                else
                                    WarrantyCost = Regex.Match(WarrantyCost, @"\d+").Value;

                                var DelInstRemWarranty = Request.Form["DelInstRemWarranty" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(DelInstRemWarranty))
                                {
                                    DelInstRemWarranty = "";
                                }
                                else
                                    DelInstRemWarranty = Regex.Match(DelInstRemWarranty, @"\d+").Value;

                                var DelInstWarranty = Request.Form["txtPDelInstWarranty" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(DelInstWarranty))
                                {
                                    DelInstWarranty = "";
                                }
                                else
                                    DelInstWarranty = Regex.Match(DelInstWarranty, @"\d+").Value;

                                var DelRemWarranty = Request.Form["txtPDelRemWarranty" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(DelRemWarranty))
                                {
                                    DelRemWarranty = "";
                                }
                                else
                                    DelRemWarranty = Regex.Match(DelRemWarranty, @"\d+").Value;

                                var PickupWarranty = Request.Form["txtP" + vendorType.ToUpper() + "PickUp/Warranty" + j];
                                if (string.IsNullOrEmpty(PickupWarranty))
                                {
                                    PickupWarranty = "";
                                }
                                else
                                    PickupWarranty = Regex.Match(PickupWarranty, @"\d+").Value;

                                var WarrantyDesc = Request.Form["txtPWarrantyDesc" + vendorType.ToUpper() + j];
                                if (string.IsNullOrEmpty(WarrantyDesc))
                                {
                                    WarrantyDesc = "";
                                }

                                str += "P" + j + " " + vendorType.ToUpper() + " Price Removal:!" + Removal + "~P" + j + " " + vendorType.ToUpper() + " Price Install:!" + Inst + "~ P" + j + " " + vendorType.ToUpper() + " Comment:!" + Comment + "~ P" + j + " " + vendorType.ToUpper() + " Price Delivery:!" + Delivery + "~ P" + j + " " + vendorType.ToUpper() + " Price Pick Up:!" + Pickup + "~";
                                str += "P" + j + " " + vendorType.ToUpper() + " Price Warranty:!" + WarrantyCost + "~P" + j + " " + vendorType.ToUpper() + " Warranty Desc:!" + WarrantyDesc + "~";


                                Revenue = !string.IsNullOrEmpty(Revenue) ? Revenue : "0";
                                var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + j + " Ordered From"), oCSCase.Key);
                                if (!string.IsNullOrEmpty(OrderFrom))
                                {
                                    VendorPlaceOrder.Value = OrderFrom;
                                }
                                else
                                {
                                    VendorPlaceOrder.Value = SubmitTo;
                                    if (radFilterType.SelectedValue == "Direct Order")
                                    {
                                        if (VendorPlaceOrder.Value == "GG" || VendorPlaceOrder.Value == "The Good Guys")
                                        {
                                            oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("P" + j + " Ordered From"), oCSCase.Key, "The Good Guys");
                                        }
                                        else
                                        {
                                            oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("P" + j + " Ordered From"), oCSCase.Key, "JBHIFI");
                                        }
                                    }
                                }

                                if (hfSaveEnquiry.Value != "SaveOnlyData")
                                {
                                    //Create Opportunity
                                    string currentKey = Request.QueryString["CurrentKey"];
                                    Opportunity opp = new Opportunity();
                                    if (RequestType == "Vendor" && !string.IsNullOrEmpty(OppKey))
                                    {
                                        if (!string.IsNullOrEmpty(RevenuePrice.Value)) //need to update with product price or other price options, delivery, install, removal
                                            Revenue = Regex.Match(RevenuePrice.Value, @"\d+").Value;


                                        string UDFKey = GetUdfOppKey("Submit To");
                                        string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");


                                        Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("KEY(" + OppKey + ")");

                                        foreach (Maximizer.Data.Opportunity oOPp in OppList)
                                        {
                                            //
                                            opp = oOPp;
                                            opp.ActualRevenue.Value = Convert.ToDouble(Revenue);
                                            opp.ForecastRevenue.Value = Convert.ToDouble(Revenue);
                                            if (opp.Status.Value != 5)
                                            {
                                                opp.Status.Value = 0;//New
                                            }
                                            opp.Comment.Value = "Brand Name:" + Brand + "Product Name:" + Product + "Model:" + Model + "Suburb and Postcode:" + PostCode;
                                            oOppAccess.Update(opp);

                                        }
                                    }
                                    else
                                    {
                                        opp = new Opportunity(currentKey);
                                        opp.Status.Value = 0;//New
                                        opp.Comment.Value = "Brand Name:" + Brand + "Product Name:" + Product + "Model:" + Model + "Suburb and Postcode:" + PostCode;
                                        opp.Objective.Value = "Direct Order -" + oCSCase.CaseNumber.Value + " from " + VendorPlaceOrder.Value;
                                        //  opp.ForecastRevenue.Value = Convert.ToDouble(Revenue);
                                        oOppAccess.Insert(opp);

                                        string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                                        var createDate = DateTime.Now.ToString("yyyy-MM-dd");
                                        oUDFAccess.SetFieldValue(UDFCreatedDateKey, opp.Key, createDate);
                                    }

                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Vendor Order Number"), opp.Key, VendorOrderNumber);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("ItemNumber"), opp.Key, j.ToString());

                                    //this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Vendor Order Number"), opp.Key, (oCSCase.CaseNumber.Value).Replace("US","UN"));
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("ItemNumber"), opp.Key, j.ToString());
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("CaseNumber"), opp.Key, oCSCase.CaseNumber.Value);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Brand Name"), opp.Key, Brand);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Product Name"), opp.Key, Product);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Model"), opp.Key, Model);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Submit To"), opp.Key, SubmitTo);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Pay By"), opp.Key, PayBy);
                                    //this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Pickup or Delivery"), opp.Key, PickOrDelivery);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Warranty Cost"), opp.Key, DirectOrderWarrantyCost);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Member Price Desc"), opp.Key, QuotePrice);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Description"), opp.Key, Desc);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Delivery Address"), opp.Key, DeliveryAddress);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Suburb and Postcode"), opp.Key, PostCode);
                                    //if (PickOrDelivery == "Delivery")
                                    //{
                                    //    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Options"), opp.Key, Options);
                                    //}

                                    string UnionId = oUDFAccess.GetFieldValue(this.GetUdfKey("Union ID"), currentKey);
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Union ID"), opp.Key, UnionId);

                                    if (VendorPlaceOrder.Value == "GG" || VendorPlaceOrder.Value == "The Good Guys")
                                    {
                                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("TheGoodGuys Comment"), opp.Key, Convert.ToString(Comment));
                                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Ordered From"), opp.Key, "The Good Guys");
                                    }
                                    else
                                    {
                                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("JBHIFI Comment"), opp.Key, Convert.ToString(Comment));
                                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Ordered From"), opp.Key, VendorPlaceOrder.Value);
                                    }

                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Product Price Delivery"), opp.Key, Convert.ToString(Delivery));
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Product Price Pick Up"), opp.Key, Convert.ToString(Pickup));
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("+Inst"), opp.Key, !string.IsNullOrEmpty(Inst) ? Inst : "0");
                                    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("+Rem"), opp.Key, !string.IsNullOrEmpty(Removal) ? Removal : "0");

                                    if (radFilterType.SelectedValue == "Direct Order")
                                    {
                                        if (SubmitTo == "JBHiFI,The Good Guys")
                                        {
                                            OrderedOppKeyGG += opp.Key + "-" + j + ",";
                                            OrderItemNumberGG += j + ",";

                                            OrderedOppKeyJBHIFI += opp.Key + "-" + j + ",";
                                            OrderItemNumberJBHIFI += j + ",";
                                        }
                                        else if (SubmitTo.Contains("The Good Guys"))
                                        {
                                            OrderedOppKeyGG += opp.Key + "-" + j + ",";
                                            OrderItemNumberGG += j + ",";
                                        }
                                        else if (SubmitTo.Contains("JBHiFI"))
                                        {
                                            OrderedOppKeyJBHIFI += opp.Key + "-" + j + ",";
                                            OrderItemNumberJBHIFI += j + ",";
                                        }
                                    }
                                    //Create Note
                                    Note oNote = new Note(opp.Key);
                                    oNote.Text.Value = str;
                                    oNoteAccess.Insert(oNote);
                                }

                            }
                        }

                    }
                }
                string CaseTstatus = "";
                if (oCSCase != null)
                {
                    if (Request.Form["chkSubmitBackToVendor"] != null && Request.Form["chkSubmitBackToVendor"] == "on")
                    {
                        CaseTstatus = oCSCase.StatusName.Value;
                        oCSCase.Status.Value = GetCSCaseStausKey("Waiting on Vendor");
                        InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewEnquiryEmail"]);
                    }
                    else
                    {
                        if (radFilterType.SelectedValue == "Direct Enquiry")
                        {
                            var enqType = oDetailFieldAccess.GetFieldValue(60025, oCSCase.Key);
                            if (!string.IsNullOrEmpty(enqType))
                            {
                                enqType = oDetailFieldAccess.GetFieldValue(60026, oCSCase.Key);
                            }
                            if (enqType == "Electrical Enquiry")
                            {
                                oCSCase.Subject.Value = "Electrical Enquiry";

                                oDetailFieldAccess.SetFieldValue(60025, oCSCase.Key, "Electrical Enquiry");
                                oDetailFieldAccess.SetFieldValue(60026, oCSCase.Key, "Electrical Enquiry");
                            }
                            else
                            {
                                oCSCase.Subject.Value = "General Enquiry";

                                oDetailFieldAccess.SetFieldValue(60025, oCSCase.Key, "Enquiry");
                                oDetailFieldAccess.SetFieldValue(60026, oCSCase.Key, "Quote");
                            }
                            string CaseNo = Request.QueryString["CaseNo"];
                            if (string.IsNullOrEmpty(CaseNo))
                            {
                                // oCSCase.Status.Value = 1;//Waiting
                                if (!string.IsNullOrEmpty(hfSaveEnquiry.Value))
                                {
                                    oCSCase.Status.Value = GetCSCaseStausKey("Saved");

                                }
                                else
                                {
                                    CaseTstatus = oCSCase.StatusName.Value;
                                    oCSCase.Status.Value = GetCSCaseStausKey("Waiting on Vendor");
                                    InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewEnquiryEmail"]);
                                }

                            }

                            else if (string.IsNullOrEmpty(hfSaveEnquiry.Value) && oCSCase.StatusName.Value == "Saved")
                            {
                                CaseTstatus = oCSCase.StatusName.Value;
                                oCSCase.Status.Value = GetCSCaseStausKey("Waiting on Vendor");
                                InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewEnquiryEmail"]);
                            }
                            else if (RequestType == "CallCenter" && TotalItemsAddedInEnquiry == CancelledItemsCount)
                            {
                                CaseTstatus = oCSCase.StatusName.Value;
                                oCSCase.Status.Value = GetCSCaseStausKey("Cancelled");

                            }
                            else if (RequestType == "CallCenter" && WaitForCustomerItemsCount > 0)
                            {
                                CaseTstatus = oCSCase.StatusName.Value;
                                oCSCase.Status.Value = GetCSCaseStausKey("Wait for customer");

                            }
                            else if (RequestType == "CallCenter" && QuotedItemsCount > 0 && CancelledItemsCount > 0 && TotalItemsAddedInEnquiry == (QuotedItemsCount + CancelledItemsCount))
                            {
                                CaseTstatus = oCSCase.StatusName.Value;
                                oCSCase.Status.Value = GetCSCaseStausKey("Complete");

                            }
                            else if (RequestType == "CallCenter" && WaitForCustomerItemsCount == 0 && QuotedItemsCount > 0)
                            {
                                CaseTstatus = oCSCase.StatusName.Value;
                                oCSCase.Status.Value = GetCSCaseStausKey("Quoted");

                            }
                            else if (CompleteEnquiryByUSRep && RequestType == "CallCenter")
                            {
                                CaseTstatus = oCSCase.StatusName.Value;
                                oCSCase.Status.Value = GetCSCaseStausKey("Complete");

                            }
                        }
                        else
                        {
                            oCSCase.Subject.Value = "Direct Order";
                            oDetailFieldAccess.SetFieldValue(60025, oCSCase.Key, "Direct Order");
                            oDetailFieldAccess.SetFieldValue(60026, oCSCase.Key, "Direct Order");
                            if (isFlagCancel && RequestType == "CallCenter")
                            {
                                //oCSCase.Status.Value = 4;//Cancel
                                if (!string.IsNullOrEmpty(hfSaveEnquiry.Value))
                                {
                                    oCSCase.Status.Value = GetCSCaseStausKey("Saved");
                                }
                                else
                                {
                                    CaseTstatus = oCSCase.StatusName.Value;
                                    oCSCase.Status.Value = GetCSCaseStausKey("Cancelled");
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(hfSaveEnquiry.Value))
                                {
                                    oCSCase.Status.Value = GetCSCaseStausKey("Saved");
                                }
                                else if (CompleteEnquiryByUSRep && (RequestType == "CallCenter" || string.IsNullOrEmpty(RequestType)))
                                {
                                    CaseTstatus = oCSCase.StatusName.Value;
                                    oCSCase.Status.Value = GetCSCaseStausKey("Complete");
                                }
                                else
                                {
                                    CaseTstatus = oCSCase.StatusName.Value;
                                    if (CaseTstatus != "Complete")
                                        oCSCase.Status.Value = GetCSCaseStausKey("Pending");

                                    InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewEnquiryEmail"]);
                                }
                            }
                        }



                        if (!string.IsNullOrEmpty(oCSCase.Description.Value))
                        {
                            oCSCase.Description.Value = strDesc.TrimEnd(',');
                        }
                    }
                    oCSCaseAccess.Update(oCSCase);
                    //Set Product Services to .. Union Shopper .. Category Enquiry or Direct Order
                    oDetailFieldAccess.SetFieldValue(60028, oCSCase.Key, "Union Shopper");

                }


                sLoginString = loginToMax();
                string[] arr = str.Split('~');
                foreach (var item in arr)
                {
                    if (string.IsNullOrEmpty(item))
                        continue;

                    try
                    {
                        string[] arrItems = item.Split(new string[] { ":!" }, StringSplitOptions.None);
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey(arrItems[0].Trim()), Key, string.IsNullOrEmpty(Convert.ToString(arrItems[1])) ? "" : Convert.ToString(arrItems[1]));
                    }
                    catch (Exception ex)
                    {
                        common.LogError(ex, item, "SetUDF, Case Number:-" + oCSCase.CaseNumber);
                        continue;
                    }
                }
                oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Mode"), Key, radFilterType.SelectedValue);



                if (!string.IsNullOrEmpty(vendor))
                {
                    if (Request.Form["chkSubmitBackToVendor"] != null && Request.Form["chkSubmitBackToVendor"] == "on")
                    {
                        this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Updated by TheGoodGuys"), oCSCase.Key, "No");
                        this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Updated by JBHIFI"), oCSCase.Key, "No");
                    }
                    else if (vendor.ToUpper() == "GG" && CaseTstatus != "Saved")
                    {
                        this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Updated by TheGoodGuys"), oCSCase.Key, "Yes");
                    }
                    else if (vendor.ToUpper() == "JBHIFI" && CaseTstatus != "Saved")
                    {
                        this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Updated by JBHIFI"), oCSCase.Key, "Yes");

                    }

                    sLoginString = loginToMax();
                    string strUpdatedByJBHIHI = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by JBHIFI"), oCSCase.Key);
                    string strUpdatedByTheGoodGuys = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by TheGoodGuys"), oCSCase.Key);
                    //string SubmitToMax = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Submit To"), oCSCase.Key);

                    if (EnquiryType == "CE")
                        oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Enquiry Updated"), oCSCase.Key, "Yes");
                    //Clicking 'Update' button on 'Vendor Feedback page does not hide the button. It is only hidden when 'Confirm Order' button is pressed.
                    //else if (EnquiryType == "CO")
                    //    oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Order Updated"), oCSCase.Key, "Yes");


                    if (strUpdatedByJBHIHI == "Yes" && strUpdatedByTheGoodGuys == "Yes" && CaseTstatus != "Saved")
                    {
                        //current Order for Not allow to updateEnquiry status 
                        if (EnquiryType != "CURRORD" && oCSCase.Status.Value != GetCSCaseStausKey("Complete"))
                        {
                            if (Request.Form["chkSubmitBackToVendor"] != null && Request.Form["chkSubmitBackToVendor"] == "on")
                            {
                                InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewEnquiryEmail"]);
                                oCSCase.Status.Value = GetCSCaseStausKey("Waiting on Vendor");
                            }
                            else
                            {
                                InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewEnquiryEmail"]);
                                //oCSCase.Status.Value = 57996;//call Back
                                if (!string.IsNullOrEmpty(hfSaveEnquiry.Value))
                                {
                                    oCSCase.Status.Value = GetCSCaseStausKey("Saved");
                                }
                                else
                                {
                                    //if (oCSCase.StatusName.Value != "Complete")
                                    //{
                                    if (radFilterType.SelectedValue == "Direct Enquiry")
                                    {
                                        oCSCase.Status.Value = GetCSCaseStausKey("Call Back");
                                    }
                                    //}
                                }
                            }

                            oCSCaseAccess.Update(oCSCase);
                        }
                    }
                    else if (submitToSingleVendor && (strUpdatedByJBHIHI == "Yes" || strUpdatedByTheGoodGuys == "Yes") && CaseTstatus != "Saved")
                    {
                        if (Request.Form["chkSubmitBackToVendor"] != null && Request.Form["chkSubmitBackToVendor"] == "on")
                        {
                            oCSCase.Status.Value = GetCSCaseStausKey("Waiting on Vendor");
                            InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewEnquiryEmail"]);
                        }
                        else
                        {
                            if (radFilterType.SelectedValue == "Direct Enquiry")
                            {
                                oCSCase.Status.Value = GetCSCaseStausKey("Call Back");
                            }
                        }
                        oCSCaseAccess.Update(oCSCase);
                    }





                }
                else if (Request.Form["chkSubmitBackToVendor"] != null && Request.Form["chkSubmitBackToVendor"] == "on")
                {
                    this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Updated by TheGoodGuys"), oCSCase.Key, "No");
                    this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Updated by JBHIFI"), oCSCase.Key, "No");
                }

                if (string.IsNullOrEmpty(Mode))
                {
                    if (sendEmailForVendors.Count > 0)
                    {
                        foreach (var item in sendEmailForVendors.Distinct())
                        {
                            if (!String.IsNullOrEmpty(item))
                            {
                                string CaseNo = Request.QueryString["CaseNo"];
                                string UpdateSubject = "";
                                if (!string.IsNullOrEmpty(CaseNo))
                                {
                                    UpdateSubject = "Updated ";
                                }
                                string OrderItemNumber = OrderItemNumberJBHIFI;
                                string OrderedOppKey = OrderedOppKeyJBHIFI;
                                string selectedValue = item.ToUpper();
                                if (item.ToUpper() == "The Good Guys".ToUpper() || item.ToUpper() == "GG")
                                {
                                    OrderItemNumber = OrderItemNumberGG;
                                    OrderedOppKey = OrderedOppKeyGG;
                                    selectedValue = "GG";


                                }
                                string LiveUrl = WebConfigurationManager.AppSettings["LiveURl"];






                                //101.179.13.240
                                //string Url = "http://101.177.144.86:5050/US/Dialogs/CustomDialogs/CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&CaseNo=" + oCSCase.CaseNumber.Value;
                                string Url = LiveUrl + "/Dialogs/CustomDialogs/CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&Vendor=" + selectedValue + "&CaseNo=" + oCSCase.CaseNumber.Value + "&RequestType=Email";
                                string strEmailBody = "";

                                if (radFilterType.SelectedValue == "Direct Enquiry")
                                {
                                    strEmailBody += "<html><head></head><body><div class='gmail_quote'><p>Dear " + item + ",</p>";
                                    strEmailBody += "<p>We have received an enquiry from one of our members for a quote on a product you may stock. </p>";
                                    strEmailBody += "<p>Please click the below link to view the enquiry</p><p><a href='" + Url + "' target='_blank' >Click To View Enquiry</a></p>";
                                    strEmailBody += "<br><p style='color:red;'><b>*Please don't reply to this email as it is not monitored<b></p>";
                                    //strEmailBody += "<br>Please proceed with the following order"+ItemsForEmail;

                                    strEmailBody += "</div></body></html>";

                                    //sendMail("quotes@unionshopper.com.au", "", oCSCase.CaseNumber.Value + " - " + UpdateSubject + "Quote Enquiry For " + selectedValue, strEmailBody);
                                }
                                else if (radFilterType.SelectedValue == "Direct Order")
                                {
                                    // Url = LiveUrl + "/Dialogs/CustomDialogs/DirectOrderConfirm.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&Vendor=" + selectedValue + "&CaseNo=" + oCSCase.CaseNumber.Value;
                                    Url = LiveUrl + "/Dialogs/CustomDialogs/CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&Vendor=" + selectedValue + "&CaseNo=" + oCSCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + OrderItemNumber.TrimEnd(',') + "&OppKeys=" + OrderedOppKey.TrimEnd(',');
                                    strEmailBody += "<html><head></head><body><div class='gmail_quote'><p>Dear " + item + ",</p>";
                                    strEmailBody += "<p style='margin: 9px 40px;'><b>Union Shopper Member:</b>" + lblMember.Text + "</p>";
                                    strEmailBody += "<p style='margin: 9px 40px;'><b>Name:</b>" + lbluser.Text + "</p>";
                                    strEmailBody += "<p style='margin: 9px 40px;'><b>Phone:</b>" + lblMobileNumber.Text + "</p>";
                                    strEmailBody += "<p style='margin: 9px 40px;'><b>Address:</b>" + lblAddress.Text + "</p>";
                                    strEmailBody += "<p>Please <a href='" + Url + "' target='_blank' >click</a>  here to confirm order has been received</p><p></p>";
                                    //strEmailBody += "<p>Would like to order:</p>";
                                    //strEmailBody += "<p>Please proceed with the following order</p>";

                                    //strEmailBody += "<p>" + ItemsForEmail + "</p>";
                                    //strEmailBody += "<p>" + ItemsForEmail + "</p>";

                                    strEmailBody += " </div></body></html>";
                                    //sendMail("order@unionshopper.com.au", "ryan@crmpro.com.au", oCSCase.CaseNumber.Value + " - " + UpdateSubject + "Quote Enquiry For " + selectedValue, strEmailBody);
                                }
                            }
                            //}
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                common.LogError(ex, "CreateCSCase", "CreateEnquiry");
            }
            string RequestTypes = Request.QueryString["RequestType"];
            if (!string.IsNullOrEmpty(RequestTypes) && (RequestTypes == "Email" || RequestTypes == "CallCenter"))
            {
                if (hfManageCancelOrderStatus.Value == "true")
                {
                    Response.Redirect(Request.RawUrl);
                }
                else if (RequestTypes == "CallCenter")
                {
                    if (isSavedTypeEnquiry == "Saved" || (Request.Form["chkSubmitBackToVendor"] != null && Request.Form["chkSubmitBackToVendor"] == "on"))
                    {
                        Response.Redirect("ThankYou.aspx");
                    }
                    Response.Redirect(Request.RawUrl);
                }
                else if (RequestTypes == "Email")
                {
                    Response.Redirect("VendorEnquiries.aspx?CurrentKey" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&RequestType=CallCenter&Vendor=" + Request.QueryString["Vendor"] + "&ET=" + Request.QueryString["ET"]);
                    //Response.Redirect("ThankYouUpdating.aspx");
                    // ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.open('close.html', '_self', null);", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseWindow();", true);
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "none", "<script>window.open('', '_self', ''); window.close();</script>", false);
                    // Page.ClientScript.RegisterStartupScript(GetType(), "msgbox", "alert('Enquiry updated successfully');javascript: window.opener = ' '; window.close();", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "none", "window.open('', '_self', '');window.close(); ", true);
                }

            }
            else if (!string.IsNullOrEmpty(RequestTypes) && RequestTypes == "Vendor" && !string.IsNullOrEmpty(Request.QueryString["Items"]))
            {
                //reload for Vendor Feedback Page as @paul suggested
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                //Response.Redirect(Request.RawUrl);
                Response.Redirect("ThankYou.aspx");
                //if (string.IsNullOrEmpty(Request.QueryString["RequestType"]))
                //    Response.Redirect("MemberEnquiry.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&RequestType=CallCenter");
            }
            //ending here



        }

        private int AddTableUDFItems(string UDFKey, string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    value = value.Trim();
                    Maximizer.Data.UdfSetupList setupList;

                    setupList = oUDFAccess.ReadCSCaseUdfSetupList("KEY(" + UDFKey + ")", true);

                    foreach (Maximizer.Data.UdfSetupTable tbl in setupList)
                    {
                        if (!tbl.Items.Cast<Maximizer.Data.UdfSetupTableItem>().Where(x => x.Name.Value.ToLower() == value.ToLower()).Any())
                        {
                            //foreach (Maximizer.Data.UdfSetupTableItem item in tbl.Items)
                            //{
                            Maximizer.Data.UdfSetupTableItem newItemIns = new Maximizer.Data.UdfSetupTableItem();
                            // newItemIns = item;
                            newItemIns.Name.Value = value;
                            oUDFAccess.UdfSetupTableItemInsert(UDFKey, ref newItemIns);// inset Item

                            //oUDFAccess.UdfSetupTableItemUpdate(udfCSCaseKey, ref newItemIns);//UPdate Items
                            //}
                        }
                    }
                }
                return -1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
        public void sendMail(string fromaddress, string AssignedEmailAddress, string Subject, string Body)
        {
            try
            {
                List<MailAddress> lst = new List<MailAddress>();

                //if (!string.IsNullOrEmpty(cc))
                //{
                //	lst.Add(new MailAddress(cc));
                //}
                //lst.Add(new MailAddress("finance@agworkforce.com.au"));
                //lst.Add(new MailAddress("brent@crmpro.com.au"));

                if (!Subject.Contains("Error"))
                {
                    lst.Add(new MailAddress("paul@crmpro.com.au"));
                    //lst.Add(new MailAddress("rory.mcelwee@m1it.com.au"));
                    lst.Add(new MailAddress("ryan@crmpro.com.au"));
                }

                if (!string.IsNullOrEmpty(AssignedEmailAddress))
                    lst.Add(new MailAddress(AssignedEmailAddress));



                // Command line argument must the the SMTP host.
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.Host = "smtp.sendgrid.net";
                //client.Host = "smtp.vic.svc.cloud.telstra.com";

                client.Timeout = 10000;

                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("ryancrmpro2", "0wnzyourdick");
                //client.Credentials = new System.Net.NetworkCredential();

                MailMessage mm = new MailMessage();
                if (string.IsNullOrEmpty(fromaddress))
                {
                    fromaddress = "submit@agworkforce.com.au";
                }
                mm.From = new MailAddress(fromaddress);

                //add each email adress
                foreach (MailAddress m in lst)
                {
                    mm.To.Add(m);
                }
                mm.Bcc.Add("rajendra.dev18@gmail.com");

                mm.Subject = Subject;
                mm.Body = Body;

                mm.Headers.Add("Content-Type", "content=text/html; charset=\"UTF-8\"");
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.IsBodyHtml = true;

                //System.Net.Mail.Attachment attachment;
                //HttpFileCollectionBase uploadedFiles = Files;
                //for (int i = 0; i < uploadedFiles.Count; i++)
                //{
                //    HttpPostedFileBase userPostedFile = uploadedFiles[i];

                //try
                //{
                //	//if (userPostedFile.ContentLength > 0)
                //	//{
                //	//if (File.Exists(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + filename + ".pdf")))
                //	//{
                //	//	attachment = new System.Net.Mail.Attachment(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + filename + ".pdf"));
                //	//	attachment.Name = UserName + " CV Web " + DateTime.Now.ToString("dd-MM-yyyy") + ".pdf";
                //	//	mm.Attachments.Add(attachment);
                //	//}
                //	//switch (filetype)
                //	//{
                //	//	case "Single":
                //	//		{
                //	//			if (File.Exists(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + "cv_" + filename + extCV)))
                //	//			{
                //	//				attachment = new System.Net.Mail.Attachment(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + "cv_" + filename + extCV));
                //	//				attachment.Name = UserName + " CV " + DateTime.Now.ToString("dd-MM-yyyy") + extCV;
                //	//			}
                //	//			break;
                //	//		}
                //	//	case "Both":
                //	//		{
                //	//			if (File.Exists(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + "img_" + filename + extPhoto)))
                //	//			{
                //	//				attachment = new System.Net.Mail.Attachment(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + "img_" + filename + extPhoto));
                //	//				attachment.Name = UserName + " Photo " + DateTime.Now.ToString("dd-MM-yyyy") + extPhoto;
                //	//				mm.Attachments.Add(attachment);
                //	//			}
                //	//			if (File.Exists(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + "cv_" + filename + extCV)))
                //	//			{
                //	//				attachment = new System.Net.Mail.Attachment(ctx.Server.MapPath("~/fdat/txt_f_test/unproc/" + "cv_" + filename + extCV));
                //	//				attachment.Name = UserName + " CV " + DateTime.Now.ToString("dd-MM-yyyy") + extCV;
                //	//				mm.Attachments.Add(attachment);
                //	//			}
                //	//			break;
                //	//		}
                //	//}

                //	//attachment = new System.Net.Mail.Attachment(uploadFile);                
                //	//}
                //}

                //catch (Exception ex)
                //{
                //	this.LogError(ex);
                //	errorMsg = ex.Message;
                //}
                //}

                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);
            }
            catch (Exception ex)
            {
                //this.LogError(ex);
                //errorMsg = ex.Message;
            }
        }
        public void loadCSCase(string CaseNumber)
        {
            string CaseNo = Request.QueryString["CaseNo"];
            try
            {
                string ItemsNumber = "";

                string RequestType = Request.QueryString["RequestType"];
                hfRequestType.Value = RequestType;
                string BrandOption = OptionList("Brand1");
                string ProductOption = OptionList("Product1");
                string PayByOption = OptionList("Pay By1");
                //string PUDELOption = OptionList("Pickup or Delivery1");
                string PickOrDeliveryyOption = OptionList("Pickup or Delivery1");
                string SubmitToOption = OptionList("Submit To1");
                string DeliveyOptions = OptionList("Options1");

                string VendorType = Request.QueryString["Vendor"];
                hfVendorName.Value = VendorType;


                if (RequestType == "CallCenter")
                {
                    divCancelOrder.Style.Add("display", "block");
                }
                string ET = Request.QueryString["ET"];

                if (!string.IsNullOrEmpty(ET) && (ET == "CO" || ET == "CURRORD"))
                {
                    Page.Title = "Vendor Feedback";
                    lblMidEQ.Text = "Vendor Feedback";
                }
                else if (RequestType == "CallCenter" && !string.IsNullOrEmpty(VendorType) && !string.IsNullOrEmpty(CaseNo))
                {
                    Page.Title = "Union Shopper Review";
                    lblMidEQ.Text = "Union Shopper Review";
                }
                else if (!string.IsNullOrEmpty(VendorType) && RequestType != "CallCenter")
                {
                    Page.Title = "Vendor Update";
                    lblMidEQ.Text = "Vendor Update";
                }


                if (!string.IsNullOrEmpty(VendorType) && VendorType.ToLower() == "gg")
                {
                    VendorType = "The Good Guys";
                }
                string filter = "EQ(CaseNumber," + CaseNumber + ")";
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                foreach (CSCase oCSCase in oCSCaseList)
                {
                    //lblLoginUser
                    var User = oAbEntryAccess.GetLoggedInUser(sLoginString);
                    var Username = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Enquiry Opened with user"), oCSCase.Key);
                    var EQOpen = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Enquiry Status With User"), oCSCase.Key);
                    var EQOpenDate = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Enquiry Open DateTime"), oCSCase.Key);
                    bool flagAllowNewAccessEQ = false;
                    try
                    {
                        if (!string.IsNullOrEmpty(EQOpenDate))
                        {
                            var hours = (Convert.ToDateTime(EQOpenDate) - DateTime.Now).TotalHours;
                            if (hours >= 11)
                            {
                                flagAllowNewAccessEQ = true;
                            }
                        }
                        else
                        {
                            flagAllowNewAccessEQ = true;
                        }
                    }
                    catch (Exception ex)
                    {


                    }
                    if (flagAllowNewAccessEQ && (string.IsNullOrEmpty(RequestType) || RequestType == "CallCenter"))
                    {
                        lblLooginUserHD.Text = "";
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Enquiry Opened with user"), oCSCase.Key, User.DisplayName);
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Enquiry Status With User"), oCSCase.Key, "Open");
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Enquiry Open DateTime"), oCSCase.Key, DateTime.Now.ToString("yyyy-MM-dd hh:mm"));
                    }
                    else if (EQOpen == "Open" && Username != User.DisplayName && (string.IsNullOrEmpty(RequestType) || RequestType == "CallCenter"))
                    {
                        btnClosebrowser.Style.Add("display", "inline-block");
                        lblLoginUser.Text = Username;
                    }
                    else if (string.IsNullOrEmpty(RequestType) || RequestType == "CallCenter")
                    {
                        lblLooginUserHD.Text = "";
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Enquiry Opened with user"), oCSCase.Key, User.DisplayName);
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Enquiry Status With User"), oCSCase.Key, "Open");
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Enquiry Open DateTime"), oCSCase.Key, DateTime.Now.ToString("yyyy-MM-dd hh:mm"));
                    }
                    else
                    {
                        lblLooginUserHD.Text = "";
                    }
                    if (oCSCase.StatusName.Value == "Saved")
                    {
                        Page.Title = "Create Enquiry";
                        lblMidEQ.Text = "CREATE ENQUIRY";
                        hfSaveEnquiry.Value = "Saved";
                    }
                    //GetCSCaseStausKey("");
                    string strUpdatedByJBHIHI = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by JBHIFI"), oCSCase.Key);
                    string strUpdatedByTheGoodGuys = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by TheGoodGuys"), oCSCase.Key);
                    string EnquiryType = Request.QueryString["ET"];
                    if (EnquiryType == "CE")
                    {
                        btnBack.Style.Add("display", "inline-block");
                        btnCreateEnquiry.Visible = false;
                    }

                    if (RequestType == "Email" || EnquiryType == "CO" || EnquiryType == "CURRORD")
                        btnBack.Style.Add("display", "inline-block");

                    //else
                    //{
                    //    string EnquiryUpdated = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Enquiry Updated"), oCSCase.Key);
                    //    string OrderUpdated = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Order Updated"), oCSCase.Key);

                    //    if (VendorType == "The Good Guys")
                    //    {
                    //        if (strUpdatedByTheGoodGuys == "Yes" && (EnquiryUpdated == "Yes" || OrderUpdated == "Yes"))
                    //        {
                    //            btnCreateEnquiry.Visible = false;
                    //        }
                    //    }
                    //    else if (VendorType == "JBHIFI")
                    //    {
                    //        if (strUpdatedByJBHIHI == "Yes" && (EnquiryUpdated == "Yes" || OrderUpdated == "Yes"))
                    //        {
                    //            btnCreateEnquiry.Visible = false;
                    //        }
                    //    }
                    //}


                    string mode = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Mode"), oCSCase.Key);

                    if (!string.IsNullOrEmpty(mode))
                        radFilterType.Items.FindByValue(mode).Selected = true;
                    else
                        radFilterType.Items.FindByValue("Direct Enquiry").Selected = true;


                    string hidePlaceOrder = "style='display:block;'";
                    if (mode == "Direct Order" && (RequestType == "Email" || RequestType == "Vendor"))
                    {
                        hidePlaceOrder = "style='display:none'";
                        btnCreateEnquiry.Text = "SAVE QUOTE";
                    }


                    //string isUpdatedByJBHIHI = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by JBHIFI"), oCSCase.Key);
                    //string isUpdatedByTheGoodGuys = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by TheGoodGuys"), oCSCase.Key);
                    //if (!string.IsNullOrEmpty(RequestType))
                    //{
                    //    if ((RequestType == "GG" && isUpdatedByJBHIHI == "Yes") || (RequestType == "JBHIFI" && isUpdatedByTheGoodGuys == "Yes"))
                    //    {
                    //        btnCreateEnquiry.Enabled = true;
                    //    }
                    //}



                    string str = "";

                    txtEnquiryComments.Text = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Enquiry Comments"), oCSCase.Key);
                    string Items = Request.QueryString["Items"];
                    for (int i = 1; i <= 10; i++)
                    {
                        string VendorTypeURL = Request.QueryString["Vendor"];
                        var ddlcancel = "";
                        if (RequestType == "CallCenter")
                        {
                            ddlcancel += "<option value=''></option>";
                            ddlcancel += "<option value='Ordered'>Ordered</option>";
                            ddlcancel += "<option value='Quoted'>Quoted</option>";
                            ddlcancel += "<option value='Cancelled - Too Competitive'>Cancelled - Too Competitive</option>";
                            ddlcancel += "<option value='Cancelled - Stock Issues'>Cancelled - Stock Issues</option>";
                            ddlcancel += "<option value='Wait for Customer'>Wait for Customer</option>";
                            ddlcancel += "<option value='Cancelled by Vendor'>Cancelled by Vendor</option>";
                            ddlcancel += "<option value='Cancelled - Other'>Cancelled - Other</option>";


                            string cancelItemType = GetUdfCSCaseKey("Ordered Item Status " + i);
                            cancelItemType = this.oUDFAccess.GetFieldValue(cancelItemType, oCSCase.Key);
                            if (!string.IsNullOrEmpty(cancelItemType))
                            {
                                if (i == 1)
                                {
                                    hfOrderStatus.Value = cancelItemType;
                                    ddlCancelOrdered.Items.FindByValue(cancelItemType).Selected = true;
                                }
                                else
                                {
                                    ddlcancel = ddlcancel.Replace("<option value='" + cancelItemType + "'>" + cancelItemType + "</option>", "<option value='" + cancelItemType + "' selected='selected' >" + cancelItemType + "</option>");
                                }
                            }
                        }
                        string SubmitToudfKey = GetUdfCSCaseKey("Submit To" + i);
                        var vendor = this.oUDFAccess.GetFieldValue(SubmitToudfKey, oCSCase.Key);
                        if (string.IsNullOrEmpty(vendor))
                        {
                            var entype = oDetailFieldAccess.GetFieldValue(60025, oCSCase.Key);
                            if (!string.IsNullOrEmpty(entype) && entype == "Electrical Enquiry")
                            {

                            }
                            else
                            {

                                continue;
                            }
                        }
                        else if (!string.IsNullOrEmpty(Items) && !Items.Contains(i.ToString()))
                        {
                            if (i == 1)
                            {
                                item1.Attributes.Add("style", "display:none;");
                                addItemsDiv.Attributes.Add("style", "display:none;");

                                legendItem1.Attributes.Add("class", "hide");
                                continue;
                            }

                        }
                        else if (RequestType == "Email" && !string.IsNullOrEmpty(VendorType) && !vendor.ToLower().Contains(VendorType.ToLower()))
                        {
                            if (i == 1)
                            {
                                item1.Attributes.Add("style", "display:none;");
                                addItemsDiv.Attributes.Add("style", "display:none;");

                                legendItem1.Attributes.Add("class", "hide");
                            }
                            continue;
                        }
                        if (!string.IsNullOrEmpty(Items) && !Items.Contains(i.ToString()))
                        {
                            continue;
                        }
                        string display = "display:block;";
                        string col_div = "3";


                        string ProductudfKey = GetUdfCSCaseKey("Product" + i);
                        string BrandudfKey = GetUdfCSCaseKey("Brand" + i);
                        string ModeludfKey = GetUdfCSCaseKey("Model" + i);
                        string PayByudfKey = GetUdfCSCaseKey("Pay By" + i);
                        string WarrantyudfKey = GetUdfCSCaseKey("P" + i + " Warranty");
                        //string QuotedPriceudfKey = GetUdfCSCaseKey("Quoted Price" + i);
                        string QuotedPriceudfKey = GetUdfCSCaseKey("MP Price" + i);

                        string PUDELudfKey = GetUdfCSCaseKey("MP Option" + i);
                        string PostCodeKey = GetUdfCSCaseKey("Suburb and Postcode" + i);
                        string DeliveryAddressudfKey = GetUdfCSCaseKey("Delivery Address" + i);

                        string DescriptionudfKey = GetUdfCSCaseKey("Description" + i);

                        string OptionsKey = GetUdfCSCaseKey("Options" + i);


                        if ((string.IsNullOrEmpty(oUDFAccess.GetFieldValue(BrandudfKey, oCSCase.Key)) && string.IsNullOrEmpty(oUDFAccess.GetFieldValue(ProductudfKey, oCSCase.Key))))
                        {
                            var entype = oDetailFieldAccess.GetFieldValue(60025, oCSCase.Key);
                            if (!string.IsNullOrEmpty(entype) && entype == "Electrical Enquiry")
                            {

                            }
                            else
                            {
                                break;
                            }
                        }
                        string PUDELValue = oUDFAccess.GetFieldValue(PUDELudfKey, oCSCase.Key);
                        string WarrantyValue = oUDFAccess.GetFieldValue(WarrantyudfKey, oCSCase.Key);
                        string OptionsValue = oUDFAccess.GetFieldValue(OptionsKey, oCSCase.Key);
                        string PayByudfValue = oUDFAccess.GetFieldValue(PayByudfKey, oCSCase.Key);
                        string VendorOrderNumber = "";
                        if (RequestType == "Vendor")
                        {
                            string OppKey = "";
                            if (!string.IsNullOrEmpty(Items))
                            {
                                if (Items.Contains(i.ToString()))
                                {
                                    string[] arrOpp = null;
                                    var OppKeys = Request.QueryString["OppKeys"];
                                    if (!string.IsNullOrEmpty(OppKeys))
                                    {
                                        arrOpp = OppKeys.Split(',');
                                        OppKey = arrOpp.Where(x => x.Contains("-" + i.ToString())).FirstOrDefault();
                                        OppKey = OppKey.Replace("-" + i.ToString(), "");
                                        VendorOrderNumber = oUDFAccess.GetFieldValue(this.GetUdfOppKey("Vendor Order Number"), OppKey);


                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(OppKey))
                            {
                                Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("KEY(" + OppKey + ")");

                                foreach (Maximizer.Data.Opportunity oOPp in OppList)
                                {
                                    string OrderReason = oUDFAccess.GetFieldValue(GetUdfOppKey("Order Reason"), oOPp.Key);
                                    if (!string.IsNullOrEmpty(OrderReason))
                                    {
                                        ddlCancelReason.SelectedValue = OrderReason;
                                    }
                                }
                            }
                        }

                        if (i == 1)
                        {
                            hfddlBrand.Value = oUDFAccess.GetFieldValue(BrandudfKey, oCSCase.Key);
                            hfddlProduct.Value = oUDFAccess.GetFieldValue(ProductudfKey, oCSCase.Key);
                            ddlProduct.Items.FindByValue(hfddlProduct.Value).Selected = true;
                            ddlBrand.Items.FindByValue(hfddlBrand.Value).Selected = true;
                            //ddlPayBY.Items.FindByValue(oUDFAccess.GetFieldValue(PayByudfKey, oCSCase.Key)).Selected = true; 
                            ddlPickupOrDelivery.Items.FindByValue(PUDELValue).Selected = true;



                            txtModel.Text = oUDFAccess.GetFieldValue(ModeludfKey, oCSCase.Key);
                            txtQuotedPrice.Text = oUDFAccess.GetFieldValue(QuotedPriceudfKey, oCSCase.Key);
                            txtQuotedPriceHF.Value = txtQuotedPrice.Text;
                            txtDeliveryPostCode.Text = oUDFAccess.GetFieldValue(PostCodeKey, oCSCase.Key);

                            txtDesc.Text = oUDFAccess.GetFieldValue(DescriptionudfKey, oCSCase.Key);
                            txtDeliveryAddress.Text = oUDFAccess.GetFieldValue(DeliveryAddressudfKey, oCSCase.Key);
                            txtVendorOrderNumber.Text = VendorOrderNumber;
                            if (RequestType == "CallCenter")
                            {
                                string cancelItemType = GetUdfCSCaseKey("Ordered Item Status " + i);
                                cancelItemType = this.oUDFAccess.GetFieldValue(cancelItemType, oCSCase.Key);

                                var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " Ordered From"), oCSCase.Key);
                                if (!string.IsNullOrEmpty(OrderFrom) && cancelItemType == "Ordered")
                                {
                                    ddlCancelOrdered.Attributes.Add("disabled", "disabled");
                                }
                            }
                            if (RequestType == "Email")
                            {
                                col_div = "3";
                                //if (WarrantyValue == "Yes")
                                //{
                                //    col_div = "2";
                                //}


                                display = "display:none;";
                                #region disable


                                ddlProduct.Attributes.Add("disabled", "disabled");
                                ddlBrand.Attributes.Add("disabled", "disabled");

                                if (!string.IsNullOrEmpty(ET) && (ET == "CO" || ET == "CURRORD"))
                                {
                                }
                                else
                                {
                                    txtModel.Attributes.Add("disabled", "disabled");
                                }
                                txtQuotedPrice.Attributes.Add("disabled", "disabled");
                                txtDesc.Attributes.Add("disabled", "disabled");
                                txtDeliveryPostCode.Attributes.Add("disabled", "disabled");

                                divDeliveryAddress.Visible = false;
                                divPickupOrDelivery.Visible = false;
                                txtEnquiryComments.Attributes.Add("disabled", "disabled");
                                #endregion

                                string TextBackColor = "#90EE90";
                                if (VendorType.ToUpper() == "JBHIFI")
                                {
                                    TextBackColor = "yellow";
                                }
                                string vendors = "";
                                if (VendorType == "The Good Guys")
                                {
                                    vendors = "GG";
                                }
                                else
                                {
                                    vendors = VendorType;
                                }
                                if (oCSCase.StatusName.Value != "Saved")
                                {
                                    var Comment = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + vendors + " Comment"), oCSCase.Key);
                                    var WarrantyDesc = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + vendors + " Warranty Desc"), oCSCase.Key);
                                    var PricePickUp = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + vendors + " Price Pick Up"), oCSCase.Key);
                                    var PriceDelivery = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + vendors + " Price Delivery"), oCSCase.Key);
                                    var PriceInstall = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + vendors + " Price Install"), oCSCase.Key);
                                    var PriceRemoval = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + vendors + " Price Removal"), oCSCase.Key);
                                    var PriceWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + vendors + " Price Warranty"), oCSCase.Key);

                                    //str += "<div class='col-lg-3'>&nbsp;</div>";


                                    str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + vendors + " Comments</span><input name='txtP" + vendors + "Comment" + i + "' type='text' id='txtP" + vendors + "Comment" + i + "' class='txt-item PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + Comment + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblProductPrice" + i + "'>" + vendors + " Product Price : $</span><input name='txtP" + vendors + "PickUp" + i + "' type='text'  id='txtP" + vendors + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PricePickUp + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblDeliveryPrice" + i + "'>" + vendors + " +Delivery Price : $</span><input name='txtP" + vendors + "PriceDelivery" + i + "' type='text' id='txtP" + vendors + "PriceDelivery" + i + "'  class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceDelivery + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblInstallPrice" + i + "'>" + vendors + " +Install Price : $</span><input name='txtPInst" + vendors + i + "' type='text' id='txtPInst" + vendors + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceInstall + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblRemovalPrice" + i + "'>" + vendors + " +Removal Price : $</span><input name='txtPDelInstRem" + vendors + i + "' type='text' id='txtPDelInstRem" + vendors + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceRemoval + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblWarrentyPrice" + i + "'>" + vendors + " Warrenty Price : $</span><input name='txtPWarranty" + vendors + i + "' type='text' id='txtPWarranty" + vendors + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceWarranty + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblWarrentyDesc" + i + "'>" + vendors + " Warrenty Description</span><input name='txtPWarrantyDesc" + vendors + i + "' type='text' id='txtPWarrantyDesc" + vendors + i + "' class='txt-item PO1 ' style='width: 100%;background-color:" + TextBackColor + ";'  value='" + WarrantyDesc + "'></div>";

                                    //ddlPayBY.Attributes.Add("disabled", "disabled");
                                    //ddlPickupOrDelivery.Attributes.Add("disabled", "disabled");




                                    //txtDeliveryAddress.Attributes.Add("disabled", "disabled");

                                    //txtDeliveryAddress.Visible = false;
                                    //ddlPickupOrDelivery.Visible = false;

                                    #region for vendor


                                    ltrPayBY.Text = FillPayByOptionsValue("", "", "");

                                    #endregion

                                    //chkDeliveryWarranty.Enabled = false;
                                    //chkPickupWarranty.Enabled = false;
                                }
                            }

                            //if (oCSCase.StatusName.Value != "Saved")
                            //{
                            if (!string.IsNullOrEmpty(VendorType))
                            {
                                #region 1
                                //string[] arr = vendor.Split(',');
                                //for (int j = 0; j < arr.Length; j++)
                                //{
                                string disabled = "";
                                bool flag = true;
                                string PComment = "";
                                string PPD = "";
                                string PPU = "";

                                string OptionCostValue = "";
                                string backgroundColor = "";
                                string VendorMode = VendorType;
                                string PlaceOrderVendor = "";
                                string PPUWarranty = "";
                                string vendors = "";
                                string selectedPriceItemtype = "";

                                var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " Ordered From"), oCSCase.Key);



                                if (VendorType == "The Good Guys")
                                {
                                    vendors = "GG";
                                }
                                else
                                {
                                    vendors = VendorType;
                                }




                                if (!string.IsNullOrEmpty(OrderFrom))
                                {
                                    flag = false;
                                }
                                if (!string.IsNullOrEmpty(vendor))
                                {

                                    //if (Convert.ToString(arr[j]) == "The Good Guys")
                                    if (VendorMode.ToLower() == "The Good Guys".ToLower())
                                    {
                                        if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("The Good Guys"))
                                        {
                                            PlaceOrderVendor = "ORDERED FROM GG";
                                        }
                                        else
                                        {
                                            PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order GG" : PlaceOrderVendor;
                                        }
                                        VendorMode = "GG";
                                        backgroundColor = "background-color:#90EE90;";
                                        PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Comment"), oCSCase.Key);
                                        PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Delivery"), oCSCase.Key);
                                        PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Pick Up"), oCSCase.Key);
                                        PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Warranty"), oCSCase.Key);
                                        selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked GG" + i), oCSCase.Key);
                                    }
                                    else if (VendorMode.ToLower() == "JBHiFI".ToLower())
                                    {
                                        if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("JBHIFI"))
                                        {
                                            PlaceOrderVendor = "ORDERED FROM JBHIFI";
                                        }
                                        else
                                        {
                                            PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order JBHIFI" : PlaceOrderVendor;
                                        }
                                        VendorMode = "JBHIFI";
                                        backgroundColor = "background-color:Yellow";
                                        PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Comment"), oCSCase.Key);
                                        PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Delivery"), oCSCase.Key);
                                        PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Pick Up"), oCSCase.Key);
                                        PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Warranty"), oCSCase.Key);
                                        selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked JBHIFI" + i), oCSCase.Key);
                                    }
                                }
                                string priceCheckSelected = "";
                                string str1 = "";
                                string optionsText = "";
                                string displayPriceOptionValues = "";
                                if (mode == "Direct Order")
                                {
                                    if (RequestType == "Vendor" && (EnquiryType == "CURRORD" || EnquiryType == "CO"))
                                    {
                                        str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + vendors + " Comments</span><input name='txtP" + vendors + "Comment" + i + "' type='text' id='txtP" + vendors + "Comment" + i + "' class='txt-item PO1' style='width: 100%; " + backgroundColor + "' value='" + PComment + "'></div>";
                                    }
                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                    { priceCheckSelected = "checked"; }
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }
                                    str1 += "<div class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                    var OptionCostValueInstall = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                    var OptionCostValueRemoval = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                    string displayType = "style='display:none;'";
                                    if (!string.IsNullOrEmpty(PPD) || !string.IsNullOrEmpty(OptionCostValueInstall) || !string.IsNullOrEmpty(OptionCostValueRemoval))
                                        displayType = "";

                                    displayPriceOptionValues = "";
                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                        priceCheckSelected = "checked";
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }
                                    string strValue = "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                    displayPriceOptionValues = "";
                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                        priceCheckSelected = "checked";
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }
                                    optionsText = VendorMode + " +Install Price";
                                    strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueInstall + "'></div>";

                                    displayPriceOptionValues = "";
                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                        priceCheckSelected = "checked";
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }
                                    optionsText = VendorMode + " +Removal Price";
                                    strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueRemoval + "'></div>";

                                    string selected = "";
                                    if (string.IsNullOrEmpty(displayType))
                                        selected = "checked";

                                    str1 += "<div class='col-lg-8' style='height: 86px;'>";
                                    str1 += "<input type='checkbox'  id='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' class='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' style='margin: 0px 0px 0px 5px; display: inline-block;' " + selected + ">";
                                    str1 += "<span id='lblPriceDelivery11111'>Show additional prices</span><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /></div>";

                                    str1 += strValue;
                                }
                                else
                                {
                                    if (oCSCase.StatusName.Value != "Saved")
                                    {
                                        if (RequestType != "Email" && VendorMode != "")
                                        {
                                            if (RequestType == "Vendor" && (EnquiryType == "CURRORD" || EnquiryType == "CO"))
                                            {
                                                str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + vendors + " Comments</span><input name='txtP" + vendors + "Comment" + i + "' type='text' id='txtP" + vendors + "Comment" + i + "' class='txt-item PO1' style='width: 100%; " + backgroundColor + "' value='" + PComment + "'></div>";
                                            }

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }

                                            str1 += "<div  class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                            optionsText = VendorMode + " +Install Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                            optionsText = VendorMode + " +Removal Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";


                                            //OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Del Rem Cost"), oCSCase.Key);
                                            //optionsText = VendorMode + " Del+Rem Cost";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' data-id='" + VendorMode + " Del/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Warranty"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Warranty Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Warranty Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPWarranty" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Warranty Cost' id='txtPWarranty" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";


                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Warranty Desc"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Description";
                                            str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":</span><input name='txtPWarrantyDesc" + VendorMode + i + "' type='text' data-id='" + VendorMode + "Warranty Desc' id='txtPWarrantyDesc" + VendorMode + i + "' class='txt-item  PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";
                                        }

                                    }

                                }
                                if (oCSCase.StatusName.Value != "Saved")
                                {
                                    if (!string.IsNullOrEmpty(VendorMode))
                                    {
                                        if (RequestType != "Email" && mode != "Direct Order" && RequestType != "Vendor")
                                        {
                                            str += "<div class='row' id='divDirectOrder" + i + "' class='divOrder" + VendorMode + "' style='display:inline;'><div class='col-lg-12'><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /><span id='lblPComment" + i + "'> " + VendorMode + " Comments</span>";
                                            str += "<textarea name='txtP" + VendorMode + "Comment" + i + "'  data-id='" + VendorMode + " Comment' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO" + i + "' style='width: 100%; " + backgroundColor + "' >" + PComment + "</textarea></div>";
                                        }

                                        else if (RequestType == "Vendor")
                                        {
                                            str += "<div class='row' id='divDirectOrder" + i + "' class='divOrder" + VendorMode + "' style='display:inline;'>";
                                        }
                                        str += str1;

                                        string hide = "";
                                        if (!flag)
                                        {
                                            disabled = "disabled='disabled'";
                                            if (!PlaceOrderVendor.Contains("ORDERED FROM"))
                                                hide = "hide";
                                        }
                                        string Order = i + "-" + VendorMode;
                                        StringBuilder strScript = new StringBuilder();

                                        strScript.Append("\"" + Order + "\"");
                                        var styleRedColorbutton = "";
                                        if (!string.IsNullOrEmpty(disabled))
                                        {
                                            styleRedColorbutton = "style='background:red !important;'";
                                        }
                                        if (mode == "Direct Order")
                                            display = "display:none;";

                                        str += "<div class='col-lg-2 " + hide + "'" + hidePlaceOrder + "><input " + styleRedColorbutton + " type='button' id='btnPlaceOrder" + VendorMode + "" + i + "' " + disabled + " class='btn btnPlaceOrder' onclick='PlaceOrder(" + strScript + ");' value='" + PlaceOrderVendor + "' style='" + display + "'/></div>";
                                        if (RequestType == "Vendor")
                                        {
                                            Opportunity opp = GetOpp(i.ToString());
                                            if (opp != null && opp.Status.Value != 3)
                                            {

                                                HfIsConfirmOrderPage.Value = "true";
                                                str += "<div class='col-lg-12 text-right'><input type='button' name='btnConfirmOrder" + i + "' data-OppKey='" + opp.Key + "' value='Confirm Order' id='btnConfirmOrder" + i + "' class='btn btn-success ConfirmOrder'>";
                                                str += "<input type='hidden' id='hfOppKey" + i + "' name='hfOppKey" + i + "' value='" + opp.Key + "'></div>";
                                            }
                                            else
                                            {
                                                btnCreateEnquiry.Visible = false;
                                            }
                                        }
                                        if (mode != "Direct Order")
                                            str += "</div>";
                                    }
                                }
                                else
                                {
                                    str += str1;
                                }
                                //}
                                #endregion
                            }
                            else
                            {
                                #region 2
                                var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " Ordered From"), oCSCase.Key);
                                bool flag = true;
                                if (!string.IsNullOrEmpty(OrderFrom))
                                {
                                    flag = false;
                                }
                                string[] arr = vendor.Split(',');
                                for (int j = 0; j < arr.Length; j++)
                                {
                                    string disabled = "";

                                    string PComment = "";
                                    string PPD = "";
                                    string PPU = "";
                                    string PPUWarranty = "";

                                    string OptionCostValue = "";
                                    string backgroundColor = "";
                                    string VendorMode = VendorType;
                                    string PlaceOrderVendor = "";
                                    string selectedPriceItemtype = "";



                                    if (!string.IsNullOrEmpty(arr[j]))
                                    {
                                        if (Convert.ToString(arr[j]) == "The Good Guys")
                                        {
                                            VendorMode = "GG";
                                            backgroundColor = "background-color:#90EE90;";
                                            PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Comment"), oCSCase.Key);
                                            PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Delivery"), oCSCase.Key);
                                            PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Pick Up"), oCSCase.Key);
                                            PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Warranty"), oCSCase.Key);
                                            selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked GG" + i), oCSCase.Key);

                                            if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("The Good Guys"))
                                            {
                                                PlaceOrderVendor = "ORDERED FROM GG";
                                            }
                                            else
                                            {
                                                PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order GG" : PlaceOrderVendor;
                                            }
                                        }
                                        else if (Convert.ToString(arr[j]).ToLower() == "JBHiFI".ToLower())
                                        {
                                            if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("JBHIFI"))
                                            {
                                                PlaceOrderVendor = "ORDERED FROM JBHIFI";
                                            }
                                            else
                                            {
                                                PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order JBHIFI" : PlaceOrderVendor;
                                            }
                                            VendorMode = "JBHIFI";
                                            backgroundColor = "background-color:Yellow";
                                            PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Comment"), oCSCase.Key);
                                            PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Delivery"), oCSCase.Key);
                                            PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Pick Up"), oCSCase.Key);
                                            PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Warranty"), oCSCase.Key);
                                            selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked JBHIFI" + i), oCSCase.Key);
                                        }
                                    }
                                    string str1 = "";
                                    string optionsText = "";
                                    string priceCheckSelected = "";
                                    string displayPriceOptionValues = "";
                                    if (mode == "Direct Order")
                                    {
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        str1 += "<div  class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                        var OptionCostValueInstall = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                        var OptionCostValueRemoval = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                        string displayType = "style='display:none;'";
                                        if (!string.IsNullOrEmpty(PPD) || !string.IsNullOrEmpty(OptionCostValueInstall) || !string.IsNullOrEmpty(OptionCostValueRemoval))
                                            displayType = "";

                                        displayPriceOptionValues = "";
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        string strValue = "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                        optionsText = VendorMode + " +Install Price";

                                        displayPriceOptionValues = "";
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueInstall + "'></div>";

                                        optionsText = VendorMode + " +Removal Price";

                                        displayPriceOptionValues = "";
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueRemoval + "'></div>";

                                        string selected = "";
                                        if (string.IsNullOrEmpty(displayType))
                                            selected = "checked";

                                        str1 += "<div class='col-lg-8' style='height: 86px;'>";
                                        str1 += "<input type='checkbox'  id='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' class='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' style='margin: 0px 0px 0px 5px; display: inline-block;' " + selected + ">";
                                        str1 += "<span id='lblPriceDelivery11111'>Show additional prices</span><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /></div>";

                                        str1 += strValue;
                                    }
                                    else
                                    {
                                        if (oCSCase.StatusName.Value != "Saved")
                                        {
                                            if (RequestType == "Vendor" && (EnquiryType == "CURRORD" || EnquiryType == "CO"))
                                            {
                                                str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + VendorMode + " Comments</span><input name='txtP" + VendorMode + "Comment" + i + "' type='text' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO1' style='width: 100%; " + backgroundColor + "' value='" + PComment + "'></div>";
                                            }
                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' " + priceCheckSelected + "  data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                            optionsText = VendorMode + " +Install Price";

                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + "  data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                            optionsText = VendorMode + " +Removal Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + "  data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";


                                            //OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Del Rem Cost"), oCSCase.Key);
                                            //optionsText = VendorMode + " Del+Rem Cost";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' data-id='" + VendorMode + " Del/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Warranty"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Warranty Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + "  data-id='" + VendorMode + " Warranty Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPWarranty" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Warranty Cost' id='txtPWarranty" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";



                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Warranty Desc"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Description";
                                            str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":</span><input name='txtPWarrantyDesc" + VendorMode + i + "' type='text' data-id='" + VendorMode + "Warranty Desc' id='txtPWarrantyDesc" + VendorMode + i + "' class='txt-item  PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";




                                            //var WarrantyDesc = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Warranty Desc"), oCSCase.Key);
                                            //var PricePickUp = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Pick Up"), oCSCase.Key);
                                            //var PriceDelivery = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Delivery"), oCSCase.Key);
                                            //var PriceInstall = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                            //var PriceRemoval = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                            //var PriceWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Warranty"), oCSCase.Key);



                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblProductPrice" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtProductPrice" + VendorMode + i + "' data-id='" + VendorMode + " Product Price' type='text' id='txtProductPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PricePickUp + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblDeliveryPrice" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtDeliveryPrice" + VendorMode + i + "' data-id='" + VendorMode + " Delivery Price' type='text' id='txtDeliveryPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceDelivery + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblInstallPrice" + i + "'> " + VendorMode + " +Install Price:$</span><input type='checkbox' data-id='" + VendorMode + " Install Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtInstallPrice" + VendorMode + i + "' data-id='" + VendorMode + " Install Price' type='text' id='txtInstallPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceInstall + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblRemovalPrice" + i + "'> " + VendorMode + " +Removal Price:$</span><input type='checkbox' data-id='" + VendorMode + " Removal Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtRemovalPrice" + VendorMode + i + "' data-id='" + VendorMode + " Removal Price' type='text' id='txtRemovalPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceRemoval + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyPrice" + i + "'> " + VendorMode + " Warrenty Price:$</span><input type='checkbox' data-id='" + VendorMode + " Warrenty Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtWarrentyPrice" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Price' type='text' id='txtWarrentyPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceWarranty + "'></div>";
                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyDesc" + i + "'> " + VendorMode + " Warrenty Description:$</span><input type='checkbox' data-id='" + VendorMode + " Warrenty Description' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtWarrentyDesc" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Description' type='text' id='txtWarrentyDesc" + VendorMode + i + "' class='txt-item PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + WarrantyDesc + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyDesc" + i + "'> " + VendorMode + " Warrenty Description:</span><input name='txtWarrentyDesc" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Description' type='text' id='txtWarrentyDesc" + VendorMode + i + "' class='txt-item PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + WarrantyDesc + "'></div>";

                                        }
                                    }

                                    if (oCSCase.StatusName.Value != "Saved")
                                    {
                                        if (!string.IsNullOrEmpty(VendorMode))
                                        {
                                            if (mode != "Direct Order")
                                            {
                                                str += "<div class='row' id='divDirectOrder" + i + "' class='divOrder" + VendorMode + "' style='display:inline;'><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /><div class='col-lg-12'><span id='lblPComment" + i + "'> " + VendorMode + " Comments</span>";

                                                str += "<textarea name='txtP" + VendorMode + "Comment" + i + "'  data-id='" + VendorMode + " Comment' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO" + i + "' style='width: 100%; " + backgroundColor + "' >" + PComment + "</textarea></div>";

                                                //var Comment = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Comment"), oCSCase.Key);

                                                //str += "<textarea name='txtComments" + VendorMode + i + "'  data-id='" + VendorMode + " Comment' id='txtComments" + VendorMode  + i + "' class='txt-item PO" + i + "' style='width: 100%; " + backgroundColor + "' >" + PComment + "</textarea></div>";

                                            }
                                            str += str1;

                                            string hide = "";
                                            if (!flag)
                                            {
                                                disabled = "disabled='disabled'";
                                                if (!PlaceOrderVendor.Contains("ORDERED FROM"))
                                                    hide = "hide";
                                            }
                                            string Order = i + "-" + VendorMode;
                                            StringBuilder strScript = new StringBuilder();

                                            strScript.Append("\"" + Order + "\"");

                                            var btnPlaceOrderText = "Order";
                                            var styleRedColorbutton = "";
                                            if (!string.IsNullOrEmpty(disabled))
                                            {
                                                btnPlaceOrderText = "Ordered";
                                                styleRedColorbutton = "style='background:red !important;'";
                                            }
                                            if (mode == "Direct Order")
                                                display = "display:none;";

                                            //str += "<div class='col-lg-2 " + hide + "' " + hidePlaceOrder + "><input type='button' id='btnPlaceOrder" + VendorMode + "" + i + "' " + disabled + " class='btn btnPlaceOrder' onclick='PlaceOrder(" + strScript + ");' value='" + PlaceOrderVendor + "' style='" + display + "'/></div>";
                                            str += "<div class='col-lg-2 " + hide + "' " + hidePlaceOrder + "><input " + styleRedColorbutton + " type='button' id='btnPlaceOrder" + VendorMode + "" + i + "' " + disabled + " class='btn btnPlaceOrder' onclick='PlaceOrder(" + strScript + ");' value='" + btnPlaceOrderText + "' style='" + display + "'/></div>";

                                            if (RequestType == "Vendor")
                                            {
                                                Opportunity opp = GetOpp(i.ToString());
                                                if (opp != null && opp.Status.Value != 3)
                                                {
                                                    HfIsConfirmOrderPage.Value = "true";
                                                    str += "<div class='col-lg-12 text-right'><input type='button' name='btnConfirmOrder" + i + "' data-OppKey='" + opp.Key + "' value='Confirm Order' id='btnConfirmOrder" + i + "' class='btn btn-success ConfirmOrder'>";
                                                    str += "<input type='hidden' id='hfOppKey" + i + "' name='hfOppKey" + i + "' value='" + opp.Key + "'></div>";
                                                }
                                                else
                                                {
                                                    btnCreateEnquiry.Visible = false;
                                                }
                                            }
                                            if (mode != "Direct Order")
                                                str += "</div>";
                                        }
                                    }
                                    else
                                    {
                                        str += str1;
                                    }
                                }
                                #endregion
                            }

                            DivDO.InnerHtml = str;
                            if (!string.IsNullOrEmpty(str))
                            {
                                DivDO.Style.Add("display", "block");

                            }

                            str = "";

                            //}

                            ltrPayBY.Text = FillPayByOptionsValue(PayByudfValue, "", RequestType);
                            hfPayBY.Value = PayByudfValue;



                            string SubmitTo = oUDFAccess.GetFieldValue(SubmitToudfKey, oCSCase.Key);
                            ltrSubmitedTo.Text = FillSubmitToValue(SubmitTo, "", RequestType);
                            hfSubmitTo1.Value = SubmitTo;
                            ItemsNumber += i.ToString() + ",";
                        }
                        else if ((!string.IsNullOrEmpty(oUDFAccess.GetFieldValue(BrandudfKey, oCSCase.Key)) && !string.IsNullOrEmpty(oUDFAccess.GetFieldValue(ProductudfKey, oCSCase.Key))) || !string.IsNullOrEmpty(vendor))
                        {
                            string ProductValues = oUDFAccess.GetFieldValue(ProductudfKey, oCSCase.Key);
                            ProductValues = ProductOption.Replace("<option value='" + ProductValues + "'>" + ProductValues + "</option>", "<option selected='selected' value='" + ProductValues + "'>" + ProductValues + "</option>");

                            string BrandValues = oUDFAccess.GetFieldValue(BrandudfKey, oCSCase.Key);
                            BrandValues = BrandOption.Replace("<option value='" + BrandValues + "'>" + BrandValues + "</option>", "<option selected='selected' value='" + BrandValues + "'>" + BrandValues + "</option>");

                            //string PayByValues = oUDFAccess.GetFieldValue(PayByudfKey, oCSCase.Key);
                            //PayByValues = PayByOption.Replace("<option value='" + PayByValues + "'>" + PayByValues + "</option>", "<option selected='selected' value='" + PayByValues + "'>" + PayByValues + "</option>");

                            string PUDELValues = oUDFAccess.GetFieldValue(PUDELudfKey, oCSCase.Key);
                            PUDELValues = PickOrDeliveryyOption.Replace("<option value='" + PUDELValues + "'>" + PUDELValues + "</option>", "<option selected='selected' value='" + PUDELValues + "'>" + PUDELValues + "</option>");

                            string Options = oUDFAccess.GetFieldValue(OptionsKey, oCSCase.Key);
                            string OptionsValues = FillOptionsValue(Options, Convert.ToString(i), RequestType);

                            string Model = oUDFAccess.GetFieldValue(ModeludfKey, oCSCase.Key);
                            string QuotePrice = oUDFAccess.GetFieldValue(QuotedPriceudfKey, oCSCase.Key);

                            string Desc = oUDFAccess.GetFieldValue(DescriptionudfKey, oCSCase.Key);
                            string DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressudfKey, oCSCase.Key);

                            string PickupOrDeliveryy = oUDFAccess.GetFieldValue(PUDELudfKey, oCSCase.Key);
                            string PickupOrDeliveryyValues = FillddlPickOrDeliveryyValue(PickupOrDeliveryy, Convert.ToString(i));


                            string PostCode = oUDFAccess.GetFieldValue(PostCodeKey, oCSCase.Key);

                            string SubmitTo = oUDFAccess.GetFieldValue(SubmitToudfKey, oCSCase.Key);
                            string SubmitToValues = FillSubmitToValue(SubmitTo, Convert.ToString(i), RequestType);

                            string PayBy = oUDFAccess.GetFieldValue(PayByudfKey, oCSCase.Key);
                            string PayByValues = FillPayByOptionsValue(PayBy, Convert.ToString(i), RequestType);


                            string styleDeliveryAddress = "style='display:none'";
                            if (!string.IsNullOrEmpty(DeliveryAddress))
                                styleDeliveryAddress = "";

                            string divMemberPrice = ""; string DivQuotePrice = "";
                            if (mode == "Direct Order" && RequestType == "CallCenter")
                            {
                                divMemberPrice = "DivQuotePrice";
                                DivQuotePrice = "divMemberPrice";
                            }

                            string PComment = "";
                            string PPD = "";
                            string PPU = "";
                            string PPUWarranty = "";
                            string selectedPriceItemtype = "";
                            string Radonly = "";
                            if (RequestType == "Email")
                            {
                                col_div = "3";
                                display = "display:none;";
                                Radonly = "disabled";
                            }
                            string Warrantyselected = "";
                            if (WarrantyValue == "Yes")
                            {
                                Warrantyselected = "checked";
                            }


                            if (RequestType == "Email")
                            {


                                str += "<div>";
                                str += "<div><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='margin-bottom:15px;'><fieldset><legend>Item #" + i + "</legend>";
                                str += "<div><div class='col-lg-3 selectdiv'><span id='lblBrand'>Brand</span><select " + Radonly + " name='ddlBrand" + i + "' id='ddlBrand" + i + "' class='ddlBrand'>" + BrandValues + "";





                                //str += "<div class='row'><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='margin-bottom:15px;'><fieldset><legend>Item #" + i + "</legend>";
                                //str += "<div class='row'><div class='col-lg-3 selectdiv'><span id='lblBrand'>Brand</span><select " + Radonly + " name='ddlBrand" + i + "' id='ddlBrand" + i + "' class='ddlBrand'>" + BrandValues + "";
                                str += "</select></div><div class='col-lg-3 selectdiv'><span id='lblProduct'>Product</span><select " + Radonly + " name='ddlProduct" + i + "' id='ddlProduct" + i + "'  class='ddlProduct'>" + ProductValues + "</select></div><div class='col-lg-3'>";

                                if (!string.IsNullOrEmpty(ET) && (ET == "CO" || ET == "CURRORD"))
                                {
                                    str += "<span id='lblModel'>Model</span><input  name='txtModel" + i + "' type='text' id='txtModel" + i + "' class='txt-item' value='" + Model + "'></div><div class='col-lg-3 " + divMemberPrice + "'><div class='QuotedPrice'><span class='QuotePrice' id='lblQuotedPrice'>Member Price $: </span>";
                                }
                                else
                                {
                                    str += "<span id='lblModel'>Model</span><input " + Radonly + "  name='txtModel" + i + "' type='text' id='txtModel" + i + "' class='txt-item' value='" + Model + "'></div><div class='col-lg-3 " + divMemberPrice + "'><div class='QuotedPrice'><span class='QuotePrice' id='lblQuotedPrice'>Member Price $: </span>";
                                }
                                //str += "<input " + Radonly + " name='txtQuotedPrice" + i + "' type='number' id='txtQuotedPrice" + i + "' class='txt-item' value='" + QuotePrice + "' step='0.01'><input type='hidden' id='txtQuotedPriceHF" + i + "' name='txtQuotedPriceHF" + i + "' value='" + QuotePrice + "'/> </div></div><div class='col-lg-3'><span id='lblPayBY'>Pay BY</span>";
                                str += "<input " + Radonly + " name='txtQuotedPrice" + i + "'  id='txtQuotedPrice" + i + "' class='DirectQuotePrice txt-item' value='" + QuotePrice + "' step='0.01'><input type='hidden' id='txtQuotedPriceHF" + i + "' name='txtQuotedPriceHF" + i + "' value='" + QuotePrice + "'/> </div></div>";
                                str += "<div class='col-lg-3'><span id='lblPayBY'>Pay BY</span>" + PayByValues + " </div>";
                                //str += "<div class='col-lg-3'><span id='lblDeliveryAddress" + i + "'>Street Address</span><input " + Radonly + " name='txtDeliveryAddress" + i + "' type='text' id='txtDeliveryAddress" + i + "' class='txt-item' style='width: 100%;' value='" + DeliveryAddress + "'></div>";
                                str += "<div class='col-lg-3'><span id='lblDeliveryPostCode" + i + "'>Suburb and Postcode</span><input " + Radonly + " name='txtDeliveryPostCode" + i + "' type='text' id='txtDeliveryPostCode" + i + "' class='txt-item DeliveryPostCode' style='width: 100%;' value='" + PostCode + "'></div>";

                                //str += "<div class='col-lg-3 divPickupOrDelivery selectdiv divMemberPrice' id='divPickupOrDelivery" + i + "'> <span id='lblPickupOrDeliveryy'>Member Price Option</span> " + PickupOrDeliveryyValues + " <input type='hidden' style='display: none;' name='hfPickupOrDeliveryy" + i + "' id='hfPickupOrDeliveryy" + i + "' value='" + PickupOrDeliveryy + "'></div>";


                                //str += PayByValues + "</select> </div>":
                                //str += "<div class='col-lg-3'><span id='lblDescription'>Description</span><textarea " + Radonly + " name='txtDesc" + i + "' rows='2' cols='20' id='txtDesc" + i + "' class='txt-item' >" + Desc + "</textarea><span class='spnChar' style='float: left; vertical-align: bottom; margin-top: 30px;'>250</span></div>";
                                str += "<div class='col-lg-3'><div class='SubmitTo'> <span id='lblSubmitTo'>Submit To </span> " + SubmitToValues + " <input type='hidden' name='hfSubmitTo" + i + "' id='hfSubmitTo" + i + "' value='" + SubmitTo + "'></div></div>";
                                bool descAdd = false;
                                if (RequestType == "CallCenter")
                                {
                                    descAdd = true;
                                    string cancelItemType = GetUdfCSCaseKey("Ordered Item Status " + i);
                                    cancelItemType = this.oUDFAccess.GetFieldValue(cancelItemType, oCSCase.Key);

                                    var disableCancelOrder = "";
                                    var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " Ordered From"), oCSCase.Key);
                                    if (!string.IsNullOrEmpty(OrderFrom) && cancelItemType == "Ordered")
                                    {
                                        disableCancelOrder = "disabled";
                                    }

                                    str += "<div class='col-lg-3 divCancelOrdered'><div class='divCancelOrdered'> <span id='lblCancelOrdered'>Order Status </span> <select " + disableCancelOrder + " id='ddlCancelOrdered" + i + "' name='ddlCancelOrdered" + i + "' class='txt-item CancelOrdered'>" + ddlcancel + "</select><input type='hidden' id='hfOrderStatus" + i + "' name='hfOrderStatus" + i + "' runat='server' value='" + cancelItemType + "'/></div></div>";

                                    str += "<div id='divToalPriceShow' class='col-lg-3 divToalPriceShow " + DivQuotePrice + "'>";

                                    str += "    <div style='padding-top: 10px;float: left;width: 100%;'><span id='lblTotlPriceJBHIFI" + i + "' class='lblTotlPriceJBHIFI' style='width: 50%; float: left; font-weight: bold;display:none;'>Total JBHIFI Price</span>";
                                    str += "<span id='lblTotlPriceAmountJBHIFI" + i + "' class='lblTotlPriceAmountJBHIFI' style='width: 50%; float: left;  padding-left: 10px;'></span></div>";
                                    str += "   <div style='padding-top: 10px;float: left;width: 100%;'><span id='lblTotlPriceGG" + i + "' class='lblTotlPriceGG' style='width: 50%; float: left; font-weight: bold;display:none;'>Total GG Price</span>";

                                    str += " <span id='lblTotlPriceAmountGG" + i + "' class='lblTotlPriceAmountGG' style='width: 50%; float: left;  padding-left: 10px;'></span></div>";
                                    str += "</div>";
                                    str += "<div class='col-lg-12' style='float:left;'><span id='lblDescription'  style='float:left;'>Description</span><textarea " + Radonly + " name='txtDesc" + i + "' rows='3' cols='20' id='txtDesc" + i + "' class='txt-item' style='clear:both;'>" + Desc + "</textarea><span class='spnChar' style='float: left; vertical-align: bottom; margin-top: 30px;'>250</span></div>";
                                }
                                if (oCSCase.StatusName.Value != "Saved")
                                {
                                    descAdd = true;
                                    str += "<div class='col-lg-3 VendorOrderNumber'><span id='lblVendorOrderNumber'>Vendor Order Number</span><input type='text' " + Radonly + " name='txtVendorOrderNumber" + i + "'  id='txtVendorOrderNumber" + i + "' class='txt-item' value='" + VendorOrderNumber + "' /></div>";
                                    str += "<div class='col-lg-12' style='float:left;'><span id='lblDescription'  style='float:left;'>Description</span><textarea " + Radonly + " name='txtDesc" + i + "' rows='3' cols='20' id='txtDesc" + i + "' class='txt-item' style='clear:both;'>" + Desc + "</textarea><span class='spnChar' style='float: left; vertical-align: bottom; margin-top: 30px;'>250</span></div>";
                                    string VendorTypeName = "";
                                    if (VendorType == "The Good Guys")
                                    {
                                        VendorTypeName = "GG";
                                    }
                                    else if (VendorType == "JBHIFI")
                                    {
                                        VendorTypeName = "JBHIFI";
                                    }
                                    string TextBackColor = "#90EE90";
                                    if (VendorType.ToUpper() == "JBHIFI")
                                    {
                                        TextBackColor = "yellow";
                                    }

                                    var Comment = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorTypeName + " Comment"), oCSCase.Key);
                                    var WarrantyDesc = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorTypeName + " Warranty Desc"), oCSCase.Key);
                                    var PricePickUp = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorTypeName + " Price Pick Up"), oCSCase.Key);
                                    var PriceDelivery = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorTypeName + " Price Delivery"), oCSCase.Key);
                                    var PriceInstall = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorTypeName + " Price Install"), oCSCase.Key);
                                    var PriceRemoval = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorTypeName + " Price Removal"), oCSCase.Key);
                                    var PriceWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorTypeName + " Price Warranty"), oCSCase.Key);

                                    str += "<div class='col-lg-3'>&nbsp;</div>";


                                    str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + VendorTypeName + " Comments</span><input name='txtP" + VendorTypeName + "Comment" + i + "' type='text' id='txtP" + VendorTypeName + "Comment" + i + "' class='txt-item PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + Comment + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblProductPrice" + i + "'>" + VendorTypeName + " Product Price : $</span><input name='txtP" + VendorTypeName + "PickUp" + i + "' type='text'  id='txtP" + VendorTypeName + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PricePickUp + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblDeliveryPrice" + i + "'>" + VendorTypeName + " +Delivery Price : $</span><input name='txtP" + VendorTypeName + "PriceDelivery" + i + "' type='text' id='txtP" + VendorTypeName + "PriceDelivery" + i + "'  class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceDelivery + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblInstallPrice" + i + "'>" + VendorTypeName + " +Install Price : $</span><input name='txtPInst" + VendorTypeName + i + "' type='text' id='txtPInst" + VendorTypeName + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceInstall + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblRemovalPrice" + i + "'>" + VendorTypeName + " +Removal Price : $</span><input name='txtPDelInstRem" + VendorTypeName + i + "' type='text' id='txtPDelInstRem" + VendorTypeName + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceRemoval + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblWarrentyPrice" + i + "'>" + VendorTypeName + " Warrenty Price : $</span><input name='txtPWarranty" + VendorTypeName + i + "' type='text' id='txtPWarranty" + VendorTypeName + i + "' class='txt-item PlaceOrderAmount PO1' style='width: 100%;background-color:" + TextBackColor + ";' value='" + PriceWarranty + "'></div>";
                                    str += "<div class='col-lg-3'><span id='lblWarrentyDesc" + i + "'>" + VendorTypeName + " Warrenty Description</span><input name='txtPWarrantyDesc" + VendorTypeName + i + "' type='text' id='txtPWarrantyDesc" + VendorTypeName + i + "' class='txt-item PO1 ' style='width: 100%;background-color:" + TextBackColor + ";'  value='" + WarrantyDesc + "'></div>";

                                }
                                if (!descAdd)
                                {
                                    str += "<div class='col-lg-12' style='float:left;'><span id='lblDescription'  style='float:left;'>Description</span><textarea " + Radonly + " name='txtDesc" + i + "' rows='3' cols='20' id='txtDesc" + i + "' class='txt-item' style='clear:both;'>" + Desc + "</textarea><span class='spnChar' style='float: left; vertical-align: bottom; margin-top: 30px;'>250</span></div>";
                                }
                            }
                            else
                            {
                                str += "<div>";
                                str += "<div class='row'><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='margin-bottom:15px;'><fieldset><legend>Item #" + i + "</legend>";
                                str += "<div class='row'><div class='col-lg-3 selectdiv'><span id='lblBrand'>Brand</span><select " + Radonly + " name='ddlBrand" + i + "' id='ddlBrand" + i + "' class='ddlBrand'>" + BrandValues + "";
                                str += "</select></div><div class='col-lg-3 selectdiv'><span id='lblProduct'>Product</span><select " + Radonly + " name='ddlProduct" + i + "' id='ddlProduct" + i + "'  class='ddlProduct'>" + ProductValues + "</select></div><div class='col-lg-3'>";

                                if (!string.IsNullOrEmpty(ET) && (ET == "CO" || ET == "CURRORD"))
                                {
                                    str += "<span id='lblModel'>Model</span><input name='txtModel" + i + "' type='text' id='txtModel" + i + "' class='txt-item' value='" + Model + "'></div><div class='col-lg-3 " + divMemberPrice + "'><div class='QuotedPrice'><span class='QuotePrice' id='lblQuotedPrice'>Member Price $: </span>";
                                }
                                else
                                {
                                    str += "<span id='lblModel'>Model</span><input " + Radonly + "  name='txtModel" + i + "' type='text' id='txtModel" + i + "' class='txt-item' value='" + Model + "'></div><div class='col-lg-3 " + divMemberPrice + "'><div class='QuotedPrice'><span class='QuotePrice' id='lblQuotedPrice'>Member Price $: </span>";

                                }
                                //str += "<input " + Radonly + " name='txtQuotedPrice" + i + "' type='number' id='txtQuotedPrice" + i + "' class='txt-item' value='" + QuotePrice + "' step='0.01'><input type='hidden' id='txtQuotedPriceHF" + i + "' name='txtQuotedPriceHF" + i + "' value='" + QuotePrice + "'/> </div></div><div class='col-lg-3'><span id='lblPayBY'>Pay BY</span>";
                                str += "<input " + Radonly + " name='txtQuotedPrice" + i + "'  id='txtQuotedPrice" + i + "' class='DirectQuotePrice txt-item' value='" + QuotePrice + "' step='0.01'><input type='hidden' id='txtQuotedPriceHF" + i + "' name='txtQuotedPriceHF" + i + "' value='" + QuotePrice + "'/> </div></div>";
                                str += "<div class='col-lg-3'><span id='lblPayBY'>Pay BY</span>" + PayByValues + " </div>";

                                if (mode != "Direct Order")
                                {
                                    str += "<div class='col-lg-3'><span id='lblDeliveryAddress" + i + "'>Delivery Address</span><input " + Radonly + " name='txtDeliveryAddress" + i + "' type='text' id='txtDeliveryAddress" + i + "' class='txt-item DeliveryAddress' style='width: 100%;' value='" + DeliveryAddress + "'></div>";
                                }
                                str += "<div class='col-lg-3'><span id='lblDeliveryPostCode" + i + "'>Suburb and Postcode</span><input " + Radonly + " name='txtDeliveryPostCode" + i + "' type='text' id='txtDeliveryPostCode" + i + "' class='txt-item DeliveryPostCode' style='width: 100%;' value='" + PostCode + "'></div>";

                                string a = mode;

                                //str += "<div class='col-lg-3 divPickupOrDelivery selectdiv divMemberPrice' id='divPickupOrDelivery" + i + "'> <span id='lblPickupOrDeliveryy'>Member Price Option</span> " + PickupOrDeliveryyValues + " <input type='hidden' style='display: none;' name='hfPickupOrDeliveryy" + i + "' id='hfPickupOrDeliveryy" + i + "' value='" + PickupOrDeliveryy + "'></div>";
                                //str += "<div class='col-lg-3'><div class='SubmitTo'> <span id='lblSubmitTo'>Submit To </span> " + SubmitToValues + " <input type='hidden' name='hfSubmitTo" + i + "' id='hfSubmitTo" + i + "' value='" + SubmitTo + "'></div></div>";


                                //str += "<div class='row'><div class='col-lg-3 selectdiv'><span id='lblPickupOrDelivery'>Member Price Option</span><select name='ddlPickupOrDelivery" + i + "' id='ddlPickupOrDelivery" + i + "' class='ddlPickupOrDelivery'><option value=''></option><option value='Pickup'>Pickup</option><option value='Delivery'>Delivery</option></select></div></div>" + PickupOrDeliveryy + "";
                                if (string.IsNullOrEmpty(Items))
                                {
                                    if (PickupOrDeliveryy == "Pickup")
                                    {
                                        str += "<div class='row " + DivQuotePrice + "' ><div class='col-lg-3 selectdiv'><span id='lblPickupOrDelivery'>Member Price Option</span><select name='ddlPickupOrDelivery" + i + "' id='ddlPickupOrDelivery" + i + "' class='ddlPickupOrDelivery'><option value=''></option><option value='Pickup' selected>Pickup</option><option value='Delivery'>Delivery</option></select></div></div>";

                                    }
                                    else if (PickupOrDeliveryy == "Delivery")
                                    {
                                        str += "<div class='row " + DivQuotePrice + "' ><div class='col-lg-3 selectdiv'><span id='lblPickupOrDelivery'>Member Price Option</span><select name='ddlPickupOrDelivery" + i + "' id='ddlPickupOrDelivery" + i + "' class='ddlPickupOrDelivery'><option value=''></option><option value='Pickup' selected>Pickup</option><option value='Delivery' selected>Delivery</option></select></div></div>";

                                    }
                                    else
                                    {
                                        str += "<div class='row " + DivQuotePrice + "'><div class='col-lg-3 selectdiv'><span id='lblPickupOrDelivery'>Member Price Option</span><select name='ddlPickupOrDelivery" + i + "' id='ddlPickupOrDelivery" + i + "' class='ddlPickupOrDelivery'><option value=''></option><option value='Pickup'>Pickup</option><option value='Delivery'>Delivery</option></select></div></div>";
                                    }
                                }


                                //str += PayByValues + "</select> </div>":
                                //str += "<div class='col-lg-3'><span id='lblDescription'>Description</span><textarea " + Radonly + " name='txtDesc" + i + "' rows='2' cols='20' id='txtDesc" + i + "' class='txt-item' >" + Desc + "</textarea><span class='spnChar' style='float: left; vertical-align: bottom; margin-top: 30px;'>250</span></div>";
                                str += "<div class='col-lg-3'><div class='SubmitTo'> <span id='lblSubmitTo'>Submit To </span> " + SubmitToValues + " <input type='hidden' name='hfSubmitTo" + i + "' id='hfSubmitTo" + i + "' value='" + SubmitTo + "'></div></div>";
                                if (RequestType == "CallCenter")
                                {
                                    string cancelItemType = GetUdfCSCaseKey("Ordered Item Status " + i);
                                    cancelItemType = this.oUDFAccess.GetFieldValue(cancelItemType, oCSCase.Key);

                                    var disableCancelOrder = "";
                                    var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " Ordered From"), oCSCase.Key);
                                    if (!string.IsNullOrEmpty(OrderFrom) && cancelItemType == "Ordered")
                                    {
                                        disableCancelOrder = "disabled";
                                    }

                                    str += "<div class='col-lg-3 divCancelOrdered'><div class='divCancelOrdered'> <span id='lblCancelOrdered'>Order Status </span> <select " + disableCancelOrder + " id='ddlCancelOrdered" + i + "' name='ddlCancelOrdered" + i + "' class='txt-item CancelOrdered'>" + ddlcancel + "</select><input type='hidden' id='hfOrderStatus" + i + "' name='hfOrderStatus" + i + "' runat='server' value='" + cancelItemType + "'/></div></div>";

                                    str += "<div id='divToalPriceShow' class='col-lg-3 divToalPriceShow " + DivQuotePrice + "'>";

                                    str += "    <div style='padding-top: 10px;float: left;width: 100%;'><span id='lblTotlPriceJBHIFI" + i + "' class='lblTotlPriceJBHIFI' style='width: 50%; float: left; font-weight: bold;display:none;'>Total JBHIFI Price</span>";
                                    str += "<span id='lblTotlPriceAmountJBHIFI" + i + "' class='lblTotlPriceAmountJBHIFI' style='width: 50%; float: left;  padding-left: 10px;'></span></div>";
                                    str += "   <div style='padding-top: 10px;float: left;width: 100%;'><span id='lblTotlPriceGG" + i + "' class='lblTotlPriceGG' style='width: 50%; float: left; font-weight: bold;display:none;'>Total GG Price</span>";

                                    str += " <span id='lblTotlPriceAmountGG" + i + "' class='lblTotlPriceAmountGG' style='width: 50%; float: left;  padding-left: 10px;'></span></div>";
                                    str += "</div>";
                                }
                                if (oCSCase.StatusName.Value != "Saved")
                                {

                                    str += "<div class='col-lg-3 VendorOrderNumber'><span id='lblVendorOrderNumber'>Vendor Order Number</span><input type='text' " + Radonly + " name='txtVendorOrderNumber" + i + "'  id='txtVendorOrderNumber" + i + "' class='txt-item' value='" + VendorOrderNumber + "'/></div>";
                                }

                                str += "<div class='col-lg-12' style='float:left;'><span id='lblDescription'  style='float:left;'>Description</span><textarea " + Radonly + " name='txtDesc" + i + "' rows='3' cols='20' id='txtDesc" + i + "' class='txt-item' style='clear:both;'>" + Desc + "</textarea><span class='spnChar' style='float: left; vertical-align: bottom; margin-top: 30px;'>250</span></div>";



                            }

                            if (string.IsNullOrEmpty(Items))
                            {
                                str += "<div class='row' ><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right SubmitTo'  style='padding-right:31px;'><input id='btnAdd' type='button' value='ADD' class='btn btn-success btnAdd' onclick='addItem(" + i + ");'  style='line-height: 20px;margin-right: 5px;'><a id='btnDelete' href='javascript:void(0)' class='btn btn-danger btnDelete' onclick='DeleteDiv(this);'><i class='zmdi zmdi-delete zmdi-hc-fw'></i> Delete</a></div></div>";
                            }
                            else
                            {
                                str += "<div class='row' ><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right SubmitTo'>&nbsp;</div></div>";
                            }
                            str += "<input type='hidden' name='hfddlBrand" + i + "' id='hfddlBrand" + i + "' value='" + oUDFAccess.GetFieldValue(BrandudfKey, oCSCase.Key) + "'><input type='hidden' name='hfProduct" + i + "' id='hfProduct" + i + "' value='" + oUDFAccess.GetFieldValue(ProductudfKey, oCSCase.Key) + "'>";

                            string styleDisplay = "none";
                            if (!string.IsNullOrEmpty(vendor))
                            {
                                styleDisplay = "inline";
                            }

                            //if (oCSCase.StatusName.Value != "Saved")
                            //{
                            if (!string.IsNullOrEmpty(VendorType))
                            {
                                //string[] arr = vendor.Split(',');
                                //for (int j = 0; j < arr.Length; j++)
                                //{
                                string disabled = "";
                                bool flag = true;
                                string OptionCostValue = "";
                                string backgroundColor = "";
                                string VendorMode = VendorType;
                                string PlaceOrderVendor = "";

                                var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " Ordered From"), oCSCase.Key);
                                if (!string.IsNullOrEmpty(OrderFrom))
                                {
                                    flag = false;
                                }


                                if (!string.IsNullOrEmpty(vendor))
                                {
                                    //if (Convert.ToString(arr[j]) == "The Good Guys")
                                    if (VendorMode.ToLower() == "The Good Guys".ToLower())
                                    {
                                        if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("The Good Guys"))
                                        {
                                            PlaceOrderVendor = "ORDERED FROM GG";
                                        }
                                        else
                                        {
                                            PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order GG" : PlaceOrderVendor;
                                        }
                                        VendorMode = "GG";
                                        backgroundColor = "background-color:#90EE90;";
                                        PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Comment"), oCSCase.Key);
                                        PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Delivery"), oCSCase.Key);
                                        PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Pick Up"), oCSCase.Key);
                                        PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Warranty"), oCSCase.Key);
                                        selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked GG" + i), oCSCase.Key);

                                    }
                                    else if (VendorMode.ToLower() == "JBHiFI".ToLower())
                                    {
                                        if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("JBHIFI"))
                                        {
                                            PlaceOrderVendor = "ORDERED FROM JBHIFI";
                                        }
                                        else
                                        {
                                            PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order JBHIFI" : PlaceOrderVendor;
                                        }
                                        VendorMode = "JBHIFI";
                                        backgroundColor = "background-color:Yellow";
                                        PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Comment"), oCSCase.Key);
                                        PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Delivery"), oCSCase.Key);
                                        PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Pick Up"), oCSCase.Key);
                                        PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Warranty"), oCSCase.Key);
                                        selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked JBHIFI" + i), oCSCase.Key);

                                    }
                                }
                                string str1 = "";
                                string optionsText = "";
                                string priceCheckSelected = "";
                                string displayPriceOptionValues = "";
                                if (mode == "Direct Order")
                                {
                                    if (RequestType == "Vendor" && (EnquiryType == "CURRORD" || EnquiryType == "CO"))
                                    {
                                        str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + VendorMode + " Comments</span><input name='txtP" + VendorMode + "Comment" + i + "' type='text' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO1' style='width: 100%; " + backgroundColor + "' value='" + PComment + "'></div>";
                                    }

                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                        priceCheckSelected = "checked";
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }
                                    str1 += "<div  class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                    var OptionCostValueInstall = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                    var OptionCostValueRemoval = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                    string displayType = "style='display:none;'";
                                    if (!string.IsNullOrEmpty(PPD) || !string.IsNullOrEmpty(OptionCostValueInstall) || !string.IsNullOrEmpty(OptionCostValueRemoval))
                                        displayType = "";

                                    displayPriceOptionValues = "";
                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                        priceCheckSelected = "checked";
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }
                                    string strValue = "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                    displayPriceOptionValues = "";
                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                        priceCheckSelected = "checked";
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }
                                    optionsText = VendorMode + " +Install Price";
                                    strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueInstall + "'></div>";

                                    displayPriceOptionValues = "";
                                    priceCheckSelected = "";
                                    if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                        priceCheckSelected = "checked";
                                    else
                                    {
                                        if (EnquiryType == "CO")
                                            displayPriceOptionValues = "style='display:none;'";
                                    }

                                    optionsText = VendorMode + " +Removal Price";
                                    strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueRemoval + "'></div>";

                                    string selected = "";
                                    if (string.IsNullOrEmpty(displayType))
                                        selected = "checked";

                                    str1 += "<div class='col-lg-8' style='height: 86px;'>";
                                    str1 += "<input type='checkbox'  id='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' class='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' style='margin: 0px 0px 0px 5px; display: inline-block;' " + selected + ">";
                                    str1 += "<span id='lblPriceDelivery11111'>Show additional prices</span><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /></div>";

                                    str1 += strValue;
                                }
                                else
                                {
                                    if (oCSCase.StatusName.Value != "Saved")
                                    {
                                        if (RequestType != "Email")
                                        {
                                            if (RequestType == "Vendor" && (EnquiryType == "CURRORD" || EnquiryType == "CO"))
                                            {
                                                str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + VendorMode + " Comments</span><input name='txtP" + VendorMode + "Comment" + i + "' type='text' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO1' style='width: 100%; " + backgroundColor + "' value='" + PComment + "'></div>";
                                            }
                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div  class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox'  " + priceCheckSelected + " data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                            optionsText = VendorMode + " +Install Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                                priceCheckSelected = "checked";

                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + " data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                            optionsText = VendorMode + " +Removal Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + " data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";


                                            //OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Del Rem Cost"), oCSCase.Key);
                                            //optionsText = VendorMode + " Del+Rem Cost";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' data-id='" + VendorMode + " Del/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Warranty"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Warranty Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + " data-id='" + VendorMode + " Warranty Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPWarranty" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Warranty Cost' id='txtPWarranty" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";


                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Warranty Desc"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Description";

                                            str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input name='txtPWarrantyDesc" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Warranty Cost' id='txtPWarranty" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";
                                        }
                                    }
                                }

                                if (oCSCase.StatusName.Value != "Saved")
                                {
                                    if (!string.IsNullOrEmpty(VendorMode))
                                    {
                                        if (mode != "Direct Order" && RequestType != "Email" && RequestType != "Vendor")
                                        {
                                            str += "<div class='row' id='divDirectOrder" + i + "' class='divOrder" + VendorMode + "' style='display:inline;'><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /><div class='col-lg-12'><span id='lblPComment" + i + "'> " + VendorMode + " Comments</span>";
                                            str += "<textarea name='txtP" + VendorMode + "Comment" + i + "'  data-id='" + VendorMode + " Comment' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO" + i + "' style='width: 100%; " + backgroundColor + "' >" + PComment + "</textarea></div>";
                                        }
                                        else if (RequestType == "Vendor")
                                        {
                                            str += "<div class='row' id='divDirectOrder" + i + "' class='divOrder" + VendorMode + "' style='display:inline;'><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' />";
                                        }
                                        str += str1;

                                        string hide = "";
                                        if (!flag)
                                        {
                                            disabled = "disabled='disabled'";
                                            if (!PlaceOrderVendor.Contains("ORDERED FROM"))
                                                hide = "hide";
                                        }

                                        string Order = i + "-" + VendorMode;
                                        StringBuilder strScript = new StringBuilder();

                                        strScript.Append("\"" + Order + "\"");
                                        var styleRedColorbutton = "";
                                        if (!string.IsNullOrEmpty(disabled))
                                        {
                                            styleRedColorbutton = "style='background:red !important;'";
                                        }
                                        if (mode == "Direct Order")
                                            display = "display:none;";

                                        str += "<div class='col-lg-2 " + hide + "' " + hidePlaceOrder + "><input " + styleRedColorbutton + " type='button' id='btnPlaceOrder" + i + "' " + disabled + " class='btn btnPlaceOrder' onclick='PlaceOrder(" + strScript + ");' value='" + PlaceOrderVendor + "' style='" + display + "'/></div>";
                                        if (RequestType == "Vendor")
                                        {

                                            Opportunity opp = GetOpp(i.ToString());
                                            if (opp != null && opp.Status.Value != 3)
                                            {

                                                HfIsConfirmOrderPage.Value = "true";
                                                str += "<div class='col-lg-12 text-right'><input type='button' name='btnConfirmOrder" + i + "' data-OppKey='" + opp.Key + "' value='Confirm Order' id='btnConfirmOrder" + i + "' class='btn btn-success ConfirmOrder'>";
                                                str += "<input type='hidden' id='hfOppKey" + i + "' name='hfOppKey" + i + "' value='" + opp.Key + "'></div>";
                                            }
                                            else
                                            {
                                                btnCreateEnquiry.Visible = false;
                                            }

                                        }
                                        if (mode != "Direct Order")
                                            str += "</div>";
                                    }
                                }
                                else
                                {
                                    str += str1;
                                }
                                //}
                            }
                            else
                            {
                                var OrderFrom = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " Ordered From"), oCSCase.Key);
                                bool flag = true;
                                if (!string.IsNullOrEmpty(OrderFrom))
                                {
                                    flag = false;
                                }
                                string[] arr = vendor.Split(',');
                                for (int j = 0; j < arr.Length; j++)
                                {
                                    string disabled = "";

                                    string OptionCostValue = "";
                                    string backgroundColor = "";
                                    string VendorMode = VendorType;
                                    string PlaceOrderVendor = "";

                                    if (!string.IsNullOrEmpty(vendor))
                                    {
                                        if (Convert.ToString(arr[j]) == "The Good Guys")
                                        {
                                            if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("The Good Guys"))
                                            {
                                                PlaceOrderVendor = "ORDERED FROM GG";
                                            }
                                            else
                                            {
                                                PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order GG" : PlaceOrderVendor;
                                            }
                                            VendorMode = "GG";
                                            backgroundColor = "background-color:#90EE90;";
                                            PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Comment"), oCSCase.Key);
                                            PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Delivery"), oCSCase.Key);
                                            PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Pick Up"), oCSCase.Key);
                                            PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " GG Price Warranty"), oCSCase.Key);
                                            selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked GG" + i), oCSCase.Key);
                                        }
                                        else if (Convert.ToString(arr[j]).ToLower() == "JBHiFI".ToLower())
                                        {
                                            if (!string.IsNullOrEmpty(OrderFrom) && OrderFrom.Contains("JBHIFI"))
                                            {
                                                PlaceOrderVendor = "ORDERED FROM JBHIFI";
                                            }
                                            else
                                            {
                                                PlaceOrderVendor = string.IsNullOrEmpty(PlaceOrderVendor) ? "Place Order JBHIFI" : PlaceOrderVendor;
                                            }
                                            VendorMode = "JBHIFI";
                                            backgroundColor = "background-color:Yellow";
                                            PComment = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Comment"), oCSCase.Key);
                                            PPD = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Delivery"), oCSCase.Key);
                                            PPU = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Pick Up"), oCSCase.Key);
                                            PPUWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " JBHIFI Price Warranty"), oCSCase.Key);
                                            selectedPriceItemtype = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Item Price Tick Marked JBHIFI" + i), oCSCase.Key);

                                        }
                                    }

                                    string str1 = "";
                                    string optionsText = "";
                                    string priceCheckSelected = "";
                                    string displayPriceOptionValues = "";
                                    if (mode == "Direct Order")
                                    {
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        str1 += "<div  class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                        var OptionCostValueInstall = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                        var OptionCostValueRemoval = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                        string displayType = "style='display:none;'";
                                        if (!string.IsNullOrEmpty(PPD) || !string.IsNullOrEmpty(OptionCostValueInstall) || !string.IsNullOrEmpty(OptionCostValueRemoval))
                                            displayType = "";

                                        displayPriceOptionValues = "";
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        string strValue = "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                        displayPriceOptionValues = "";
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        optionsText = VendorMode + " +Install Price";
                                        strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueInstall + "'></div>";

                                        displayPriceOptionValues = "";
                                        priceCheckSelected = "";
                                        if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                            priceCheckSelected = "checked";
                                        else
                                        {
                                            if (EnquiryType == "CO")
                                                displayPriceOptionValues = "style='display:none;'";
                                        }
                                        optionsText = VendorMode + " +Removal Price";
                                        strValue += "<div " + displayPriceOptionValues + " class='DirectOrderItemsPrice" + i + " col-lg-" + col_div + "' " + displayType + "><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValueRemoval + "'></div>";

                                        string selected = "";
                                        if (string.IsNullOrEmpty(displayType))
                                            selected = "checked";

                                        str1 += "<div class='col-lg-8' style='height: 86px;'>";
                                        str1 += "<input type='checkbox'  id='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' class='ShowAdditionalPrice" + i + "' name='ShowAdditionalPrice" + i + "' style='margin: 0px 0px 0px 5px; display: inline-block;' " + selected + ">";
                                        str1 += "<span id='lblPriceDelivery11111'>Show additional prices</span><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /></div>";

                                        str1 += strValue;
                                    }
                                    else
                                    {
                                        if (oCSCase.StatusName.Value != "Saved")
                                        {
                                            if (RequestType == "Vendor" && (EnquiryType == "CURRORD" || EnquiryType == "CO"))
                                            {
                                                str += "<div class='col-lg-12'><span id='lblComments" + i + "'>" + VendorMode + " Comments</span><input name='txtP" + VendorMode + "Comment" + i + "' type='text' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO1' style='width: 100%; " + backgroundColor + "' value='" + PComment + "'></div>";
                                            }
                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Product Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div  class='col-lg-3'><span id='lblPricePickUp" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' " + priceCheckSelected + " data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PickUp" + i + "' data-id='" + VendorMode + " Price PickUp' type='text' id='txtP" + VendorMode + "PickUp" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPU + "'></div>";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Delivery Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPriceDelivery" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox'  " + priceCheckSelected + "  data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtP" + VendorMode + "PriceDelivery" + i + "' type='text' data-id='" + VendorMode + " Price Delivery' id='txtP" + VendorMode + "PriceDelivery" + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PPD + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                            optionsText = VendorMode + " +Install Price";

                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Install Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + "  data-id='" + VendorMode + " Del/Installation Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPInst" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Inst Cost' id='txtPInst" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                            optionsText = VendorMode + " +Removal Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Removal Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + "  data-id='" + VendorMode + " Del/Inst/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelInstRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelInstRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";


                                            //OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Del Rem Cost"), oCSCase.Key);
                                            //optionsText = VendorMode + " Del+Rem Cost";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'> " + optionsText + ":$</span><input type='checkbox' data-id='" + VendorMode + " Del/Rem Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPDelRem" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Removal Cost' id='txtPDelRem" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Warranty"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Price";

                                            displayPriceOptionValues = "";
                                            priceCheckSelected = "";
                                            if (!string.IsNullOrEmpty(selectedPriceItemtype) && selectedPriceItemtype.Contains("Warranty Price"))
                                                priceCheckSelected = "checked";
                                            else
                                            {
                                                if (EnquiryType == "CO")
                                                    displayPriceOptionValues = "style='display:none;'";
                                            }
                                            str1 += "<div " + displayPriceOptionValues + " class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":$</span><input type='checkbox'  " + priceCheckSelected + "  data-id='" + VendorMode + " Warranty Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtPWarranty" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Warranty Cost' id='txtPWarranty" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";

                                            OptionCostValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Warranty Desc"), oCSCase.Key);
                                            optionsText = VendorMode + " Warranty Description";
                                            str1 += "<div class='col-lg-" + col_div + "'><span id='lblPrice" + i + "'>" + optionsText + ":</span><input name='txtPWarrantyDesc" + VendorMode + i + "' type='text' data-id='" + VendorMode + " Warranty Desc' id='txtPWarrantyDesc" + VendorMode + i + "' class='txt-item  PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + OptionCostValue + "'></div>";


                                            //var Comment = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Comment"), oCSCase.Key);
                                            //var WarrantyDesc = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Warranty Desc"), oCSCase.Key);
                                            //var PricePickUp = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Pick Up"), oCSCase.Key);
                                            //var PriceDelivery = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Delivery"), oCSCase.Key);
                                            //var PriceInstall = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Install"), oCSCase.Key);
                                            //var PriceRemoval = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Removal"), oCSCase.Key);
                                            //var PriceWarranty = this.oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Price Warranty"), oCSCase.Key);


                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblProductPrice" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtProductPrice" + VendorMode + i + "' data-id='" + VendorMode + " Product Price' type='text' id='txtProductPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PricePickUp + "'></div>";
                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblDeliveryPrice" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtDeliveryPrice" + VendorMode + i + "' data-id='" + VendorMode + " Delivery Price' type='text' id='txtDeliveryPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceDelivery + "'></div>";
                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblInstallPrice" + i + "'> " + VendorMode + " +Install Price:$</span><input type='checkbox' data-id='" + VendorMode + " Install Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtInstallPrice" + VendorMode + i + "' data-id='" + VendorMode + " Install Price' type='text' id='txtInstallPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceInstall + "'></div>";
                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblRemovalPrice" + i + "'> " + VendorMode + " +Removal Price:$</span><input type='checkbox' data-id='" + VendorMode + " Removal Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtRemovalPrice" + VendorMode + i + "' data-id='" + VendorMode + " Removal Price' type='text' id='txtRemovalPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceRemoval + "'></div>";
                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyPrice" + i + "'> " + VendorMode + " Warrenty Price:$</span><input type='checkbox' data-id='" + VendorMode + " Warrenty Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtWarrentyPrice" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Price' type='text' id='txtWarrentyPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceWarranty + "'></div>";
                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyDesc" + i + "'> " + VendorMode + " Warrenty Description:$</span><input type='checkbox' data-id='" + VendorMode + " Warrenty Description' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtWarrentyDesc" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Description' type='text' id='txtWarrentyDesc" + VendorMode + i + "' class='txt-item  PO" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + WarrantyDesc + "'></div>";


                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblProductPrice" + i + "'> " + VendorMode + " Product Price:$</span><input type='checkbox' data-id='" + VendorMode + " Product Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtProductPrice" + VendorMode + i + "' data-id='" + VendorMode + " Product Price' type='text' id='txtProductPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO1" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PricePickUp + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblDeliveryPrice" + i + "'> " + VendorMode + " +Delivery Price:$</span><input type='checkbox' data-id='" + VendorMode + " Delivery Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtDeliveryPrice" + VendorMode + i + "' data-id='" + VendorMode + " Delivery Price' type='text' id='txtDeliveryPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO1" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceDelivery + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblInstallPrice" + i + "'> " + VendorMode + " +Install Price:$</span><input type='checkbox' data-id='" + VendorMode + " Install Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtInstallPrice" + VendorMode + i + "' data-id='" + VendorMode + " Install Price' type='text' id='txtInstallPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO1" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceInstall + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblRemovalPrice" + i + "'> " + VendorMode + " +Removal Price:$</span><input type='checkbox' data-id='" + VendorMode + " Removal Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtRemovalPrice" + VendorMode + i + "' data-id='" + VendorMode + " Removal Price' type='text' id='txtRemovalPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO1" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceRemoval + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyPrice" + i + "'> " + VendorMode + " Warrenty Price:$</span><input type='checkbox' data-id='" + VendorMode + " Warrenty Price' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtWarrentyPrice" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Price' type='text' id='txtWarrentyPrice" + VendorMode + i + "' class='txt-item PlaceOrderAmount PO1" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + PriceWarranty + "'></div>";
                                            ////str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyDesc" + i + "'> " + VendorMode + " Warrenty Description:$</span><input type='checkbox' data-id='" + VendorMode + " Warrenty Description' id='POPrice" + i + "' name='POPrice" + VendorMode + i + "' class='POPrice' style='margin:0 0 0 5px;'><input name='txtWarrentyDesc" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Description' type='text' id='txtWarrentyDesc" + VendorMode + i + "' class='txt-item  PO1" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + WarrantyDesc + "'></div>";
                                            //str1 += "<div class='col-lg-" + col_div + "'><span id='lblWarrentyDesc" + i + "'> " + VendorMode + " Warrenty Description:</span><input name='txtWarrentyDesc" + VendorMode + i + "' data-id='" + VendorMode + " Warrenty Description' type='text' id='txtWarrentyDesc" + VendorMode + i + "' class='txt-item  PO1" + i + "' data-item='" + i + "' style='width: 100%; " + backgroundColor + "' value='" + WarrantyDesc + "'></div>";
                                        }
                                    }
                                    if (oCSCase.StatusName.Value != "Saved")
                                    {
                                        if (!string.IsNullOrEmpty(VendorMode))
                                        {
                                            if (mode != "Direct Order")
                                            {
                                                str += "<div class='row' id='divDirectOrder" + i + "' class='divOrder" + VendorMode + "' style='display:inline;'><input type='hidden' id='hfSeletectedItemType" + VendorMode + i + "' name='hfSeletectedItemType" + VendorMode + i + "' /><div class='col-lg-12'><span id='lblPComment" + i + "'> " + VendorMode + " Comments</span>";
                                                str += "<textarea name='txtP" + VendorMode + "Comment" + i + "'  data-id='" + VendorMode + " Comment' id='txtP" + VendorMode + "Comment" + i + "' class='txt-item PO" + i + "' style='width: 100%; " + backgroundColor + "' >" + PComment + "</textarea></div>";


                                                //var Comment = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + i + " " + VendorMode + " Comment"), oCSCase.Key);

                                                //str += "<textarea name='txtComments" + VendorMode + i + "'  data-id='" + VendorMode + " Comment' id='txtComments" + VendorMode  + i + "' class='txt-item PO" + i + "' style='width: 100%; " + backgroundColor + "' >" + PComment + "</textarea></div>";
                                            }
                                            str += str1;


                                            string hide = "";
                                            if (!flag)
                                            {
                                                disabled = "disabled='disabled'";
                                                if (!PlaceOrderVendor.Contains("ORDERED FROM"))
                                                    hide = "hide";
                                            }

                                            string Order = i + "-" + VendorMode;
                                            StringBuilder strScript = new StringBuilder();

                                            strScript.Append("\"" + Order + "\"");

                                            var btnPlaceOrderText = "Order";
                                            var styleRedColorbutton = "";
                                            if (!string.IsNullOrEmpty(disabled))
                                            {
                                                btnPlaceOrderText = "Ordered";
                                                styleRedColorbutton = "style='background:red !important;'";
                                            }
                                            if (mode == "Direct Order")
                                                display = "display:none;";

                                            //str += "<div class='col-lg-2 " + hide + "' " + hidePlaceOrder + "><input type='button' id='btnPlaceOrder" + i + "' " + disabled + " class='btn btnPlaceOrder' onclick='PlaceOrder(" + strScript + ");' value='" + PlaceOrderVendor + "' style='" + display + "'/></div>";
                                            str += "<div class='col-lg-2 " + hide + "' " + hidePlaceOrder + "><input " + styleRedColorbutton + " type='button' id='btnPlaceOrder" + i + "' " + disabled + " class='btn btnPlaceOrder' onclick='PlaceOrder(" + strScript + ");' value='" + btnPlaceOrderText + "' style='" + display + "'/></div>";

                                            if (RequestType == "Vendor")
                                            {
                                                Opportunity opp = GetOpp(i.ToString());
                                                if (opp != null && opp.Status.Value != 3)
                                                {
                                                    HfIsConfirmOrderPage.Value = "true";
                                                    str += "<div class='col-lg-12 text-right'><input type='button' name='btnConfirmOrder" + i + "' data-OppKey='" + opp.Key + "' value='Confirm Order' id='btnConfirmOrder" + i + "' class='btn btn-success ConfirmOrder'>";
                                                    str += "<input type='hidden' id='hfOppKey" + i + "' name='hfOppKey" + i + "' value='" + opp.Key + "'></div>";
                                                }
                                                else
                                                {
                                                    btnCreateEnquiry.Visible = false;
                                                }
                                            }
                                            if (mode != "Direct Order")
                                                str += "</div>";
                                        }
                                    }
                                    else
                                    {
                                        str += str1;
                                    }

                                }
                            }
                            //}

                            //str += "<div class='row'style='display: inline;'><div class='col-lg-3'><span id='lblDirectOrder'>Direct Order</span><span class='DO'><input id='chkGG" + i + "' type='checkbox' name='chkGG" + i + "' onclick='diplayOrderList(this);' " + ggchecked + "><label for='chkGG" + i + "'>GG</label></span>";
                            //str += "<span class='DO'><input id='chkJBHIFI" + i + "' type='checkbox' name='chkJBHIFI" + i + "' onclick='diplayOrderList(this);' " + jbhifichecked + "><label for='chkJBHIFI" + i + "'>JBHIFI</label></span></div></div>";

                            str += "</div>";


                            hfCountItems.Value = Convert.ToString(i);
                            ItemsNumber += i.ToString() + ",";
                        }
                    }


                    dvAddItemsHTML.InnerHtml = "<div class='container' style='background-color: #fdfdfd; padding: 10px 10px;'>" + str + "</div>";
                    ViewState["ItemsNumber"] = ItemsNumber;
                }
            }
            catch (Exception ex)
            {
                common.LogError(ex, "CreateCSCase", "loadCSCase, Case Number:-" + CaseNo);
            }
        }
        public string OptionList(string UDF)
        {
            string OptionList = "";
            try
            {
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadCSCaseUdfSetupList("KEY(" + GetUdfCSCaseKey(UDF) + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                        OptionList += "<option value='" + udfSetupTableItem.Name.Value + "'>" + udfSetupTableItem.Name.Value + "</option>";

                }
            }
            catch (Exception ex)
            {

            }
            return OptionList;
        }

        public void fillEnquiryNotesHistory(int countNote)
        {
            NoteList source1 = this.oNoteAccess.ReadList("KEY(" + this.Request.QueryString["CurrentKey"] + ")");
            string str1 = "";
            int num1 = 1;
            //this.divenquirynoteshistory.InnerHtml = str1;
            IEnumerable<Note> notes = (uint)countNote <= 0U ? (IEnumerable<Note>)source1.Cast<Note>().OrderByDescending<Note, string>((Func<Note, string>)(x => x.DateTime.Value)).ToList<Note>() : (IEnumerable<Note>)source1.Cast<Note>().OrderByDescending<Note, string>((Func<Note, string>)(x => x.DateTime.Value)).Take<Note>(countNote).ToList<Note>();
            int num2 = 0;
            foreach (Note note in notes)
            {
                if (note.TypeName.Value == "History")
                    continue;

                ++num2;
                UserList userList = this.oUserAccess.ReadList("KEY(" + note.Creator.Value + ")");
                string str2 = "";
                foreach (AppMember appMember in (CollectionBase)userList)
                    str2 = appMember.DisplayName.ToString();
                string str3 = "+";
                string str4 = " style='display: none;padding: 11px 46px 11px 87px;text-align: justify;line-height: 21px;' ";
                string str5 = "";
                if (num2 <= 3)
                {
                    str5 = "";
                    str3 = "+";
                    str4 = " style='display: none;padding: 11px 46px 11px 87px;text-align: justify;line-height: 21px;' ";
                }
                DateTime dateTime = Convert.ToDateTime(note.DateTime.Value);
                str1 = str1 + "<div class='row'><div class='col-lg-12'><div class='row heading-notes'><div class='col-lg-half text-left' style='float:left;'><div class='divshowhide btn btn-primary show_hide' data-text='' data-id='divshowhide" + (object)num1 + "'>" + str3 + " </div>";
                str1 = str1 + "</div><div class='col-lg-2 text-left textNoteHeading'><span>" + dateTime.ToString("dd/MM/yyyy hh:mm:ss").Replace('-', '/') + "</span>";
                str1 = str1 + "</div><div class='col-lg-2 textNoteHeading'><span class='heading-notetxt'><b>Creator:</b></span><span>" + str2 + "</span></div><div class='col-lg-2 textNoteHeading'><span class='heading-notetxt'><b>Cat:</b></span><span>" + (!string.IsNullOrEmpty(note.Category.Value) ? note.Category.Value : "Manual") + "</span></div>";
                string source2 = "";
                string str6 = "";
                if (string.IsNullOrEmpty(source2))
                {
                    source2 = Regex.Replace(note.Text.Value, "<.*?>", string.Empty);
                    str6 = Regex.Replace(note.Text.Value, "\\r\\n?|\\n", "<br />");
                }
                if (source2.Length > 44)
                    source2 = new string(source2.Take<char>(44).ToArray<char>()) + "...";
                str1 = str1 + "<div class='col-lg-5 textNoteHeading' style='text-align: left;'><span class='heading-notetxt'><b>Note: </b></span><span>" + source2 + "</span></div>";
                str1 = str1 + "</div></div><div class='col-lg-12 divshow " + str5 + "' id='divshowhide" + (object)num1 + "' " + str4 + ">" + str6 + "</div></div>";
                ++num1;
            }
            //if (string.IsNullOrEmpty(str1))
            //    this.divenquirynoteshistory.InnerHtml = "<div class='row'><div class='col-lg-12 text-center'><span style='font-size:17px;color: red'><b>Record not found</b></span></div></div>";
            //else
            //    this.divenquirynoteshistory.InnerHtml = str1;
        }

        public void fillClientNotesHistory(int countNote)
        {
            sLoginString = loginToMax();
            string CaseNumber = "US-" + lblEnquiryNumber.Text;
            if (!string.IsNullOrEmpty(CaseNumber))
            {
                string str1 = "";
                string RequestType = Request.QueryString["RequestType"];
                //if (RequestType == "Vendor" || RequestType == "Email")
                //{
                //    var itemNumber = hfItemsNumber.Value;
                //    if (string.IsNullOrEmpty(itemNumber) && ViewState["ItemsNumber"] != null)
                //    {
                //        itemNumber = Convert.ToString(ViewState["ItemsNumber"]);
                //    }
                //    if (!string.IsNullOrEmpty(itemNumber))
                //    {
                //        string str2 = "";
                //        this.divnoteshistory.InnerHtml = str2;

                //        itemNumber = itemNumber.TrimEnd(',');
                //        var arrItems = itemNumber.Split(',');
                //        foreach (var item in arrItems)
                //        {
                //            string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                //            string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                //            string UDFKey = GetUdfOppKey("Submit To");
                //            string Vendor = Request.QueryString["Vendor"];
                //            if (Vendor == "GG")
                //            {
                //                Vendor = "The Good Guys";
                //            }
                //            else if (Vendor.ToLower() == "JBHiFI".ToLower())
                //            {
                //                Vendor = "JBHiFI";
                //            }
                //            //string search = String.Format("LIKE(EQ(UDF," + UDFKey + ",'" + Vendor + "'))");
                //            //string search = String.Format("LIKE(UDF," + UDFKey + ", \"%{0}\")", Vendor);
                //            string Cate1 = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", UDFKey, Vendor);
                //            string searchkey = "AND( EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "'), " + Cate1 + ", EQ(UDF," + UDFItemNumberKey + "," + item + "))";

                //            Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList( searchkey);

                //            foreach (Maximizer.Data.Opportunity opp in OppList)
                //            {
                //                str1 = opp.Key;

                //                var Brand = oUDFAccess.GetFieldValue(this.GetUdfOppKey("Brand Name"), str1);
                //                var Product = oUDFAccess.GetFieldValue(this.GetUdfOppKey("Product Name"), str1);
                //                int countCSCase = 10;
                //                foreach (MxObject read in (CollectionBase)this.oOppAccess.ReadList("Key(" + str1 + ")"))
                //                {
                //                    NoteList source1 = this.oNoteAccess.ReadList("AND(KEY(" + read.Key + "))");
                //                    int num1 = 1 + countCSCase;
                //                    //IEnumerable<Note> notes = (uint)countNote <= 0U ? (IEnumerable<Note>)source1.Cast<Note>().OrderByDescending<Note, string>((Func<Note, string>)(x => x.DateTime.Value)).ToList<Note>() : (IEnumerable<Note>)source1.Cast<Note>().OrderByDescending<Note, string>((Func<Note, string>)(x => x.DateTime.Value)).Take<Note>(countNote).ToList<Note>();
                //                    int num2 = 0;
                //                    foreach (Note note in source1)
                //                    {
                //                        if (note.TypeName.Value == "History")
                //                            continue;

                //                        if (note.Category.Value != "Vendor")
                //                            continue;

                //                        ++num2;
                //                        UserList userList = this.oUserAccess.ReadList("KEY(" + note.Creator.Value + ")");
                //                        string str3 = "";
                //                        foreach (AppMember appMember in (CollectionBase)userList)
                //                            str3 = appMember.DisplayName.ToString();
                //                        string str4 = "+";
                //                        string str5 = " style='display: none;padding: 11px 46px 11px 87px;text-align: justify;line-height: 21px;' ";
                //                        string str6 = "";
                //                        if (num2 <= 3)
                //                        {
                //                            str6 = "";
                //                            str4 = "+";
                //                            str5 = " style='display: none;padding: 11px 46px 11px 87px;text-align: justify;line-height: 21px;' ";
                //                        }
                //                        DateTime dateTime = Convert.ToDateTime(note.DateTime.Value);
                //                        str2 = str2 + "<div class='row'><div class='col-lg-12'><div class='row heading-notes'><div class='col-lg-half text-left' style='float:left;'><div class='divshowhide btn btn-primary show_hide' data-text='' data-id='divClientshowhide" + (object)num1 + "'>" + str4 + " </div>";
                //                        str2 = str2 + "</div><div class='col-lg-2 text-left textNoteHeading'><label class='text-primary text-center' style='vertical-align: top; width: 32%;'><b>Date:</b></label><span>" + dateTime.ToString("dd/MM/yyyy").Replace('-', '/') + "</span>";
                //                        str2 = str2 + "</div><div class='col-lg-2 textNoteHeading'><span class='heading-notetxt'><b>Creator:</b></span><span>" + str3 + "</span></div><div class='col-lg-1 textNoteHeading'><span class='heading-notetxt'><b>Cat:</b></span><span>" + (!string.IsNullOrEmpty(note.Category.Value) ? note.Category.Value : "Manual") + "</span></div>";
                //                        str2 += "<div class='col-lg-1 textNoteHeading' style='width:131px;'><span class='heading-notetxt'><b>Brand:</b></span><span>" + Brand + "</span></div><div class='col-lg-2 textNoteHeading'><span class='heading-notetxt'><b>Product:</b></span><span>" + Product + "</span></div>";
                //                        string source2 = "";
                //                        string str7 = "";
                //                        if (string.IsNullOrEmpty(source2))
                //                        {
                //                            source2 = Regex.Replace(note.Text.Value, "<.*?>", string.Empty);
                //                            str7 = Regex.Replace(note.Text.Value, "\\r\\n?|\\n", "<br />");
                //                        }
                //                        if (source2.Length > 44)
                //                            source2 = new string(source2.Take<char>(44).ToArray<char>()) + "...";
                //                        str2 = str2 + "<div class='col-lg-3 textNoteHeading' style='text-align: left;height: 34px !important'><span class='heading-notetxt'><b>Note: </b></span><span>" + source2 + "</span></div>";
                //                        str2 = str2 + "</div></div><div class='col-lg-12 divshow " + str6 + "' id='divClientshowhide" + (object)num1 + "' " + str5 + ">" + str7 + "</div></div>";
                //                        ++num1;
                //                    }
                //                    countCSCase += 10;
                //                }
                //            }
                //        }

                //        if (string.IsNullOrEmpty(str2))
                //            this.divnoteshistory.InnerHtml = "<div class='row'><div class='col-lg-12 text-center'><span style='font-size:17px;color: red'><b>Record not found</b></span></div></div>";
                //        else
                //            this.divnoteshistory.InnerHtml = str2;
                //    }

                //}
                //else
                //{
                string filter = "EQ(CaseNumber," + CaseNumber + ")";
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                foreach (CSCase oCSCase in oCSCaseList)
                {
                    str1 = oCSCase.Key;
                }
                string str2 = "";
                this.divnoteshistory.InnerHtml = str2;
                string vendor = Request.QueryString["Vendor"];
                int countCSCase = 10;
                string EnquiryType = Request.QueryString["ET"];
                hfEnquiryType.Value = EnquiryType;
                Maximizer.Data.NoteList oNoteList = new NoteList();
                if (!string.IsNullOrEmpty(vendor) && vendor == "JBHIFI")
                {
                    //oNoteList = oNoteAccess.ReadList("AND(KEY(" + abKey + "),EQ(Category,\"" + categoryType + "\"))");
                    var categoryType = "JBHIFI";
                    var AllVendors = "All Vendors";
                    oNoteList = oNoteAccess.ReadList("AND(KEY(" + str1 + "),OR(EQ(Category,\"" + categoryType + "\"),EQ(Category,\"" + AllVendors + "\")))");
                    //oNoteList = oNoteAccess.ReadList("AND(KEY(" + str1 + "), EQ(Category," + "JBHIFI" + "))");
                }
                else if (!string.IsNullOrEmpty(vendor) && vendor == "GG")
                {
                    var categoryType = "THEGOODGUYS";
                    var AllVendors = "All Vendors";
                    oNoteList = oNoteAccess.ReadList("AND(KEY(" + str1 + "),OR(EQ(Category,\"" + categoryType + "\"),EQ(Category,\"" + AllVendors + "\")))");

                    // oNoteList = oNoteAccess.ReadList("AND(KEY(" + str1 + "), EQ(Category," + "THEGOODGUYS" + "))");
                }
                else if (RequestType == "CallCenter" || string.IsNullOrEmpty(Request.QueryString["CaseNo"]))
                {
                    oNoteList = oNoteAccess.ReadList("AND(KEY(" + str1 + "))");
                }

                NoteList source1 = oNoteList;
                int num1 = 1 + countCSCase;
                //IEnumerable<Note> notes = (uint)countNote <= 0U ? (IEnumerable<Note>)source1.Cast<Note>().OrderByDescending<Note, string>((Func<Note, string>)(x => x.DateTime.Value)).ToList<Note>() : (IEnumerable<Note>)source1.Cast<Note>().OrderByDescending<Note, string>((Func<Note, string>)(x => x.DateTime.Value)).Take<Note>(countNote).ToList<Note>();
                int num2 = 0;
                foreach (Note note in source1)
                {
                    if (note.TypeName.Value == "History")
                        continue;

                    //if (RequestType == "Vendor" || RequestType == "Email")
                    //{
                    //    if (note.Category.Value != "Vendor")
                    //    {
                    //        continue;
                    //    }
                    //}

                    ++num2;
                    UserList userList = this.oUserAccess.ReadList("KEY(" + note.Creator.Value + ")");
                    string str3 = "";
                    foreach (AppMember appMember in (CollectionBase)userList)
                        str3 = appMember.DisplayName.ToString();
                    string str4 = "+";
                    string str5 = " style='display: none;padding: 11px 46px 11px 87px;text-align: justify;line-height: 21px;' ";
                    string str6 = "";
                    if (num2 <= 3)
                    {
                        str6 = "";
                        str4 = "+";
                        str5 = " style='display: none;padding: 11px 46px 11px 87px;text-align: justify;line-height: 21px;' ";
                    }
                    DateTime dateTime = Convert.ToDateTime(note.DateTime.Value);
                    str2 = str2 + "<div class='row'><div class='col-lg-12'><div class='row heading-notes'><div class='col-lg-half text-left' style='float:left;'><div class='divshowhide btn btn-primary show_hide' data-text='' data-id='divClientshowhide" + (object)num1 + "'>" + str4 + " </div>";
                    str2 = str2 + "</div><div class='col-lg-2 text-left textNoteHeading'><span>" + dateTime.ToString("dd/MM/yyyy hh:mm:ss").Replace('-', '/') + "</span>";
                    str2 = str2 + "</div><div class='col-lg-2 textNoteHeading'><span class='heading-notetxt'><b>Creator:</b></span><span>" + str3 + "</span></div><div class='col-lg-2 textNoteHeading'><span class='heading-notetxt'><b>Cat:</b></span><span>" + (!string.IsNullOrEmpty(note.Category.Value) ? note.Category.Value : "Manual") + "</span></div>";
                    string source2 = "";
                    string str7 = "";
                    if (string.IsNullOrEmpty(source2))
                    {
                        source2 = Regex.Replace(note.Text.Value, "<.*?>", string.Empty);
                        str7 = Regex.Replace(note.Text.Value, "\\r\\n?|\\n", "<br />");
                    }
                    if (source2.Length > 44)
                        source2 = new string(source2.Take<char>(44).ToArray<char>()) + "...";
                    str2 = str2 + "<div class='col-lg-5 textNoteHeading' style='text-align: left;'><span class='heading-notetxt'><b>Note: </b></span><span>" + source2 + "</span></div>";
                    str2 = str2 + "</div></div><div class='col-lg-12 divshow " + str6 + "' id='divClientshowhide" + (object)num1 + "' " + str5 + ">" + str7 + "</div></div>";
                    ++num1;
                }
                countCSCase += 10;



                if (string.IsNullOrEmpty(str2))
                    this.divnoteshistory.InnerHtml = "<div class='row'><div class='col-lg-12 text-center'><span style='font-size:17px;color: red'><b>Record not found</b></span></div></div>";
                else
                    this.divnoteshistory.InnerHtml = str2;
                // }

            }
        }

        protected void ddlEnquiryFilternote_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //this.fillEnquiryNotesHistory(Convert.ToInt32(this.ddlEnquiryFilternote.SelectedValue));
            }
            catch (Exception ex)
            {
            }
        }

        protected void ddlFilternote_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.fillClientNotesHistory(Convert.ToInt32(this.ddlFilternote.SelectedValue));
            }
            catch (Exception ex)
            {
            }
        }

        public void createNote(string key)
        {
            try
            {
                Note ONote = new Note(key);
                ONote.Text.Value = RadNotes.Text;
                if (!string.IsNullOrEmpty(ddlcategory.SelectedItem.Value))
                {
                    ONote.Category.Value = ddlcategory.SelectedItem.Value;
                }
                oNoteAccess.Insert(ONote);
            }
            catch (Exception ex)
            {
                common.LogError(ex, "CreateCSCase", "CreateNote");
            }
        }

        protected void btnSaveClient_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "HidePopup", "$('#myModal').modal('hide');   $('#loading ').hide();$('#loading ').css('display', 'none');", true);
                string CaseNumber = "US-" + lblEnquiryNumber.Text;
                string RequestType = Request.QueryString["RequestType"];
                //if (RequestType == "Vendor" || RequestType == "Email")
                //{

                //    var itemNumber = hfItemsNumber.Value;
                //    if (string.IsNullOrEmpty(itemNumber) && ViewState["ItemsNumber"] != null)
                //    {
                //        itemNumber = Convert.ToString(ViewState["ItemsNumber"]);
                //    }
                //    if (!string.IsNullOrEmpty(itemNumber))
                //    {
                //        string str2 = "";
                //        this.divnoteshistory.InnerHtml = str2;

                //        itemNumber = itemNumber.TrimEnd(',');
                //        var arrItems = itemNumber.Split(',');
                //        foreach (var item in arrItems)
                //        {
                //            string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                //            string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                //            string UDFKey = GetUdfOppKey("Submit To");
                //            string Vendor = Request.QueryString["Vendor"];
                //            if (Vendor == "GG")
                //            {
                //                Vendor = "The Good Guys";
                //            }
                //            else if (Vendor.ToLower() == "JBHiFI".ToLower())
                //            {
                //                Vendor = "JBHiFI";
                //            }
                //            //string search = String.Format("LIKE(EQ(UDF," + UDFKey + ",'" + Vendor + "'))");
                //            //string search = String.Format("LIKE(UDF," + UDFKey + ", \"%{0}\")", Vendor);
                //            string Cate1 = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", UDFKey, Vendor);
                //            string searchkey = "AND( EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "'), "+ Cate1 + ", EQ(UDF," + UDFItemNumberKey + "," + item + "))";

                //            Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList(searchkey);

                //            foreach (Maximizer.Data.Opportunity opp in OppList)
                //            {
                //                this.createNote(opp.Key);
                //            }
                //        }
                //    }


                //}
                //else
                //{
                if (!string.IsNullOrEmpty(CaseNumber))
                {
                    string filter = "EQ(CaseNumber," + CaseNumber + ")";
                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                    foreach (CSCase oCSCase in oCSCaseList)
                    {
                        this.createNote(oCSCase.Key);
                    }
                }
                //}
                this.fillClientNotesHistory(Convert.ToInt32(this.ddlFilternote.SelectedValue));
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnSaveEnquiry_Click(object sender, EventArgs e)
        {
            try
            {
                this.createNote(this.Request.QueryString["CurrentKey"]);
                //this.fillEnquiryNotesHistory(Convert.ToInt32(this.ddlEnquiryFilternote.SelectedValue));
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnHFPlaceOrder_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["RequestType"] == "CallCenter" && !string.IsNullOrEmpty(VendorPlaceOrder.Value))
            {
                try
                {
                    CSCase oCSCase = null;
                    string CurrentKey = Request.QueryString["CurrentKey"];
                    string i = string.Empty;
                    var VendorType = "";
                    string j = hfCountForPlaceOrder.Value;
                    if (hfCountForPlaceOrder.Value != "1")
                    {
                        i = hfCountForPlaceOrder.Value;
                    }
                    string Revnue = "0";
                    string ItemsForEmail = string.Empty;
                    var Brand = Request.Form["ddlBrand" + i];
                    if (string.IsNullOrEmpty(Brand))
                        Brand = "";

                    var Product = Request.Form["ddlProduct" + i];
                    if (string.IsNullOrEmpty(Product))
                        Product = "";

                    var Model = Request.Form["txtModel" + i];
                    if (string.IsNullOrEmpty(Model))
                        Model = "";

                    var QuotePrice = Request.Form["txtQuotedPriceHF" + i];
                    if (string.IsNullOrEmpty(QuotePrice))
                        QuotePrice = "";
                    else
                        QuotePrice = Regex.Match(QuotePrice, @"\d+").Value;

                    var PayBy = Request.Form["ddlPayBY" + i];
                    if (string.IsNullOrEmpty(PayBy))
                        PayBy = "";

                    var Desc = Request.Form["txtDesc" + i];
                    if (string.IsNullOrEmpty(Desc))
                        Desc = "";

                    var PickupOrDeliveryy = Request.Form["ddlPickupOrDelivery" + i];
                    if (string.IsNullOrEmpty(PickupOrDeliveryy))
                        PickupOrDeliveryy = "";

                    var DeliveryAddress = Request.Form["txtDeliveryAddress" + i];
                    if (string.IsNullOrEmpty(DeliveryAddress))
                        DeliveryAddress = "";

                    var Options = Request.Form["ddlOption" + i];
                    if (string.IsNullOrEmpty(Options))
                        Options = "";

                    var PostCode = Request.Form["txtDeliveryPostCode" + i];
                    if (string.IsNullOrEmpty(PostCode))
                        PostCode = "";

                    var CancelOrdered = Request.Form["hfOrderStatus" + i];
                    //var CancelOrdered = Request.Form["ddlCancelOrdered" + i];
                    if (string.IsNullOrEmpty(CancelOrdered))
                        CancelOrdered = "Ordered";

                    string Comment = "";
                    string Pickup = "";
                    string PickupWarranty = "";
                    string Delivery = "";
                    string Removal = "";
                    string DelRem = "";
                    string Inst = "";
                    string WarrantyCost = "";
                    string DelInstRemWarranty = "";
                    string DelInstWarranty = "";
                    string DelRemWarranty = "";
                    string WarrantyDesc = "";
                    string VendorOrderNumber = "";

                    //var PickupOrDeliveryy = Request.Form["hfPickupOrDeliveryy" + j];
                    var SubmitTo = Request.Form["hfSubmitTo" + j];
                    if (VendorPlaceOrder.Value == "GG" || VendorPlaceOrder.Value == "The Good Guys")
                    {
                        Comment = Request.Form["txtPGGComment" + j];
                        if (string.IsNullOrEmpty(Comment))
                            Comment = "";

                        Pickup = Request.Form["txtPGGPickUp" + j];
                        if (string.IsNullOrEmpty(Pickup))
                        {
                            Pickup = "";
                        }

                        PickupWarranty = Request.Form["txtPGGPickUp/Warranty" + j];
                        if (string.IsNullOrEmpty(PickupWarranty))
                        {
                            PickupWarranty = "";
                        }

                        Delivery = Request.Form["txtPGGPriceDelivery" + j];
                        if (string.IsNullOrEmpty(Delivery))
                        {
                            Delivery = "";
                        }
                        else
                            Delivery = Regex.Match(Delivery, @"\d+").Value;

                        Removal = Request.Form["txtPDelInstRemGG" + j];
                        if (string.IsNullOrEmpty(Removal))
                        {
                            Removal = "";
                        }
                        else
                            Removal = Regex.Match(Removal, @"\d+").Value;

                        DelRem = Request.Form["txtPDelRemGG" + j];
                        if (string.IsNullOrEmpty(DelRem))
                        {
                            DelRem = "";
                        }
                        else
                            DelRem = Regex.Match(DelRem, @"\d+").Value;

                        Inst = Request.Form["txtPInstGG" + j];
                        if (string.IsNullOrEmpty(Inst))
                        {
                            Inst = "";
                        }
                        else
                            Inst = Regex.Match(Inst, @"\d+").Value;

                        WarrantyCost = Request.Form["txtPWarrantyGG" + j];
                        if (string.IsNullOrEmpty(WarrantyCost))
                        {
                            WarrantyCost = "";
                        }
                        else
                            WarrantyCost = Regex.Match(WarrantyCost, @"\d+").Value;

                        DelInstRemWarranty = Request.Form["DelInstRemWarrantyGG" + j];
                        if (string.IsNullOrEmpty(DelInstRemWarranty))
                        {
                            DelInstRemWarranty = "";
                        }
                        else
                            DelInstRemWarranty = Regex.Match(DelInstRemWarranty, @"\d+").Value;

                        DelInstWarranty = Request.Form["txtPDelInstWarrantyGG" + j];
                        if (string.IsNullOrEmpty(DelInstWarranty))
                        {
                            DelInstWarranty = "";
                        }
                        else
                            DelInstWarranty = Regex.Match(DelInstWarranty, @"\d+").Value;

                        DelRemWarranty = Request.Form["txtPDelRemWarrantyGG" + j];
                        if (string.IsNullOrEmpty(DelRemWarranty))
                        {
                            DelRemWarranty = "";
                        }
                        else
                            DelRemWarranty = Regex.Match(DelRemWarranty, @"\d+").Value;

                        WarrantyDesc = Request.Form["txtPWarrantyDescGG" + j];
                        if (string.IsNullOrEmpty(WarrantyDesc))
                        {
                            WarrantyDesc = "";
                        }

                        VendorOrderNumber = Request.Form["txtVendorOrderNumber" + i];
                        if (string.IsNullOrEmpty(VendorOrderNumber))
                        {
                            VendorOrderNumber = "";
                        }
                        #region  add for GG

                        //Comment = Request.Form["txtCommentsGG" + j];
                        //if (string.IsNullOrEmpty(Comment))
                        //{
                        //    Comment = "";
                        //}
                        //PricePickUp = Request.Form["txtProductPriceGG" + j];
                        //if (string.IsNullOrEmpty(PricePickUp))
                        //{
                        //    PricePickUp = "";
                        //}
                        //PriceDelivery = Request.Form["txtDeliveryPriceGG" + j];
                        //if (string.IsNullOrEmpty(PriceDelivery))
                        //{
                        //    PriceDelivery = "";
                        //}
                        //PriceInstall = Request.Form["txtInstallPriceGG" + j];
                        //if (string.IsNullOrEmpty(PriceInstall))
                        //{
                        //    PriceInstall = "";
                        //}
                        //PriceRemoval = Request.Form["txtRemovalPriceGG" + j];
                        //if (string.IsNullOrEmpty(PriceRemoval))
                        //{
                        //    PriceRemoval = "";
                        //}
                        //PriceWarranty = Request.Form["txtWarrentyPriceGG" + j];
                        //if (string.IsNullOrEmpty(PriceWarranty))
                        //{
                        //    PriceWarranty = "";
                        //}
                        //WarrantyDesc = Request.Form["txtWarrentyDescGG" + j];
                        //if (string.IsNullOrEmpty(WarrantyDesc))
                        //{
                        //    WarrantyDesc = "";
                        //}

                        #endregion


                    }
                    else
                    {
                        VendorOrderNumber = Request.Form["txtVendorOrderNumber" + i];
                        if (string.IsNullOrEmpty(VendorOrderNumber))
                        {
                            VendorOrderNumber = "";
                        }
                        Comment = Request.Form["txtPJBHIFIComment" + j];
                        Pickup = Request.Form["txtPJBHIFIPickUp" + j];
                        if (string.IsNullOrEmpty(Pickup))
                        {
                            Pickup = "";
                        }
                        else
                            Pickup = Regex.Match(Pickup, @"\d+").Value;

                        PickupWarranty = Request.Form["txtPJBHIFIPickUp/Warranty" + j];
                        if (string.IsNullOrEmpty(PickupWarranty))
                        {
                            PickupWarranty = "";
                        }
                        else
                            PickupWarranty = Regex.Match(PickupWarranty, @"\d+").Value;

                        Delivery = Request.Form["txtPJBHIFIPriceDelivery" + j];
                        if (string.IsNullOrEmpty(Delivery))
                        {
                            Delivery = "";
                        }
                        else
                            Delivery = Regex.Match(Delivery, @"\d+").Value;

                        Removal = Request.Form["txtPDelInstRemJBHIFI" + j];
                        if (string.IsNullOrEmpty(Removal))
                        {
                            Removal = "";
                        }
                        else
                            Removal = Regex.Match(Removal, @"\d+").Value;

                        DelRem = Request.Form["txtPDelRemJBHIFI" + j];
                        if (string.IsNullOrEmpty(DelRem))
                        {
                            DelRem = "";
                        }
                        else
                            DelRem = Regex.Match(DelRem, @"\d+").Value;

                        Inst = Request.Form["txtPInstJBHIFI" + j];
                        if (string.IsNullOrEmpty(Inst))
                        {
                            Inst = "";
                        }
                        else
                            Inst = Regex.Match(Inst, @"\d+").Value;

                        WarrantyCost = Request.Form["txtPWarrantyJBHIFI" + j];
                        if (string.IsNullOrEmpty(WarrantyCost))
                        {
                            WarrantyCost = "";
                        }
                        else
                            WarrantyCost = Regex.Match(WarrantyCost, @"\d+").Value;

                        DelInstRemWarranty = Request.Form["DelInstRemWarrantyJBHIFI" + j];
                        if (string.IsNullOrEmpty(DelInstRemWarranty))
                        {
                            DelInstRemWarranty = "";
                        }
                        else
                            DelInstRemWarranty = Regex.Match(DelInstRemWarranty, @"\d+").Value;

                        DelInstWarranty = Request.Form["txtPDelInstWarrantyJBHIFI" + j];
                        if (string.IsNullOrEmpty(DelInstWarranty))
                        {
                            DelInstWarranty = "";
                        }
                        else
                            DelInstWarranty = Regex.Match(DelInstWarranty, @"\d+").Value;

                        DelRemWarranty = Request.Form["txtPDelRemWarrantyJBHIFI" + j];
                        if (string.IsNullOrEmpty(DelRemWarranty))
                        {
                            DelRemWarranty = "";
                        }
                        else
                            DelRemWarranty = Regex.Match(DelRemWarranty, @"\d+").Value;

                        WarrantyDesc = Request.Form["txtPWarrantyDescJBHIFI" + j];
                        if (string.IsNullOrEmpty(WarrantyDesc))
                        {
                            WarrantyDesc = "";
                        }

                        #region  add for JBHIFI

                        //Comment = Request.Form["txtPJBHIFIComment" + j];
                        //if (string.IsNullOrEmpty(Comment))
                        //{
                        //    Comment = "";
                        //}
                        //PricePickUp = Request.Form["txtPJBHIFIPickUp" + j];
                        //if (string.IsNullOrEmpty(PricePickUp))
                        //{
                        //    PricePickUp = "";
                        //}
                        //PriceDelivery = Request.Form["txtPJBHIFIPriceDelivery" + j];
                        //if (string.IsNullOrEmpty(PriceDelivery))
                        //{
                        //    PriceDelivery = "";
                        //}
                        //PriceInstall = Request.Form["txtPInstJBHIFI" + j];
                        //if (string.IsNullOrEmpty(PriceInstall))
                        //{
                        //    PriceInstall = "";
                        //}
                        //PriceRemoval = Request.Form["txtPDelInstRemJBHIFI" + j];
                        //if (string.IsNullOrEmpty(PriceRemoval))
                        //{
                        //    PriceRemoval = "";
                        //}
                        //PriceWarranty = Request.Form["txtPDelRemJBHIFI" + j];
                        //if (string.IsNullOrEmpty(PriceWarranty))
                        //{
                        //    PriceWarranty = "";
                        //}
                        //WarrantyDesc = Request.Form["txtWarrentyDescJBHIFI" + j];
                        //if (string.IsNullOrEmpty(WarrantyDesc))
                        //{
                        //    WarrantyDesc = "";
                        //}

                        #endregion
                    }

                    string selectedTickmarkPrice = Request.Form["hfSeletectedItemType" + VendorPlaceOrder.Value.ToUpper() + j];

                    if (string.IsNullOrEmpty(selectedTickmarkPrice))
                        selectedTickmarkPrice = "";
                    else
                        selectedTickmarkPrice = selectedTickmarkPrice.TrimEnd(',');

                    if (!string.IsNullOrEmpty(RevenuePrice.Value))
                        Revnue = Regex.Match(RevenuePrice.Value, @"\d+").Value; ;

                    string str = "Brand" + j + ":!" + Brand + "~  Product" + j + ":!" + Product + "~  MP Price" + j + ":!" + QuotePrice + "~ Model" + j + ":!" + Model + "~ Pay By" + j + ":!" + PayBy + "~ Suburb and Postcode" + j + ":!" + PostCode + "~ MP Option" + j + ":!" + (string.IsNullOrEmpty(PickupOrDeliveryy) ? "" : PickupOrDeliveryy) + "~ Description" + j + ":!" + Desc + "~ Submit To" + j + ":!" + (string.IsNullOrEmpty(SubmitTo) ? "" : SubmitTo) + "~ Delivery Address" + j + ":!" + DeliveryAddress + "~ Options" + j + ":!" + (string.IsNullOrEmpty(Options) ? "" : Options) + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Removal:!" + Removal + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Install:!" + Inst + "~ P" + j + " " + VendorPlaceOrder.Value + " Comment:!" + Comment + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Pick Up:!" + Pickup + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Delivery:!" + Delivery + "~P" + j + " " + VendorPlaceOrder.Value + " Price Warranty:!" + WarrantyCost + "~";
                    // str += "P" + j + " " + VendorPlaceOrder.Value + "Warranty Cost:!" + WarrantyCost + "~ P" + j + " " + VendorPlaceOrder.Value + " Del+Inst+Rem+Warranty Cost:!" + DelInstRemWarranty + "~ P" + j + " " + VendorPlaceOrder.Value + " Del+Inst+Warranty Cost:!" + DelInstWarranty + "~ P" + j + " " + VendorPlaceOrder.Value + " Del+Rem+Warranty Cost:!" + DelRemWarranty + "~P" + j + " " + VendorPlaceOrder.Value + " Del Rem Cost:!" + DelRem + "~ P" + j + " " + VendorPlaceOrder.Value + " " + "Warranty Desc" + ":!" + WarrantyDesc + "~";
                    str += "~ P" + j + " " + VendorPlaceOrder.Value + " " + "Warranty Desc" + ":!" + WarrantyDesc + "~ Item Price Tick Marked " + VendorPlaceOrder.Value.ToUpper() + j + ":!" + selectedTickmarkPrice + "~";
                    string strDesc = "Brand Name:!" + Brand + "Product Name:!" + Product + "Model:!" + Model + "Suburb and Postcode:!" + PostCode + "],";


                    if (!string.IsNullOrEmpty(CancelOrdered))
                        str += "Ordered Item Status " + j + ":!" + CancelOrdered + "~";

                    #region for save

                    //string str = "Brand" + j + ":" + Brand + "~  Product" + j + ":" + Product + "~ MP Price" + j + ":" + QuotePrice + "~ Model" + j + ":" + Model + "~ Pay By" + j + ":" + PayBy + "~ Suburb and Postcode" + j + ":" + PostCode + "~ MP Option" + j + ":" + (string.IsNullOrEmpty(PickupOrDeliveryy) ? "" : PickupOrDeliveryy) + "~ Description" + j + ":" + Desc + "~ Submit To" + j + ":" + (string.IsNullOrEmpty(SubmitTo) ? "" : SubmitTo) + "~ Delivery Address" + j + ":" + DeliveryAddress + "~ Options" + j + ":" + (string.IsNullOrEmpty(Options) ? "" : Options) + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Removal:" + PriceRemoval + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Install:" + PriceInstall + "~ P" + j + " " + VendorPlaceOrder.Value + " Comment:" + Comment + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Pick Up:" + PricePickUp + "~ P" + j + " " + VendorPlaceOrder.Value + " Price Delivery:" + PriceDelivery + "~P" + j + " " + VendorPlaceOrder.Value + " Price Warranty:" + PriceWarranty + "~";
                    //str +="~ P" + j + " " + VendorPlaceOrder.Value + " " + "Warranty Desc" + ":" + WarrantyDesc + "~";
                    //string strDesc = "[" + Brand + "," + Product + ",$" + QuotePrice + "," + Model + "," + PayBy + "," + Desc + "," + SubmitTo + "," + PickupOrDeliveryy + ",";


                    #endregion



                    if (!string.IsNullOrEmpty(Request.QueryString["CaseNo"]))
                    {
                        oCSCase = new CSCase();
                        CSCaseList oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + Request.QueryString["CaseNo"] + ")");
                        foreach (CSCase oCSCaseItems in oCSCaseList)
                        {
                            oCSCase = oCSCaseItems;
                            oCSCase.Description.Value = strDesc.TrimEnd(',');
                            var OrderedItemCount = Request.Form["hfOrderedItemCount"];
                            var TotalItems = Request.Form["hfCountItems"];
                            if (OrderedItemCount == TotalItems)
                            {
                                oCSCase.Status.Value = GetCSCaseStausKey("Complete");
                            }
                            else
                            {
                                //oCSCase.Status.Value = 57999;//Pendding
                                oCSCase.Status.Value = GetCSCaseStausKey("Pending");
                            }
                            oCSCaseAccess.Update(oCSCase);
                        }
                    }
                    else
                    {
                        oCSCase = new CSCase(CurrentKey);
                        //oCSCase.Status.Value = 3;
                        //oCSCase.Description.Value = strDesc.TrimEnd(',');
                        oCSCase.AssignedTo.Value = oAbEntryAccess.GetLoggedInUserKey();
                        oCSCaseAccess.Insert(oCSCase);
                    }

                    sLoginString = loginToMax();
                    string[] arr = str.Split('~');
                    foreach (var item in arr)
                    {
                        if (string.IsNullOrEmpty(item))
                            continue;
                        try
                        {
                            string[] arrItems = item.Split(new string[] { ":!" }, StringSplitOptions.None);
                            oUDFAccess.SetFieldValue(GetUdfCSCaseKey(arrItems[0].Trim()), oCSCase.Key, string.IsNullOrEmpty(Convert.ToString(arrItems[1])) ? "" : Convert.ToString(arrItems[1]));

                        }
                        catch (Exception ex)
                        {
                            common.LogError(ex, item, "SetUDF on Place Order, Case Number:-" + oCSCase.CaseNumber);
                            continue;
                        }
                    }

                    string OrderFromValue = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + j + " Ordered From"), oCSCase.Key);

                    if (VendorPlaceOrder.Value == "GG" || VendorPlaceOrder.Value == "The Good Guys")
                    {
                        oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("P" + j + " Ordered From"), oCSCase.Key, string.IsNullOrEmpty(OrderFromValue) ? "The Good Guys" : OrderFromValue + ",The Good Guys");
                    }
                    else
                    {
                        oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("P" + j + " Ordered From"), oCSCase.Key, string.IsNullOrEmpty(OrderFromValue) ? "JBHIFI" : OrderFromValue + ",JBHIFI");
                    }

                    this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Vendor" + j), oCSCase.Key, Convert.ToString(SubmitTo));
                    if (VendorPlaceOrder.Value == "GG" || VendorPlaceOrder.Value == "The Good Guys")
                    {
                        OrderFromValue = "The Good Guys";
                    }
                    else
                    {
                        OrderFromValue = "JBHiFI";
                    }


                    sLoginString = loginToMax();
                    var oCSCaseListAll = oCSCaseAccess.ReadList("EQ(CaseNumber," + Request.QueryString["CaseNo"] + ")");
                    foreach (CSCase oCSCaseItems in oCSCaseListAll)
                    {
                        string ClientID = "";
                        string Phone = "";
                        this.oAbEntryList = this.oAbEntryAccess.ReadList("KEY(" + oCSCaseItems.ParentKey + ")");
                        foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                        {
                            ClientID = oAbEntry.ClientId.Value;
                            Phone = oAbEntry.Phone1.Value;
                        }

                        bool isFlagCancel = true; ;
                        for (int k = 1; k <= 10; k++)
                        {
                            var CancelOrderedStatus = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Ordered Item Status " + j), oCSCaseItems.Key);

                            if (string.IsNullOrEmpty(CancelOrderedStatus) || CancelOrderedStatus != "Ordered")
                            {
                                isFlagCancel = false;
                            }
                        }
                        if (isFlagCancel)
                        {
                            oCSCase.Status.Value = GetCSCaseStausKey("Complete");
                            oCSCaseAccess.Update(oCSCaseItems);
                        }

                        InsertMaxCampaignAccount(ClientID, Phone, WebConfigurationManager.AppSettings["CampIDForUSNewOrderEmail"]);
                    }


                    //Create Opportunity
                    string currentKey = Request.QueryString["CurrentKey"];
                    Opportunity opp = new Opportunity(currentKey);
                    opp.Comment.Value = "Brand Name:" + Brand + "Product Name:" + Product + "Model:" + Model + "Suburb and Postcode:" + PostCode;
                    opp.Objective.Value = "Direct Order -" + oCSCase.CaseNumber.Value + " from " + VendorPlaceOrder.Value;
                    opp.ForecastRevenue.Value = Convert.ToDouble(Revnue);
                    oOppAccess.Insert(opp);
                    string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                    var createDate = DateTime.Now.ToString("yyyy-MM-dd");
                    oUDFAccess.SetFieldValue(UDFCreatedDateKey, opp.Key, createDate);
                    try
                    {

                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Vendor Order Number"), opp.Key, VendorOrderNumber);
                        //this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Vendor Order Number"), opp.Key, (oCSCase.CaseNumber.Value).Replace("US", "UN"));
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("CaseNumber"), opp.Key, oCSCase.CaseNumber.Value);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Brand Name"), opp.Key, Brand);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Product Name"), opp.Key, Product);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Model"), opp.Key, Model);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Submit To"), opp.Key, OrderFromValue);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Pay By"), opp.Key, PayBy);
                        //this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Pickup or Delivery"), opp.Key, PickupOrDeliveryy);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Warranty Cost"), opp.Key, WarrantyCost);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Member Price Desc"), opp.Key, QuotePrice);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Description"), opp.Key, Desc);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Delivery Address"), opp.Key, DeliveryAddress);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Suburb and Postcode"), opp.Key, PostCode);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("ItemNumber"), opp.Key, j.ToString());

                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Options"), opp.Key, !string.IsNullOrEmpty(Options) ? Options : "");
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("+Inst"), opp.Key, !string.IsNullOrEmpty(Inst) ? Inst : "0");
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("+Rem"), opp.Key, !string.IsNullOrEmpty(Removal) ? Removal : "0");
                        //this.oUDFAccess.SetFieldValue(this.GetUdfOppKey(VendorPlaceOrder.Value + " Price Install"), opp.Key, !string.IsNullOrEmpty(Inst) ? Inst : "0");
                        //this.oUDFAccess.SetFieldValue(this.GetUdfOppKey(VendorPlaceOrder.Value + " Price Removal"), opp.Key, !string.IsNullOrEmpty(Removal) ? Inst : "0");


                        string UnionId = oUDFAccess.GetFieldValue(this.GetUdfKey("Union ID"), CurrentKey);
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Union ID"), opp.Key, UnionId);

                        if (VendorPlaceOrder.Value == "GG" || VendorPlaceOrder.Value == "The Good Guys")
                        {
                            this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("TheGoodGuys Comment"), opp.Key, Convert.ToString(Comment));
                            this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Ordered From"), opp.Key, "The Good Guys");
                        }
                        else
                        {
                            this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("JBHIFI Comment"), opp.Key, Convert.ToString(Comment));
                            this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Ordered From"), opp.Key, VendorPlaceOrder.Value);
                        }

                        //this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Product Comment"), opp.Key, Convert.ToString(Comment));
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Product Price Delivery"), opp.Key, Convert.ToString(Delivery));
                        this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Product Price Pick Up"), opp.Key, Convert.ToString(Pickup));

                        //}
                        //else
                        //{
                        //    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("Ordered From"), opp.Key, OrderFromValue);

                        //    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("P" + j + " JBHIFI Comment"), opp.Key, Convert.ToString(Comment));
                        //    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("P" + j + " JBHIFI Price Delivery"), opp.Key, Convert.ToString(Delivery));
                        //    this.oUDFAccess.SetFieldValue(this.GetUdfOppKey("P" + j + " JBHIFI Price Pick Up"), opp.Key, Convert.ToString(Pickup));
                        //}
                    }
                    catch (Exception ex)
                    {
                        common.LogError(ex, "Error:- Set UDf in Opp for Place Order", "SetUDF Opp, Case Number:-" + oCSCase.CaseNumber + "ItemNumber:" + j.ToString() + ", Vendor:" + VendorPlaceOrder.Value);
                    }



                    //Create Note
                    Note oNote = new Note(opp.Key);
                    oNote.Text.Value = str;
                    oNoteAccess.Insert(oNote);

                    string Item = string.Empty;
                    //Item = "P"+j;
                    Item = j;


                    //ItemsForEmail += "<fieldset><legend>Item #" + j + "</legend><table style='border-collapse:collapse;width:100%;'><tbody><tr>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>Brand:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(Brand) + "</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>Product:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(Product) + "</td>";
                    //ItemsForEmail += "</tr><tr><td style='border:1px solid #cecfd5;padding:10px 15px'>Model:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(Model) + "</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>&nbsp;</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>&nbsp;</td>";
                    //ItemsForEmail += "</tr><tr><td style='border:1px solid #cecfd5;padding:10px 15px'>Pay BY:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(PayBy) + "</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>Pickup or Delivery:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(PickupOrDeliveryy) + "</td>";
                    //ItemsForEmail += "</tr><tr><td style='border:1px solid #cecfd5;padding:10px 15px'>Delivery Address:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(DeliveryAddress) + "</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>Description:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(Desc) + "</td>";
                    //ItemsForEmail += "</tr><tr><td style='border:1px solid #cecfd5;padding:10px 15px'>P" + i + " " + "VendorType" + " Comments:</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(Comment) + "</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>P" + i + " " + "VendorType" + " Price Pick Up:$</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(Pickup) + "</td>";
                    //ItemsForEmail += "</tr><tr><td style='border:1px solid #cecfd5;padding:10px 15px'>P" + i + " " + "VendorType" + " Price Delivery:$</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'>" + Convert.ToString(Delivery) + "</td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'></td>";
                    //ItemsForEmail += "<td style='border:1px solid #cecfd5;padding:10px 15px'></td>";
                    //ItemsForEmail += "</tr></tbody></table></fieldset>";
                    string LiveUrl = WebConfigurationManager.AppSettings["LiveURl"];
                    // string Url = LiveUrl + "/Dialogs/CustomDialogs/DirectOrderConfirm.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&Vendor=" + VendorPlaceOrder.Value + "&CaseNo=" + oCSCase.CaseNumber.Value + "&Item=" + Item;
                    string Url = LiveUrl + "/Dialogs/CustomDialogs/CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&Vendor=" + VendorPlaceOrder.Value + "&CaseNo=" + oCSCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + Item + "&OppKeys=" + opp.Key + "-" + Item;
                    string strEmailBody = "<html><head></head><body><div class='gmail_quote'><p>Dear VendorType,</p>";
                    strEmailBody += "<p style='margin: 9px 40px;'><b>Union Shopper Member:</b>" + lblMember.Text + "</p>";
                    strEmailBody += "<p style='margin: 9px 40px;'><b>Name:</b>" + lbluser.Text + "</p>";
                    strEmailBody += "<p style='margin: 9px 40px;'><b>Phone:</b>" + lblMobileNumber.Text + "</p>";
                    strEmailBody += "<p style='margin: 9px 40px;'><b>Address:</b>" + lblAddress.Text + "</p>";
                    strEmailBody += "<p>Please <a href='" + Url + "' target='_blank' >click</a> here to review order</p><p></p>";
                    //strEmailBody += "<p>Would like to order:</p>";
                    //strEmailBody += "<p>Please proceed with the following order</p>";

                    //strEmailBody += "<p>" + ItemsForEmail + "</p>";

                    strEmailBody += " </div></body></html>";

                    strEmailBody += "</div></body></html>";
                    if (VendorPlaceOrder.Value.ToLower() == "JBHiFI".ToLower())
                    {
                        VendorType = "JBHiFI";
                        Url += VendorType;
                        strEmailBody = strEmailBody.Replace("VendorType", VendorType);
                        //sendMail("order@unionshopper.com.au", "", oCSCase.CaseNumber.Value + " - Order Placed With " + VendorType, strEmailBody);



                    }
                    else if (VendorPlaceOrder.Value.ToLower() == "GG".ToLower())
                    {
                        VendorType = "The Good Guys";
                        Url += VendorType;
                        strEmailBody = strEmailBody.Replace("VendorType", VendorType);
                        //sendMail("order@unionshopper.com.au", "", oCSCase.CaseNumber.Value + " - Order Placed With " + VendorType, strEmailBody);


                    }






                    #region for ConfirmStatus

                    //bool flag = false;
                    ////string Vendor = string.Empty;
                    //int k = 0;

                    //int CountItems = Convert.ToInt32(hfCountItems.Value);
                    //string[] orderform1 = new string[4];
                    //for (int p = 1; p <= CountItems; p++)
                    //{
                    //    orderform1[p] = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + p + " Ordered From"), oCSCase.Key);
                    //    for (int n = 1; n <= p; n++)
                    //    {
                    //        string[] orderform = new string[4];
                    //        orderform[n] = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + n + " Ordered From"), oCSCase.Key);

                    //        // if (orderform[n] == "The Good Guys")
                    //        // {
                    //        //Vendor = "The Good Guys";
                    //        // }
                    //        // else
                    //        // {
                    //        // Vendor = orderform[n];
                    //        //}

                    //        //if (orderform[n] != "" && Vendor == orderform1[p])

                    //        if (orderform[n] != "")
                    //        {
                    //            k++;
                    //            if (k == n)
                    //            {
                    //                flag = true;

                    //            }
                    //            else
                    //            {
                    //                flag = false;

                    //            }
                    //        }
                    //        else
                    //        {
                    //            flag = false;

                    //        }
                    //    }
                    //    k = 0;
                    //}
                    //if (flag == true)
                    //{
                    //    oCSCase.Status.Value = 57993;
                    //    oCSCaseAccess.Update(oCSCase);

                    //}

                    #endregion
                }
                catch (Exception ex)
                {
                    common.LogError(ex, "CreateCSCase", "PlaceOrder, Case Number: -" + Request.QueryString["CaseNo"]);

                }

                //Response.Redirect("MemberEnquiry.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&RequestType=CallCenter");

                Response.Redirect("CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&CaseNo=" + Convert.ToString(Request.QueryString["CaseNo"]) + "&RequestType=CallCenter");


            }

        }

        protected void btnConfirmOrder_Click(object sender, EventArgs e)
        {
            try
            {
                string RequestType = Request.QueryString["RequestType"];
                if (RequestType == "Vendor")
                {
                    int j = Convert.ToInt32(hfItemsNumber.Value.Replace(",", ""));
                    CSCase oCSCase = new CSCase();
                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + Request.QueryString["CaseNo"] + ")");
                    foreach (CSCase oCSCaseItems in oCSCaseList)
                    {
                        oCSCase = oCSCaseItems;
                        //oCSCase.ReasonName.Value = "too competitive";
                        //oCSCase.Reason.Value = 10;

                        //oCSCaseAccess.Update(oCSCase);
                    }

                    string vendorType = Request.QueryString["Vendor"];

                    string selectedTickmarkPrice = Request.Form["hfSeletectedItemType" + vendorType.ToUpper() + j];

                    if (string.IsNullOrEmpty(selectedTickmarkPrice))
                        selectedTickmarkPrice = "";
                    else
                        selectedTickmarkPrice = selectedTickmarkPrice.TrimEnd(',');

                    var Pickup = Request.Form["txtP" + vendorType.ToUpper() + "PickUp" + j];
                    if (string.IsNullOrEmpty(Pickup))
                        Pickup = "";
                    else
                        Pickup = Regex.Match(Pickup, @"\d+").Value;

                    var Delivery = Request.Form["txtP" + vendorType.ToUpper() + "PriceDelivery" + j];
                    if (string.IsNullOrEmpty(Delivery))
                        Delivery = "";
                    else
                        Pickup = Regex.Match(Pickup, @"\d+").Value;


                    var Removal = Request.Form["txtPDelInstRem" + vendorType.ToUpper() + j];
                    if (string.IsNullOrEmpty(Removal))
                    {
                        Removal = "";
                    }
                    else
                        Removal = Regex.Match(Removal, @"\d+").Value;

                    var Inst = Request.Form["txtPInst" + vendorType.ToUpper() + j];
                    if (string.IsNullOrEmpty(Inst))
                        Inst = "";
                    else
                        Inst = Regex.Match(Inst, @"\d+").Value;


                    string str = "P" + j + " " + vendorType.ToUpper() + " Price Removal:!" + Removal + "~P" + j + " " + vendorType.ToUpper() + " Price Install:!" + Inst + "~ P" + j + " " + vendorType.ToUpper() + " Price Delivery:!" + Delivery + "~ P" + j + " " + vendorType.ToUpper() + " Price Pick Up:!" + Pickup + "~ Item Price Tick Marked " + vendorType.ToUpper() + j + ":!" + selectedTickmarkPrice + "~";


                    sLoginString = loginToMax();
                    string[] arr = str.Split('~');
                    foreach (var item in arr)
                    {
                        if (string.IsNullOrEmpty(item))
                            continue;
                        try
                        {
                            string[] arrItems = item.Split(new string[] { ":!" }, StringSplitOptions.None);
                            oUDFAccess.SetFieldValue(GetUdfCSCaseKey(arrItems[0].Trim()), oCSCase.Key, string.IsNullOrEmpty(Convert.ToString(arrItems[1])) ? "" : Convert.ToString(arrItems[1]));

                        }
                        catch (Exception ex)
                        {
                            common.LogError(ex, item, "SetUDF on confirm Order, Case Number: -" + Request.QueryString["CaseNo"]);
                            continue;
                        }

                    }


                    string EnquiryType = Request.QueryString["ET"];
                    if (EnquiryType == "CE")
                        oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Enquiry Updated"), oCSCase.Key, "Yes");
                    else if (EnquiryType == "CO")
                        oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Order Updated"), oCSCase.Key, "Yes");

                    string OppKey = hfConfirmOrderOppKey.Value;
                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("KEY(" + OppKey + ")");


                    //t is total of product price, delivery cost, install cost and removal cost
                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        if (!string.IsNullOrEmpty(RevenuePrice.Value))
                        {
                            opp.ForecastRevenue.Value = Convert.ToDouble(RevenuePrice.Value);
                            opp.ActualRevenue.Value = Convert.ToDouble(RevenuePrice.Value);

                            opp.Status.Value = 3;//Won Status
                            oOppAccess.Update(opp);
                            oUDFAccess.SetFieldValue(this.GetUdfOppKey("Ordered Date"), opp.Key, DateTime.Now.ToString("yyyy-MM-dd"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            //Response.Redirect("ThankYouOrdering.aspx");
            Response.Redirect("VendorEnquiries.aspx?CurrentKey" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&RequestType=CallCenter&Vendor=" + Request.QueryString["Vendor"] + "&ET=" + Request.QueryString["ET"]);

        }

        protected void btnCancelOrder_Click(object sender, EventArgs e)
        {
            //
            try
            {
                sLoginString = loginToMax();
                if (!string.IsNullOrEmpty(ddlCancelReason.SelectedValue))
                {
                    var OppKeys = Request.QueryString["OppKeys"];
                    OppKeys = OppKeys.Split('-')[0];
                    OpportunityList oOPList = oOppAccess.ReadList("KEY(" + OppKeys + ")");
                    foreach (Opportunity opp in oOPList)
                    {
                        //oCSCase.Reason.Value = Convert.ToInt32(ddlCancelReason.SelectedValue);
                        opp.Status.Value = 5;//Cancel
                        oOppAccess.Update(opp);
                        oUDFAccess.SetFieldValue(this.GetUdfOppKey("Ordered Date"), opp.Key, DateTime.Now.ToString("yyyy-MM-dd"));

                        oUDFAccess.SetFieldValue(this.GetUdfOppKey("Order Reason"), opp.Key, ddlCancelReason.SelectedValue);
                        var vendor = Request.QueryString["Vendor"];
                        var NoteCategory = "Union Shopper";
                        if (vendor == "GG")
                        {
                            NoteCategory = "THE GOOD GUY" +
                                "S";

                        }
                        else if (vendor == "JBHIFI")
                        {
                            NoteCategory = "JBHIFI";
                        }

                        var strText = "Item Cancelled <br /><b>Order Reason:</b>" + ddlCancelReason.SelectedValue + "<br/>";
                        string str = "";

                        for (int j = 1; j <= 10; j++)
                        {
                            string i = j.ToString();
                            if (j == 1)
                            {
                                i = "";
                            }

                            var Brand = Request.Form["ddlBrand" + i];
                            if (string.IsNullOrEmpty(Brand))
                                Brand = "";

                            var Product = Request.Form["ddlProduct" + i];
                            if (string.IsNullOrEmpty(Product))
                                Product = "";

                            var Model = Request.Form["txtModel" + i];
                            if (string.IsNullOrEmpty(Model))
                                Model = "";

                            var Desc = Request.Form["txtDesc" + i];
                            if (string.IsNullOrEmpty(Desc))
                                Desc = "";

                            if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                            {
                                str += "<b>Product: </b>" + Product + "</br>";
                                str += "<b>Brand: </b>" + Brand + "</br>";
                                str += "<b>Model: </b>" + Model + "</br>";
                                str += "<b>Description: </b>" + Desc + "</br>";
                            }


                        }
                        if (!string.IsNullOrEmpty(str))
                        {
                            strText += str;

                            //Create Note
                            Maximizer.Data.Note oNote = null;
                            oNote = new Maximizer.Data.Note(opp.Key);
                            oNote.RichText.Value = strText;
                            oNote.Text.Value = strText;
                            //oNote.Category.Value = NoteCategory;
                            oNote.Owner.Value = oAbEntryAccess.GetLoggedInUserKey();
                            oNoteAccess.Insert(oNote);
                        }
                    }
                }


                CSCaseList oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + Request.QueryString["CaseNo"] + ")");
                foreach (CSCase oCSCaseItems in oCSCaseList)
                {
                    oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Ordered Item Status " + hfItemsNumber.Value.Replace(",", "")), oCSCaseItems.Key, "Cancelled by Vendor");
                }
                sLoginString = loginToMax();
                oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + Request.QueryString["CaseNo"] + ")");
                foreach (CSCase oCSCaseItems in oCSCaseList)
                {
                    bool isFlagCancel = true; ;
                    for (int j = 1; j <= 10; j++)
                    {
                        var CancelOrdered = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Ordered Item Status " + j), oCSCaseItems.Key);

                        if (string.IsNullOrEmpty(CancelOrdered) || CancelOrdered == "Ordered" || CancelOrdered == "Quoted")
                        {
                            isFlagCancel = false;
                        }
                    }
                    if (isFlagCancel)
                    {
                        oCSCaseItems.Status.Value = GetCSCaseStausKey("Cancelled");
                        oCSCaseAccess.Update(oCSCaseItems);
                    }
                }
                // Response.Redirect("ThankYou.aspx?msg=CancelOrder");
                Response.Redirect("VendorEnquiries.aspx?CurrentKey" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&RequestType=CallCenter&Vendor=" + Request.QueryString["Vendor"] + "&ET=" + Request.QueryString["ET"]);

            }
            catch (Exception ex)
            {

                //throw;
            }
        }
        public int GetCSCaseStausKey(string Status)
        {
            int Key = 0;
            try
            {
                //sLoginString = loginToMax();
                var oCSCaseStatusList = oCSCaseAccess.ReadStatusOptionListForAssign();
                foreach (Maximizer.Data.Option item in oCSCaseStatusList)
                {
                    if (item.DisplayValue.ToLower() == Status.ToLower())
                    {
                        return Convert.ToInt32(item.FieldValue);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return Key;
        }
        private string LookupUserKey(string UserID)
        {
            try
            {
                string[] items;
                //  items = maxLoginStr.Split('~');

                Maximizer.Data.UserAccess oUserAccess;
                Maximizer.Data.UserList oUserList;
                oUserAccess = oAb.CreateUserAccess(sLoginString);
                oUserList = oUserAccess.ReadList((""));
                foreach (Maximizer.Data.User oUser in oUserList)
                {
                    string sUserDisplayName = oUser.DisplayName.ToString();
                    if (oUser.DisplayName.ToUpper() == UserID.ToUpper())
                    {
                        return oUser.Key;
                    }
                }
                return "";
            }

            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return "";
            }
        }
        public string MaximizerDate(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                string[] datesplit = date.Split('-');
                string[] yearsplit = datesplit[2].Split(' ');
                string newdate = yearsplit[0] + "-" + datesplit[1] + "-" + datesplit[0];
                return Convert.ToDateTime(newdate).ToString("yyyy-MM-dd");
            }
            return "";
        }
        public string MaximizerDateTime(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                string[] datesplit = date.Split('-');
                string[] yearsplit = datesplit[2].Split(' ');
                if (yearsplit.Length > 1)
                {
                    string newdate = yearsplit[0] + "-" + datesplit[1] + "-" + datesplit[0] + " " + yearsplit[1] + " " + yearsplit[2];
                    return Convert.ToDateTime(newdate).ToString("yyyy-MM-dd HH:mm");
                }
                else
                {
                    string newdate = yearsplit[0] + "-" + datesplit[1] + "-" + datesplit[0] + " " + DateTime.Now.ToString("HH:mm");
                    return Convert.ToDateTime(newdate).ToString("yyyy-MM-dd HH:mm");
                }

            }
            return "";
        }
        public void closeBrowser()
        {
            try
            {
                sLoginString = loginToMax();
                string RequestType = Request.QueryString["RequestType"];
                if (!string.IsNullOrEmpty(lblLoginUser.Text))
                {
                    string CaseNumber = Request.QueryString["CaseNo"];
                    string filter = "EQ(CaseNumber," + CaseNumber + ")";
                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                    foreach (CSCase oCSCase in oCSCaseList)
                    {
                        oUDFAccess.SetFieldValue(GetUdfCSCaseKey("Enquiry Status With User"), oCSCase.Key, "Closed");
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [WebMethod()]
        public static string update(string CaseNo, string sLoginString)
        {
            try
            {
                AddressBookMaster oAb = new AddressBookMaster();
                GlobalAccess ga = new GlobalAccess();
                UdfAccess oUDFAccess = (UdfAccess)null;
                CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
                AddressBookList addressBookList = ga.ReadAddressBookList();
                AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;

                oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
                oUDFAccess = oAb.CreateUdfAccess(sLoginString);
                oCSCaseAccess = oAb.CreateCSCaseAccess(sLoginString);

                string path = HttpContext.Current.Server.MapPath("~/UDFKeyFile/CsCase.xml");
                XDocument xCsCase = XDocument.Load(path);
                var UDFKey = "";
                var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == "Enquiry Status With User");

                foreach (var item in sFilterItems)
                {
                    // var aa = item.Element("UDFName").Value;
                    UDFKey = item.Element("UDFKey").Value;
                    break;
                }
                var UDFKeyUser = "";
                sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == "Enquiry Opened with user");

                foreach (var item in sFilterItems)
                {
                    // var aa = item.Element("UDFName").Value;
                    UDFKeyUser = item.Element("UDFKey").Value;
                    break;
                }

                var User = oAbEntryAccess.GetLoggedInUser(sLoginString);

                string CaseNumber = CaseNo;
                string filter = "EQ(CaseNumber," + CaseNumber + ")";
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                foreach (CSCase oCSCase in oCSCaseList)
                {
                    var UserOpenedEQ = oUDFAccess.GetFieldValue(UDFKeyUser, oCSCase.Key);
                    if (UserOpenedEQ == User.DisplayName)
                        oUDFAccess.SetFieldValue(UDFKey, oCSCase.Key, "Closed");
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return "Posted";
        }
        protected void Closebrowser_Click(object sender, EventArgs e)
        {
            closeBrowser();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("VendorEnquiries.aspx?CurrentKey" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&RequestType=CallCenter&Vendor=" + Request.QueryString["Vendor"] + "&ET=" + Request.QueryString["ET"]);

        }

        public void InsertMaxCampaignAccount(string ClientID, string ContactNumber, string CampID)
        {
            try
            {
                if (!string.IsNullOrEmpty(ContactNumber))
                {
                    ContactNumber = "0";
                    // ContactNumber = Regex.Match(ContactNumber, @"\d+").Value;
                }
                else
                {
                    ContactNumber = "0";
                }

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStrig"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT_CMGR_AUTO_CAMPAIGN_ACCOUNTS", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        //191206252110543120038M, clientid, 0, date/time, date/time, 1, 0, null, 0
                        cmd.Parameters.Add("@CampID", SqlDbType.NVarChar).Value = CampID;
                        cmd.Parameters.Add("@ClientID", SqlDbType.NVarChar).Value = ClientID;
                        cmd.Parameters.Add("@ContactNo", SqlDbType.Int).Value = Convert.ToInt32(ContactNumber);
                        cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@NextDate", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@AccStatus", SqlDbType.Int).Value = 1;
                        cmd.Parameters.Add("@ActCount", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@ActType", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@ActFlag", SqlDbType.Int).Value = 0;

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void btnCancelOrderItems_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["ET"] == "CO")
            {
                string[] arrOpp = null;
                var OppKeys = Request.QueryString["OppKeys"];
                if (!string.IsNullOrEmpty(OppKeys))
                {
                    arrOpp = OppKeys.Split(',');
                    string OppKey = arrOpp[0].Split('-')[0].Trim();
                    string itemNumber = arrOpp[0].Split('-')[1].Trim();
                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("KEY(" + OppKey + ")");

                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {

                        opp.Status.Value = 5;//Cancel
                        oOppAccess.Update(opp);
                        
                   }
                    
                    var oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + Request.QueryString["CaseNo"] + ")");
                    foreach (CSCase oCSCaseItems in oCSCaseList)
                    {
                        oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Ordered Item Status " + itemNumber), oCSCaseItems.Key, "Cancelled by Vendor");
                        sLoginString = loginToMax();
                        bool isFlagCancel = true; ;
                        for (int j = 1; j <= 10; j++)
                        {
                            var CancelOrdered = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Ordered Item Status " + j), oCSCaseItems.Key);

                            if (string.IsNullOrEmpty(CancelOrdered) || CancelOrdered == "Ordered" || CancelOrdered == "Quoted")
                            {
                                isFlagCancel = false;
                            }
                        }
                        if (isFlagCancel)
                        {
                            oCSCaseItems.Status.Value = GetCSCaseStausKey("Cancelled");
                            oCSCaseAccess.Update(oCSCaseItems);
                        }
                    }
                }
                Response.Redirect("VendorEnquiries.aspx?CurrentKey" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&RequestType=CallCenter&Vendor=" + Request.QueryString["Vendor"] + "&ET=" + Request.QueryString["ET"]);
            }
        }
    }
}