﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace US
{
    public partial class EnquirySheet : System.Web.UI.Page
    {
        Maximizer.Data.AddressBookMaster oAb = new Maximizer.Data.AddressBookMaster();
        Maximizer.Data.AbEntryAccess oAbEntryAccess = null;
        Maximizer.Data.GlobalAccess ga = new Maximizer.Data.GlobalAccess();
        Maximizer.Data.AbEntryList oAbEntryList = null;
        Maximizer.Data.UdfAccess oUDFAccess = null;
        Maximizer.Data.NoteAccess oNoteAccess = null;
        Maximizer.Data.TaskAccess oTaskAccess = null;
        Maximizer.Data.UserAccess oUserAccess = null;
        Maximizer.Data.OpportunityAccess oOppAccess = null;
        Maximizer.Data.DetailFieldAccess oDetailFieldAccess = null;
        Maximizer.Data.AddressAccess oAddressAccess = null;
        string sLoginString = "";
        string CurrentKey = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //sLoginString = loginToMax();

            //if (!IsPostBack)
            //{
            //    string sFilterKey = "KEY(" + CurrentKey + ")";
            //    oAbEntryList = oAbEntryAccess.ReadList(sFilterKey);
            //    foreach (Maximizer.Data.AbEntry oAbE in oAbEntryList)
            //    {
            //        //Maximizer.Data.Individual oIndividual = (Maximizer.Data.Individual)(oAbE);
            //        //txtEQFirstName.Text = oIndividual.FirstName.Value;
            //        //txtEQLastName.Text = oIndividual.LastName.Value;
            //        //txtEQPhoneNumber.Text = oIndividual.Phone1.Value;

            //        //string MemberID = oUDFAccess.GetFieldValue(GetUdfKey("Member ID", sLoginString, oAb), oIndividual.Key);
            //        //string UnionID = oUDFAccess.GetFieldValue(GetUdfKey("Union ID", sLoginString, oAb), oIndividual.Key);

            //        //txtEQMemberID.Text = MemberID;
            //        //txtEQUnionNumber.Text = UnionID;
            //        //chkUsePhoneticSearch.Checked = true;
            //    }
            //}
        }
        private string loginToMax()
        {
            Maximizer.Data.AddressBookList abList = ga.ReadAddressBookList();
            string sRt = "";
            sLoginString = Convert.ToString(Session["loginString"]);
            CurrentKey = Convert.ToString(Session["abGrid!_CurrentKey"]);

            if (sLoginString == "" || sLoginString == null)
            {
                string database = ConfigurationManager.AppSettings["Database"];
                var oList = abList.Cast<Maximizer.Data.AddressBook>().Where(x => x.Name.Value.ToUpper() == database.ToUpper());
                foreach (Maximizer.Data.AddressBook ab in oList)
                {
                    sRt = ab.Key;
                    break;
                }

                string Username = ConfigurationManager.AppSettings["UserName"];
                string Password = ConfigurationManager.AppSettings["Password"];
                sLoginString = oAb.LoginMaximizer(sRt, Username, Password);

            }
            if (string.IsNullOrEmpty(CurrentKey))
            {
                sLoginString = "97146828199322275262052735z7d657862736a0f43624855136157404150404b097474667671746a720c616503617d636f7c610f66707407020705620709627a71047a7067627d6003616e7d0e404b5359475d555943040307000e";
                CurrentKey = "	SW5kaXZpZHVhbAkwNDA0MDgwMDAwNjA3NzIyMzgxNjlDCTA=";
            }

            // Always need this to create access to Maximizer and access to special fields.
            oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
            oUDFAccess = oAb.CreateUdfAccess(sLoginString);
            oOppAccess = oAb.CreateOpportunityAccess(sLoginString);
            oNoteAccess = oAb.CreateNoteAccess(sLoginString);
            oTaskAccess = oAb.CreateTaskAccess(sLoginString);
            oUserAccess = oAb.CreateUserAccess(sLoginString);
            oAddressAccess = oAb.CreateAddressAccess(sLoginString);
            oDetailFieldAccess = oAb.CreateDetailFieldAccess(sLoginString);

            return sLoginString;
        }

        public string GetUdfKey(string strUdfName, string strLoginString, Maximizer.Data.AddressBookMaster abMaster)
        {
            Maximizer.Data.UdfAccess udfAccess =
            abMaster.CreateUdfAccess(strLoginString);
            Maximizer.Data.UdfDefinitionList udfDefList =
            udfAccess.ReadUdfDefinitionList(new Maximizer.Data.AbEntry().Key);
            var oList = udfDefList.Cast<Maximizer.Data.UdfDefinition>().Where(x => x.FieldName.Equals(strUdfName));
            foreach (Maximizer.Data.UdfDefinition udfDef in oList)
            {
                return udfDef.Key;
            }
            return String.Empty;

        }
    }
}