﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Maximizer.Data;
using US.Comomn;

namespace US
{
    public partial class VendorEnquiries : System.Web.UI.Page
    {
        static AddressBookMaster oAb = new AddressBookMaster();
        static AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        static GlobalAccess ga = new GlobalAccess();
        static AbEntryList oAbEntryList = (AbEntryList)null;
        static UdfAccess oUDFAccess = (UdfAccess)null;
        static NoteAccess oNoteAccess = (NoteAccess)null;
        static UserAccess oUserAccess = (UserAccess)null;
        static TaskAccess oTaskAccess = (TaskAccess)null;
        static OpportunityAccess oOppAccess = (OpportunityAccess)null;
        static DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        static SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        static CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        static connection connection = new connection();
        static string sLoginString = "";
        static XDocument xAB = null; static XDocument xCsCase = null; static XDocument xOpportunity = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");
            xCsCase = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            xOpportunity = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");
            xAB = XDocument.Load(path);
            if (!IsPostBack)
            {
                sLoginString = loginToMax();
                hfLoginString.Value = Request.QueryString["Loginstring"];
                hfVendor.Value = Request.QueryString["Vendor"];
                hfEnquiryType.Value = Request.QueryString["ET"];
                hfCurrentKey.Value = Request.QueryString["CurrentKey"];
                hfRequestType.Value = Request.QueryString["RequestType"];

                string Vendor = Request.QueryString["Vendor"];
                if (Vendor == "GG")
                {
                    Vendor = "The Good Guys";
                }
                else
                {
                    Vendor = "JBHiFI";
                }
                lblVendor.Text = Vendor;

                string ET = Request.QueryString["ET"];
                if (!string.IsNullOrEmpty(ET))
                {
                    if (ET == "CE")
                    {
                        lnkCurrantEnquiries.CssClass = "menulink";
                        lnkCompletedOrders.CssClass = "menulink";
                        lnkCurrantOrders.CssClass = "menulink";
                        lnkCompletedEnquiries.CssClass = "active menulink";


                        CaseType = "COMP_ENQ";
                        BindEnquires();
                    }
                    else if (ET == "CURRORD")
                    {
                        lnkCurrantEnquiries.CssClass = "menulink";
                        lnkCompletedOrders.CssClass = "menulink";
                        lnkCurrantOrders.CssClass = "active menulink";
                        lnkCompletedEnquiries.CssClass = "menulink";


                        CaseType = "CURR_ORD";
                        BindEnquires();
                    }
                    else if (ET == "CO")
                    {
                        lnkCurrantEnquiries.CssClass = "menulink";
                        lnkCompletedOrders.CssClass = "active menulink";
                        lnkCurrantOrders.CssClass = "menulink";
                        lnkCompletedEnquiries.CssClass = "menulink";

                        CaseType = "COMP_ORD";
                        BindEnquires();
                    }
                    else
                    {
                        lnkCurrantEnquiries.CssClass = "active menulink";
                        lnkCompletedOrders.CssClass = "menulink";
                        lnkCurrantOrders.CssClass = "menulink";
                        lnkCompletedEnquiries.CssClass = " menulink";

                        CaseType = "CURR_ENQ";
                        BindEnquires();
                    }
                }
                else
                {
                    CaseType = "CURR_ENQ";
                    BindEnquires();
                }
            }

        }
        protected void lnkCurrantEnquiries_Click(object sender, EventArgs e)
        {
            lnkCurrantEnquiries.CssClass = "active menulink";
            lnkCompletedOrders.CssClass = "menulink";
            lnkCurrantOrders.CssClass = "menulink";
            lnkCompletedEnquiries.CssClass = " menulink";

            CaseType = "CURR_ENQ";
            BindEnquires();
        }

        protected void lnkCompletedEnquiries_Click(object sender, EventArgs e)
        {
            lnkCurrantEnquiries.CssClass = "menulink";
            lnkCompletedOrders.CssClass = "menulink";
            lnkCurrantOrders.CssClass = "menulink";
            lnkCompletedEnquiries.CssClass = "active menulink";


            CaseType = "COMP_ENQ";
            BindEnquires();
        }

        protected void lnkCurrantOrders_Click(object sender, EventArgs e)
        {
            lnkCurrantEnquiries.CssClass = "menulink";
            lnkCompletedOrders.CssClass = "menulink";
            lnkCurrantOrders.CssClass = "active menulink";
            lnkCompletedEnquiries.CssClass = "menulink";


            CaseType = "CURR_ORD";
            BindEnquires();
        }

        protected void lnkCompletedOrders_Click(object sender, EventArgs e)
        {
            lnkCurrantEnquiries.CssClass = "menulink";
            lnkCompletedOrders.CssClass = "active menulink";
            lnkCurrantOrders.CssClass = "menulink";
            lnkCompletedEnquiries.CssClass = "menulink";

            CaseType = "COMP_ORD";
            BindEnquires();
        }

        protected void BindEnquires()
        {

            Session["CurrentKey"] = hfCurrentKey.Value;
            switch (CaseType)
            {
                case "CURR_ENQ":
                    pnlControls.Controls.Clear();
                    Control _CurrantEnquiries = (Control)Page.LoadControl("UserControls/CurrantEnquiries.ascx");
                    pnlControls.Controls.Add(_CurrantEnquiries);
                    break;

                case "COMP_ENQ":
                    pnlControls.Controls.Clear();
                    Control _CompletedEnquires = (Control)Page.LoadControl("UserControls/CompletedEnquires.ascx");
                    pnlControls.Controls.Add(_CompletedEnquires);
                    break;

                case "CURR_ORD":
                    pnlControls.Controls.Clear();
                    Control _CurrantOrders = (Control)Page.LoadControl("UserControls/CurrantOrders.ascx");
                    pnlControls.Controls.Add(_CurrantOrders);
                    break;

                case "COMP_ORD":
                    pnlControls.Controls.Clear();
                    Control _CompletedOrders = (Control)Page.LoadControl("UserControls/CompletedOrders.ascx");
                    pnlControls.Controls.Add(_CompletedOrders);
                    break;
                case "Report":
                    pnlControls.Controls.Clear();
                    Control _report = (Control)Page.LoadControl("UserControls/Report.ascx");
                    pnlControls.Controls.Add(_report);
                    break;

            }
        }


        /// <summary>
        /// Properties
        /// </summary>
        string strCaseType = "CURR_ENQ";
        public string CaseType
        {

            get
            {
                return strCaseType;
            }

            set
            {
                strCaseType = value;
            }
        }



        private string loginToMax()
        {
            AddressBookList addressBookList = ga.ReadAddressBookList();
            string sAddressBookKey = "";
            sLoginString = hfLoginString.Value;
            if (sLoginString == "" || sLoginString == null)
            {
                //foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                //{
                //    string appSetting = ConfigurationManager.AppSettings["Database"];
                //    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                //    {
                //        sAddressBookKey = addressBook.Key;
                //        break;
                //    }
                //}
                //string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                //string appSetting2 = ConfigurationManager.AppSettings["Password"];
                //sLoginString = oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
                sLoginString = ConfigurationManager.AppSettings["VENJBHIFILoginKey"];
            }
            oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
            oUDFAccess = oAb.CreateUdfAccess(sLoginString);
            oOppAccess = oAb.CreateOpportunityAccess(sLoginString);
            oNoteAccess = oAb.CreateNoteAccess(sLoginString);
            oUserAccess = oAb.CreateUserAccess(sLoginString);
            oTaskAccess = oAb.CreateTaskAccess(sLoginString);
            oDetailFieldAccess = oAb.CreateDetailFieldAccess(sLoginString);
            oSalesProcess = oAb.CreateSalesProcessAccess(sLoginString);
            oCSCaseAccess = oAb.CreateCSCaseAccess(sLoginString);
            return sLoginString;
        }
        private static string loginToMax(string sLoginString)
        {
            AddressBookList addressBookList = ga.ReadAddressBookList();
            string sAddressBookKey = "";

            if (sLoginString == "" || sLoginString == null)
            {
                sLoginString = ConfigurationManager.AppSettings["VENJBHIFILoginKey"];
            }
            oAbEntryAccess = oAb.CreateAbEntryAccess(sLoginString);
            oUDFAccess = oAb.CreateUdfAccess(sLoginString);
            oOppAccess = oAb.CreateOpportunityAccess(sLoginString);
            oNoteAccess = oAb.CreateNoteAccess(sLoginString);
            oUserAccess = oAb.CreateUserAccess(sLoginString);
            oTaskAccess = oAb.CreateTaskAccess(sLoginString);
            oDetailFieldAccess = oAb.CreateDetailFieldAccess(sLoginString);
            oSalesProcess = oAb.CreateSalesProcessAccess(sLoginString);
            oCSCaseAccess = oAb.CreateCSCaseAccess(sLoginString);
            return sLoginString;
        }

        public static string GetUdfKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xAB.Element("ab").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public static string GetUdfOppKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            //var path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            //xOpportunity = XDocument.Load(path);
            var sFilterItems = xOpportunity.Element("Opp").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public static string GetUdfCSCaseKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        protected void lnkReports_Click(object sender, EventArgs e)
        {
            lnkCurrantEnquiries.CssClass = "menulink";
            lnkCompletedOrders.CssClass = "menulink";
            lnkCurrantOrders.CssClass = "menulink";
            lnkCompletedEnquiries.CssClass = "menulink";
            lnkReports.CssClass = "active menulink";

            CaseType = "Report";
            BindEnquires();
        }

        public StringBuilder GenerateCSV()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("Enquiry Number", typeof(string));
                dt.Columns.Add("Customer Name", typeof(string));
                dt.Columns.Add("Brand", typeof(string));
                dt.Columns.Add("Product", typeof(string));
                dt.Columns.Add("Model", typeof(string));
                dt.Columns.Add("Vendor Order Number", typeof(string));
                dt.Columns.Add("Total Cost", typeof(string));
                dt.Columns.Add("Ordered Date", typeof(string));

                string OrderedDateKey = GetUdfOppKey("Ordered Date");
                string CaseNumberKey = GetUdfOppKey("CaseNumber");
                string ItemNumberKey = GetUdfOppKey("ItemNumber");
                string BrandKey = GetUdfOppKey("Brand Name");
                string ProductKey = GetUdfOppKey("Product Name");
                string ModelKey = GetUdfOppKey("Model");
                string OrderedFromKey = GetUdfOppKey("Ordered From");
                string VendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");

                string vendor = Request.QueryString["Vendor"];
                if (!string.IsNullOrEmpty(vendor) && vendor.ToUpper() == "GG")
                {
                    vendor = "The Good Guys";
                }
                else if (!string.IsNullOrEmpty(vendor) && vendor.ToUpper() == "JBHIFI")
                {
                    vendor = "JBHiFI";
                }

                sLoginString = loginToMax();
                var StatusReport = Request.Form["hfStatus"];
                string StatusFilter = "";
                if (!string.IsNullOrEmpty(StatusReport))
                {
                    if (StatusReport == "Won,Cancelled" || StatusReport == "Cancelled,Won")
                    {
                        StatusFilter = "EQ(Status,3),EQ(Status,5)";
                    }
                    else if (StatusReport.Contains("Won"))
                    {
                        StatusFilter = "EQ(Status,3)";
                    }
                    else if (StatusReport.Contains("Cancelled"))
                    {
                        StatusFilter = "EQ(Status,5)";
                    }
                    StatusFilter = "OR(" + StatusFilter + "),";
                }
                //OR(EQ(Status,3),EQ(Status,5))
                //"2019-11-13"
                string OrderedFrom = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", OrderedFromKey, vendor);
                string OrderedDateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", OrderedDateKey, hfFrom.Value, hfTo.Value);
                string searchkey = "AND(" + StatusFilter + "" + OrderedFrom + "," + OrderedDateFilter + ")";
                Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList(searchkey);

                foreach (Maximizer.Data.Opportunity opp in OppList)
                {
                    //var OrderedFrom = oUDFAccess.GetFieldValue(CaseNumberKey, opp.Key);
                    //if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToUpper() == vendor.ToUpper())
                    //{
                    string CaseNumber = oUDFAccess.GetFieldValue(CaseNumberKey, opp.Key);
                    string OrderedDate = oUDFAccess.GetFieldValue(OrderedDateKey, opp.Key);
                    string Brand = oUDFAccess.GetFieldValue(BrandKey, opp.Key);
                    string Product = oUDFAccess.GetFieldValue(ProductKey, opp.Key);
                    string Model = oUDFAccess.GetFieldValue(ModelKey, opp.Key);
                    string VendorOrderNumber = oUDFAccess.GetFieldValue(VendorOrderNumberKey, opp.Key);

                    dt.Rows.Add(opp.StatusName.Value, CaseNumber, opp.ParentName.Value, Brand, Product, Model, VendorOrderNumber, opp.ActualRevenue.Value, OrderedDate);
                    //}
                }
                var fileHeader = hfFrom.Value + " to " + hfTo.Value + " - " + vendor + " Report ";
                sb.Append(fileHeader.ToString() + ",");
                sb.Remove(sb.Length - 1, 1);
                sb.AppendLine();
                sb.AppendLine();

                foreach (DataColumn column in dt.Columns)
                {
                    //Add the Header row for CSV file.                    
                    sb.Append(column.ColumnName.ToString() + ",");

                }
                sb.Remove(sb.Length - 1, 1);
                sb.AppendLine();


                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                        sb.Append(FormatCSV(dr[dc.ColumnName].ToString()) + ",");

                    sb.Remove(sb.Length - 1, 1);
                    sb.AppendLine();
                }
                ///File.WriteAllText(Server.MapPath("/SampleCSV.csv"), sb.ToString());
            }
            catch (Exception ex)
            {

                throw;
            }
            return sb;
        }
        public static string FormatCSV(string input)
        {
            try
            {
                if (input == null)
                    return string.Empty;

                bool containsQuote = false;
                bool containsComma = false;
                int len = input.Length;
                for (int i = 0; i < len && (containsComma == false || containsQuote == false); i++)
                {
                    char ch = input[i];
                    if (ch == '"')
                        containsQuote = true;
                    else if (ch == ',')
                        containsComma = true;
                }

                if (containsQuote && containsComma)
                    input = input.Replace("\"", "\"\"");

                if (containsComma)
                    return "\"" + input + "\"";
                else
                    return input;
            }
            catch
            {
                throw;
            }
        }
        protected void btnGenerateCSV_Click(object sender, EventArgs e)
        {
            lnkCurrantEnquiries.CssClass = "menulink";
            lnkCompletedOrders.CssClass = "menulink";
            lnkCurrantOrders.CssClass = "menulink";
            lnkCompletedEnquiries.CssClass = "menulink";
            lnkReports.CssClass = "active menulink";

            CaseType = "Report";
            BindEnquires();

            StringBuilder sb = GenerateCSV();
            if (sb != null)
            {
                byte[] bytes = Encoding.ASCII.GetBytes(GenerateCSV().ToString());

                if (bytes != null)
                {
                    string FileName = hfFrom.Value + " to " + hfTo.Value + " - " + Request.QueryString["Vendor"] + " Report ";

                    //Download the CSV file.
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(sb.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            String csname1 = "PopupScript";
            Type cstype = GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            StringBuilder cstext1 = new StringBuilder();
            cstext1.Append("<script type=text/javascript> alert('Reports Generated Successfully') </");
            cstext1.Append("script>");

            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
        }
        [WebMethod]
        public static string GetCustomers(int pageIndex, string sLoginString, string VendorType, string RequestType, string fromDate, string toDate)
        {
            //Added to similate delay so that we see the loader working
            //Must be removed when moving to production
            int SkipRecords = 0;
            if (pageIndex != 1)
            {
                pageIndex = 50 * pageIndex;
                SkipRecords = pageIndex - 50;
            }
            //System.Threading.Thread.Sleep(2000);
            var dt = BindCurrentEnquiryGrid(sLoginString, VendorType, RequestType, pageIndex, SkipRecords);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            string dsXml = ds.GetXml();

            return dsXml;
        }
        [WebMethod]
        public static string GetCompletedEnquiry(int pageIndex, string sLoginString, string VendorType, string RequestType, string fromDate, string toDate)
        {
            //Added to similate delay so that we see the loader working
            //Must be removed when moving to production
            int SkipRecords = 0;
            if (pageIndex != 1)
            {
                pageIndex = 20 * pageIndex;
                SkipRecords = pageIndex - 20;
            }
            //System.Threading.Thread.Sleep(2000);
            var dt = BindCompletedEnquiryGrid(sLoginString, VendorType, RequestType, pageIndex, SkipRecords);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            string dsXml = ds.GetXml();

            return dsXml;
        }

        [WebMethod]
        public static string GetCompletedOrder(int pageIndex, string sLoginString, string VendorType, string RequestType, string fromDate, string toDate)
        {
            //Added to similate delay so that we see the loader working
            //Must be removed when moving to production
            int SkipRecords = 0;
            if (pageIndex != 1)
            {
                pageIndex = 20 * pageIndex;
                SkipRecords = pageIndex - 20;
            }
            //System.Threading.Thread.Sleep(2000);
            var dt = BindCompletedOrderGrid(sLoginString, VendorType, RequestType, pageIndex, SkipRecords);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            string dsXml = ds.GetXml();

            return dsXml;
        }

        [WebMethod]
        public static string GetCurrentOrder(int pageIndex, string sLoginString, string VendorType, string RequestType, string fromDate, string toDate)
        {
            //Added to similate delay so that we see the loader working
            //Must be removed when moving to production
            int SkipRecords = 0;
            if (pageIndex != 1)
            {
                pageIndex = 20 * pageIndex;
                SkipRecords = pageIndex - 20;
            }
            //System.Threading.Thread.Sleep(2000);
            var dt = BindCurrentOrderGrid(sLoginString, VendorType, RequestType, pageIndex, SkipRecords);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            string dsXml = ds.GetXml();

            return dsXml;
        }

        public static DataTable BindCompletedEnquiryGrid(string sLoginString, string VendorType, string RequestType, int PageIndex, int SkipRecords, string fromDate = "", string toDate = "", string filterType = "", string SearchString = "")
        {
            DataTable dt = new DataTable();
            sLoginString = loginToMax(sLoginString);
            var vendorItemNumber = "";
            string Vendor = VendorType;
            string VendorShort = "";
            string UDFUpdatedByKey = "";
            if (Vendor == "GG")
            {
                VendorShort = "GG";
                vendorItemNumber = "2";
                Vendor = "The Good Guys";
                UDFUpdatedByKey = GetUdfCSCaseKey("Updated by TheGoodGuys");
            }
            else
            {
                VendorShort = "JBHIFI";
                vendorItemNumber = "1";
                Vendor = "JBHiFI";
                UDFUpdatedByKey = GetUdfCSCaseKey("Updated by JBHIFI");
            }

            if (!string.IsNullOrEmpty(Vendor))
            {
                dt.Columns.Add("CaseNo");
                dt.Columns.Add("DateFormat");
                dt.Columns.Add("Time");
                dt.Columns.Add("Assigned");
                dt.Columns.Add("Status");
                dt.Columns.Add("Desc");
                dt.Columns.Add("Description1");
                dt.Columns.Add("EnquiryStatus");
                dt.Columns.Add("Revenue");
                dt.Columns.Add("URL");


                //CSCaseList oCSCaseList1 = oCSCaseAccess.ReadList("EQ(CaseNumber," + "US-01326" + ")");
                //CSCaseList oCSCaseList = oCSCaseAccess.ReadList("KEY(" + this.Request.QueryString["CurrentKey"] + ")");

                //string search = String.Format("LIKE(EQ(UDF," + UDFKey + ",'" + Vendor + "'))");

                string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");

                //AND(EQ(City,Vancouver), OR(LIKE(Phone1,604%), LIKE(Phone2,604%)))

                //string search1 = "EQ(UDF,"+ UDFSubmitTo1Key + ",'1' )";
                //string search = String.Format("LIKE(UDF," + UDFSubmitTo1Key + ", \"{0}%\")", Vendor);
                //string Cate1 = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", UDFSubmitTo1Key, Vendor);
                //string CurrentKeys = "KEY(" + this.Request.QueryString["CurrentKey"] + ")";
                //string Searchkey = "LIKE(UDF, " + UDFSubmitTo1Key + ",'1')";

                //case status=2 means Update
                // string searchkey = "AND(EQ(Status,2),EQ(UDF," + UDFUpdatedByKey + ",'1' ), OR(EQ(UDF," + UDFSubmitTo1Key + ",'"+ vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";
                var CancellEQ = GetCSCaseStausKey("Cancelled");
                string UDFCreatedDateKey = GetUdfCSCaseKey("Created Date");

                string searchkey = "EQ(UDF," + UDFUpdatedByKey + ",'1' ), OR(EQ(Status," + CancellEQ + "),EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' ))";
                string strFilterKey = "";
                if (filterType == "DateRange")
                {
                    //CreationDate
                    // String.Format("LIKE(City, \"{0}%\")", sCityName);
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);

                    strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";
                }
                else if (filterType == "Case Number" || filterType == "Name")
                {
                    string UDFKey = "";
                    string Filter = "";
                    if (filterType == "Name")
                    {
                        Filter = String.Format("LIKE(ParentName, \"{0}%\")", SearchString);

                    }
                    else if (filterType == "Case Number")
                    {
                        Filter = String.Format("LIKE(CaseNumber, \"{0}%\")", SearchString);
                        // Filter = String.Format("LIKE(CaseNumber, \"%{0}%\")", SearchString);
                    }
                    //UDFKey = GetUdfCSCaseKey("Updated by TheGoodGuys");
                    //Filter = String.Format("LIKE(UDF,\"{0}\",'\"%{1}%\"')", UDFSubmitTo1Key, Vendor);
                    //string Filter=  String.Format("LIKE(City, \"{0}%\")", sCityName);
                    strFilterKey = "AND(" + searchkey + "," + Filter + ")";
                }
                else
                {
                    strFilterKey = "AND(" + searchkey + ")";
                }
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(strFilterKey, PageIndex);
                List<Maximizer.Data.CSCase> oList = new List<Maximizer.Data.CSCase>();
                if (SkipRecords != 0)
                {
                    if (oCSCaseList != null)
                    {
                        oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().Skip(SkipRecords).ToList();
                    }
                }
                else
                {
                    if (oCSCaseList != null)
                    {
                        oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().ToList();
                    }
                }

                foreach (CSCase oCsCase in oList)
                {
                    //var cancel = GetCSCaseStausKey("Cancelled");
                    //if (oCsCase.Status.Value == cancel)//4
                    //    continue;

                    var hide = "";
                    var more = "<a href='#' class='aMore'>...more</a>";
                    var morePrice = "<a href='#' class='aMorePrice'>...more</a>";

                    string Description = "";
                    string Price = "";
                    int j = 0;
                    string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");
                    var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                    for (int i = 1; i <= 10; i++)
                    {
                        if (i != 1)
                        {
                            hide = "style='display:none;'";
                        }
                        var SubmitTo = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Submit To" + i), oCsCase.Key);
                        if (string.IsNullOrEmpty(SubmitTo))
                        {
                            continue;
                        }
                        if (!string.IsNullOrEmpty(SubmitTo) && !SubmitTo.ToLower().Contains(Vendor.ToLower()))
                        {
                            continue;
                        }
                        string UDFBrandKey = GetUdfCSCaseKey("Brand" + i);
                        string UDFProductKey = GetUdfCSCaseKey("Product" + i);
                        string UDFModelKey = GetUdfCSCaseKey("Model" + i);
                        string UDFStateCodeKey = GetUdfCSCaseKey("Suburb and Postcode" + i);

                        var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, oCsCase.Key);
                        var Product = oUDFAccess.GetFieldValue(UDFProductKey, oCsCase.Key);
                        var Model = oUDFAccess.GetFieldValue(UDFModelKey, oCsCase.Key);
                        var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, oCsCase.Key);

                        if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                        {
                            j++;
                            if (string.IsNullOrEmpty(Description))
                            {
                                hide = "";
                            }
                            Description += "<span class='more' " + hide + ">";
                            Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>:" + Model + "</br><b>Suburb and Postcode: </b>" + State;
                            Description += "</span></br>";
                            string UDFPUKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Pick Up");
                            string UDFDelKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Delivery");
                            string UDFInstKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Install");
                            string UDFRemKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Removal");

                            var PU = oUDFAccess.GetFieldValue(UDFPUKey, oCsCase.Key);
                            var Del = oUDFAccess.GetFieldValue(UDFDelKey, oCsCase.Key);
                            var Inst = oUDFAccess.GetFieldValue(UDFInstKey, oCsCase.Key);
                            var Rem = oUDFAccess.GetFieldValue(UDFRemKey, oCsCase.Key);

                            Price += "<span class='morePrice' " + hide + ">";
                            Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                            Price += "</span></br>";
                        }
                        else
                        {
                            break;
                        }
                    }
                    Description += "<b>Enquiry Comments: </b>" + EnquiryComments + "</br>";
                    if (j > 1)
                    {
                        Description += more;
                        Price += morePrice;
                    }

                    string userName = "";
                    if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                    {
                        UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                        foreach (User oUser in oUserList)
                        {
                            userName = oUser.DisplayName;
                        }
                    }

                    //string UDFKey = GetUdfOppKey("Submit To");
                    string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                    string CaseNumber = oCsCase.CaseNumber.Value;
                    double RevenuValue = 0;

                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "')");
                    string UDFItemNumberKey = GetUdfOppKey("ItemNumber");

                    var items = "";
                    var OppKeystring = "";
                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                        items = itemNumber + ",";
                        OppKeystring = opp.Key + "-" + itemNumber + ",";
                        RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);
                    }
                    OppKeystring = OppKeystring.TrimEnd(',');
                    items = items.TrimEnd(',');
                    string status = "UPDATED";
                    if (oCsCase.Status.Value == CancellEQ)
                        status = "Cancelled".ToUpper();

                    string EnquiryStatus = oCsCase.Status.Value.ToString();
                    //string Description = oCsCase.Description.Value;
                    //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                    //    Description = Description.Substring(0, 50) + ".....";

                    //var suffix = "&Vendor=" + Request.QueryString["Vendor"] + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=CallCenter";
                    //var URL = "CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + suffix;

                    var suffix = "&Vendor=" + VendorType + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Email&ET=CE";
                    var URL = "CustomDialogs/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;

                    dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oCsCase.CreationDate.DateValue).ToString("dd-MM-yyyy"), Convert.ToDateTime(oCsCase.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, oCsCase.Description.Value, EnquiryStatus, Price, URL);
                    //}
                }
            }
            else
            {
            }
            if (dt.Rows.Count <= 0)
            {
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            return dt;
        }
        public static DataTable BindCurrentEnquiryGrid(string sLoginString, string VendorType, string RequestType, int PageIndex, int SkipRecords, string fromDate = "", string toDate = "", string filterType = "", string SearchString = "")
        {
            DataTable dt = new DataTable();
            try
            {
                sLoginString = loginToMax(sLoginString);

                var vendorItemNumber = "";
                string VendorShort = "";
                var Vendor = VendorType;
                if (Vendor == "GG")
                {
                    VendorShort = "GG";
                    vendorItemNumber = "2";
                    Vendor = "The Good Guys";
                }
                else
                {
                    VendorShort = "JBHIFI";
                    vendorItemNumber = "1";
                    Vendor = "JBHiFI";
                }

                if (!string.IsNullOrEmpty(Vendor))
                {
                    dt.Columns.Add("CaseNo");
                    dt.Columns.Add("DateFormat");
                    dt.Columns.Add("Time");
                    dt.Columns.Add("Assigned");
                    dt.Columns.Add("Status");
                    dt.Columns.Add("Desc");
                    dt.Columns.Add("Description1");
                    dt.Columns.Add("EnquiryStatus");
                    dt.Columns.Add("Revenue");
                    dt.Columns.Add("URL");

                    string UDFCreatedDateKey = GetUdfCSCaseKey("Created Date");

                    string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                    string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                    string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                    string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                    string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                    string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                    string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                    string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                    string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                    string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                    //case status=1 means Waiting
                    var Waiting = GetCSCaseStausKey("Wait for customer");
                    string searchkey = "EQ(Status," + Waiting + "), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' ))";
                    string strFilterKey = "";
                    if (filterType == "DateRange")
                    {
                        //CreationDate
                        // String.Format("LIKE(City, \"{0}%\")", sCityName);


                        string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                        strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";
                    }
                    else if (filterType == "Case Number" || filterType == "Name")
                    {
                        string UDFKey = "";
                        string Filter = "";
                        if (filterType == "Name")
                        {
                            Filter = String.Format("LIKE(ParentName, \"{0}%\")", SearchString);

                        }
                        else if (filterType == "Case Number")
                        {
                            Filter = String.Format("LIKE(CaseNumber, \"{0}%\")", SearchString);
                            // Filter = String.Format("LIKE(CaseNumber, \"%{0}%\")", SearchString);
                        }


                        //UDFKey = GetUdfCSCaseKey("Updated by TheGoodGuys");
                        //Filter = String.Format("LIKE(UDF,\"{0}\",'\"%{1}%\"')", UDFSubmitTo1Key, Vendor);
                        //string Filter=  String.Format("LIKE(City, \"{0}%\")", sCityName);
                        strFilterKey = "AND(" + searchkey + "," + Filter + ")";
                    }
                    else
                    {
                        strFilterKey = "AND(" + searchkey + ")";
                    }
                    //"2019-11-13"
                    //string OrderedFrom = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", OrderedFromKey, vendor);
                    //string OrderedDateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", OrderedDateKey, hfFrom.Value, hfTo.Value);
                    //string searchkey = "AND(" + OrderedFrom + "," + OrderedDateFilter + ")";

                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList(strFilterKey, PageIndex);
                    List<Maximizer.Data.CSCase> oList = new List<Maximizer.Data.CSCase>();
                    if (SkipRecords != 0)
                    {
                        if (oCSCaseList != null)
                        {
                            oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().Skip(SkipRecords).ToList();
                        }
                    }
                    else
                    {
                        if (oCSCaseList != null)
                        {
                            oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().ToList();
                        }
                    }
                    foreach (CSCase oCsCase in oList)
                    {
                        var cancel = GetCSCaseStausKey("Cancelled");
                        if (oCsCase.Status.Value == cancel)//4
                            continue;

                        string strUpdatedBy = "";
                        string VendorValue = Vendor;
                        if (VendorValue == "GG" || VendorValue == "The Good Guys")
                        {
                            strUpdatedBy = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Updated by TheGoodGuys"), oCsCase.Key);
                        }
                        else
                        {
                            strUpdatedBy = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Updated by JBHIFI"), oCsCase.Key);
                        }
                        if (oCsCase.Status.Value.ToString() == Convert.ToString(Waiting) && (strUpdatedBy == "No" || strUpdatedBy == ""))
                        {
                            var hide = "";
                            var more = "<a href='#' class='aMore'>...more</a>";
                            var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                            string Description = "";
                            string Price = "";
                            int j = 0;
                            string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");
                            var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                            for (int i = 1; i <= 10; i++)
                            {
                                if (i != 1)
                                {
                                    hide = "style='display:none;'";
                                }

                                var SubmitTo = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Submit To" + i), oCsCase.Key);
                                if (string.IsNullOrEmpty(SubmitTo))
                                {
                                    continue;
                                }
                                if (!string.IsNullOrEmpty(SubmitTo) && !SubmitTo.ToLower().Contains(Vendor.ToLower()))
                                {
                                    continue;
                                }
                                string UDFBrandKey = GetUdfCSCaseKey("Brand" + i);
                                string UDFProductKey = GetUdfCSCaseKey("Product" + i);
                                string UDFModelKey = GetUdfCSCaseKey("Model" + i);
                                string UDFStateCodeKey = GetUdfCSCaseKey("Suburb and Postcode" + i);


                                var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, oCsCase.Key);
                                var Product = oUDFAccess.GetFieldValue(UDFProductKey, oCsCase.Key);
                                var Model = oUDFAccess.GetFieldValue(UDFModelKey, oCsCase.Key);
                                var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, oCsCase.Key);

                                if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                {
                                    j++;
                                    if (string.IsNullOrEmpty(Description))
                                    {
                                        hide = "";
                                    }
                                    Description += "<span class='more' " + hide + ">";
                                    Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State;
                                    Description += "<br/><br/></span>";
                                    string UDFPUKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Pick Up");
                                    string UDFDelKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Delivery");
                                    string UDFInstKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Install");
                                    string UDFRemKey = GetUdfCSCaseKey("P" + i + " " + VendorShort + " Price Removal");

                                    var PU = oUDFAccess.GetFieldValue(UDFPUKey, oCsCase.Key);
                                    var Del = oUDFAccess.GetFieldValue(UDFDelKey, oCsCase.Key);
                                    var Inst = oUDFAccess.GetFieldValue(UDFInstKey, oCsCase.Key);
                                    var Rem = oUDFAccess.GetFieldValue(UDFRemKey, oCsCase.Key);
                                    Price += "<span class='morePrice' " + hide + ">";
                                    Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                                    Price += "<br/><br/></span>";
                                }
                                else
                                {
                                    break;
                                }

                            }
                            Description += "<span><b>Enquiry Comments: </b>" + EnquiryComments + "<br/></span>";
                            if (j > 1)
                            {
                                Description += more;
                                Price += morePrice;
                            }
                            string userName = "";
                            if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                            {
                                UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                                foreach (User oUser in oUserList)
                                {
                                    userName = oUser.DisplayName;
                                }
                            }
                            //string UDFKey = GetUdfOppKey("Submit To");
                            string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                            string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                            string CaseNumber = oCsCase.CaseNumber.Value;
                            double RevenuValue = 0;

                            Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "')");
                            var items = "";
                            var OppKeystring = "";
                            foreach (Maximizer.Data.Opportunity opp in OppList)
                            {
                                var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                items = itemNumber + ",";
                                OppKeystring = opp.Key + "-" + itemNumber + ",";
                                RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);
                            }
                            OppKeystring = OppKeystring.TrimEnd(',');
                            items = items.TrimEnd(',');
                            string status = "PENDING";


                            string EnquiryStatus = oCsCase.Status.Value.ToString();
                            //string Description = oCsCase.Description.Value;
                            //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                            //    Description = Description.Substring(0, 50) + ".....";
                            var suffix = "&Vendor=" + VendorType + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Email";
                            var URL = "CustomDialogs/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;
                            dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oCsCase.CreationDate.DateValue).ToString("dd-MM-yyyy"), Convert.ToDateTime(oCsCase.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, oCsCase.Description.Value, EnquiryStatus, Price, URL);
                        }
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {

            }
            if (dt.Rows.Count <= 0)
            {
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            return dt;
        }
        public static DataTable BindCompletedOrderGrid(string sLoginString, string VendorType, string RequestType, int PageIndex, int SkipRecords, string fromDate = "", string toDate = "", string filterType = "", string SearchString = "")
        {
            DataTable dt = new DataTable();
            string VendorShort = "";
            var vendorItemNumber = "";
            string Vendor = VendorType;
            if (Vendor == "GG")
            {
                VendorShort = "GG";
                vendorItemNumber = "2";
                Vendor = "The Good Guys";
            }
            else
            {
                VendorShort = "JBHIFI";
                vendorItemNumber = "1";
                Vendor = "JBHiFI";
            }

            if (!string.IsNullOrEmpty(Vendor))
            {
                sLoginString = loginToMax(sLoginString);
                //if (this.IsPostBack)
                // return;

                dt.Columns.Add("CaseNo");
                dt.Columns.Add("DateFormat");
                dt.Columns.Add("Time");
                dt.Columns.Add("Assigned");
                dt.Columns.Add("Status");
                dt.Columns.Add("Desc");
                dt.Columns.Add("Description1");
                dt.Columns.Add("EnquiryStatus");
                dt.Columns.Add("Revenue");
                dt.Columns.Add("URL");

                string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                var Complete = GetCSCaseStausKey("Complete");
                var Cancelled = GetCSCaseStausKey("Cancelled");
                var Pending = GetCSCaseStausKey("Pending");//57999
                string searchkey = "AND(OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + Cancelled + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";

                string strFilterKey = "";
                if (filterType == "DateRange")
                {
                    //CreationDate
                    // String.Format("LIKE(City, \"{0}%\")", sCityName);

                    string UDFCreatedDateKey = GetUdfCSCaseKey("Created Date");
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                    strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";
                }
                else if (filterType == "Case Number" || filterType == "Name")
                {
                    string UDFKey = "";
                    string Filter = "";
                    if (filterType == "Name")
                    {
                        Filter = String.Format("LIKE(ParentName, \"{0}%\")", SearchString);

                    }
                    else if (filterType == "Case Number")
                    {
                        Filter = String.Format("LIKE(CaseNumber, \"{0}%\")", SearchString);
                        // Filter = String.Format("LIKE(CaseNumber, \"%{0}%\")", SearchString);
                    }
                    //UDFKey = GetUdfCSCaseKey("Updated by TheGoodGuys");
                    //Filter = String.Format("LIKE(UDF,\"{0}\",'\"%{1}%\"')", UDFSubmitTo1Key, Vendor);
                    //string Filter=  String.Format("LIKE(City, \"{0}%\")", sCityName);
                    strFilterKey = "AND(" + searchkey + "," + Filter + ")";
                }
                else
                {
                    strFilterKey = "AND(" + searchkey + ")";
                }
                //"2019-11-13"
                //string OrderedFrom = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", OrderedFromKey, vendor);
                //string OrderedDateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", OrderedDateKey, hfFrom.Value, hfTo.Value);
                //string searchkey = "AND(" + OrderedFrom + "," + OrderedDateFilter + ")";

                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(strFilterKey, PageIndex);
                List<Maximizer.Data.CSCase> oList = new List<Maximizer.Data.CSCase>();
                if (SkipRecords != 0)
                {
                    if (oCSCaseList != null)
                    {
                        oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().Skip(SkipRecords).ToList();
                    }
                }
                else
                {
                    if (oCSCaseList != null)
                    {
                        oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().ToList();
                    }
                }


                string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                foreach (CSCase oCsCase in oList)
                {
                    var hide = "";
                    var more = "<a href='#' class='aMore'>...more</a>";
                    var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                    if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(Cancelled))
                    {
                        int j = 0;
                        int i = 1;


                        if (i != 1)
                        {
                            hide = "style='display:none;'";
                        }


                        i++;



                        string userName = "";
                        if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                        {
                            UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                            foreach (User oUser in oUserList)
                            {
                                userName = oUser.DisplayName;
                            }
                        }

                        string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                        string UDFSubmitToKey = GetUdfOppKey("Submit To");
                        string CaseNumber = oCsCase.CaseNumber.Value;
                        double RevenuValue = 0;
                        string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");
                        var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                        //Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "')");
                        //Status=3 means Won
                        Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(OR(EQ(Status,3),EQ(Status,5)),EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "'))");

                        var items = "";
                        var OppKeystring = "";
                        string status = "COMPLETED";
                        foreach (Maximizer.Data.Opportunity opp in OppList)
                        {
                            if (opp.Status.Value == 5)
                            {
                                status = "CANCELLED";
                            }
                            string Description = "";
                            string Price = "";
                            var SubmitTo = oUDFAccess.GetFieldValue(UDFSubmitToKey, opp.Key);
                            if (!string.IsNullOrEmpty(SubmitTo) && !SubmitTo.ToLower().Contains(Vendor.ToLower()))
                            {
                                continue;
                            }
                            var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                            if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                            {
                                var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                items = itemNumber;
                                OppKeystring = opp.Key + "-" + itemNumber;
                                RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                string UDFBrandKey = GetUdfOppKey("Brand Name");
                                string UDFProductKey = GetUdfOppKey("Product Name");
                                string UDFModelKey = GetUdfOppKey("Model");
                                string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                string DeliveryAddressKey = GetUdfOppKey("Delivery Address");
                                string VendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");

                                var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                var VendorOrderNumber = oUDFAccess.GetFieldValue(VendorOrderNumberKey, opp.Key);
                                if (VendorShort == "JBHIFI")
                                {
                                    EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                }
                                else if (VendorShort == "GG")
                                {
                                    EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                }
                                var selectItemsPrice = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Item Price Tick Marked " + VendorShort + itemNumber + ""), oCsCase.Key);
                                //itemNumber
                                //Item Price Tick Marked GG1
                                if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                {
                                    if (string.IsNullOrEmpty(Description))
                                    {
                                        hide = "";
                                    }
                                    Description += "<span class='more' " + hide + ">";
                                    Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress + "</br><b>Vendor Order Number: </b>" + VendorOrderNumber;
                                    Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                    Description += "</span></br>";

                                    string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                    string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                    string UDFInstKey = GetUdfOppKey("+Inst");
                                    string UDFRemKey = GetUdfOppKey("+Rem");

                                    var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                    var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                    var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                    var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);

                                    Price += "<span class='morePrice' " + hide + ">";
                                    Price += "<b>Product: </b>" + PU + "</br>";
                                    if (!string.IsNullOrEmpty(selectItemsPrice))
                                    {
                                        //if(selectItemsPrice.Contains("Product Price"))
                                        //    Price += "<b>Product: </b>" + PU + "</br>";

                                        if (selectItemsPrice.Contains("Delivery Price"))
                                            Price += "<b> +Del: </b>" + Del + "</br>";

                                        if (selectItemsPrice.Contains("Install Price"))
                                            Price += "<b>+Inst: </b>" + Inst + "</br>";

                                        if (selectItemsPrice.Contains("Removal Price"))
                                            Price += "<b>+Rem: </b>" + Rem;

                                    }
                                    Price += "</span>";
                                }
                                var suffix = "&Vendor=" + VendorType + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CO";
                                var URL = "CustomDialogs/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;

                                string EnquiryStatus = oCsCase.Status.Value.ToString();
                                //string Description = opp.Comment.Value;
                                //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                //    Description = Description.Substring(0, 50) + ".....";

                                //if (OppList.Count > 1)
                                //{
                                //    Description += more;
                                //    Price += morePrice;
                                //}
                                string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                                dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oUDFAccess.GetFieldValue(UDFCreatedDateKey, opp.Key)).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);
                            }
                        }
                    }
                }
                if (dt.Rows.Count <= 0)
                {
                    dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
                }
                return dt;
            }
            else
            {

            }
            return null;
        }
        public static DataTable BindCurrentOrderGridByDateRange(string sLoginString, string VendorType, string RequestType, string fromDate = "", string toDate = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string VendorShort = "";
                var vendorItemNumber = "";
                string Vendor = VendorType;
                if (Vendor == "GG")
                {
                    VendorShort = "GG";
                    vendorItemNumber = "2";
                    Vendor = "The Good Guys";
                }
                else
                {
                    VendorShort = "JBHIFI";
                    vendorItemNumber = "1";
                    Vendor = "JBHiFI";
                }


                if (!string.IsNullOrEmpty(Vendor))
                {
                    sLoginString = loginToMax(sLoginString);

                    dt.Columns.Add("CaseNo");
                    dt.Columns.Add("DateFormat");
                    dt.Columns.Add("Time");
                    dt.Columns.Add("Assigned");
                    dt.Columns.Add("Status");
                    dt.Columns.Add("Desc");
                    dt.Columns.Add("Description1");
                    dt.Columns.Add("EnquiryStatus");
                    dt.Columns.Add("Revenue");
                    dt.Columns.Add("URL");

                    string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                    string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                    string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                    string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                    string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                    string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                    string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                    string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                    string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                    string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                    string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");

                    var Pending = GetCSCaseStausKey("Pending");
                    var Complete = GetCSCaseStausKey("Complete");
                    var CallBack = GetCSCaseStausKey("Call Back");

                    string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");

                    double RevenuValue = 0;

                    string UDFVendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");
                    //var fromDate = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                    //var toDate = DateTime.Now.ToString("yyyy-MM-dd");

                    string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                    //strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";

                    //0Status for New and 1 for Updated
                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(" + DateFilter + ",OR(EQ(Status,0),EQ(Status,1),EQ(Status,2)))");
                    var items = "";
                    var OppKeystring = "";
                    string status = "PENDING";


                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        var CaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);

                        string CSsearchkey = "AND( EQ(CaseNumber, " + CaseNumber + "),OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + CallBack + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";
                        CSCaseList oCSCaseList = oCSCaseAccess.ReadList(CSsearchkey);
                        string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                        string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                        foreach (CSCase oCsCase in oCSCaseList)
                        {
                            var cancel = GetCSCaseStausKey("Cancelled");
                            if (oCsCase.Status.Value == cancel)//4
                                continue;


                            if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(CallBack))
                            {

                                var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);

                                var hide = "";
                                var more = "<a href='#' class='aMore'>...more</a>";
                                var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                                int j = 0;
                                int i = 1;


                                if (i != 1)
                                {
                                    hide = "style='display:none;'";
                                }


                                i++;

                                string userName = "";
                                if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                                {
                                    UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                                    foreach (User oUser in oUserList)
                                    {
                                        userName = oUser.DisplayName;
                                    }
                                }


                                //
                                //if(oCsCase.CaseNumber.Value== "US-01581")
                                // {

                                // }
                                if (opp.Status.Value == 1 || opp.Status.Value == 2)
                                    status = "UPDATED";
                                else
                                    status = "PENDING";

                                string Description = "";
                                string Price = "";
                                var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                                if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                                {
                                    var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                    items = itemNumber;
                                    OppKeystring = opp.Key + "-" + itemNumber;
                                    RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                    string UDFBrandKey = GetUdfOppKey("Brand Name");
                                    string UDFProductKey = GetUdfOppKey("Product Name");
                                    string UDFModelKey = GetUdfOppKey("Model");
                                    string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                    string DeliveryAddressKey = GetUdfOppKey("Delivery Address");

                                    var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                    var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                    var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                    var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                    var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                    if (VendorShort == "JBHIFI")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                    }
                                    else if (VendorShort == "GG")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                    }
                                    if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                    {
                                        j++;
                                        if (string.IsNullOrEmpty(Description))
                                        {
                                            hide = "";
                                        }
                                        Description += "<span class='more' " + hide + ">";
                                        Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress;
                                        Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                        Description += "</span></br>";

                                        string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                        string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                        string UDFInstKey = GetUdfOppKey("+Inst");
                                        string UDFRemKey = GetUdfOppKey("+Rem");

                                        var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                        var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                        var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                        var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);
                                        Price += "<span class='morePrice' " + hide + ">";
                                        Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                                        Price += "</span></br>";
                                    }

                                    var suffix = "&Vendor=" + VendorShort + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CURRORD";
                                    var URL = "/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;


                                    string EnquiryStatus = oCsCase.Status.Value.ToString();
                                    //string Description = opp.Comment.Value;
                                    //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                    //    Description = Description.Substring(0, 50) + ".....";

                                    //if (OppList.Count>1)
                                    //{
                                    //    Description += more;
                                    //    Price += morePrice;
                                    //}
                                    dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oUDFAccess.GetFieldValue(UDFCreatedDateKey, opp.Key)).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);

                                }





                            }

                        }
                    }







                }
                else
                {

                }

            }
            catch (Exception ex)
            {

                throw;
            }
            if (dt.Rows.Count <= 0)
            {
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            return dt;


        }

        public static DataTable BindCurrentOrderGrid(string sLoginString, string VendorType, string RequestType, int PageIndex, int SkipRecords, string fromDate = "", string toDate = "", string filterType = "", string SearchString = "")
        {
            string VendorShort = "";
            var vendorItemNumber = "";
            string Vendor = VendorType;
            if (Vendor == "GG")
            {
                VendorShort = "GG";
                vendorItemNumber = "2";
                Vendor = "The Good Guys";
            }
            else
            {
                VendorShort = "JBHIFI";
                vendorItemNumber = "1";
                Vendor = "JBHiFI";
            }

            if (!string.IsNullOrEmpty(Vendor))
            {
                sLoginString = loginToMax(sLoginString);
                //if (this.IsPostBack)
                // return;
                DataTable dt = new DataTable();
                dt.Columns.Add("CaseNo");
                dt.Columns.Add("DateFormat");
                dt.Columns.Add("Time");
                dt.Columns.Add("Assigned");
                dt.Columns.Add("Status");
                dt.Columns.Add("Desc");
                dt.Columns.Add("Description1");
                dt.Columns.Add("EnquiryStatus");
                dt.Columns.Add("Revenue");
                dt.Columns.Add("URL");

                string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                var Pending = GetCSCaseStausKey("Pending");
                var Complete = GetCSCaseStausKey("Complete");
                var CallBack = GetCSCaseStausKey("Call Back");
                string searchkey = "AND( OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + CallBack + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";


                string strFilterKey = "";
                if (filterType == "DateRange")
                {
                    //CreationDate
                    // String.Format("LIKE(City, \"{0}%\")", sCityName);

                    string UDFCreatedDateKey = GetUdfCSCaseKey("Created Date");
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);
                    strFilterKey = "AND(" + searchkey + "," + DateFilter + ")";
                }
                else if (filterType == "Case Number" || filterType == "Name")
                {
                    string UDFKey = "";
                    string Filter = "";
                    if (filterType == "Name")
                    {
                        Filter = String.Format("LIKE(ParentName, \"{0}%\")", SearchString);

                    }
                    else if (filterType == "Case Number")
                    {
                        Filter = String.Format("LIKE(CaseNumber, \"{0}%\")", SearchString);
                        // Filter = String.Format("LIKE(CaseNumber, \"%{0}%\")", SearchString);
                    }
                    //UDFKey = GetUdfCSCaseKey("Updated by TheGoodGuys");
                    //Filter = String.Format("LIKE(UDF,\"{0}\",'\"%{1}%\"')", UDFSubmitTo1Key, Vendor);
                    //string Filter=  String.Format("LIKE(City, \"{0}%\")", sCityName);
                    strFilterKey = "AND(" + searchkey + "," + Filter + ")";
                }
                else
                {
                    strFilterKey = "AND(" + searchkey + ")";
                }
                //"2019-11-13"
                //string OrderedFrom = String.Format("EQ(UDF,\"{0}\",'\"{1}\"')", OrderedFromKey, vendor);
                //string OrderedDateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", OrderedDateKey, hfFrom.Value, hfTo.Value);
                //string searchkey = "AND(" + OrderedFrom + "," + OrderedDateFilter + ")";

                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(strFilterKey, PageIndex);
                List<Maximizer.Data.CSCase> oList = new List<Maximizer.Data.CSCase>();
                if (SkipRecords != 0)
                {
                    if (oCSCaseList != null)
                    {
                        oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().Skip(SkipRecords).ToList();
                    }
                }
                else
                {
                    if (oCSCaseList != null)
                    {
                        oList = oCSCaseList.Cast<Maximizer.Data.CSCase>().ToList();
                    }
                }


                string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                foreach (CSCase oCsCase in oList)
                {
                    var cancel = GetCSCaseStausKey("Cancelled");
                    if (oCsCase.Status.Value == cancel)//4
                        continue;


                    if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(CallBack))
                    {
                        var hide = "";
                        var more = "<a href='#' class='aMore'>...more</a>";
                        var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                        int j = 0;
                        int i = 1;


                        if (i != 1)
                        {
                            hide = "style='display:none;'";
                        }


                        i++;

                        string userName = "";
                        if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                        {
                            UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                            foreach (User oUser in oUserList)
                            {
                                userName = oUser.DisplayName;
                            }
                        }

                        string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                        string CaseNumber = oCsCase.CaseNumber.Value;
                        double RevenuValue = 0;
                        string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");
                        var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                        //0Status for New and 1 for Updated
                        Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(OR(EQ(Status,0),EQ(Status,1),EQ(Status,2)),EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "'))");
                        var items = "";
                        var OppKeystring = "";
                        string status = "PENDING";


                        foreach (Maximizer.Data.Opportunity opp in OppList)
                        {
                            //
                            //if(oCsCase.CaseNumber.Value== "US-01581")
                            // {

                            // }
                            if (opp.Status.Value == 1 || opp.Status.Value == 2)
                                status = "UPDATED";
                            else
                                status = "PENDING";

                            string Description = "";
                            string Price = "";
                            var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                            if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                            {
                                var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                items = itemNumber;
                                OppKeystring = opp.Key + "-" + itemNumber;
                                RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                string UDFBrandKey = GetUdfOppKey("Brand Name");
                                string UDFProductKey = GetUdfOppKey("Product Name");
                                string UDFModelKey = GetUdfOppKey("Model");
                                string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                string DeliveryAddressKey = GetUdfOppKey("Delivery Address");

                                var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                if (VendorShort == "JBHIFI")
                                {
                                    EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                }
                                else if (VendorShort == "GG")
                                {
                                    EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                }
                                if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                {
                                    j++;
                                    if (string.IsNullOrEmpty(Description))
                                    {
                                        hide = "";
                                    }
                                    Description += "<span class='more' " + hide + ">";
                                    Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress;
                                    Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                    Description += "</span></br>";

                                    string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                    string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                    string UDFInstKey = GetUdfOppKey("+Inst");
                                    string UDFRemKey = GetUdfOppKey("+Rem");

                                    var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                    var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                    var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                    var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);
                                    Price += "<span class='morePrice' " + hide + ">";
                                    Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                                    Price += "</span></br>";
                                }

                                var suffix = "&Vendor=" + VendorType + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CURRORD";
                                var URL = "CustomDialogs/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;


                                string EnquiryStatus = oCsCase.Status.Value.ToString();
                                //string Description = opp.Comment.Value;
                                //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                //    Description = Description.Substring(0, 50) + ".....";

                                //if (OppList.Count>1)
                                //{
                                //    Description += more;
                                //    Price += morePrice;
                                //}
                                string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                                dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oUDFAccess.GetFieldValue(UDFCreatedDateKey, opp.Key)).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);
                            }
                        }





                    }

                }
                if (dt.Rows.Count <= 0)
                {
                    dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
                }
                return dt;


            }
            else
            {

            }
            return null;
        }
        public static int GetCSCaseStausKey(string Status)
        {
            int Key = 0;
            try
            {
                //sLoginString = loginToMax();
                var oCSCaseStatusList = oCSCaseAccess.ReadStatusOptionListForAssign();
                foreach (Maximizer.Data.Option item in oCSCaseStatusList)
                {
                    if (item.DisplayValue.ToLower() == Status.ToLower())
                    {
                        return Convert.ToInt32(item.FieldValue);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Key;
        }



        [WebMethod]
        public static string GetGridDataForFilterData(string ActiveTab, string sLoginString, string VendorType, string RequestType, string fromDate, string toDate)
        {
            try
            {
                DataTable dt = new DataTable();
                if (ActiveTab.ToUpper().Trim() == "CURRENT ENQUIRIES")
                {
                    dt = BindCurrentEnquiryGrid(sLoginString, VendorType, RequestType, 0, 0, fromDate, toDate, "DateRange");
                }
                else if (ActiveTab.ToUpper().Trim() == "COMPLETED ENQUIRIES")
                {
                    dt = BindCompletedEnquiryGrid(sLoginString, VendorType, RequestType, 0, 0, fromDate, toDate, "DateRange");
                }
                else if (ActiveTab.ToUpper().Trim() == "CURRENT ORDERS")
                {
                    //dt = BindCurrentOrderGrid(sLoginString, VendorType, RequestType, 0, 0, fromDate, toDate, "DateRange");
                    dt = BindCurrentOrderGridByDateRange(sLoginString, VendorType, RequestType,fromDate, toDate);
                }
                else if (ActiveTab.ToUpper().Trim() == "COMPLETED ORDERS")
                {
                    //dt = BindCompletedOrderGrid(sLoginString, VendorType, RequestType, 0, 0, fromDate, toDate, "DateRange");
                    dt = BindCompletedOrderByDateRange(sLoginString, VendorType, RequestType, fromDate, toDate);
                }

                DataView dv = dt.DefaultView;
                dv.Sort = "CaseNo desc";
                dt = dv.ToTable();

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);

                string dsXml = ds.GetXml();
                return dsXml;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }
        public static DataTable BindCompletedOrderByDateRange(string sLoginString, string VendorType, string RequestType,string fromDate,string toDate)
        {
            DataTable dt = new DataTable();
            try
            {
                string VendorShort = "";
                var vendorItemNumber = "";
                string Vendor = VendorType;
                if (Vendor == "GG")
                {
                    VendorShort = "GG";
                    vendorItemNumber = "2";
                    Vendor = "The Good Guys";
                }
                else
                {
                    VendorShort = "JBHIFI";
                    vendorItemNumber = "1";
                    Vendor = "JBHiFI";
                }

                if (!string.IsNullOrEmpty(Vendor))
                {
                    sLoginString = loginToMax(sLoginString);

                    dt.Columns.Add("CaseNo");
                    dt.Columns.Add("DateFormat");
                    dt.Columns.Add("Time");
                    dt.Columns.Add("Assigned");
                    dt.Columns.Add("Status");
                    dt.Columns.Add("Desc");
                    dt.Columns.Add("Description1");
                    dt.Columns.Add("EnquiryStatus");
                    dt.Columns.Add("Revenue");
                    dt.Columns.Add("URL");

                    string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                    string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                    string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                    string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                    string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                    string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                    string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                    string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                    string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                    string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                    string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");

                    var Complete = GetCSCaseStausKey("Complete");
                    var Cancelled = GetCSCaseStausKey("Cancelled");
                    var Pending = GetCSCaseStausKey("Pending");//57999


                    string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                    string UDFSubmitToKey = GetUdfOppKey("Submit To");
                    string UDFVendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");

                    double RevenuValue = 0;

                    //var fromDate = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                    //var toDate = DateTime.Now.ToString("yyyy-MM-dd");

                    string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                    string DateFilter = String.Format("RANGE(UDF,\"{0}\", \"{1}\", \"{2}\")", UDFCreatedDateKey, fromDate, toDate);

                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(OR(EQ(Status,3),EQ(Status,5))," + DateFilter + ")");

                    var items = "";
                    var OppKeystring = "";
                    string status = "COMPLETED";
                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        var CaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);

                        string CSsearchkey = "AND(EQ(CaseNumber, " + CaseNumber + "),OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + Cancelled + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";

                        CSCaseList oCSCaseList = oCSCaseAccess.ReadList(CSsearchkey);
                        string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                        string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                        foreach (CSCase oCsCase in oCSCaseList)
                        {
                            var hide = "";
                            var more = "<a href='#' class='aMore'>...more</a>";
                            var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                            if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(Cancelled))
                            {
                                int j = 0;
                                int i = 1;


                                if (i != 1)
                                {
                                    hide = "style='display:none;'";
                                }


                                i++;



                                string userName = "";
                                if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                                {
                                    UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                                    foreach (User oUser in oUserList)
                                    {
                                        userName = oUser.DisplayName;
                                    }
                                }


                                if (opp.Status.Value == 5)
                                {
                                    status = "CANCELLED";
                                }
                                string Description = "";
                                string Price = "";
                                var SubmitTo = oUDFAccess.GetFieldValue(UDFSubmitToKey, opp.Key);
                                if (!string.IsNullOrEmpty(SubmitTo) && !SubmitTo.ToLower().Contains(Vendor.ToLower()))
                                {
                                    continue;
                                }
                                var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                                if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                                {
                                    var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                    items = itemNumber;
                                    OppKeystring = opp.Key + "-" + itemNumber;
                                    RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                    string UDFBrandKey = GetUdfOppKey("Brand Name");
                                    string UDFProductKey = GetUdfOppKey("Product Name");
                                    string UDFModelKey = GetUdfOppKey("Model");
                                    string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                    string DeliveryAddressKey = GetUdfOppKey("Delivery Address");
                                    string VendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");

                                    var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                    var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                    var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                    var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                    var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                    var VendorOrderNumber = oUDFAccess.GetFieldValue(VendorOrderNumberKey, opp.Key);

                                    var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                                    if (VendorShort == "JBHIFI")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                    }
                                    else if (VendorShort == "GG")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                    }
                                    var selectItemsPrice = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Item Price Tick Marked " + VendorShort + itemNumber + ""), oCsCase.Key);
                                    //itemNumber
                                    //Item Price Tick Marked GG1
                                    if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                    {
                                        if (string.IsNullOrEmpty(Description))
                                        {
                                            hide = "";
                                        }
                                        Description += "<span class='more' " + hide + ">";
                                        Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress + "</br><b>Vendor Order Number: </b>" + VendorOrderNumber;
                                        Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                        Description += "</span></br>";

                                        string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                        string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                        string UDFInstKey = GetUdfOppKey("+Inst");
                                        string UDFRemKey = GetUdfOppKey("+Rem");

                                        var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                        var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                        var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                        var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);

                                        Price += "<span class='morePrice' " + hide + ">";
                                        Price += "<b>Product: </b>" + PU + "</br>";
                                        if (!string.IsNullOrEmpty(selectItemsPrice))
                                        {
                                            //if(selectItemsPrice.Contains("Product Price"))
                                            //    Price += "<b>Product: </b>" + PU + "</br>";

                                            if (selectItemsPrice.Contains("Delivery Price"))
                                                Price += "<b> +Del: </b>" + Del + "</br>";

                                            if (selectItemsPrice.Contains("Install Price"))
                                                Price += "<b>+Inst: </b>" + Inst + "</br>";

                                            if (selectItemsPrice.Contains("Removal Price"))
                                                Price += "<b>+Rem: </b>" + Rem;


                                        }

                                        Price += "</span>";
                                    }

                                    var suffix = "&Vendor=" + VendorType + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CO";
                                    var URL = "/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;


                                    string EnquiryStatus = oCsCase.Status.Value.ToString();
                                    //string Description = opp.Comment.Value;
                                    //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                    //    Description = Description.Substring(0, 50) + ".....";

                                    //if (OppList.Count > 1)
                                    //{
                                    //    Description += more;
                                    //    Price += morePrice;
                                    //}
                                    dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oUDFAccess.GetFieldValue(UDFCreatedDateKey, opp.Key)).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);
                                }
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                throw;
            }
            if (dt.Rows.Count <= 0)
            {
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            return dt;
        }

        [WebMethod]
        public static string GetGridDataForFilterType(string ActiveTab, string sLoginString, string VendorType, string RequestType, string FilterType, string SearchString)
        {
            try
            {
                DataTable dt = new DataTable();
                if (ActiveTab.ToUpper().Trim() == "CURRENT ENQUIRIES")
                {
                    dt = BindCurrentEnquiryGrid(sLoginString, VendorType, RequestType, 0, 0, "", "", FilterType, SearchString);
                }
                else if (ActiveTab.ToUpper().Trim() == "COMPLETED ENQUIRIES")
                {
                    dt = BindCompletedEnquiryGrid(sLoginString, VendorType, RequestType, 0, 0, "", "", FilterType, SearchString);
                }
                else if (ActiveTab.ToUpper().Trim() == "CURRENT ORDERS")
                {
                    if (FilterType == "Vendor Order Number")
                    {
                        dt = FilterOrdersDataForCurrentOrder(sLoginString, VendorType, RequestType, 0, 0, "", "", FilterType, SearchString);
                    }
                    else
                    {
                        dt = BindCurrentOrderGrid(sLoginString, VendorType, RequestType, 0, 0, "", "", FilterType, SearchString);
                    }
                }
                else if (ActiveTab.ToUpper().Trim() == "COMPLETED ORDERS")
                {
                    if (FilterType == "Vendor Order Number")
                    {
                        dt = FilterOrdersDataForCompletedOrder(sLoginString, VendorType, RequestType, 0, 0, "", "", FilterType, SearchString);
                    }
                    else
                    {
                        dt = BindCompletedOrderGrid(sLoginString, VendorType, RequestType, 0, 0, "", "", FilterType, SearchString);
                    }
                }

                DataView dv = dt.DefaultView;
                dv.Sort = "CaseNo desc";
                dt = dv.ToTable();

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);

                string dsXml = ds.GetXml();
                return dsXml;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public static DataTable FilterOrdersDataForCompletedOrder(string sLoginString, string VendorType, string RequestType, int PageIndex, int SkipRecords, string fromDate = "", string toDate = "", string filterType = "", string SearchString = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string VendorShort = "";
                var vendorItemNumber = "";
                string Vendor = VendorType;
                if (Vendor == "GG")
                {
                    VendorShort = "GG";
                    vendorItemNumber = "2";
                    Vendor = "The Good Guys";
                }
                else
                {
                    VendorShort = "JBHIFI";
                    vendorItemNumber = "1";
                    Vendor = "JBHiFI";
                }

                if (!string.IsNullOrEmpty(Vendor))
                {
                    sLoginString = loginToMax(sLoginString);

                    dt.Columns.Add("CaseNo");
                    dt.Columns.Add("DateFormat");
                    dt.Columns.Add("Time");
                    dt.Columns.Add("Assigned");
                    dt.Columns.Add("Status");
                    dt.Columns.Add("Desc");
                    dt.Columns.Add("Description1");
                    dt.Columns.Add("EnquiryStatus");
                    dt.Columns.Add("Revenue");
                    dt.Columns.Add("URL");

                    string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                    string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                    string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                    string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                    string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                    string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                    string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                    string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                    string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                    string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                    string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");

                    var Complete = GetCSCaseStausKey("Complete");
                    var Cancelled = GetCSCaseStausKey("Cancelled");
                    var Pending = GetCSCaseStausKey("Pending");//57999


                    string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");
                    string UDFSubmitToKey = GetUdfOppKey("Submit To");
                    string UDFVendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");

                    double RevenuValue = 0;

                    //Status=3 means Won
                    string searchVendorOrderNumber = String.Format("LIKE(UDF," + UDFVendorOrderNumberKey + ", \"{0}%\")", SearchString);
                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(OR(EQ(Status,3),EQ(Status,5))," + searchVendorOrderNumber + ")");

                    var items = "";
                    var OppKeystring = "";
                    string status = "COMPLETED";
                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        var CaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);

                        string CSsearchkey = "AND(EQ(CaseNumber, " + CaseNumber + "),OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + Cancelled + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";

                        CSCaseList oCSCaseList = oCSCaseAccess.ReadList(CSsearchkey);
                        string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                        string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                        foreach (CSCase oCsCase in oCSCaseList)
                        {
                            var hide = "";
                            var more = "<a href='#' class='aMore'>...more</a>";
                            var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                            if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(Cancelled))
                            {
                                int j = 0;
                                int i = 1;


                                if (i != 1)
                                {
                                    hide = "style='display:none;'";
                                }


                                i++;



                                string userName = "";
                                if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                                {
                                    UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                                    foreach (User oUser in oUserList)
                                    {
                                        userName = oUser.DisplayName;
                                    }
                                }


                                if (opp.Status.Value == 5)
                                {
                                    status = "CANCELLED";
                                }
                                string Description = "";
                                string Price = "";
                                var SubmitTo = oUDFAccess.GetFieldValue(UDFSubmitToKey, opp.Key);
                                if (!string.IsNullOrEmpty(SubmitTo) && !SubmitTo.ToLower().Contains(Vendor.ToLower()))
                                {
                                    continue;
                                }
                                var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                                if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                                {
                                    var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                    items = itemNumber;
                                    OppKeystring = opp.Key + "-" + itemNumber;
                                    RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                    string UDFBrandKey = GetUdfOppKey("Brand Name");
                                    string UDFProductKey = GetUdfOppKey("Product Name");
                                    string UDFModelKey = GetUdfOppKey("Model");
                                    string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                    string DeliveryAddressKey = GetUdfOppKey("Delivery Address");
                                    string VendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");

                                    var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                    var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                    var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                    var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                    var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                    var VendorOrderNumber = oUDFAccess.GetFieldValue(VendorOrderNumberKey, opp.Key);

                                    var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);
                                    if (VendorShort == "JBHIFI")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                    }
                                    else if (VendorShort == "GG")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                    }
                                    var selectItemsPrice = oUDFAccess.GetFieldValue(GetUdfCSCaseKey("Item Price Tick Marked " + VendorShort + itemNumber + ""), oCsCase.Key);
                                    //itemNumber
                                    //Item Price Tick Marked GG1
                                    if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                    {
                                        if (string.IsNullOrEmpty(Description))
                                        {
                                            hide = "";
                                        }
                                        Description += "<span class='more' " + hide + ">";
                                        Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress + "</br><b>Vendor Order Number: </b>" + VendorOrderNumber;
                                        Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                        Description += "</span></br>";

                                        string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                        string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                        string UDFInstKey = GetUdfOppKey("+Inst");
                                        string UDFRemKey = GetUdfOppKey("+Rem");

                                        var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                        var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                        var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                        var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);

                                        Price += "<span class='morePrice' " + hide + ">";
                                        Price += "<b>Product: </b>" + PU + "</br>";
                                        if (!string.IsNullOrEmpty(selectItemsPrice))
                                        {
                                            //if(selectItemsPrice.Contains("Product Price"))
                                            //    Price += "<b>Product: </b>" + PU + "</br>";

                                            if (selectItemsPrice.Contains("Delivery Price"))
                                                Price += "<b> +Del: </b>" + Del + "</br>";

                                            if (selectItemsPrice.Contains("Install Price"))
                                                Price += "<b>+Inst: </b>" + Inst + "</br>";

                                            if (selectItemsPrice.Contains("Removal Price"))
                                                Price += "<b>+Rem: </b>" + Rem;


                                        }

                                        Price += "</span>";
                                    }

                                    var suffix = "&Vendor=" + VendorType + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CO";
                                    var URL = "CustomDialogs/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;


                                    string EnquiryStatus = oCsCase.Status.Value.ToString();
                                    //string Description = opp.Comment.Value;
                                    //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                    //    Description = Description.Substring(0, 50) + ".....";

                                    //if (OppList.Count > 1)
                                    //{
                                    //    Description += more;
                                    //    Price += morePrice;
                                    //}
                                    string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                                    dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oUDFAccess.GetFieldValue(UDFCreatedDateKey, opp.Key)).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);
                                }
                            }
                        }


                    }

                }

            }
            catch (Exception ex)
            {

                throw;
            }
            if (dt.Rows.Count <= 0)
            {
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            return dt;
        }
        public static DataTable FilterOrdersDataForCurrentOrder(string sLoginString, string VendorType, string RequestType, int PageIndex, int SkipRecords, string fromDate = "", string toDate = "", string filterType = "", string SearchString = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string VendorShort = "";
                var vendorItemNumber = "";
                string Vendor = VendorType;
                if (Vendor == "GG")
                {
                    VendorShort = "GG";
                    vendorItemNumber = "2";
                    Vendor = "The Good Guys";
                }
                else
                {
                    VendorShort = "JBHIFI";
                    vendorItemNumber = "1";
                    Vendor = "JBHiFI";
                }


                if (!string.IsNullOrEmpty(Vendor))
                {
                    sLoginString = loginToMax(sLoginString);

                    dt.Columns.Add("CaseNo");
                    dt.Columns.Add("DateFormat");
                    dt.Columns.Add("Time");
                    dt.Columns.Add("Assigned");
                    dt.Columns.Add("Status");
                    dt.Columns.Add("Desc");
                    dt.Columns.Add("Description1");
                    dt.Columns.Add("EnquiryStatus");
                    dt.Columns.Add("Revenue");
                    dt.Columns.Add("URL");

                    string UDFSubmitTo1Key = GetUdfCSCaseKey("Submit To1");
                    string UDFSubmitTo2Key = GetUdfCSCaseKey("Submit To2");
                    string UDFSubmitTo3Key = GetUdfCSCaseKey("Submit To3");
                    string UDFSubmitTo4Key = GetUdfCSCaseKey("Submit To4");
                    string UDFSubmitTo5Key = GetUdfCSCaseKey("Submit To5");
                    string UDFSubmitTo6Key = GetUdfCSCaseKey("Submit To6");
                    string UDFSubmitTo7Key = GetUdfCSCaseKey("Submit To7");
                    string UDFSubmitTo8Key = GetUdfCSCaseKey("Submit To8");
                    string UDFSubmitTo9Key = GetUdfCSCaseKey("Submit To9");
                    string UDFSubmitTo10Key = GetUdfCSCaseKey("Submit To10");
                    string UDFEnquiryComments = GetUdfCSCaseKey("Enquiry Comments");

                    var Pending = GetCSCaseStausKey("Pending");
                    var Complete = GetCSCaseStausKey("Complete");
                    var CallBack = GetCSCaseStausKey("Call Back");

                    string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");

                    double RevenuValue = 0;

                    string UDFVendorOrderNumberKey = GetUdfOppKey("Vendor Order Number");

                    //Status=3 means Won
                    string searchVendorOrderNumber = String.Format("LIKE(UDF," + UDFVendorOrderNumberKey + ", \"{0}%\")", SearchString);
                    //0Status for New and 1 for Updated
                    Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("AND(" + searchVendorOrderNumber + ",OR(EQ(Status,0),EQ(Status,1),EQ(Status,2)),EQ(UDF," + UDFCaseNumberKey + "))");
                    var items = "";
                    var OppKeystring = "";
                    string status = "PENDING";


                    foreach (Maximizer.Data.Opportunity opp in OppList)
                    {
                        var CaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);

                        string CSsearchkey = "AND( EQ(CaseNumber, " + CaseNumber + "),OR(EQ(Status," + Pending + "),EQ(Status," + Complete + "),EQ(Status," + CallBack + ")), OR(EQ(UDF," + UDFSubmitTo1Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo2Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo3Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo4Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo5Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo6Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo7Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo8Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo9Key + ",'" + vendorItemNumber + "' ),EQ(UDF," + UDFSubmitTo10Key + ",'" + vendorItemNumber + "' )))";
                        CSCaseList oCSCaseList = oCSCaseAccess.ReadList(CSsearchkey);
                        string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                        string UDFOrderedFromKey = GetUdfOppKey("Ordered From");
                        foreach (CSCase oCsCase in oCSCaseList)
                        {
                            var cancel = GetCSCaseStausKey("Cancelled");
                            if (oCsCase.Status.Value == cancel)//4
                                continue;


                            if (oCsCase.Status.Value.ToString() == Convert.ToString(Pending) || oCsCase.Status.Value.ToString() == Convert.ToString(Complete) || oCsCase.Status.Value.ToString() == Convert.ToString(CallBack))
                            {

                                var EnquiryComments = oUDFAccess.GetFieldValue(UDFEnquiryComments, oCsCase.Key);

                                var hide = "";
                                var more = "<a href='#' class='aMore'>...more</a>";
                                var morePrice = "<a href='#' class='aMorePrice'>...more</a>";
                                int j = 0;
                                int i = 1;


                                if (i != 1)
                                {
                                    hide = "style='display:none;'";
                                }


                                i++;

                                string userName = "";
                                if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                                {
                                    UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                                    foreach (User oUser in oUserList)
                                    {
                                        userName = oUser.DisplayName;
                                    }
                                }


                                //
                                //if(oCsCase.CaseNumber.Value== "US-01581")
                                // {

                                // }
                                if (opp.Status.Value == 1 || opp.Status.Value == 2)
                                    status = "UPDATED";
                                else
                                    status = "PENDING";

                                string Description = "";
                                string Price = "";
                                var OrderedFrom = oUDFAccess.GetFieldValue(UDFOrderedFromKey, opp.Key);
                                if (!string.IsNullOrEmpty(OrderedFrom) && OrderedFrom.ToLower().Contains(Vendor.ToLower()))
                                {
                                    var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                                    items = itemNumber;
                                    OppKeystring = opp.Key + "-" + itemNumber;
                                    RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);

                                    string UDFBrandKey = GetUdfOppKey("Brand Name");
                                    string UDFProductKey = GetUdfOppKey("Product Name");
                                    string UDFModelKey = GetUdfOppKey("Model");
                                    string UDFStateCodeKey = GetUdfOppKey("Suburb and Postcode");
                                    string DeliveryAddressKey = GetUdfOppKey("Delivery Address");

                                    var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                                    var Product = oUDFAccess.GetFieldValue(UDFProductKey, opp.Key);
                                    var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                                    var State = oUDFAccess.GetFieldValue(UDFStateCodeKey, opp.Key);
                                    var DeliveryAddress = oUDFAccess.GetFieldValue(DeliveryAddressKey, opp.Key);
                                    if (VendorShort == "JBHIFI")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("JBHIFI Comment"), opp.Key);
                                    }
                                    else if (VendorShort == "GG")
                                    {
                                        EnquiryComments = oUDFAccess.GetFieldValue(GetUdfOppKey("TheGoodGuys Comment"), opp.Key);
                                    }
                                    if (!string.IsNullOrEmpty(Brand) && !string.IsNullOrEmpty(Product) && !string.IsNullOrEmpty(Model))
                                    {
                                        j++;
                                        if (string.IsNullOrEmpty(Description))
                                        {
                                            hide = "";
                                        }
                                        Description += "<span class='more' " + hide + ">";
                                        Description += "<b>Brand: </b>" + Brand + "</br><b>Product: </b>" + Product + "</br><b>Model: </b>" + Model + "</br><b>Suburb and Postcode: </b>" + State + "</br><b>Delivery Address: </b>" + DeliveryAddress;
                                        Description += "</br><b>" + Vendor + " Comments: </b>" + EnquiryComments;
                                        Description += "</span></br>";

                                        string UDFPUKey = GetUdfOppKey("Product Price Pick Up");
                                        string UDFDelKey = GetUdfOppKey("Product Price Delivery");
                                        string UDFInstKey = GetUdfOppKey("+Inst");
                                        string UDFRemKey = GetUdfOppKey("+Rem");

                                        var PU = oUDFAccess.GetFieldValue(UDFPUKey, opp.Key);
                                        var Del = oUDFAccess.GetFieldValue(UDFDelKey, opp.Key);
                                        var Inst = oUDFAccess.GetFieldValue(UDFInstKey, opp.Key);
                                        var Rem = oUDFAccess.GetFieldValue(UDFRemKey, opp.Key);
                                        Price += "<span class='morePrice' " + hide + ">";
                                        Price += "<b>Product: </b>" + PU + "</br><b>+Del: </b>" + Del + "</br><b>+Inst: </b>" + Inst + "</br><b>+Rem: </b>" + Rem;
                                        Price += "</span></br>";
                                    }

                                    var suffix = "&Vendor=" + VendorShort + "&CaseNo=" + oCsCase.CaseNumber.Value + "&RequestType=Vendor&Items=" + items + "&OppKeys=" + OppKeystring + "&ET=CURRORD";
                                    var URL = "CustomDialogs/CreateCSCase.aspx?CurrentKey=" + oCsCase.ParentKey + "&Loginstring=" + sLoginString + suffix;


                                    string EnquiryStatus = oCsCase.Status.Value.ToString();
                                    //string Description = opp.Comment.Value;
                                    //if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                                    //    Description = Description.Substring(0, 50) + ".....";

                                    //if (OppList.Count>1)
                                    //{
                                    //    Description += more;
                                    //    Price += morePrice;
                                    //}
                                    string UDFCreatedDateKey = GetUdfOppKey("Created Date");
                                    dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oUDFAccess.GetFieldValue(UDFCreatedDateKey, opp.Key)).ToString("dd-MM-yyyy"), Convert.ToDateTime(opp.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, opp.Comment.Value, EnquiryStatus, Price, URL);

                                }





                            }

                        }
                    }







                }
                else
                {

                }

            }
            catch (Exception ex)
            {

                throw;
            }
            if (dt.Rows.Count <= 0)
            {
                dt.Rows.Add("", "", "", "", "", "", "", "", "", "");
            }
            return dt;
        }
    }
}