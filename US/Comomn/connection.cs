﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace US.Comomn
{
    public class connection
    {
        private static string getConnectionString()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectionStrig"].ConnectionString;
            return connectionString;
        }

        public SqlConnection getConnection()
        {
            SqlConnection Connsql = null;
            try
            {
                Connsql = new SqlConnection(getConnectionString());
                Connsql.Open();
                return (Connsql);
            }
            catch (SqlException ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }
        }

        public SqlCommand getCommandSp()
        {
            SqlCommand connCommand = null;
            try
            {
                connCommand = new SqlCommand();
                connCommand.CommandTimeout = 0;
                connCommand.CommandType = CommandType.StoredProcedure;
                return (connCommand);
            }
            catch (SqlException ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }
        }

        public SqlCommand getCommandSp(string spname)
        {
            SqlCommand connCommand = new SqlCommand();
            try
            {
                connCommand = new SqlCommand();
                connCommand.CommandTimeout = 0;
                connCommand.CommandType = CommandType.StoredProcedure;
                connCommand.CommandText = spname;
                return (connCommand);
            }
            catch (SqlException ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }
        }

        public DataSet fillds(string qry)
        {
            SqlConnection conSql = null;
            SqlCommand comd = new SqlCommand();
            try
            {
                conSql = getConnection();
                comd.Connection = conSql;
                comd.CommandText = qry;
                SqlDataAdapter conAdpt = new SqlDataAdapter();
                conAdpt.SelectCommand = comd;
                DataSet ds = new DataSet();
                conAdpt.Fill(ds);
                return (ds);

            }
            catch (SqlException ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }
            finally
            {
                comd.Dispose();
                conSql.Close();
                conSql.Dispose();
            }
        }

        public DataTable filldt(string qry)
        {
            SqlConnection conSql = null;
            SqlCommand comd = new SqlCommand();
            try
            {
                conSql = getConnection();
                comd.Connection = conSql;
                comd.CommandText = qry;
                comd.CommandTimeout = 0;
                SqlDataAdapter conAdpt = new SqlDataAdapter();
                conAdpt.SelectCommand = comd;

                DataTable dt = new DataTable();
                conAdpt.Fill(dt);
                return (dt);

            }
            catch (SqlException ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }
            finally
            {
                comd.Dispose();
                conSql.Close();
                conSql.Dispose();
            }
        }

        public int IUD(string qry)
        {
            SqlCommand ConnCommand = new SqlCommand();
            SqlConnection ConnSql = getConnection();
            SqlTransaction ConnTransaction = ConnSql.BeginTransaction();
            int NoofRowAfected = 0;
            try
            {
                ConnCommand.Connection = ConnSql;
                ConnCommand.Transaction = ConnTransaction;
                ConnCommand.CommandText = qry;
                NoofRowAfected = ConnCommand.ExecuteNonQuery();
                ConnTransaction.Commit();
                return (NoofRowAfected);
            }
            catch (SqlException ex)
            {
                ConnTransaction.Rollback();
                throw new ApplicationException(ex.Message.ToString());
            }
            finally
            {
                ConnCommand.Dispose();
                ConnSql.Close();
                ConnSql.Dispose();
            }
        }

        public int IsExist(SqlCommand ConnCommand)
        {
            SqlConnection conSql = null;
            int Exist = 0;
            try
            {
                conSql = getConnection();
                ConnCommand.Connection = conSql;
                Exist = Convert.ToInt32(ConnCommand.ExecuteScalar());
                return (Exist);

            }
            catch (SqlException ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }
            finally
            {
                ConnCommand.Dispose();
                conSql.Close();
                conSql.Dispose();
            }
        }

        public SqlDataReader GetReader(SqlCommand ConnCommand)
        {
            try
            {
                ConnCommand.Connection = getConnection();
                SqlDataReader ConnReader = ConnCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return (ConnReader);

            }
            catch (SqlException ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }
        }
    }
}