﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using US.Comomn;

namespace US
{
    public partial class GenerateXML : System.Web.UI.Page
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        string sLoginString = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                sLoginString = loginToMax();
                GenerateUDFXML();

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public void GenerateUDFXML()
        {
            try
            {
                string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");

                using (XmlWriter writer = XmlWriter.Create(path))
                {
                    writer.WriteStartElement("CsCase");

                    var arr = new[] { "Item Price Tick Marked JBHIFI", "Item Price Tick Marked GG" };
                    foreach (var item in arr)
                    {
                        for (int i = 1; i <= 10; i++)
                        {
                            string UDFKey = GetUdfCSCaseKey(item + i);
                            writer.WriteStartElement("UDF");
                            writer.WriteElementString("UDFName", item + i);
                            writer.WriteElementString("UDFKey", UDFKey);
                            writer.WriteEndElement();
                        }
                    }

                    //var arrWithItemsPrice = new[] { "GG Price Pick Up", "JBHIFI Price Pick Up", "GG Price Delivery", "JBHIFI Price Delivery", "GG Price Install", "JBHIFI Price Install", "GG Price Removal", "JBHIFI Price Removal", "Warranty", "GG Comment", "JBHIFI Comment", "GG Warranty Desc", "JBHIFI Warranty Desc", "GG Price Warranty", "JBHIFI Price Warranty", "Ordered From" };
                    //foreach (var item in arrWithItemsPrice)
                    //{
                    //    for (int i = 1; i <= 10; i++)
                    //    {
                    //        string UDFKey = GetUdfCSCaseKey("P" + i + " " + item);
                    //        writer.WriteStartElement("UDF");
                    //        writer.WriteElementString("UDFName", "P" + i + " " + item);
                    //        writer.WriteElementString("UDFKey", UDFKey);
                    //        writer.WriteEndElement();
                    //    }
                    //}

                    //var arrWithNormalUDF = new[] { "Enquiry Updated", "Order Updated", "Enquiry Comments", "Mode", "Updated by TheGoodGuys", "Updated by JBHIFI", "Submit To", "Vendor1", "Vendor2", "Vendor3", "Vendor4", "Vendor5", "Vendor6", "Vendor7", "Vendor8", "Vendor9", "Vendor10" };
                    //foreach (var item in arrWithNormalUDF)
                    //{
                    //    string UDFKey = GetUdfCSCaseKey(item);
                    //    writer.WriteStartElement("UDF");
                    //    writer.WriteElementString("UDFName", item);
                    //    writer.WriteElementString("UDFKey", UDFKey);
                    //    writer.WriteEndElement();

                    //}

                    writer.WriteEndElement();
                    writer.Flush();
                    writer.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            //try
            //{
            //    string path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");

            //    using (XmlWriter writer = XmlWriter.Create(path))
            //    {
            //        writer.WriteStartElement("ab");

            //        var arrWithNormalUDF = new[] { "Member ID", "Union ID", "VC Contact", "VC Name", "VC Relationship" };
            //        foreach (var item in arrWithNormalUDF)
            //        {
            //            string UDFKey = GetUdfKey(item);
            //            writer.WriteStartElement("UDF");
            //            writer.WriteElementString("UDFName", item);
            //            writer.WriteElementString("UDFKey", UDFKey);
            //            writer.WriteEndElement();
            //        }
            //        writer.WriteEndElement();
            //        writer.Flush();
            //        writer.Close();
            //    }
            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}

            //try
            //{
            //    string path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");

            //    using (XmlWriter writer = XmlWriter.Create(path))
            //    {
            //        writer.WriteStartElement("Opp");

            //        var arrWithNormalUDF = new[] { "Order Reason", "Vendor Order Number", "Suburb and Postcode", "+Inst", "+Rem", "ItemNumber", "CaseNumber", "Submit To", "CaseNumber", "Brand Name", "Product Name", "Model", "Pay By", "Warranty Cost", "Member Price Desc", "Description", "Delivery Address", "Ordered From", "Union ID", "Options", "Product Comment", "Product Price Delivery", "Product Price Pick Up" };
            //        foreach (var item in arrWithNormalUDF)
            //        {
            //            string UDFKey = GetUdfOppKey(item);
            //            writer.WriteStartElement("UDF");
            //            writer.WriteElementString("UDFName", item);
            //            writer.WriteElementString("UDFKey", UDFKey);
            //            writer.WriteEndElement();

            //        }

            //        writer.WriteEndElement();
            //        writer.Flush();
            //        writer.Close();
            //    }
            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}

        }
        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                {
                    string appSetting = ConfigurationManager.AppSettings["Database"];
                    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                    {
                        sAddressBookKey = addressBook.Key;
                        break;
                    }
                }
                string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                string appSetting2 = ConfigurationManager.AppSettings["Password"];
                this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
                //this.sLoginString = ConfigurationManager.AppSettings["VENJBHIFILoginKey"];
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }
        public string GetUdfKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))

                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
        public string GetUdfOppKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
    }
}