﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using US.Comomn;

namespace US
{
    public partial class MemberEnquiry : System.Web.UI.Page
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        string sLoginString = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            string Vendor = Request.QueryString["Vendor"];
            string RequestType = Request.QueryString["RequestType"];
            if (string.IsNullOrEmpty(Vendor) && RequestType == "CallCenter")
            {
                this.sLoginString = this.loginToMax();
                if (this.IsPostBack)
                   return;
                DataTable dt = new DataTable();
                dt.Columns.Add("CaseNo");
                dt.Columns.Add("DateFormat");
                dt.Columns.Add("Time");
                dt.Columns.Add("Assigned");
                dt.Columns.Add("Status");
                dt.Columns.Add("Desc");
                dt.Columns.Add("Description1");
                dt.Columns.Add("EnquiryStatus");

                CSCaseList oCSCaseList = oCSCaseAccess.ReadList("KEY(" + this.Request.QueryString["CurrentKey"] + ")");
                foreach (CSCase oCsCase in oCSCaseList)
                {
                    string userName = "";
                    if (!string.IsNullOrEmpty(oCsCase.AssignedTo.Value))
                    {
                        UserList oUserList = oUserAccess.ReadList("Key(" + oCsCase.AssignedTo.Value + ")");
                        foreach (User oUser in oUserList)
                        {
                            userName = oUser.DisplayName;
                        }
                    }
                    string GGStatus = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by TheGoodGuys"), oCsCase.Key);
                    string JBHIFIStatus = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Updated by JBHIFI"), oCsCase.Key);
                    string status = "Not Updated";
                    if (GGStatus == "Yes" && JBHIFIStatus == "Yes")
                    {
                        status = "Updated By GG and JBHIFI";
                    }
                    else if (GGStatus == "Yes")
                    {
                        status = "Updated By GG";
                    }
                    else if (JBHIFIStatus == "Yes")
                    {
                        status = "Updated ByJBHIFI";
                    }

                    string EnquiryStatus = oCsCase.Status.Value.ToString();
                    string Description = oCsCase.Description.Value;
                    if (!string.IsNullOrEmpty(Description) && Description.Length >= 70)
                        Description = Description.Substring(0, 50) + ".....";

                    dt.Rows.Add(oCsCase.CaseNumber.Value, Convert.ToDateTime(oCsCase.CreationDate.DateValue).ToString("dd-MM-yyyy"), Convert.ToDateTime(oCsCase.CreationDate.TimeValue).ToString("hh:mm tt"), oCsCase.ParentName.Value, status, Description, oCsCase.Description.Value, EnquiryStatus);
                }
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {

            }
        }

        private int FillDropDownWithNormalUDF(AddressBookMaster maxAb, string maxLoginstr, string productname, DropDownList ddlTarget, string SystemUDFID)
        {
            try
            {
                foreach (UdfSetupTable readAbEntryUdfSetup in (CollectionBase)maxAb.CreateUdfAccess(maxLoginstr).ReadAbEntryUdfSetupList("KEY(" + SystemUDFID + ")", true))
                {
                    foreach (UdfSetupTableItem udfSetupTableItem in (CollectionBase)readAbEntryUdfSetup.Items)
                        ddlTarget.Items.Add(new ListItem()
                        {
                            Text = udfSetupTableItem.Name.Value,
                            Value = udfSetupTableItem.Key
                        });
                }
                return -1;
            }
            catch (Exception ex)
            {
                this.Response.Write(ex.Message);
                return -1;
            }
        }

        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                {
                    string appSetting = ConfigurationManager.AppSettings["Database"];
                    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                    {
                        sAddressBookKey = addressBook.Key;
                        break;
                    }
                }
                string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                string appSetting2 = ConfigurationManager.AppSettings["Password"];
                this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }

        public string GetUdfKey(string strUdfName, string strLoginString, AddressBookMaster abMaster)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)abMaster.CreateUdfAccess(strLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string caseNo = e.CommandArgument.ToString();
                    //CSCaseList ocSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + caseNo + ")");

                    Response.Redirect("CreateCSCase.aspx?CurrentKey=" + Request.QueryString["CurrentKey"] + "&Loginstring=" + Request.QueryString["Loginstring"] + "&CaseNo=" + caseNo + "&RequestType=CallCenter");
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlEnquiryStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);
            DropDownList ddlEnquiryStatus = (DropDownList)gvr.FindControl("ddlEnquiryStatus");
            int value = Convert.ToInt32(ddlEnquiryStatus.SelectedValue);
            string strStatusName = ddlEnquiryStatus.SelectedItem.Text;

           
            if (ddlEnquiryStatus.SelectedValue == "57996")
            {

                
                ddlEnquiryStatus.BackColor = System.Drawing.Color.LightGreen;
              
            }
            else if (ddlEnquiryStatus.SelectedValue == "57994")
            {
                
                ddlEnquiryStatus.BackColor = System.Drawing.Color.LightPink;
            }
            else if (ddlEnquiryStatus.SelectedValue == "57993")
            {
               
                ddlEnquiryStatus.BackColor = System.Drawing.Color.LightYellow;
            }
            else
            {
                
                ddlEnquiryStatus.BackColor = System.Drawing.Color.White;


               
            }





            Label lblcaseNo = (Label)gvr.FindControl("lblCaseNo");
            string hdnfid = Convert.ToString(lblcaseNo.Text);


            CSCase oCSCase = new CSCase();
            CSCaseList oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + hdnfid + ")");

            foreach (CSCase oCSCaseItems in oCSCaseList)
            {
                oCSCase = oCSCaseItems;
                oCSCase.Status.Value = value;

                oCSCaseAccess.Update(oCSCase);



            }



        }

        




        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlEnquiryStatus = (DropDownList)e.Row.FindControl("ddlEnquiryStatus");
                Label hdnStatus = (Label)e.Row.FindControl("hdnStatusid");


                System.Web.UI.WebControls.DropDownList ddl = (System.Web.UI.WebControls.DropDownList)e.Row.FindControl("ddlEnquiryStatus");

                if (hdnStatus != null)
                {
                    ddlEnquiryStatus.SelectedValue = hdnStatus.Text;
                }
                if (ddlEnquiryStatus.SelectedValue == "57996")
                {

                    e.Row.BackColor = System.Drawing.Color.LightGreen;
                    ddlEnquiryStatus.BackColor = System.Drawing.Color.LightGreen;
                  
                }
                else if (ddlEnquiryStatus.SelectedValue == "57994")
                {
                     e.Row.BackColor = System.Drawing.Color.LightPink;
                      ddlEnquiryStatus.BackColor = System.Drawing.Color.LightPink;
                }
                else if (ddlEnquiryStatus.SelectedValue == "57993")
                {
                    e.Row.BackColor = System.Drawing.Color.LightYellow;
                    ddlEnquiryStatus.BackColor = System.Drawing.Color.LightYellow;
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.White;
                    ddlEnquiryStatus.BackColor = System.Drawing.Color.White;
                     

                   
                }




            }
        }



        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            Control ddlCtrlEnquiryStatus = e.Row.Cells[0].FindControl("ddlEnquiryStatus");
            if (ddlCtrlEnquiryStatus != null)
            {
                ScriptManager1.RegisterAsyncPostBackControl(ddlCtrlEnquiryStatus);
            }
        }
    }
}