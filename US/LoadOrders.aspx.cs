﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using US.Comomn;

namespace US
{
    public partial class LoadOrders : System.Web.UI.Page
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        connection connection = new connection();
        string sLoginString = "";
        string CurrentKey = "";

        XDocument xAB = null; XDocument xCsCase = null; XDocument xOpportunity = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/UDFKeyFile/CsCase.xml");
            xCsCase = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            xOpportunity = XDocument.Load(path);
            path = Server.MapPath("~/UDFKeyFile/AddressBook.xml");
            xAB = XDocument.Load(path);

            this.sLoginString = this.loginToMax();
            if (this.IsPostBack)
                return;
            if (!string.IsNullOrEmpty(Request.QueryString["CaseNumber"]))
            {
                lblCaseNumber.Text = Request.QueryString["CaseNumber"];
                Bind(Request.QueryString["CaseNumber"]);
            }
            if (!string.IsNullOrEmpty(CurrentKey))
            {
                this.oAbEntryList = this.oAbEntryAccess.ReadList("Key(" + CurrentKey + ")");
                foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                {
                    hfClientId.Value = oAbEntry.ClientId.Value;
                    this.lbluser.Text = oAbEntry.FirstName.Value + " " + oAbEntry.LastName.Value;
                    this.lblAddress.Text = oAbEntry.CurrentAddress.AddressLine1.Value + " " + oAbEntry.CurrentAddress.City.Value + " " + oAbEntry.CurrentAddress.StateProvince.Value + " " + oAbEntry.CurrentAddress.ZipCode.Value;
                    this.lblHomeNumber.Text = oAbEntry.Phone1.Value;
                    this.lblEmailAddress.Text = oAbEntry.EmailAddress1.Value;
                    //this.lblBusNumber.Text = oAbEntry.Phone3.Value;
                    string fieldValue1 = this.oUDFAccess.GetFieldValue(GetUdfKey("Member ID"), oAbEntry.Key);
                    string fieldValue2 = this.oUDFAccess.GetFieldValue(GetUdfKey("Union ID"), oAbEntry.Key);
                    this.lblMember.Text = fieldValue2;
                    //this.lblUnion.Text = fieldValue2;
                }
            }
        }

        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                {
                    string appSetting = ConfigurationManager.AppSettings["Database"];
                    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                    {
                        sAddressBookKey = addressBook.Key;
                        break;
                    }
                }
                string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                string appSetting2 = ConfigurationManager.AppSettings["Password"];
                this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }

        public string GetUdfKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xAB.Element("ab").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfOppKey(string strUdfName)
        {
            //sLoginString = loginToMax();
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            //var path = Server.MapPath("~/UDFKeyFile/Opportunity.xml");
            //xOpportunity = XDocument.Load(path);
            var sFilterItems = xOpportunity.Element("Opp").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            //foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            //{
            //    if (readUdfDefinition.FieldName.Equals(strUdfName))
            //        return readUdfDefinition.Key;
            //}
            var sFilterItems = xCsCase.Element("CsCase").Elements("UDF").Where(E => E.Element("UDFName").Value == strUdfName);

            foreach (var item in sFilterItems)
            {
                // var aa = item.Element("UDFName").Value;
                return item.Element("UDFKey").Value;
            }
            return string.Empty;
        }

        public void Bind(string CaseNumber)
        {
            try
            {
                sLoginString = loginToMax();
                DataTable dt = new DataTable();
                dt.Columns.Add("Brand");
                dt.Columns.Add("Product");
                dt.Columns.Add("Model");
                dt.Columns.Add("Members Price");
                dt.Columns.Add("Ordered Price");
                dt.Columns.Add("Vendor");
                dt.Columns.Add("Item Status");
                dt.Columns.Add("Vendor Status");

                string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");

                double RevenuValue = 0;

                Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("EQ(UDF," + UDFCaseNumberKey + ",'" + CaseNumber + "')");
                string UDFItemNumberKey = GetUdfOppKey("ItemNumber");
                var items = "";
                var OppKeystring = "";
                string UDFBrandKey = GetUdfOppKey("Brand Name");
                string UDFModelKey = GetUdfOppKey("Model");
                string UDFProductNameKey = GetUdfOppKey("Product Name");
                string UDFOrderFromKey = GetUdfOppKey("Ordered From");
                string UDFMemberPriceDescKey = GetUdfOppKey("Member Price Desc");

                string CSCaseKey = "";
                string filter = "EQ(CaseNumber," + CaseNumber + ")";
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                foreach (CSCase oCSCase in oCSCaseList)
                {
                    CSCaseKey = oCSCase.Key;
                }
                foreach (Maximizer.Data.Opportunity opp in OppList)
                {
                    CurrentKey = opp.ParentKey;
                    var itemNumber = oUDFAccess.GetFieldValue(UDFItemNumberKey, opp.Key);
                    items = itemNumber + ",";
                    OppKeystring = opp.Key + "-" + itemNumber + ",";
                    RevenuValue = RevenuValue + Convert.ToDouble(opp.ForecastRevenue.Value);


                    string UDFOrderStatusKey = GetUdfCSCaseKey("Ordered Item Status " + itemNumber);
                    var OrderStatus = oUDFAccess.GetFieldValue(UDFOrderStatusKey, CSCaseKey);

                    var Brand = oUDFAccess.GetFieldValue(UDFBrandKey, opp.Key);
                    var Model = oUDFAccess.GetFieldValue(UDFModelKey, opp.Key);
                    var Product = oUDFAccess.GetFieldValue(UDFProductNameKey, opp.Key);
                    var OrderFrom = oUDFAccess.GetFieldValue(UDFOrderFromKey, opp.Key);
                    var UDFMemberPriceDesc = oUDFAccess.GetFieldValue(UDFMemberPriceDescKey, opp.Key);
                    if (string.IsNullOrEmpty(UDFMemberPriceDesc))
                    {
                        UDFMemberPriceDesc = "0";
                    }
                    var status = opp.StatusName.Value;
                    if (opp.StatusName.Value == "Won")
                    {
                        status = "Confirmed";
                    }
                    dt.Rows.Add(Brand, Product, Model, UDFMemberPriceDesc, RevenuValue, OrderFrom, OrderStatus, status);
                }
                gvLoadOrder.DataSource = dt;
                gvLoadOrder.DataBind();

                //here add code for column total sum and show in footer  
                if (dt.Rows.Count > 0)
                {
                    gvLoadOrder.FooterRow.Cells[2].Text = "Total";
                    gvLoadOrder.FooterRow.Cells[2].Font.Bold = true;
                    gvLoadOrder.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Left;

                    int sum = 0;
                    int sumMembersPrice = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        sum += Convert.ToInt32(dr["Ordered Price"]);
                        sumMembersPrice += Convert.ToInt32(dr["Members Price"]);
                    }

                    gvLoadOrder.FooterRow.Cells[3].Text = sumMembersPrice.ToString();
                    gvLoadOrder.FooterRow.Cells[3].Font.Bold = true;

                    gvLoadOrder.FooterRow.Cells[4].Text = sum.ToString();
                    gvLoadOrder.FooterRow.Cells[4].Font.Bold = true;
                    gvLoadOrder.FooterRow.BackColor = System.Drawing.Color.Beige;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hfClientId.Value))
            {
                string filter = "EQ(CaseNumber," + Request.QueryString["CaseNumber"] + ")";
                CSCaseList oCSCaseList = oCSCaseAccess.ReadList(filter);
                foreach (CSCase oCSCase in oCSCaseList)
                {
                    oAbEntryList = this.oAbEntryAccess.ReadList("Key(" + oCSCase.ParentKey + ")");
                    foreach (Individual oAbEntry in (CollectionBase)this.oAbEntryList)
                    {
                        hfClientId.Value = oAbEntry.ClientId.Value;
                    }
                    }
            }
            Response.Redirect("LoadEnquiries.aspx?ClientID=" + hfClientId.Value);
        }
    }
}