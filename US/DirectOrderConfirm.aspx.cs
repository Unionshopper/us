﻿using Maximizer.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace US
{
    public partial class DirectOrderConfirm : System.Web.UI.Page
    {
        AddressBookMaster oAb = new AddressBookMaster();
        AbEntryAccess oAbEntryAccess = (AbEntryAccess)null;
        GlobalAccess ga = new GlobalAccess();
        AbEntryList oAbEntryList = (AbEntryList)null;
        UdfAccess oUDFAccess = (UdfAccess)null;
        NoteAccess oNoteAccess = (NoteAccess)null;
        UserAccess oUserAccess = (UserAccess)null;
        TaskAccess oTaskAccess = (TaskAccess)null;
        OpportunityAccess oOppAccess = (OpportunityAccess)null;
        DetailFieldAccess oDetailFieldAccess = (DetailFieldAccess)null;
        SalesProcessAccess oSalesProcess = (SalesProcessAccess)null;
        CSCaseAccess oCSCaseAccess = (CSCaseAccess)null;
        string sLoginString = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string CaseNumber = Request.QueryString["CaseNo"];
                string Vendor = Request.QueryString["Vendor"];
                string CurrentKey = Request.QueryString["CurrentKey"];
                string Item = Request.QueryString["Item"];
                CSCase oCSCase = null;

                if (!string.IsNullOrEmpty(CaseNumber))
                {
                    sLoginString = loginToMax();
                    oCSCase = new CSCase();
                    CSCaseList oCSCaseList = oCSCaseAccess.ReadList("EQ(CaseNumber," + CaseNumber + ")");
                    foreach (CSCase oCSCaseItems in oCSCaseList)
                    {
                        oCSCase = oCSCaseItems;
                        if (Vendor == "GG")
                        {
                            Vendor = "The Good Guys";
                            //this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Order Confirmed By GG"), oCSCase.Key, "Yes");
                        }
                        else if (Vendor.ToLower() == "JBHiFI".ToLower())
                        {
                            Vendor = "JBHiFI";
                            //this.oUDFAccess.SetFieldValue(this.GetUdfCSCaseKey("Order Confirmed By JBHIFI"), oCSCase.Key, "Yes");
                        }
                        string UDFKey = GetUdfOppKey("Submit To");
                        string UDFCaseNumberKey = GetUdfOppKey("CaseNumber");

                        //string search = String.Format("LIKE(EQ(UDF," + UDFKey + ",'" + Vendor + "'))");
                        //string search = String.Format("LIKE(UDF," + UDFKey + ", \"%{0}\")", Vendor);
                        //string Cate1 = String.Format("EQ(UDF,\"{0}\", \"{1}\")", category_old, "26");
                        //string searchkey = "AND(KEY(" + CurrentKey + "), EQ(UDF," + UDFKey + "," + Vendor + "))";

                        Maximizer.Data.OpportunityList OppList = oOppAccess.ReadList("KEY(" + CurrentKey + ")");
                        Maximizer.Data.Opportunity oOPp = null;
                        foreach (Maximizer.Data.Opportunity opp in OppList)
                        {
                            var UDFValue = oUDFAccess.GetFieldValue(UDFKey, opp.Key);
                            var UDFCaseNumber = oUDFAccess.GetFieldValue(UDFCaseNumberKey, opp.Key);
                            if (!string.IsNullOrEmpty(UDFValue) && UDFCaseNumber == CaseNumber && UDFValue.Contains(Vendor))
                            {
                                opp.ActualRevenue.Value = Convert.ToDouble(opp.ForecastRevenue.Value);
                                opp.ForecastRevenue.Value = Convert.ToDouble(opp.ForecastRevenue.Value);
                                opp.Status.Value = 3;
                                oOppAccess.Update(opp);
                                if (Vendor == "The Good Guys") { Vendor = "GG"; }
                                else
                                { Vendor = "JBHiFI"; }
                                //oUDFAccess.SetFieldValue(GetUdfOppKey("Order Confirmed"), opp.Key, Vendor);

                                this.oUDFAccess.SetFieldValue(this.GetUdfOppKey(""+Item+" Order Confirmed"), opp.Key, Vendor);
                               
                            }
                            //oUDFAccess.SetFieldValue(GetUdfOppKey("Order to WON"), opp.Key, "Yes");
                            //oIndKey = opp.ParentKey.ToString();
                        }
                   
                        //string Orderform1 = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + 1 + " Ordered From"), oCSCase.Key);
                        //string Orderform2 = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("P" + 2 + " Ordered From"), oCSCase.Key);

                        //string strConfirmedByJBHIHI = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Order Confirmed By JBHIFI"), oCSCase.Key);
                        //string strConfirmedByTheGoodGuys = oUDFAccess.GetFieldValue(this.GetUdfCSCaseKey("Order Confirmed By GG"), oCSCase.Key);

                        //if (Orderform1.Length > 0 && Orderform2.Length > 0)
                        //{

                        //    if (strConfirmedByJBHIHI.Length > 0 && strConfirmedByTheGoodGuys.Length > 0)
                        //    {
                        //        oCSCase.Status.Value = 57997;
                        //        oCSCaseAccess.Update(oCSCase);
                        //    }
                        //}
                        //else if(Orderform1.Length > 0 || Orderform2.Length > 0)
                        //{
                        //    if (strConfirmedByJBHIHI.Length > 0 || strConfirmedByTheGoodGuys.Length > 0)
                        //    {
                        //        oCSCase.Status.Value = 57997;
                        //        oCSCaseAccess.Update(oCSCase);
                        //    }
                        //}




                        //lblMsg.Visible = true;
                    }


                }

            }
        }
        private string loginToMax()
        {
            AddressBookList addressBookList = this.ga.ReadAddressBookList();
            string sAddressBookKey = "";
            this.sLoginString = this.Request.QueryString["Loginstring"];
            if (this.sLoginString == "" || this.sLoginString == null)
            {
                foreach (AddressBook addressBook in (CollectionBase)addressBookList)
                {
                    string appSetting = ConfigurationManager.AppSettings["Database"];
                    if (addressBook.Name.ToString().ToUpper().CompareTo(appSetting.ToUpper()) == 0)
                    {
                        sAddressBookKey = addressBook.Key;
                        break;
                    }
                }
                string appSetting1 = ConfigurationManager.AppSettings["UserName"];
                string appSetting2 = ConfigurationManager.AppSettings["Password"];
                this.sLoginString = this.oAb.LoginMaximizer(sAddressBookKey, appSetting1.ToUpper(), appSetting2);
            }
            this.oAbEntryAccess = this.oAb.CreateAbEntryAccess(this.sLoginString);
            this.oUDFAccess = this.oAb.CreateUdfAccess(this.sLoginString);
            this.oOppAccess = this.oAb.CreateOpportunityAccess(this.sLoginString);
            this.oNoteAccess = this.oAb.CreateNoteAccess(this.sLoginString);
            this.oUserAccess = this.oAb.CreateUserAccess(this.sLoginString);
            this.oTaskAccess = this.oAb.CreateTaskAccess(this.sLoginString);
            this.oDetailFieldAccess = this.oAb.CreateDetailFieldAccess(this.sLoginString);
            this.oSalesProcess = this.oAb.CreateSalesProcessAccess(this.sLoginString);
            this.oCSCaseAccess = this.oAb.CreateCSCaseAccess(this.sLoginString);
            return this.sLoginString;
        }
        public string GetUdfKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new AbEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
        public string GetUdfCSCaseKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultCSCaseEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
        public string GetUdfOppKey(string strUdfName)
        {
            foreach (UdfDefinition readUdfDefinition in (CollectionBase)oAb.CreateUdfAccess(sLoginString).ReadUdfDefinitionList(new DefaultOpportunityEntry().Key))
            {
                if (readUdfDefinition.FieldName.Equals(strUdfName))
                    return readUdfDefinition.Key;
            }
            return string.Empty;
        }
    }
}