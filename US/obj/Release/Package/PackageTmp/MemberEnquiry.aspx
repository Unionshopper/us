﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberEnquiry.aspx.cs" Inherits="US.MemberEnquiry" %>

<%@ Register Src="MenuSection.ascx" TagName="MenuSection" TagPrefix="uc1" %>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Enquiry System</title>

    <style>
        .dataTables_wrapper .dataTables_filter {
            float: right;
            text-align: right;
            width: 50%;
        }

        input[type="search"] {
            background: #fff url("images/eye-search.png") no-repeat scroll 8px center;
            border: 2px solid #c4c5c6;
            border-radius: 5px;
            padding: 0 10px 0 40px;
            position: relative;
            z-index: 1;
            float: right !important;
            width: 94%;
            margin-top: -16px;
            margin-right: 1px;
            height: 34px;
            opacity: 1;
        }

        div.dataTables_length {
            margin-top: -4px;
        }

        table.dataTable thead th, table.dataTable thead td {
            padding: 10px 18px !important;
            color: #fff !important;
        }

        #GridView1_length {
            margin-top: 10px;
        }

        #GridView1 tr {
            border: 1px !important;
        }

        #GridView1 i.zmdi.zmdi-border-color {
            background: #B9D530;
            font-size: 19px;
            padding: 6px;
            color: black;
        }

        .tooltip {
            position: relative;
            display: inline-block;
            border-bottom: 1px dotted black;
        }

            .tooltip .tooltiptext {
                visibility: hidden;
                width: 120px;
                background-color: #555;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 5px 0;
                position: absolute;
                z-index: 1;
                bottom: 125%;
                left: 50%;
                margin-left: -60px;
                opacity: 0;
                transition: opacity 0.3s;
            }

                .tooltip .tooltiptext::after {
                    content: "";
                    position: absolute;
                    top: 100%;
                    left: 50%;
                    margin-left: -5px;
                    border-width: 5px;
                    border-style: solid;
                    border-color: #555 transparent transparent transparent;
                }

            .tooltip:hover .tooltiptext {
                visibility: visible;
                opacity: 1;
            }

        .table-secation tbody td {
            font-size: 13px !important;
        }

        .table-secation thead th {
            font-size: 14px !important;
        }

        #GridView1_wrapper {
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form runat="server">
        <uc1:MenuSection ID="MenuSection" runat="server" />
        <section class="mide">

            <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
            <script>
                $(document).ready(function () {
                    $("#GridView1").prepend($("<thead></thead>").append($('#GridView1').find("tr:first"))).DataTable({
                        "order": [[0, "desc"]]
                    });
                });
            </script>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <div class="container">
                <div class="enquriry-from member-enqury">

                    <div class="enquiry-member-btn">
                        <div class="input-headings find-enquiry">
                            <h4>Find Enquiry</h4>
                        </div>

                        <div class="view-edit-manager btns-group hide">
                            <button type="button" class="btns"><i class="zmdi zmdi-edit"></i>view / edit manager</button>
                        </div>
                    </div>


                    <div class="table-secation">
                        <div class="nano">


                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CaseNo" OnRowCommand="GridView1_RowCommand" Width="100%" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" OnRowDataBound="GridView1_RowDataBound" OnRowCreated="GridView1_RowCreated">
                                <Columns>
                                    <asp:BoundField DataField="CaseNo" HeaderText="Case No">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DateFormat" HeaderText="Date">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Time" HeaderText="Time">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Assigned" HeaderText="Assigned">
                                        <HeaderStyle Width="150px" />
                                        <ItemStyle Width="150px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <%# Eval("Desc")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="status" HeaderText="Vendor Status">
                                        <HeaderStyle Width="150px" />
                                        <ItemStyle Width="150px" />
                                    </asp:BoundField>
                                    <%--   <asp:BoundField DataField="EnquiryStatus" HeaderText="Enquiry Status">
                                        <HeaderStyle Width="150px" />
                                        <ItemStyle Width="150px" />
                                    </asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="Enquiry Status">
                                        <ItemTemplate>

                                            <asp:Label ID="hdnStatusid" runat="server" Visible="false" Text='<%#Eval("EnquiryStatus") %>'></asp:Label>
                                            <asp:Label ID="lblCaseNo" runat="server" Style="display: none;" Text='<%#Eval("CaseNo") %>'></asp:Label>

                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

                                                <ContentTemplate>
                                                    <asp:DropDownList Width="90" runat="server" ID="ddlEnquiryStatus" ClientIDMode="Static" AutoPostBack="True" OnSelectedIndexChanged="ddlEnquiryStatus_SelectedIndexChanged" onchange="javascript:UpdateEnquiry(this);">
                                                        <asp:ListItem Text="Abandoned" Value="57994"></asp:ListItem>
                                                        <asp:ListItem Text="Assigned" Value="57998"></asp:ListItem>
                                                        <asp:ListItem Text="Call Back" Value="57996"></asp:ListItem>
                                                      <%--  <asp:ListItem Text="Completed" Value="57993"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Confirmed" Value="57993"></asp:ListItem>
                                                       <%-- <asp:ListItem Text="Ordered" Value="57997"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Pending" Value="57999" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Wait for Customer" Value="57995"></asp:ListItem>
                                                        <asp:ListItem Text="Won" Value="57997"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Open">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("CaseNo")%>' Text="" CommandName="Edit"><i class="zmdi zmdi-border-color"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                <HeaderStyle BackColor="#4d4c4c" Font-Bold="True" ForeColor="#fff" />
                                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" ForeColor="#fff" />
                                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                <SortedAscendingCellStyle BackColor="#EDF6F6" />
                                <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                                <SortedDescendingCellStyle BackColor="#D6DFDF" />
                                <SortedDescendingHeaderStyle BackColor="#fff" />
                            </asp:GridView>

                            <%--<Triggers>
         <asp:AsyncPostBackTrigger ControlID="ddlEnquiryStatus" EventName="OnSelectedIndexChanged" />
     </Triggers>--%>
                        </div>

                    </div>

                    <div class="btns-group hide">
                        <div class="btn-part-two">
                            <button type="button" class="btns "><i class="zmdi zmdi-eye"></i>View Enquiry</button>
                            <button type="button" class="btns"><i class="fa fa-floppy-o" aria-hidden="true"></i>browse</button>
                            <button type="button" class="btns"><i class="zmdi zmdi-border-color"></i>New Enquiry </button>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
</body>
</html>

<script type="text/javascript">
    function UpdateEnquiry(obj) {
        debugger;

        var EnquiryStatusId = $(obj).val();
        //var ddlEnquiryStatus = $(obj).closest('tr').find("#ddlEnquiryStatus option:selected").text();

        var tr = $(obj).closest('tr');

        if (EnquiryStatusId == "57996") {
            tr.css('background-color', 'LightGreen');
            $(obj).css("background-color","LightGreen");

        }
        else if (EnquiryStatusId == "57994") {
            tr.css('background-color', 'LightPink');
            $(obj).css("background-color","LightPink");
        }
        else if (EnquiryStatusId == "57993") {

            tr.css('background-color', 'LightYellow');
            $(obj).css("background-color","LightYellow");
          //  confirm("would you like to sent thank you email to customer ?");

        } else {
            tr.css('background-color', 'White');
            $(obj).css("background-color","White");

        }
    }

</script>
