﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadEnquiries.aspx.cs" Inherits="US.LoadEnquiries" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="MenuSection.ascx" TagName="MenuSection" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Load Enquiry</title>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />

    <link rel="stylesheet" type="text/css" href="css/resat.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />

    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <style>
					div {
    font-size: 14px !important;
}
        body {
            background-color: #e8ebec !important;
        }
        .heading-notes {
			background: #F5F5F5;
			margin-top: 11px;
			width: 100%;
			margin-left: 0px !important;
			text-align: justify;
		}
        #divnoteshistory, #divenquirynoteshistory {
            background-color: #fff;
            padding: 10px 0 10px 0;
        }

        .btnAddNotes {
            color: #000;
        }

        .detailsection {
            border-radius: 5px;
            border: 2px solid rgba(185,213,49,.87) !important;
            padding: 9px;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .subheading {
            color: rgba(255,255,255,0.87) !important;
            height: 39px;
            text-decoration: none !important;
            display: block;
            background-color: #37474F;
            padding: 6px 0 5px 10px;
            box-sizing: border-box;
            border-radius: 3px 3px 0 0;
        }

        .sub-Heading-possition {
            margin-top: -4px;
            margin-left: -23px;
        }

        .show_hide {
            text-decoration: none !important;
            display: block;
            box-sizing: border-box;
            /* border-radius: 5px; */
            background: #37474F;
            width: 25px;
            text-align: center;
            position: absolute;
            margin-left: -36px;
            margin-top: 21px;
            color: rgba(255,255,255,0.87) !important;
            padding: 5px;
        }

        #RadNotesCenter {
            height: 25em !important;
        }

        .modal-body {
            padding: 6px;
        }

        .modal-footer {
            padding: 7px 20px 20px;
        }

        .reRow {
            display: none;
        }

        select#ddlFilternote, select#ddlEnquiryFilternote {
            color: #000;
        }

        .btnSearchItems {
            background-color: #616d65;
            color: #fff;
            border-radius: 0px;
            margin-top: 24px;
            text-align: center;
            width: 100%;
            font-size: 17px;
        }
        .btn:hover, .btn:focus, .btn.focus {
    color: #e5dfdf;
    text-decoration: none;
}
        .divshowhide {
         background-color:#37474F;
        }
        .textNoteHeading {
            padding-top:7px;
        }
        .divshowhide.btn.btn-primary.show_hide {
    margin-left: 0px !important;
    margin-top: 0px !important;
    position: relative !important;
    border-radius: 0px !important;
    padding: 2px !important;
	background-color:#37474F !important;
}
    </style>

    <script type="text/javascript">

        function AddNote() {
            var name = $('#lbluser').text().split('-')[0];
            $('#myModal').modal('toggle');
            $('#lblHeader').text("Add Note For " + name);
            $('#btnSaveEnquiry').addClass("hide");
            $('#btnSaveClient').removeClass("hide");

        }

        function AddNoteEnquiry() {
            var name = $('#lbluser').text().split('-')[0];
            $('#myModal').modal('toggle');
            $('#lblHeader').text("Add Note For " + name);
            $('#btnSaveClient').addClass("hide");
            $('#btnSaveEnquiry').removeClass("hide");

        }
        function notesection(id) {
            var divID = $(id).attr("data-id");
            if ($("#" + divID).hasClass("show")) {
                $("#" + divID).slideUp();
                $("#" + divID).removeClass("show");
                $(id).text("+ ");
            }
            else {
                $("#" + divID).slideDown();
                $("#" + divID).addClass("show");
                $(id).text("- ");
            }
        }
        $(document).ready(function () {
            $("#btnjbhifi").click(function () {
                var values = $('#txtsearch').val();
                var URL = "https://www.jbhifi.com.au/?q=" + values;
                window.open(URL, '_blank'); 
            });
             $("#btnGG").click(function () {
                var values = $('#txtsearch').val();
                var URL = "https://www.thegoodguys.com.au/?q=" + values;
                window.open(URL, '_blank'); 
            });

            $('.show_hide').click(function () {
                
                var text = $(this).attr("data-text");
                var divID = $(this).attr("data-id");
                if ($("#" + divID).hasClass("show")) {
                    $("#" + divID).slideUp();
                    $("#" + divID).removeClass("show");
                    if (text != "undefined") {
                        $(this).text("+" + text);
                    } else {
                        $(this).text("+");
                    }
                }
                else {
                    $("#" + divID).slideDown();
                    $("#" + divID).addClass("show");
                    if (text != "undefined") {
                        $(this).text("-" + text);
                    } else {
                        $(this).text("-");
                    }
                }
            });
        });
    </script>
</head>
<body>
    <form runat="server">
		 <uc1:MenuSection ID="MENUSECTION" runat="SERVER" />
        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <section class="mide">
            

            <section class="enquiry-sheet-part">
                <div class="enquiry-sheet-heading-part">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                <div class="enquiry-number-part">
                                    <p>Enquiry Number</p>
                                    <div class="enquiry-number">
                                        01234
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                <div class="enquiry-name">
                                    AMANDAP
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 ">
                                <div class="enquiry-sheet-heading">
                                    <h3>LOAD ENQUIRIES</h3>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 pull-right">
                                <div class="enquiry-complet">
                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                    <span>Completed</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="member-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    Member  <span><asp:Label ID="lblMember" runat="server"></asp:Label></span>
                                </div>
                                <div class="member-content">
                                    <div class="member-content-icon">
                                        <i class="zmdi zmdi-account zmdi-hc-fw"></i>
                                    </div>
                                    <div class="member-content-details half">
                                        <h4>
                                            <asp:Label ID="lbluser" runat="server"></asp:Label></h4>
                                        <span><asp:Label ID="lblAddress" runat="server"></asp:Label></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    Member
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Home <span><asp:Label ID="lblHomeNumber" runat="server"></asp:Label></span></li>
                                            <li>Bus <span><asp:Label ID="lblBusNumber" runat="server"></asp:Label></span></li>
                                            <li>Mob <span><asp:Label ID="lblMobileNumber" runat="server"></asp:Label></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 sep mem">
                            <div class="member-part">
                                <div class="member-heading">
                                    Authorised Person
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Home <span></span></li>
                                            <li>Bus <span></span></li>
                                            <li>Mob <span></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    &nbsp;
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Union/Org <span><asp:Label ID="lblUnion" runat="server"></asp:Label></span></li>
                                            <li>Num</li>
                                            <li>Auth <span></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 sep">
                            <div class="member-part">
                                <div class="member-heading">
                                    &nbsp;
                                </div>
                                <div class="member-content">
                                    <div class="member-content-details">
                                        <ul>
                                            <li>Last Enq  <span><asp:Label ID="lblLastEnqDate" runat="server"></asp:Label></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="key-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="divshowhide">
                                <a href="javascript:void(0)" data-text="" data-id="divNotesHistory" class="show_hide">-</a>
                            </div>
                            <div class="detailsection">
                                <div class="subheading" style="height: 41px;">
                                    <div class="col-lg-3">
                                        <span id="Label11" class="subheading sub-Heading-possition"><b>CLIENT NOTES HISTORY:</b></span>
                                    </div>
                                    <div class="col-lg-7 text-right">
                                        <button type="button" onclick="AddNote();" class="btnAddNotes">
                                            <span class="glyphicon glyphicon-edit"></span>Add Note
                                        </button>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="filter-label">Filter:</label>
                                        <asp:DropDownList ID="ddlFilternote" runat="server" OnSelectedIndexChanged="ddlFilternote_SelectedIndexChanged" AutoPostBack="true">
												<asp:ListItem Value="5" Text="5"></asp:ListItem>
												<asp:ListItem Value="15" Text="15"></asp:ListItem>
												<asp:ListItem Value="25" Text="25"></asp:ListItem>
												<asp:ListItem Value="0" Text="All"></asp:ListItem>
											</asp:DropDownList>
                                    </div>


                                </div>
                                <div class="form-area show" id="divNotesHistory" >
                                    <ul class="home-form">
                                        <li>
                                            <div id="divnoteshistory" runat="server">
                                                <div class="row">
                                                    <div class="col-lg-12 text-center"><span style="font-size: 17px; color: red"><b>Record not found</b></span></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>

                    </div>

                    <div class="row" style="margin-top: 17px; margin-bottom: 10px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="divshowhide">
                                <a href="javascript:void(0)" data-text="" data-id="divEnquiryNotesHistory" class="show_hide">-</a>
                            </div>
                            <div class="detailsection">
                                <div class="subheading" style="height: 41px;">
                                    <div class="col-lg-3">
                                        <span id="Label1" class="subheading sub-Heading-possition"><b>ENQUIRY NOTES HISTORY:</b></span>
                                    </div>
                                    <div class="col-lg-7 text-right">
                                        <button type="button" onclick="AddNoteEnquiry();" class="btnAddNotes">
                                            <span class="glyphicon glyphicon-edit"></span>Add Note
                                        </button>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="filter-label">Filter:</label>
                                        
                                        <asp:DropDownList ID="ddlEnquiryFilternote" runat="server" OnSelectedIndexChanged="ddlEnquiryFilternote_SelectedIndexChanged" AutoPostBack="true">
												<asp:ListItem Value="5" Text="5"></asp:ListItem>
												<asp:ListItem Value="15" Text="15"></asp:ListItem>
												<asp:ListItem Value="25" Text="25"></asp:ListItem>
												<asp:ListItem Value="0" Text="All"></asp:ListItem>
											</asp:DropDownList>
                                    </div>


                                </div>
                                <div class="form-area show" id="divEnquiryNotesHistory" >
                                    <ul class="home-form">
                                        <li>
                                            <div id="divenquirynoteshistory" runat="server">
                                                <div class="row">
                                                    <div class="col-lg-12 text-center"><span style="font-size: 17px; color: red"><b>Record not found</b></span></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>

                    </div>


                    <div class="row" style="margin-top: 17px; margin-bottom: 10px;">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <asp:Label ID="lblSearchItems" runat="server" Style="font-size: 20px;">Search Item</asp:Label>
                            <asp:TextBox ID="txtsearch" runat="server" CssClass="form-control" Style="border-radius: 0px; height: 42px; background-color: #fff;"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a id="btnGG" runat="server" class="btn btnSearchItems">Search GG</a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a id="btnjbhifi" runat="server" class="btn btnSearchItems">Search JBHIFI</a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="enquiry-sheet-bottom-part">
                <div class="container">
                    <div class="row">
                        <div class="enquiry-sheet-bottom">
                        </div>
                    </div>
                </div>
            </section>
        
        <!-- Add Notes for Contact -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">
                            <asp:Label ID="lblHeader" runat="server"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <telerik:RadEditor RenderMode="Lightweight" EditModes="Design,Preview" ToolsFile="DefaultToolsFile.xml" runat="server" ID="RadNotes" Width="100%" Height="350">
                                </telerik:RadEditor>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="margin-top: 10px; padding-left: 17px;">
                                <label>Category</label>
                                <asp:DropDownList ID="ddlcategory" runat="server" Style="border-radius: 4px;">
                                    <asp:ListItem Value="test1" Text="test1"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: right;">
                        <asp:Button ID="btnSaveClient" runat="server" CssClass="btn btn-success hide" Text="Save" OnClick="btnSaveClient_Click"/>
                        <asp:Button ID="btnSaveEnquiry" runat="server" CssClass="btn btn-success hide" Text="Save" OnClick="btnSaveEnquiry_Click"/>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <asp:HiddenField ID="hfContactId" runat="server" />
                    </div>
                </div>
            </div>
        </div>
		 </section>
    </form>
</body>
</html>

