﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompletedOrders.ascx.cs" Inherits="US.UserControls.CompletedOrders" %>


<table style="width: 100%;">
    <tr>
        <td style="vertical-align: bottom">COMPLETED  ORDERS
        </td>
        <td style="text-align: right">
            <button type="button" style="background: #ffd800; border: none;">Resfresh</button><br />
            <input type="search" />
        </td>
    </tr>
</table>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<div class="table-secation">
    <div class="nano">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CaseNo" Width="100%" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4">
            <Columns>
                <asp:BoundField DataField="CaseNo" HeaderText="CASE NUMBER">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="DateFormat" HeaderText="DATE">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Time" HeaderText="TIME">
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Assigned" HeaderText="NAME">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="DESCRIPTION">
                    <ItemTemplate>
                        <%# Eval("Desc")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="" HeaderText="PRICE OPTION">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </asp:BoundField>
                <asp:BoundField DataField="status" HeaderText="STATUS">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </asp:BoundField>

                <asp:TemplateField HeaderText="Enquiry Status" Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="hdnStatusid" runat="server" Visible="false" Text='<%#Eval("EnquiryStatus") %>'></asp:Label>
                        <asp:Label ID="lblCaseNo" runat="server" Style="display: none;" Text='<%#Eval("CaseNo") %>'></asp:Label>

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

                            <ContentTemplate>
                                <asp:DropDownList Width="90" runat="server" ID="ddlEnquiryStatus" ClientIDMode="Static" AutoPostBack="True" onchange="javascript:UpdateEnquiry(this);">
                                    <asp:ListItem Text="Abandoned" Value="57994"></asp:ListItem>
                                    <asp:ListItem Text="Assigned" Value="57998"></asp:ListItem>
                                    <asp:ListItem Text="Call Back" Value="57996"></asp:ListItem>
                                    <%--  <asp:ListItem Text="Completed" Value="57993"></asp:ListItem>--%>
                                    <asp:ListItem Text="Confirmed" Value="57993"></asp:ListItem>
                                    <%-- <asp:ListItem Text="Ordered" Value="57997"></asp:ListItem>--%>
                                    <asp:ListItem Text="Pending" Value="57999" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Wait for Customer" Value="57995"></asp:ListItem>
                                    <asp:ListItem Text="Won" Value="57997"></asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                    <EditItemTemplate>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Open">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("CaseNo")%>' Text="" CommandName="Edit"><i class="zmdi zmdi-border-color"></i></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle  Font-Bold="True" ForeColor="#fff" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#fff" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <SortedAscendingCellStyle BackColor="#EDF6F6" />
            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
            <SortedDescendingCellStyle BackColor="#D6DFDF" />
            <SortedDescendingHeaderStyle BackColor="#fff" />
        </asp:GridView>

    </div>

</div>