﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnquirySystem.aspx.cs" Inherits="US.EnquirySystem" %>

<%@ Register Src="MenuSection.ascx" TagName="MenuSection" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Enquiry System</title>
</head>
<body>
    <form runat="server">
        <uc1:MenuSection ID="MenuSection" runat="server" />
        <section class="mide">
            <div class="container">
                <div class="enquriry-from">

                    <div class="row">
                        <div class="input-headings">
                            <h4>Find Enquiry</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" class="form-control" ID="txtEQNumber" placeholder="Enquiry Number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" class="form-control" ID="txtEQPhoneNumber" placeholder="Phone Number"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-headings member-search">
                            <h4>Member search</h4>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" class="form-control" ID="txtEQFirstName" placeholder="First Name"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="label-select">
                                    <select class="form-control select-box">
                                        <option>Suburb</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" class="form-control" ID="txtEQLastName" placeholder="Last Name"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" class="form-control" ID="txtEQMemberID" placeholder="Member ID"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="checkbox-secation">
                                    <input type="checkbox" id="test11" />
                                    <asp:CheckBox runat="server" class="check-box" ID="chkUsePhoneticSearch" Text="Use Phonetic Search"></asp:CheckBox>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox runat="server" class="form-control" ID="txtEQUnionNumber" placeholder="Union Number"></asp:TextBox>
                            </div>
                        </div>

                    </div>


                    <div class="btns-group">
                        <div class="btn-part-one">
                            <button type="button" class="btns clear-btn"><i class="zmdi zmdi-close-circle-o"></i>Clear</button>
                            <button type="button" class="btns"><i class="zmdi zmdi-search"></i>search</button>
                        </div>
                        <div class="btn-part-two">
                            <button type="button" class="btns "><i class="fa fa-fax" aria-hidden="true"></i>fax manager</button>
                            <button type="button" class="btns"><i class="fa fa-floppy-o" aria-hidden="true"></i>browse</button>
                            <button type="button" class="btns"><i class="zmdi zmdi-hourglass-alt"></i>9 </button>
                            <button type="button" class="btns"><i class="zmdi zmdi-phone"></i>3</button>
                            <button type="button" class="btns"><i class="zmdi zmdi-time"></i>69</button>
                            <button type="button" class="btns">
                                <img src="images/shorly.png" alt="shorly">0</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>



    <div class='col-lg-3'><span id='lblPricePickUp1'>JBHIFI Price Pick Up:$</span><input type='checkbox' data-id='JBHIFI PickUp Price' id='POPrice1' name='POPriceJBHIFI1' class='POPrice' style='margin: 0 0 0 5px;'><input name='txtPJBHIFIPickUp1' data-id='JBHIFI Price PickUp' type='text' id='txtPJBHIFIPickUp1' class='txt-item PlaceOrderAmount PO1' data-item='1' style='width: 100%; background-color: Yellow' value=''>

    </div>
    <div class='col-lg-2 ' style='display: block;'>
        <input type='button' id='btnPlaceOrderJBHIFI1' class='btn btnPlaceOrder' onclick='PlaceOrder("1-JBHIFI");' value='Place Order JBHIFI' style='display: block;' />

    </div>
    </div>
</body>
</html>
